package com.bearcrew.groubear.approval.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.approval.model.dto.ApprovalDocListDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalDocListService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

/* 찬빈담당 */
@WebServlet("/approval/document/list")
public class ApprovalDoctList extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		
		ApprovalDocListService approvalDocListService = new ApprovalDocListService();

		/* 전체조회를위한 서비스 생성 */
		List<ApprovalDocListDTO> approvalDocList = approvalDocListService.selectAllApprovalDocList(loginDTO);

		System.out.println("approvalDocList : " + approvalDocList);

		String path = "";
		if (approvalDocList != null) {
			path = "/WEB-INF/views/approval/approvalDocList.jsp";
			request.setAttribute("approvalDocList", approvalDocList);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "문서 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		
		ApprovalDocListService approvalDocListService = new ApprovalDocListService();

		/* 전자결재 승인을 위한 서비스 생성 */
		List<ApprovalDocListDTO> approvalDocList = approvalDocListService.selectAcceptList(loginDTO);

		String path = "";
		if (approvalDocList != null) {
			path = "/WEB-INF/views/approval/approvalDocList.jsp";
			request.setAttribute("approvalDocList", approvalDocList);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "문서 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);
	
	}

}
