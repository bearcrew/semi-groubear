package com.bearcrew.groubear.approval.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.approval.model.dto.DepartmentDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalRequestService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

@WebServlet("/approval/request/detail")
public class ApprovalRequestDetailServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int no = Integer.parseInt(request.getParameter("requestNo")); 							/* 결재문서 번호 */
		int empNo = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpNo();   /* 결재자 번호 */
		
		ApprovalRequestService approvalRequestService = new ApprovalRequestService();
		DraftDocumentDTO approvalRequestDetail = approvalRequestService.selectRequestDetail(no, empNo);
		
		String path = "";
		if (approvalRequestDetail != null) { 
			path = "/WEB-INF/views/approval/approvalRequestDetail.jsp";
			request.setAttribute("approvalRequestDetail", approvalRequestDetail); 
			
		} else { 
			path = "/WEB-INF/views/common/failed.jsp"; 
			request.setAttribute("failedCode", "FailedRequestDetail"); 
		}

		request.getRequestDispatcher(path).forward(request, response); 

	}


}
