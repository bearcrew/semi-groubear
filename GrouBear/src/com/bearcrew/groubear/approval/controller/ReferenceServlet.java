package com.bearcrew.groubear.approval.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.approval.model.dto.ReferenceDTO;
import com.bearcrew.groubear.approval.model.service.SelectDepartmentService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/reference/select")
public class ReferenceServlet extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.setCharacterEncoding("UTF-8");
		String deptCode = request.getParameter("deptCode");
		
		SelectDepartmentService selectDepartmentService = new SelectDepartmentService();
		List<ReferenceDTO> ReferenceList = selectDepartmentService.selectReference(deptCode);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
		
		String jsonString = gson.toJson(ReferenceList);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		out.print(jsonString);
		
		out.flush();
		out.close();
		
	}

}
