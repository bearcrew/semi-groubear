package com.bearcrew.groubear.approval.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.approval.model.dto.ApprovalFormDTO;
import com.bearcrew.groubear.approval.model.dto.ChildApprovalFormDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalFormService;

@WebServlet("/approval/select")
public class ApprovalSelectServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<ApprovalFormDTO> approvalFormList = ApprovalFormService.SelectApprovalFormList();
		
		for(ApprovalFormDTO form : approvalFormList) {
			List<ChildApprovalFormDTO> childFormList = ApprovalFormService.SelectChildApprovalFormList(form.getFormNo());
			
			form.setChildFormList(childFormList);
			
		}
		
		String path = "";
		if(approvalFormList != null) {
			path = "/WEB-INF/views/approval/approvalList.jsp";
			request.setAttribute("approvalFormList", approvalFormList);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "양식함 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);

	}

}
