package com.bearcrew.groubear.approval.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.approval.model.dto.ApproverDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocContentDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.dto.RefReceiverDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalDocumentService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

@WebServlet("/approval/insert")
public class DraftDocumentServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = "/WEB-INF/views/approval/ApprovalWrite.jsp";
				
		request.getRequestDispatcher(path).forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* DTO, service 객체 생성 */
		
		DraftDocumentDTO draftDocumentDTO = new DraftDocumentDTO();
		DraftDocContentDTO draftDocContentDTO = new DraftDocContentDTO();
		ApproverDTO approverDTO = new ApproverDTO();
		RefReceiverDTO refReceiverDTO = new RefReceiverDTO();		
		ApprovalDocumentService approvalDocumentService = new ApprovalDocumentService();
		
		List<String> approverNoList = new ArrayList<String>();											/* 결재자 사번을 담을 List 배열 생성 */ 
		List<String> approverNameList = new ArrayList<String>();										/* 결재자 이름을 담을 List 배열 생성 */
		List<String> refReceiverNoList = new ArrayList<String>();										/* 수신 참조자 사번을 담을 List 배열 생성 */
		List<String> refReceiverNameList = new ArrayList<String>();										/* 수신 참조자 이름을 담을 List 배열 생성 */
		
		/* 결재문서 작성 내용 */
		
		String title = request.getParameter("DraftLetterTitle");										/* 결재 문서 제목 */
		String strRefReceiverName = request.getParameter("refReceiverName");							/* 수신 참조자 목록 */
		String strApproverName = request.getParameter("approverName");									/* 결재자 목록 */
		
		List<String> approverName = Arrays.asList(strApproverName.split(", "));							/* 결재자 배열을 List로 변환 */
		List<String> refReceiverName = Arrays.asList(strRefReceiverName.split(", "));					/* 수신 참조자 배열을 List로 변환 */
		
		
		for(int i = 0; i < refReceiverName.size(); i++) {
			String empNo = refReceiverName.get(i).replaceAll("[^0-9]", "");								/* 수신 참조자 사번 숫자만 추출할 정규 표현식 */
			
			refReceiverNoList.add(i, empNo);											
		}
		
		for(int i = 0; i < approverName.size(); i++) {
			String empNo = approverName.get(i).replaceAll("[^0-9]", "");								/* 결재자 사번 숫자만 추출할 정규 표현식 */
			
			approverNoList.add(i, empNo);												
		}
		
		for(int i = 0; i < refReceiverName.size(); i++) {
			String empName = refReceiverName.get(i).substring(0, 3);									/* 수신 참조자 이름만 추출할 반복문 */
			
			refReceiverNameList.add(i, empName);										
		}
		
		for(int i = 0; i < approverName.size(); i++) {
			String empName = approverName.get(i).substring(0, 3);										/* 결재자 이름만 추출할 반복문 */
			
			approverNameList.add(i, empName);											
		}
		
		
		int writerEmpNo = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpNo();		/* 작성자 번호 */
		
		/* 기안 작성 내용 */
		
		String draftDocTitle = request.getParameter("draftDocTitle");									/* 기안 제목 */
		String docContent = request.getParameter("docContent");											/* 기안 내용 */
		String draftType = request.getParameter("draftType");											/* 기안 항목 */
		
		draftDocumentDTO.setTitle(title);
		draftDocumentDTO.setRefReceiver(refReceiverNameList);
		draftDocumentDTO.setApprover(approverNameList);
		draftDocumentDTO.setDraftType(draftType);
		draftDocumentDTO.setWriterEmpNo(writerEmpNo);
		draftDocContentDTO.setTitle(draftDocTitle);
		draftDocContentDTO.setContent(docContent);
		approverDTO.setEmpNo(approverNoList);
		refReceiverDTO.setEmpNo(refReceiverNoList);
		
		/* 먼저 1번째로 결재 문서가 작성되었을 때, 해당 결재 문서 번호에 해당되는 
		 * 기안 내용과 결재자, 수신 참조자가 배치될 수 있도록 코드 순서를 배치 */
		
		int result1 = approvalDocumentService.insertDocument(draftDocumentDTO);	 						/* 1. 결재 문서 작성 */
		int result2 = approvalDocumentService.insertDocContent(draftDocContentDTO);						/* 2. 결재 문서 내용을 작성 */
		int result3 = approvalDocumentService.insertApprover(approverDTO);								/* 3. 결재자 추가 */
		int result4 = approvalDocumentService.insertRefReceiver(refReceiverDTO);						/* 4. 수신 참조자 추가 */
		
		String path = "";
		if(result1 > 0 && result2 > 0 && result3 < 0 && result4 < 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "insertAppDoc");
			request.setAttribute("draftDocumentDTO", draftDocumentDTO);
			
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "결재문서 요청 실패!");
			
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
