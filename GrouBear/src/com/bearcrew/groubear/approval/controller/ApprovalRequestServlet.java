package com.bearcrew.groubear.approval.controller;

import java.io.IOException;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.approval.model.dto.ApprovalRequestListDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalRequestService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

@WebServlet("/approval/request/list")
public class ApprovalRequestServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int loginEmpNo = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpNo();  /* 결재자 번호 파라미터 */
		
		List<ApprovalRequestListDTO> approvalRequestList = new ApprovalRequestService().selectAllRequestList(loginEmpNo);
		
		
		String path = "";
		if(approvalRequestList != null) {
			path = "/WEB-INF/views/approval/approvalRequestList.jsp";
			request.setAttribute("approvalRequestList", approvalRequestList);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "결재 승인 요청 조회 실패!");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
