package com.bearcrew.groubear.approval.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.service.ApprovalDocumentService;
import com.bearcrew.groubear.approval.model.service.ReferenceAttachmentService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.mypage.model.service.MypageService;

@WebServlet("/approval/attachment")
public class ReferenceAttachmentServlet extends HttpServlet {
	
	private String rootLocation;
	private int maxFileSize;
	private String encodingType;
	
	@Override
	public void init() {
		
		ServletContext context = getServletContext();
		
		rootLocation = context.getInitParameter("refDocument-upload-location");
		maxFileSize = Integer.parseInt(context.getInitParameter("max-file-size"));
		encodingType = context.getInitParameter("encoding-type");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int result = 0;
		
		if (ServletFileUpload.isMultipartContent(request)) { 											

			String fileUploadDirectory = request.getServletContext().getRealPath("/") + rootLocation;	
			HttpSession session = request.getSession();											
			LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");					
			
			int empNo = loginDTO.getEmpNo();
			
			File directory = new File(fileUploadDirectory);

			if (!directory.exists()) {
				directory.mkdirs();
			}

			Map<String, String> parameter = new HashMap<>();
			List<Map<String, Object>> fileList = new ArrayList<>();

			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			fileItemFactory.setRepository(new File(fileUploadDirectory));
			fileItemFactory.setSizeThreshold(maxFileSize);

			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);

			try {
				
				List<FileItem> fileItems = fileUpload.parseRequest(request);

				for (int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);

					if (!item.isFormField()) {

						if (item.getSize() > 0) {

							long fileSize = (item.getSize());
							String fileType = item.getFieldName();
							String originFileName = item.getName();
							String contentType = item.getContentType();

							int dot = originFileName.lastIndexOf("."); 
							String ext = originFileName.substring(dot);

							String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;

							File storeFile = new File(fileUploadDirectory + "/" + randomFileName); // uuid 파일 이름

							String savedFileName = (randomFileName);
							
							item.write(storeFile);

							Map<String, Object> fileMap = new HashMap<>();

							fileMap.put("savedFileName", savedFileName); 								// UUID한 세이브 파일 이름
							fileMap.put("originFileName", originFileName); 								// 실제 파일 이름
							fileMap.put("fileSize", fileSize); 											// 파일 사이즈
							fileMap.put("empNo", empNo);

							fileList.add(fileMap);

						}

					} else { // 단순 양식 필드인 경우
						parameter.put(item.getFieldName(), item.getString());
						parameter.put(item.getFieldName(),
								new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
					}
				}

				ApprovalDocumentService approvalDocumentService = new ApprovalDocumentService();
				
				int resultUpload = approvalDocumentService.attachmentUpload(fileList);

				response.setCharacterEncoding("UTF-8");
				PrintWriter out = response.getWriter();

				out.print("파일 업로드 성공");

				out.flush(); 
				out.close();

			} catch (Exception e) {
				
				int cnt = 0;

				for (int i = 0; i < result; i++) {

					Map<String, Object> file = fileList.get(i);

					File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
					boolean isDeleted = deleteFile.delete();

					if (isDeleted) {
						cnt++;
					}
				}

				if (cnt == fileList.size()) {
					
					response.setCharacterEncoding("UTF-8");
					PrintWriter out = response.getWriter();
					out.print("파일 업로드 실패");

					out.flush();
					out.close();
					
				} else {
					e.printStackTrace();
					
				}
			}
		}
	}
}
