package com.bearcrew.groubear.approval.model.dto;

import java.sql.Date;
import java.util.List;

public class ApprovalRequestListDTO implements java.io.Serializable {
	
	private int docNo; 						/* 문서 번호 */ 
	private String title; 					/* 승인요청 제목 */
	private java.sql.Date date; 			/* 작성일자 */
	private String requesterName; 			/* 요청자 */
	private String type; 					/* 결재항목 */
	
	private List<ApproverDTO> approverNo;
	private List<RefReceiverDTO> refReceiverNo;
	
	public ApprovalRequestListDTO() {}

	public ApprovalRequestListDTO(int docNo, String title, Date date, String requesterName, String type) {
		super();
		this.docNo = docNo;
		this.title = title;
		this.date = date;
		this.requesterName = requesterName;
		this.type = type;
	}

	public int getDocNo() {
		return docNo;
	}

	public void setDocNo(int docNo) {
		this.docNo = docNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public String getRequesterName() {
		return requesterName;
	}

	public void setRequesterName(String requesterName) {
		this.requesterName = requesterName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "ApprovalRequestListDTO [docNo=" + docNo + ", title=" + title + ", date=" + date + ", requesterName="
				+ requesterName + ", type=" + type + "]";
	}

}
