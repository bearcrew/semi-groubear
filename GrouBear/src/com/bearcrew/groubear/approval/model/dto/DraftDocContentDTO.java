package com.bearcrew.groubear.approval.model.dto;

public class DraftDocContentDTO implements java.io.Serializable {
	
	private int no;
	private String content;
	private int docNo;
	private String title;
	private String type;
	
	public DraftDocContentDTO() {}

	public DraftDocContentDTO(int no, String content, int docNo, String title, String type) {
		super();
		this.no = no;
		this.content = content;
		this.docNo = docNo;
		this.title = title;
		this.type = type;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDocNo() {
		return docNo;
	}

	public void setDocNo(int docNo) {
		this.docNo = docNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "DraftDocContentDTO [no=" + no + ", content=" + content + ", docNo=" + docNo + ", title=" + title
				+ ", type=" + type + "]";
	}

}
