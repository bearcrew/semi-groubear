package com.bearcrew.groubear.approval.model.dto;

import java.util.List;

public class ApprovalFormDTO implements java.io.Serializable {
	
	private int formNo;
	private String title;
	private String name;
	private String path;
	private String type;
	private int refFormNo;
	private List<ChildApprovalFormDTO> childFormList;
	
	public ApprovalFormDTO() {}

	public ApprovalFormDTO(int formNo, String title, String name, String path, String type, int refFormNo,
			List<ChildApprovalFormDTO> childFormList) {
		super();
		this.formNo = formNo;
		this.title = title;
		this.name = name;
		this.path = path;
		this.type = type;
		this.refFormNo = refFormNo;
		this.childFormList = childFormList;
	}

	public int getFormNo() {
		return formNo;
	}

	public void setFormNo(int formNo) {
		this.formNo = formNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRefFormNo() {
		return refFormNo;
	}

	public void setRefFormNo(int refFormNo) {
		this.refFormNo = refFormNo;
	}

	public List<ChildApprovalFormDTO> getChildFormList() {
		return childFormList;
	}

	public void setChildFormList(List<ChildApprovalFormDTO> childFormList) {
		this.childFormList = childFormList;
	}

	@Override
	public String toString() {
		return "ApprovalFormDTO [formNo=" + formNo + ", title=" + title + ", name=" + name + ", path=" + path
				+ ", type=" + type + ", refFormNo=" + refFormNo + ", childFormList=" + childFormList + "]";
	}

}
