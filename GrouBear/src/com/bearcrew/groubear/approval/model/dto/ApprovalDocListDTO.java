package com.bearcrew.groubear.approval.model.dto;

import java.sql.Date;
import java.util.List;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;

public class ApprovalDocListDTO implements java.io.Serializable {
	
	private int no;								// 문서 번호
	private String title;						// 문서 제목
	private java.sql.Date writeDate;			// 작성일자
	private int empNo;					// 작성자 번호
	private LoginDTO empName;					// 작성자
	private int approverEmpNo;					// 결제권자 사원번호
	private String approver;					// 결재권자
	private int refReceiverEmpNo;				// 수신 참조자 사원번호
	private String refReceiver;					// 수신 참조자
	private approvalAttachmentDTO attachment;   // 참조 문서
	private int contentNo;						// 기안문서 번호
	private DraftDocContentDTO content; 		// 기안문서 내용
	private String draftType;					// 기안 항목
	private String approvalCondition;           // 결재 상태
	
	
	
	
	public ApprovalDocListDTO() {}




	public ApprovalDocListDTO(int no, String title, Date writeDate, int empNo, LoginDTO empName, int approverEmpNo,
			String approver, int refReceiverEmpNo, String refReceiver, approvalAttachmentDTO attachment, int contentNo,
			DraftDocContentDTO content, String draftType, String approvalCondition) {
		super();
		this.no = no;
		this.title = title;
		this.writeDate = writeDate;
		this.empNo = empNo;
		this.empName = empName;
		this.approverEmpNo = approverEmpNo;
		this.approver = approver;
		this.refReceiverEmpNo = refReceiverEmpNo;
		this.refReceiver = refReceiver;
		this.attachment = attachment;
		this.contentNo = contentNo;
		this.content = content;
		this.draftType = draftType;
		this.approvalCondition = approvalCondition;
	}




	public int getNo() {
		return no;
	}




	public String getTitle() {
		return title;
	}




	public java.sql.Date getWriteDate() {
		return writeDate;
	}




	public int getEmpNo() {
		return empNo;
	}




	public LoginDTO getEmpName() {
		return empName;
	}




	public int getApproverEmpNo() {
		return approverEmpNo;
	}




	public String getApprover() {
		return approver;
	}




	public int getRefReceiverEmpNo() {
		return refReceiverEmpNo;
	}




	public String getRefReceiver() {
		return refReceiver;
	}




	public approvalAttachmentDTO getAttachment() {
		return attachment;
	}




	public int getContentNo() {
		return contentNo;
	}




	public DraftDocContentDTO getContent() {
		return content;
	}




	public String getDraftType() {
		return draftType;
	}




	public String getApprovalCondition() {
		return approvalCondition;
	}




	public void setNo(int no) {
		this.no = no;
	}




	public void setTitle(String title) {
		this.title = title;
	}




	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}




	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}




	public void setEmpName(LoginDTO empName) {
		this.empName = empName;
	}




	public void setApproverEmpNo(int approverEmpNo) {
		this.approverEmpNo = approverEmpNo;
	}




	public void setApprover(String approver) {
		this.approver = approver;
	}




	public void setRefReceiverEmpNo(int refReceiverEmpNo) {
		this.refReceiverEmpNo = refReceiverEmpNo;
	}




	public void setRefReceiver(String refReceiver) {
		this.refReceiver = refReceiver;
	}




	public void setAttachment(approvalAttachmentDTO attachment) {
		this.attachment = attachment;
	}




	public void setContentNo(int contentNo) {
		this.contentNo = contentNo;
	}




	public void setContent(DraftDocContentDTO content) {
		this.content = content;
	}




	public void setDraftType(String draftType) {
		this.draftType = draftType;
	}




	public void setApprovalCondition(String approvalCondition) {
		this.approvalCondition = approvalCondition;
	}




	@Override
	public String toString() {
		return "ApprovalDocListDTO [no=" + no + ", title=" + title + ", writeDate=" + writeDate + ", empNo=" + empNo
				+ ", empName=" + empName + ", approverEmpNo=" + approverEmpNo + ", approver=" + approver
				+ ", refReceiverEmpNo=" + refReceiverEmpNo + ", refReceiver=" + refReceiver + ", attachment="
				+ attachment + ", contentNo=" + contentNo + ", content=" + content + ", draftType=" + draftType
				+ ", approvalCondition=" + approvalCondition + "]";
	}

}
