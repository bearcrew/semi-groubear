package com.bearcrew.groubear.approval.model.dto;

public class ApprovalEmpDTO implements java.io.Serializable {
	
	private String empNo;
	private String empName;
	private String jobGrade;
	private String deptTitle;
	
	public ApprovalEmpDTO() {}
	
	public ApprovalEmpDTO(String empNo, String empName, String jobGrade, String deptTitle) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.jobGrade = jobGrade;
		this.deptTitle = deptTitle;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getJobGrade() {
		return jobGrade;
	}

	public void setJobGrade(String jobGrade) {
		this.jobGrade = jobGrade;
	}

	public String getDeptTitle() {
		return deptTitle;
	}

	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}

	@Override
	public String toString() {
		return "ApprovalEmpDTO [empNo=" + empNo + ", empName=" + empName + ", jobGrade=" + jobGrade + ", deptTitle="
				+ deptTitle + "]";
	}
	
}
