package com.bearcrew.groubear.approval.model.dto;

import java.util.List;

public class RefReceiverDTO implements java.io.Serializable {
	
	private int receptionRefNo; 		/* 수신 참조자 번호 */
	private List<String> empNo;			/*사원 번호*/
	private int DocNo; 					/* 문서 번호 */
	
	public RefReceiverDTO() {}

	public RefReceiverDTO(int receptionRefNo, List<String> empNo, int docNo) {
		super();
		this.receptionRefNo = receptionRefNo;
		this.empNo = empNo;
		DocNo = docNo;
	}

	public int getReceptionRefNo() {
		return receptionRefNo;
	}

	public void setReceptionRefNo(int receptionRefNo) {
		this.receptionRefNo = receptionRefNo;
	}

	public List<String> getEmpNo() {
		return empNo;
	}

	public void setEmpNo(List<String> empNo) {
		this.empNo = empNo;
	}

	public int getDocNo() {
		return DocNo;
	}

	public void setDocNo(int docNo) {
		DocNo = docNo;
	}

	@Override
	public String toString() {
		return "RefReceiverDTO [receptionRefNo=" + receptionRefNo + ", empNo=" + empNo + ", DocNo=" + DocNo + "]";
	}
	
}
