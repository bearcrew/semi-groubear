package com.bearcrew.groubear.approval.model.dto;

public class ApprovalLoginDTO implements java.io.Serializable {
	

	private int approvalNo; 								//결재 번호
	private int empNo;									 	//사원번호
	private String approvalCondition; 						//결재상태
	private String draftTitle; 								//문서 제목
	
	public ApprovalLoginDTO() {}
	
	
	public ApprovalLoginDTO(int approvalNo, int empNo, String approvalCondition, String draftTitle) {
		super();
		this.approvalNo = approvalNo;
		this.empNo = empNo;
		this.approvalCondition = approvalCondition;
		this.draftTitle = draftTitle;
	
	}

	public int getApprovalNo() {
		return approvalNo;
	}

	public void setApprovalNo(int approvalNo) {
		this.approvalNo = approvalNo;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getApprovalCondition() {
		return approvalCondition;
	}

	public void setApprovalCondition(String approvalCondition) {
		this.approvalCondition = approvalCondition;
	}

	public String getDraftTitle() {
		return draftTitle;
	}

	public void setDraftTitle(String draftTitle) {
		this.draftTitle = draftTitle;
	}
	
	@Override
	public String toString() {
		return "ApprovalLoginDTO [approvalNo=" + approvalNo + ", empNo=" + empNo + ", approvalCondition="
				+ approvalCondition + ", draftTitle=" + draftTitle + "]";
	}

	
}
