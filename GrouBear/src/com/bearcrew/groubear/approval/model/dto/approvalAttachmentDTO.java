package com.bearcrew.groubear.approval.model.dto;

public class approvalAttachmentDTO implements java.io.Serializable {
	
	private int attachmentNo; 				/* 첨부파일 번호 */ 
	private String savedFileName; 			/* 저장 파일이름 */
	private String originFileName; 			/* 원본 파일이름 */
	private int fileSize; 					/* 파일 크기 */
	private int draftDocNo; 				/* 문서 번호 */
	
	public approvalAttachmentDTO() {}

	public approvalAttachmentDTO(int attachmentNo, String savedFileName, String originFileName, int fileSize,
			int draftDocNo) {
		super();
		this.attachmentNo = attachmentNo;
		this.savedFileName = savedFileName;
		this.originFileName = originFileName;
		this.fileSize = fileSize;
		this.draftDocNo = draftDocNo;
	}

	public int getAttachmentNo() {
		return attachmentNo;
	}

	public void setAttachmentNo(int attachmentNo) {
		this.attachmentNo = attachmentNo;
	}

	public String getSavedFileName() {
		return savedFileName;
	}

	public void setSavedFileName(String savedFileName) {
		this.savedFileName = savedFileName;
	}

	public String getOriginFileName() {
		return originFileName;
	}

	public void setOriginFileName(String originFileName) {
		this.originFileName = originFileName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public int getDraftDocNo() {
		return draftDocNo;
	}

	public void setDraftDocNo(int draftDocNo) {
		this.draftDocNo = draftDocNo;
	}

	@Override
	public String toString() {
		return "approvalAttachmentDTO [attachmentNo=" + attachmentNo + ", savedFileName=" + savedFileName
				+ ", originFileName=" + originFileName + ", fileSize=" + fileSize + ", draftDocNo=" + draftDocNo + "]";
	}
	
}
