package com.bearcrew.groubear.approval.model.dto;

import java.util.List;
import java.util.Map;

public class ApproverDTO implements java.io.Serializable {

	private int approverNo; 			/* 결재자 번호 */
	private List<String> empNo; 		/* 사원 번호 */
	private int docNo; 					/* 문서 번호 */
	private int approverTurn; 			/* 결재 순번 */
	
	public ApproverDTO() {}

	public ApproverDTO(int approverNo, List<String> empNo, int docNo, int approverTurn) {
		super();
		this.approverNo = approverNo;
		this.empNo = empNo;
		this.docNo = docNo;
		this.approverTurn = approverTurn;
	}

	public int getApproverNo() {
		return approverNo;
	}

	public void setApproverNo(int approverNo) {
		this.approverNo = approverNo;
	}

	public List<String> getEmpNo() {
		return empNo;
	}

	public void setEmpNo(List<String> empNo) {
		this.empNo = empNo;
	}

	public int getDocNo() {
		return docNo;
	}

	public void setDocNo(int docNo) {
		this.docNo = docNo;
	}

	public int getApproverTurn() {
		return approverTurn;
	}

	public void setApproverTurn(int approverTurn) {
		this.approverTurn = approverTurn;
	}

	@Override
	public String toString() {
		return "ApproverDTO [approverNo=" + approverNo + ", empNo=" + empNo + ", docNo=" + docNo + ", approverTurn="
				+ approverTurn + "]";
	}

}
