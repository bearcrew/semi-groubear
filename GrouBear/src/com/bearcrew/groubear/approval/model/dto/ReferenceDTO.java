package com.bearcrew.groubear.approval.model.dto;

public class ReferenceDTO {

	private int empNo;
	private String empName;
	private String jobGrade;
	private String deptCode;
	private String deptTitle;

	public ReferenceDTO() {}
	
	public ReferenceDTO(int empNo, String empName, String jobGrade, String deptCode, String deptTitle) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.jobGrade = jobGrade;
		this.deptCode = deptCode;
		this.deptTitle = deptTitle;
	}
	
	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getJobGrade() {
		return jobGrade;
	}

	public void setJobGrade(String jobGrade) {
		this.jobGrade = jobGrade;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptTitle() {
		return deptTitle;
	}

	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}

	public String toString() {
		return "ReferenceDTO [empNo=" + empNo + ", empName=" + empName + ", jobGrade=" + jobGrade + ", deptCode="
				+ deptCode + ", deptTitle=" + deptTitle + "]";
	}
	
}
