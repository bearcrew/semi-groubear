package com.bearcrew.groubear.approval.model.dto;

import java.sql.Date;
import java.util.List;

import com.bearcrew.groubear.login.model.dto.LoginDTO;

public class ApprovalRequestDetailDTO implements java.io.Serializable {
	
	private int no;									// 번호
	private String title;							// 승인 요청 제목
	private LoginDTO writer;						// 작성자
	private java.sql.Date writeDate;				// 작성일자
	
	private List<RefReceiverDTO> refReceiver;		// 수신 참조자
	private List<ApproverDTO> approver;				// 결재권자
	private approvalAttachmentDTO attachment;   	// 참조 문서
	private String draftTitle;						// 기안 제목
	private String draftContent;					// 기안 내용
	
	
	public ApprovalRequestDetailDTO() {}


	public ApprovalRequestDetailDTO(int no, String title, LoginDTO writer, Date writeDate,
			List<RefReceiverDTO> refReceiver, List<ApproverDTO> approver, approvalAttachmentDTO attachment,
			String draftTitle, String draftContent) {
		super();
		this.no = no;
		this.title = title;
		this.writer = writer;
		this.writeDate = writeDate;
		this.refReceiver = refReceiver;
		this.approver = approver;
		this.attachment = attachment;
		this.draftTitle = draftTitle;
		this.draftContent = draftContent;
	}


	public int getNo() {
		return no;
	}


	public void setNo(int no) {
		this.no = no;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public LoginDTO getWriter() {
		return writer;
	}


	public void setWriter(LoginDTO writer) {
		this.writer = writer;
	}


	public java.sql.Date getWriteDate() {
		return writeDate;
	}


	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}


	public List<RefReceiverDTO> getRefReceiver() {
		return refReceiver;
	}


	public void setRefReceiver(List<RefReceiverDTO> refReceiver) {
		this.refReceiver = refReceiver;
	}


	public List<ApproverDTO> getApprover() {
		return approver;
	}


	public void setApprover(List<ApproverDTO> approver) {
		this.approver = approver;
	}


	public approvalAttachmentDTO getAttachment() {
		return attachment;
	}


	public void setAttachment(approvalAttachmentDTO attachment) {
		this.attachment = attachment;
	}


	public String getDraftTitle() {
		return draftTitle;
	}


	public void setDraftTitle(String draftTitle) {
		this.draftTitle = draftTitle;
	}


	public String getDraftContent() {
		return draftContent;
	}


	public void setDraftContent(String draftContent) {
		this.draftContent = draftContent;
	}


	@Override
	public String toString() {
		return "ApprovalRequestDetailDTO [no=" + no + ", title=" + title + ", writer=" + writer + ", writeDate="
				+ writeDate + ", refReceiver=" + refReceiver + ", approver=" + approver + ", attachment=" + attachment
				+ ", draftTitle=" + draftTitle + ", draftContent=" + draftContent + "]";
	}
	
}
