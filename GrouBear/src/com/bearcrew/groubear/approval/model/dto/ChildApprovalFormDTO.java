package com.bearcrew.groubear.approval.model.dto;

public class ChildApprovalFormDTO implements java.io.Serializable {

	private int formNo;
	private String title;
	private String name;
	private String path;
	private String type;
	private int refFormNo;
	
	public ChildApprovalFormDTO() {}

	public ChildApprovalFormDTO(int formNo, String title, String name, String path, String type, int refFormNo) {
		super();
		this.formNo = formNo;
		this.title = title;
		this.name = name;
		this.path = path;
		this.type = type;
		this.refFormNo = refFormNo;
	}
	

	public int getFormNo() {
		return formNo;
	}

	public void setFormNo(int formNo) {
		this.formNo = formNo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getRefFormNo() {
		return refFormNo;
	}

	public void setRefFormNo(int refFormNo) {
		this.refFormNo = refFormNo;
	}

	@Override
	public String toString() {
		return "ChildApprovalFormDTO [formNo=" + formNo + ", title=" + title + ", name=" + name + ", path=" + path
				+ ", type=" + type + ", refFormNo=" + refFormNo + "]";
	}

	
}
