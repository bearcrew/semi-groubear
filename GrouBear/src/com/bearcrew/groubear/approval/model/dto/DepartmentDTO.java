package com.bearcrew.groubear.approval.model.dto;

import java.util.List;

public class DepartmentDTO implements java.io.Serializable {

	private String deptCode;
	private String deptTitle;
	private String deptContent;
	private String deptType;
	private String refDeptCode;
	private List<ChildDepartmentDTO> childOrgList;
	
	public DepartmentDTO() {}

	public DepartmentDTO(String deptCode, String deptTitle, String deptContent, String deptType, String refDeptCode,
			List<ChildDepartmentDTO> childOrgList) {
		super();
		this.deptCode = deptCode;
		this.deptTitle = deptTitle;
		this.deptContent = deptContent;
		this.deptType = deptType;
		this.refDeptCode = refDeptCode;
		this.childOrgList = childOrgList;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptTitle() {
		return deptTitle;
	}

	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}

	public String getDeptContent() {
		return deptContent;
	}

	public void setDeptContent(String deptContent) {
		this.deptContent = deptContent;
	}

	public String getDeptType() {
		return deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}

	public String getRefDeptCode() {
		return refDeptCode;
	}

	public void setRefDeptCode(String refDeptCode) {
		this.refDeptCode = refDeptCode;
	}

	public List<ChildDepartmentDTO> getChildOrgList() {
		return childOrgList;
	}

	public void setChildOrgList(List<ChildDepartmentDTO> childOrgList) {
		this.childOrgList = childOrgList;
	}

	@Override
	public String toString() {
		return "DepartmentDTO [deptCode=" + deptCode + ", deptTitle=" + deptTitle + ", deptContent=" + deptContent
				+ ", deptType=" + deptType + ", refDeptCode=" + refDeptCode + ", childOrgList=" + childOrgList + "]";
	}

}
