package com.bearcrew.groubear.approval.model.dto;

import java.sql.Date;
import java.util.List;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;

public class DraftDocumentDTO implements java.io.Serializable {
	
	private int no; 							/* 문서 번호 */
	private String title; 						/* 문서 제목 */
	private java.sql.Date writeDate; 			/* 작성일자 */
	private int writerEmpNo; 					/* 작성자 번호 */
	private LoginDTO writer; 					/* 작성자 */
	private int approverEmpNo; 					/* 결제권자 사원번호 */
	private List<String> approver; 				/* 결재권자 */
	private int refReceiverEmpNo; 				/* 수신 참조자 사원번호 */
	private List<String> refReceiver; 			/* 수신 참조자 */
	private approvalAttachmentDTO attachment; 	/* 참조 문서 */
	private int contentNo; 						/* 기안문서 번호 */
	private DraftDocContentDTO content; 		/* 기안문서 내용 */
	private String draftType;				 	/* 기안 항목 */
	
	public DraftDocumentDTO() {}
	
	public DraftDocumentDTO(int no, String title, Date writeDate, int writerEmpNo, LoginDTO writer, int approverEmpNo,
			List<String> approver, int refReceiverEmpNo, List<String> refReceiver, approvalAttachmentDTO attachment,
			int contentNo, DraftDocContentDTO content, String draftType) {
		super();
		this.no = no;
		this.title = title;
		this.writeDate = writeDate;
		this.writerEmpNo = writerEmpNo;
		this.writer = writer;
		this.approverEmpNo = approverEmpNo;
		this.approver = approver;
		this.refReceiverEmpNo = refReceiverEmpNo;
		this.refReceiver = refReceiver;
		this.attachment = attachment;
		this.contentNo = contentNo;
		this.content = content;
		this.draftType = draftType;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public int getWriterEmpNo() {
		return writerEmpNo;
	}

	public void setWriterEmpNo(int writerEmpNo) {
		this.writerEmpNo = writerEmpNo;
	}

	public LoginDTO getWriter() {
		return writer;
	}

	public void setWriter(LoginDTO writer) {
		this.writer = writer;
	}

	public int getApproverEmpNo() {
		return approverEmpNo;
	}

	public void setApproverEmpNo(int approverEmpNo) {
		this.approverEmpNo = approverEmpNo;
	}

	public List<String> getApprover() {
		return approver;
	}

	public void setApprover(List<String> approver) {
		this.approver = approver;
	}

	public int getRefReceiverEmpNo() {
		return refReceiverEmpNo;
	}

	public void setRefReceiverEmpNo(int refReceiverEmpNo) {
		this.refReceiverEmpNo = refReceiverEmpNo;
	}

	public List<String> getRefReceiver() {
		return refReceiver;
	}

	public void setRefReceiver(List<String> refReceiver) {
		this.refReceiver = refReceiver;
	}

	public approvalAttachmentDTO getAttachment() {
		return attachment;
	}

	public void setAttachment(approvalAttachmentDTO attachment) {
		this.attachment = attachment;
	}

	public int getContentNo() {
		return contentNo;
	}

	public void setContentNo(int contentNo) {
		this.contentNo = contentNo;
	}

	public DraftDocContentDTO getContent() {
		return content;
	}

	public void setContent(DraftDocContentDTO content) {
		this.content = content;
	}

	public String getDraftType() {
		return draftType;
	}

	public void setDraftType(String draftType) {
		this.draftType = draftType;
	}

	@Override
	public String toString() {
		return "DraftDocumentDTO [no=" + no + ", title=" + title + ", writeDate=" + writeDate + ", writerEmpNo="
				+ writerEmpNo + ", writer=" + writer + ", approverEmpNo=" + approverEmpNo + ", approver=" + approver
				+ ", refReceiverEmpNo=" + refReceiverEmpNo + ", refReceiver=" + refReceiver + ", attachment="
				+ attachment + ", contentNo=" + contentNo + ", content=" + content + ", draftType=" + draftType + "]";
	}

}
