package com.bearcrew.groubear.approval.model.mapper;

import java.util.List;

import com.bearcrew.groubear.approval.model.dto.ApprovalDocListDTO;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

public interface ApprovalDocListMapper {

	/* 전자결재 양식 보관함 전체 조회 */
	List<ApprovalDocListDTO> selectAllApprovalDocList(LoginDTO loginDTO);

	/* 전자결재 승인 조회 */
	List<ApprovalDocListDTO> selectAcceptList(LoginDTO loginDTO);

	/* 전자결재 반려 조회 */
	List<ApprovalDocListDTO> selectPetList(LoginDTO loginDTO);



}
