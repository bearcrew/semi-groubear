package com.bearcrew.groubear.approval.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.bearcrew.groubear.approval.model.dto.ApprovalRequestListDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;

public interface ApprovalRequestMapper {

	List<ApprovalRequestListDTO> searchAllApprovalRequest(int loginEmpNo);

	DraftDocumentDTO selectRequestDetail(@Param("empNo") int empNo, @Param("no") int no);

}
