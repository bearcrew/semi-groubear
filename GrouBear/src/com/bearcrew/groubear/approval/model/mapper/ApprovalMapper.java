package com.bearcrew.groubear.approval.model.mapper;

import java.util.List;
import java.util.Map;

import com.bearcrew.groubear.approval.model.dto.ApprovalFormDTO;
import com.bearcrew.groubear.approval.model.dto.ApproverDTO;
import com.bearcrew.groubear.approval.model.dto.ChildApprovalFormDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocContentDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.dto.RefReceiverDTO;

public interface ApprovalMapper {

	int insertDocument(DraftDocumentDTO draftDocumentDTO); 			/* 결재문서 요청 추가 메소드 */

	int insertDocContent(DraftDocContentDTO draftDocContentDTO); 	/* 결재문서 내용 요청 추가 메소드 */

	int insertApprover(ApproverDTO approverDTO); 					/* 결재자 추가하는 메소드 */

	int insertRefReceiver(RefReceiverDTO refReceiverDTO); 			/* 수신 참조자를 추가하는 메소드 */

	int insertAttachment(Map<String, Object> listMap); 				/* 첨부파일을 추가하는 메소드 */

}