package com.bearcrew.groubear.approval.model.mapper;

import java.util.Map;

public interface ReferenceAttachmentMapper {
	
	int insertRefAttachment(Map<String, Object> listMap);

}
