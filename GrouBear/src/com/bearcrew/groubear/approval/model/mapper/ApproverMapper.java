package com.bearcrew.groubear.approval.model.mapper;

import java.util.List;

import com.bearcrew.groubear.approval.model.dto.DepartmentDTO;
import com.bearcrew.groubear.approval.model.dto.ReferenceDTO;

public interface ApproverMapper {

	List<DepartmentDTO> selectDeptCode(String deptCode);

	List<ReferenceDTO> selectReferenceList(String deptCode);

}
