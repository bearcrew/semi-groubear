package com.bearcrew.groubear.approval.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.approval.model.dto.ApprovalDocListDTO;
import com.bearcrew.groubear.approval.model.mapper.ApprovalDocListMapper;
import com.bearcrew.groubear.login.model.dto.LoginDTO;

public class ApprovalDocListService {

	/* 전자결재 전체 문서 조회 */
	public List<ApprovalDocListDTO> selectAllApprovalDocList(LoginDTO loginDTO) {
		SqlSession sqlSession = getSqlSession();

		ApprovalDocListMapper approvalDocListMapper = sqlSession.getMapper(ApprovalDocListMapper.class); 
		List<ApprovalDocListDTO> approvalDocList = approvalDocListMapper.selectAllApprovalDocList(loginDTO);

		sqlSession.close();

		return approvalDocList; 
	}

	/* 전자결재 승인 조회 */
	public List<ApprovalDocListDTO> selectAcceptList(LoginDTO loginDTO) {
		
		SqlSession sqlSession = getSqlSession();

		ApprovalDocListMapper approvalDocListMapper = sqlSession.getMapper(ApprovalDocListMapper.class); 
		List<ApprovalDocListDTO> acceptList = approvalDocListMapper.selectAcceptList(loginDTO);

		sqlSession.close();

		return acceptList; 
	}

	/* 전자결재 반려 조회 */
	public List<ApprovalDocListDTO> selectPetList(LoginDTO loginDTO) {
		SqlSession sqlSession = getSqlSession();

		ApprovalDocListMapper approvalDocListMapper = sqlSession.getMapper(ApprovalDocListMapper.class); 
		List<ApprovalDocListDTO> petList = approvalDocListMapper.selectPetList(loginDTO);

		sqlSession.close();

		return petList; 
	}

}
