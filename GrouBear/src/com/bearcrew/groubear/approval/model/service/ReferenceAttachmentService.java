package com.bearcrew.groubear.approval.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.approval.model.mapper.ReferenceAttachmentMapper;

public class ReferenceAttachmentService {
	
	Map<String, Object> listMap;
	
	public int fileUpload(List<Map<String, Object>> fileList) {

		SqlSession session = getSqlSession();				//	세션 생성
		
		ReferenceAttachmentMapper referenceAttachmentMapper = session.getMapper(ReferenceAttachmentMapper.class);
		
		for (Map<String, Object> file : fileList) {								

			listMap = file;

		}

		System.out.println("listMap : " + listMap);

		int result = referenceAttachmentMapper.insertRefAttachment(listMap);

		if (result > 0) {
			session.commit();
		} else {
			session.rollback();
		}

		session.close();
		
		return result;
		
	}

}
