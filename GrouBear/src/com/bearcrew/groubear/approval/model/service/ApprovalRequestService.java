package com.bearcrew.groubear.approval.model.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.approval.model.dto.ApprovalRequestListDTO;
import com.bearcrew.groubear.approval.model.dto.DepartmentDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.mapper.ApprovalRequestMapper;
import com.bearcrew.groubear.approval.model.mapper.ApproverMapper;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

public class ApprovalRequestService {
	
	/* 결재 승인 요청 전체 목록 조회용 메소드 */
	public List<ApprovalRequestListDTO> selectAllRequestList(int loginEmpNo) {
			
		SqlSession session = getSqlSession();
		
		ApprovalRequestMapper approvalRequestMapper = session.getMapper(ApprovalRequestMapper.class);
		
		List<ApprovalRequestListDTO> approvalRequestList = approvalRequestMapper.searchAllApprovalRequest(loginEmpNo);
		
		session.close();
		
		return approvalRequestList;
		
	}
	
	/* 결재 승인 요청 상세보기 메소드 */
	public DraftDocumentDTO selectRequestDetail(int empNo, int no) {
		
		SqlSession session = getSqlSession();
		
		ApprovalRequestMapper approvalRequestMapper = session.getMapper(ApprovalRequestMapper.class);
		
		DraftDocumentDTO approvalRequestDetail = approvalRequestMapper.selectRequestDetail(empNo, no);
		
		session.close();
		
		return approvalRequestDetail;
	}

}
