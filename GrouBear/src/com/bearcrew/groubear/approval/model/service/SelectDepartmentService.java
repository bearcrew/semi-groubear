package com.bearcrew.groubear.approval.model.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.approval.model.dto.DepartmentDTO;
import com.bearcrew.groubear.approval.model.dto.ReferenceDTO;
import com.bearcrew.groubear.approval.model.mapper.ApproverMapper;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

public class SelectDepartmentService {

	public List<DepartmentDTO> selectDeptCode(String deptCode) {
		
		SqlSession sqlSession = getSqlSession();
		
		ApproverMapper registMapper = sqlSession.getMapper(ApproverMapper.class);
		
		List<DepartmentDTO> deptList = registMapper.selectDeptCode(deptCode);
		
		sqlSession.close();
		
		return deptList;
	}
	
	
	public List<ReferenceDTO> selectReference(String deptCode) {
		
		SqlSession session = getSqlSession();
		
		ApproverMapper ReferenceMapper = session.getMapper(ApproverMapper.class);
		
		List<ReferenceDTO> ReferenceList = ReferenceMapper.selectReferenceList(deptCode);
		
		session.close();
		
		return ReferenceList;
		
	}
}
