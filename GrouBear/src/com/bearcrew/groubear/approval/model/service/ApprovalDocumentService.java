package com.bearcrew.groubear.approval.model.service;

import com.bearcrew.groubear.approval.model.dto.ApproverDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocContentDTO;
import com.bearcrew.groubear.approval.model.dto.DraftDocumentDTO;
import com.bearcrew.groubear.approval.model.dto.RefReceiverDTO;
import com.bearcrew.groubear.approval.model.mapper.ApprovalMapper;
import com.bearcrew.groubear.approval.model.mapper.ApproverMapper;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

public class ApprovalDocumentService {
	
	Map<String, Object> listMap;

	/* 결재 문서 작성 하는 메소드 */
	
	public int insertDocument(DraftDocumentDTO draftDocumentDTO) {

		SqlSession session = getSqlSession();

		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);

		int result = approvalMapper.insertDocument(draftDocumentDTO);

		if (result > 0) {
			session.commit();
			
		} else {
			session.rollback();
			
		}

		session.close();

		return result;
	}

	/* 결재 문서 내용을 작성하는 메소드 */
	
	public int insertDocContent(DraftDocContentDTO draftDocContentDTO) {

		SqlSession session = getSqlSession();

		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);

		int result = approvalMapper.insertDocContent(draftDocContentDTO);

		if (result > 0) {
			session.commit();
			
		} else {
			session.rollback();
			
		}

		session.close();

		return result;
	}

	/* 결재 요청 시, 결재자를 추가하는 메소드 */
	
	public int insertApprover(ApproverDTO approverDTO) {

		SqlSession session = getSqlSession();

		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);

		int result = approvalMapper.insertApprover(approverDTO);
		
		if (result < 0) {
			session.commit();
			
		} else {
			session.rollback();
			
		}

		session.close();

		return result;
	}
	
	/* 결재 요청 시, 수신 참조자를 추가하는 메소드 */
	
	public int insertRefReceiver(RefReceiverDTO refReceiverDTO) {
		
		SqlSession session = getSqlSession();

		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);

		int result = approvalMapper.insertRefReceiver(refReceiverDTO);
		
		if (result < 0) {
			session.commit();
			
		} else {
			session.rollback();
			
		}

		session.close();

		return result;
		
	}
	
	/* 결재 요청 시, 첨부 파일을 추가하는 메소드 */
	
	public int attachmentUpload(List<Map<String, Object>> fileList) {
		
		SqlSession session = getSqlSession();
		
		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);
		
		for (Map<String, Object> file : fileList) {
			listMap = file;
		}
		
		int result = approvalMapper.insertAttachment(listMap);

		if (result > 0) {
			session.commit();
			
		} else {
			session.rollback();
			
		}

		session.close();
		
		return result;
	} 

}
