package com.bearcrew.groubear.approval.model.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.approval.model.dto.ApprovalFormDTO;
import com.bearcrew.groubear.approval.model.dto.ChildApprovalFormDTO;
import com.bearcrew.groubear.approval.model.mapper.ApprovalMapper;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

public class ApprovalFormService {
	
//	양식 전체 조회하는 메소드
	public static List<ApprovalFormDTO> SelectApprovalFormList() {
		
		SqlSession session = getSqlSession();
		
		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class); 
		
		List<ApprovalFormDTO> ApprovalFormList = approvalMapper.searchApprovalForm();
		
		session.close();
		
		return ApprovalFormList;
	}

	public static List<ChildApprovalFormDTO> SelectChildApprovalFormList(int formNo) {
		
		SqlSession session = getSqlSession();
		
		ApprovalMapper approvalMapper = session.getMapper(ApprovalMapper.class);
		
		List<ChildApprovalFormDTO> childApprovalFormList = approvalMapper.searchChildApprovalForm(formNo);
		
		session.close();
		
		return childApprovalFormList;
	}

}
