package com.bearcrew.groubear.manager.empmanager.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.IdCheckDTO;
import com.bearcrew.groubear.manager.empmanager.model.registmapper.RegistMapper;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;

public class InsertEmpService {

	public int insertEmp(EmpDTO registEmp) {

		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		int result = registMapper.insertEmp(registEmp);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public EmpDTO selectDeptCode(EmpDTO registEmp) {
		
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		EmpDTO deptCode = registMapper.selectDeptCode(registEmp);
		
		sqlSession.close();
		
		return deptCode;
	}

	public EmpDTO selectJobCode(EmpDTO registEmp) {

		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		EmpDTO JobCode = registMapper.selectJobCode(registEmp);
		
		sqlSession.close();
		
		return JobCode;
	}

	public List<IdCheckDTO> selectAllId() {

		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		List<IdCheckDTO> selectAllId = registMapper.selectAllId();
		
		sqlSession.close();
		
		return selectAllId;
	}

	public List<JobDTO> selectAllJobGrade() {

		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		
		List<JobDTO> allJobGrade = registMapper.selectAllJobGrade();
		
		sqlSession.close();
		
		return allJobGrade;
	}

	public List<ChildOrgDTO> selectAllDeptTitle() {

		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		
		List<ChildOrgDTO> allDeptTitle = registMapper.selectAllDeptTitle();
		
		sqlSession.close();
		
		return allDeptTitle;
	}

}
