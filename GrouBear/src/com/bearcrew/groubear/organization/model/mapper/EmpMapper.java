package com.bearcrew.groubear.manager.empmanager.model.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.manager.empmanager.model.dto.AuthorityDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;

public interface EmpMapper {

	int selectTotalCount(Map<String, String> searchMap);
	
	List<EmpDTO> selectEmpList(SelectCriteria selectCriteria);

	EmpDTO selectEmpDetail(int no);

	int updateEmp(EmpDTO emp);

	List<AuthorityDTO> selectAuthorityList();

	/* List<OrgDTO> selectDeptList(); */


}
