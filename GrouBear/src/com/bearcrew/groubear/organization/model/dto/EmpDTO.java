package com.bearcrew.groubear.manager.empmanager.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class EmpDTO implements Serializable {

   private int no;
   private String name;
   private String id;
   private String pwd;
   private String gender;
   private java.sql.Date birthDate;
   private String jobGrade;
   private String deptTitle;
   private java.sql.Date hireDate;
   private java.sql.Date entDate;
   private String homeTel;
   private String phone;
   private String fax;
   private String emergencyContact;
   private String address;
   private String detailAddress;
   private String email;
   private String entYn;
   private String authorityName;
   private String deptCode;
   private String jobCode;
   private String authorityCode;

   public EmpDTO() {
   }

public EmpDTO(int no, String name, String id, String pwd, String gender, Date birthDate, String jobGrade,
		String deptTitle, Date hireDate, Date entDate, String homeTel, String phone, String fax,
		String emergencyContact, String address, String detailAddress, String email, String entYn, String authorityName,
		String deptCode, String jobCode, String authorityCode) {
	super();
	this.no = no;
	this.name = name;
	this.id = id;
	this.pwd = pwd;
	this.gender = gender;
	this.birthDate = birthDate;
	this.jobGrade = jobGrade;
	this.deptTitle = deptTitle;
	this.hireDate = hireDate;
	this.entDate = entDate;
	this.homeTel = homeTel;
	this.phone = phone;
	this.fax = fax;
	this.emergencyContact = emergencyContact;
	this.address = address;
	this.detailAddress = detailAddress;
	this.email = email;
	this.entYn = entYn;
	this.authorityName = authorityName;
	this.deptCode = deptCode;
	this.jobCode = jobCode;
	this.authorityCode = authorityCode;
}

public int getNo() {
	return no;
}

public void setNo(int no) {
	this.no = no;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getId() {
	return id;
}

public void setId(String id) {
	this.id = id;
}

public String getPwd() {
	return pwd;
}

public void setPwd(String pwd) {
	this.pwd = pwd;
}

public String getGender() {
	return gender;
}

public void setGender(String gender) {
	this.gender = gender;
}

public java.sql.Date getBirthDate() {
	return birthDate;
}

public void setBirthDate(java.sql.Date birthDate) {
	this.birthDate = birthDate;
}

public String getJobGrade() {
	return jobGrade;
}

public void setJobGrade(String jobGrade) {
	this.jobGrade = jobGrade;
}

public String getDeptTitle() {
	return deptTitle;
}

public void setDeptTitle(String deptTitle) {
	this.deptTitle = deptTitle;
}

public java.sql.Date getHireDate() {
	return hireDate;
}

public void setHireDate(java.sql.Date hireDate) {
	this.hireDate = hireDate;
}

public java.sql.Date getEntDate() {
	return entDate;
}

public void setEntDate(java.sql.Date entDate) {
	this.entDate = entDate;
}

public String getHomeTel() {
	return homeTel;
}

public void setHomeTel(String homeTel) {
	this.homeTel = homeTel;
}

public String getPhone() {
	return phone;
}

public void setPhone(String phone) {
	this.phone = phone;
}

public String getFax() {
	return fax;
}

public void setFax(String fax) {
	this.fax = fax;
}

public String getEmergencyContact() {
	return emergencyContact;
}

public void setEmergencyContact(String emergencyContact) {
	this.emergencyContact = emergencyContact;
}

public String getAddress() {
	return address;
}

public void setAddress(String address) {
	this.address = address;
}

public String getDetailAddress() {
	return detailAddress;
}

public void setDetailAddress(String detailAddress) {
	this.detailAddress = detailAddress;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getEntYn() {
	return entYn;
}

public void setEntYn(String entYn) {
	this.entYn = entYn;
}

public String getAuthorityName() {
	return authorityName;
}

public void setAuthorityName(String authorityName) {
	this.authorityName = authorityName;
}

public String getDeptCode() {
	return deptCode;
}

public void setDeptCode(String deptCode) {
	this.deptCode = deptCode;
}

public String getJobCode() {
	return jobCode;
}

public void setJobCode(String jobCode) {
	this.jobCode = jobCode;
}

public String getAuthorityCode() {
	return authorityCode;
}

public void setAuthorityCode(String authorityCode) {
	this.authorityCode = authorityCode;
}

@Override
public String toString() {
	return "EmpDTO [no=" + no + ", name=" + name + ", id=" + id + ", pwd=" + pwd + ", gender=" + gender + ", birthDate="
			+ birthDate + ", jobGrade=" + jobGrade + ", deptTitle=" + deptTitle + ", hireDate=" + hireDate
			+ ", entDate=" + entDate + ", homeTel=" + homeTel + ", phone=" + phone + ", fax=" + fax
			+ ", emergencyContact=" + emergencyContact + ", address=" + address + ", detailAddress=" + detailAddress
			+ ", email=" + email + ", entYn=" + entYn + ", authorityName=" + authorityName + ", deptCode=" + deptCode
			+ ", jobCode=" + jobCode + ", authorityCode=" + authorityCode + "]";
}

  
   

}