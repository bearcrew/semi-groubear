package com.bearcrew.groubear.organization.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.common.paging.Pagenation;
import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.EmpService;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgEmpDTO;
import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/emp/organization/list")
public class EmpOrgSelectListServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*
		 * 목록보기를 눌렀을 시 가장 처음에 보여지는 페이지는 1페이지이다. 파라미터로 전달되는 페이지가 있는 경우 currentPage는 파라미터로
		 * 전달받은 페이지 수 이다.
		 */
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;

		if (currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}

		/* 0보다 작은 숫자값을 입력해도 1페이지를 보여준다 */
		
		if (pageNo <= 0) {
			pageNo = 1;
		}

		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		if (searchValue == null) {
			searchValue = "";
		}

		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);

		/*
		 * 전체 회원정보 수가 필요하다. 데이터베이스에서 먼저 전체 게시물 수를 조회해올 것이다. 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물
		 * 수를 조회한다.
		 */
		
		EmpService empService = new EmpService();
		int totalCount = empService.selectTotalCount(searchMap);

		/* 한 페이지에 보여 줄 게시물 수 */
		
		int limit = 10; 	
		
		/* 한 번에 보여질 페이징 버튼의 갯수 */
		
		int buttonAmount = 3;

		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환받는다. */
		
		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}

		/* 조회해온다 */
		
		List<EmpDTO> empList = empService.nomalSelectEmpList(selectCriteria);

		/* 조직도 트리정보 불러오는 부분 start*/
		
		OrgService orgService = new OrgService();

		List<OrgDTO> orgList = orgService.selectParentOrg();
		List<OrgEmpDTO> orgEmpList = null;

		/* 부모부서리스트에서 부서들을 하나하나씩 꺼내 꺼낸부서의 부서코드로 자식부서를 조회해서 가져온후 List<자식부서DTO>에 넣어준다 */
		
		for (OrgDTO org : orgList) {
			List<ChildOrgDTO> childOrgList = orgService.selectChildOrg(org.getDeptCode());
			org.setChildOrgList(childOrgList);

			/* 자식부서와 같은 부서이름을 가진 사원 리스트 조회 */
			
			for (ChildOrgDTO chOrg : childOrgList) {
				orgEmpList = orgService.selectOrgEmpList(chOrg.getDeptCode());
				chOrg.setOrgEmpList(orgEmpList);
			}

		}

		OrgDTO companyCode = orgService.selectCompanyInfo();

		request.setAttribute("org", orgList);
		request.setAttribute("orgEmp", orgEmpList);
		request.setAttribute("company", companyCode);

		/* 조직도 트리정보 불러오는 부분 end */
		
		String path = "";
		if (empList != null) {
			path = "/WEB-INF/views/organization/empOrganization.jsp";
			request.setAttribute("empList", empList);
			request.setAttribute("selectCriteria", selectCriteria);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "searchEmpList");
		}

		request.getRequestDispatcher(path).forward(request, response);

	}
	

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 목록보기를 눌렀을 시 가장 처음에 보여지는 페이지는 1페이지이다.
		 * 파라미터로 전달되는 페이지가 있는 경우 currentPage는 파라미터로 전달받은 페이지 수 이다.
		 */ 
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 0보다 작은 숫자값을 입력해도 1페이지를 보여준다 */
		
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		/* 전체 회원정보 수가 필요하다.
		 * 데이터베이스에서 먼저 전체 게시물 수를 조회해올 것이다.
		 * 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회한다.
		 * */
		  
		EmpService empService = new EmpService();
		int totalCount = empService.selectTotalCount(searchMap);
		
		/* 한 페이지에 보여 줄 게시물 수 */
		
		int limit = 10;		
		
		/*한 번에 보여질 페이징 버튼의 갯수 */
		
		int buttonAmount = 3;
		
		/*페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환받는다. */
		
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		/* 사원목록을 조회해온다 */
		
		List<EmpDTO> empList1 = empService.selectEmpList(selectCriteria);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).setDateFormat("YYYY-MM-dd").create();
		
		String jsonString = gson.toJson(empList1);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		out.print(jsonString);
		
		out.close();
	}
		
}
