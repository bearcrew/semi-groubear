package com.bearcrew.groubear.common.headerAndSidebar.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.common.headerAndSidebar.model.mapper.HeaderAndSidebarMapper;

public class HeaderAndSidebarService {

	public List<String> searchMenu() {
		
		SqlSession sqlSession = getSqlSession();
		
		HeaderAndSidebarMapper mapper = sqlSession.getMapper(HeaderAndSidebarMapper.class);
		
		List<String> menuList = new ArrayList<>();
		
		menuList = mapper.searchMenu();
		
		
		sqlSession.close();
		
		return menuList;
	}

	public String serchFileName(int empNo) {
		
		SqlSession sqlSession = getSqlSession();
		
		HeaderAndSidebarMapper mapper = sqlSession.getMapper(HeaderAndSidebarMapper.class);
		
		String fileName = mapper.searchFileName(empNo);
		
		sqlSession.close();
		
		return fileName;
	}

	public int offProjectAlarm(int empNo) {
		
		SqlSession sqlSession = getSqlSession();
		
		HeaderAndSidebarMapper mapper = sqlSession.getMapper(HeaderAndSidebarMapper.class);
		
		int result = mapper.offProjectAlarm(empNo);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public String searchProjectAlarm(int empNo) {
		
		SqlSession sqlSession = getSqlSession();
		
		HeaderAndSidebarMapper mapper = sqlSession.getMapper(HeaderAndSidebarMapper.class);
		
		String projectAlarm = mapper.searchProjectAlarm(empNo);
		
		sqlSession.close();
		
		return projectAlarm;
	}

}
