package com.bearcrew.groubear.common.headerAndSidebar.model.mapper;

import java.util.List;

public interface HeaderAndSidebarMapper {

	List<String> searchMenu();

	String searchFileName(int empNo);

	int offProjectAlarm(int empNo);

	String searchProjectAlarm(int empNo);

}
