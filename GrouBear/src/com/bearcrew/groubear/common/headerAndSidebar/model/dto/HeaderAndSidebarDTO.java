package com.bearcrew.groubear.common.headerAndSidebar.model.dto;

public class HeaderAndSidebarDTO implements java.io.Serializable {

	private int empNo;
	private String fileName;
	
	public HeaderAndSidebarDTO() {}

	public HeaderAndSidebarDTO(int empNo, String fileName) {
		super();
		this.empNo = empNo;
		this.fileName = fileName;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Override
	public String toString() {
		return "HeaderAndSidebarDTO [empNo=" + empNo + ", fileName=" + fileName + "]";
	} 
	
	
}
