package com.bearcrew.groubear.common.headerAndSidebar.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.common.headerAndSidebar.model.service.HeaderAndSidebarService;

@WebServlet("/menu/search")
public class HeaderAndSiderbarServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HeaderAndSidebarService service = new HeaderAndSidebarService();
		
		int empNo = Integer.parseInt(request.getParameter("empNo"));
		
		List<String> menuList = service.searchMenu();
		
		String fileName = "";
		if(menuList != null) {
			
			fileName = service.serchFileName(empNo);
			
		}
		
		System.out.println("menuList : " + menuList);
		System.out.println("fileName : " + fileName);
		
		String path = "";
		if(menuList != null) {
			path = "/WEB-INF/views/common/headerAndSidebar.jsp";
			request.setAttribute("menuList", menuList);
			request.setAttribute("fileName", fileName);
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}


}
