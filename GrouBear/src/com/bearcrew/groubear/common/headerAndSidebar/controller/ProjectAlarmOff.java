package com.bearcrew.groubear.common.headerAndSidebar.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.common.headerAndSidebar.model.service.HeaderAndSidebarService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/alarm/project")
public class ProjectAlarmOff extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HeaderAndSidebarService service = new HeaderAndSidebarService();
		
		int empNo = Integer.parseInt(request.getParameter("empNo"));
		
		int result = service.offProjectAlarm(empNo);
		
		HttpSession session = request.getSession();

		String path = "";
		
		if(result > 0 ) {
			path = "/WEB-INF/views/common/alarmControll.jsp";
			session.setAttribute("alarmCode", "projectAlarmOff");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			session.setAttribute("failedCode", "projectAlarmOff");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HeaderAndSidebarService service = new HeaderAndSidebarService();
		
		int empNo = Integer.parseInt(request.getParameter("empNo"));
		
		String projectAlarm = service.searchProjectAlarm(empNo);
		
		Gson gson = new GsonBuilder().create();
		
		String alarmOnAndOff = gson.toJson(projectAlarm);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		if(alarmOnAndOff != null) {
			out.print(alarmOnAndOff);
		}
		
		out.close();
		
		
	}
}
