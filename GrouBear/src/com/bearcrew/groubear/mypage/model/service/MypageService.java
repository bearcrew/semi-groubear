package com.bearcrew.groubear.mypage.model.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;
import com.bearcrew.groubear.mypage.model.mapper.MypageMapper;

public class MypageService {

	Map<String, Object> listMap;
	Map<String, Object> signlistMap;
	Map<String, Object> selectlistMap;
	/* 비밀 번호체크 후 비밀번호 업데이트 */
	public int PwdCheck(PwdUpdateDTO pwdDTO) {       

		SqlSession sqlSession = getSqlSession();

		System.out.println("여긴 서비스" + pwdDTO); 

		String id = pwdDTO.getEmpId(); 

		String nowPwd = pwdDTO.getNowPwd(); // 현재 비밀번호
		String newPwd = pwdDTO.getNewPwd(); // 바꿀 비밀번호

		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class); // DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.
		String memberselectPWD = mypageMapper.seletePwd(id); // 현재 비밀번호가지고 이 아이디에 주인이 맡냐 물어보로가서 다시 리턴받은값
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); // 암호화 객체
		int memberPWDUpdate = 0;
		
		if (passwordEncoder.matches(nowPwd, memberselectPWD)) /* 순서 잘해야 바뀐다. */ {

			memberPWDUpdate = mypageMapper.updatePwd(pwdDTO);

			if (memberPWDUpdate > 0) {

				sqlSession.commit();

			} else {
				sqlSession.rollback();
			}
		}

		sqlSession.close();

		return memberPWDUpdate;
	}
	/* 사진 업로드시 정보가 있는지 확인 */
	public List<Integer> selectEmpNO(int empNo) {
		
		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		List<Integer> selectEmpCheck = mypageMapper.selectEmpCheck(empNo);
		
		System.out.println("selectEmpChack 서비스요~ : " + selectEmpCheck);
		
		sqlSession.close();
			
		return selectEmpCheck;
		
	}
	public List<Integer> selectSignNo(int empNo) {
		
		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		List<Integer> selectSignCheck = mypageMapper.selectSignCheck(empNo);
		
		System.out.println("selectEmpChack 서비스요~ : " + selectSignCheck);
		
		sqlSession.close();
			
		return selectSignCheck;
	}
	/* 프로필 사진 업로드 */
	public int photoUpload(List<Map<String, Object>> fileList) {

		SqlSession sqlSession = getSqlSession();				//	세션 생성

		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);					

		for (Map<String, Object> a : fileList) {								

			listMap = a;

		}

		System.out.println("listMap : " + listMap);

		int result = mypageMapper.insertMyPhoto(listMap);

		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		sqlSession.close();
		return result;
		
	}
	/* 사인 이미지 업로드*/
	public int signUpload(List<Map<String, Object>> fileList) {
		
		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		for (Map<String, Object> a : fileList) {
			
			listMap = a;
			
		}
		
		int result = mypageMapper.insertSignPhoto(listMap);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		
		sqlSession.close();
		return result;
		
		
	}
	/* 프로필 사진 업데이트 */
	public int profileUpdate(List<Map<String, Object>> fileList) {

		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		for (Map<String, Object> a : fileList) {
			
			signlistMap = a;
			
		}
		
		int result = mypageMapper.updateMyPhoto(signlistMap);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		
		sqlSession.close();
		return result;
		
	}
	/* 사인 이미지 업데이트 */
	public int signUpdate(List<Map<String, Object>> fileList) {

		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		for (Map<String, Object> a : fileList) {
			
			signlistMap = a;
			
		}
		
		int result = mypageMapper.updateSign(signlistMap);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		sqlSession.close();
		return result;
	}
	
	/* 마이페이지 정보 수정 */
	public int MyPageInfo(LoginDTO loginDTO) {

		SqlSession sqlSession = getSqlSession();
		MypageMapper mypageMapper = sqlSession.getMapper(MypageMapper.class);
		
		int resultMyPageInfo = mypageMapper.MyPageInfo(loginDTO);
		
		System.out.println("resultMyPageInfo : " + resultMyPageInfo);
		
		if (resultMyPageInfo > 0) {
			
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		
		sqlSession.close();
			
		
		return resultMyPageInfo;
	}
	

	
}

