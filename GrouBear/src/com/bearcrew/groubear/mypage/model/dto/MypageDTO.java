package com.bearcrew.groubear.mypage.model.dto;

import java.sql.Date;

public class MypageDTO implements java.io.Serializable {
	
			private int empNo;							//사원번호
			private String empName;						//사원명
			private String empId;						//아이디	
			private String empPwd;						//비밀번호
			private char gender;						//성별
			private java.sql.Date birthDate;			//생년월일
			private java.sql.Date hirtDate;				//입사일
			private java.sql.Date entDate;				//퇴사일 
			private int homeTel;						//집전화번호
			private int phone;							//휴대전화번호
			private int fax;							//팩스번호
			private int emergencyContact;				//긴급연락처
			private String address;						//집주소
			private String email;						//이메일
			private String entYn;						//퇴사여부
			private char approvalAlarm;					//전자결재 알림
			private char projectAlarm;					//프로젝트 알림
			
			private String deptCode;
			private String jobCode;
			
		public MypageDTO () {}

		public MypageDTO(int empNo, String empName, String empId, String empPwd, char gender, Date birthDate,
				Date hirtDate, Date entDate, int homeTel, int phone, int fax, int emergencyContact, String address,
				String email, String entYn, char approvalAlarm, char projectAlarm, String deptCode, String jobCode) {
			super();
			this.empNo = empNo;
			this.empName = empName;
			this.empId = empId;
			this.empPwd = empPwd;
			this.gender = gender;
			this.birthDate = birthDate;
			this.hirtDate = hirtDate;
			this.entDate = entDate;
			this.homeTel = homeTel;
			this.phone = phone;
			this.fax = fax;
			this.emergencyContact = emergencyContact;
			this.address = address;
			this.email = email;
			this.entYn = entYn;
			this.approvalAlarm = approvalAlarm;
			this.projectAlarm = projectAlarm;
			this.deptCode = deptCode;
			this.jobCode = jobCode;
		}

		public int getEmpNo() {
			return empNo;
		}

		public void setEmpNo(int empNo) {
			this.empNo = empNo;
		}

		public String getEmpName() {
			return empName;
		}

		public void setEmpName(String empName) {
			this.empName = empName;
		}

		public String getEmpId() {
			return empId;
		}

		public void setEmpId(String empId) {
			this.empId = empId;
		}

		public String getEmpPwd() {
			return empPwd;
		}

		public void setEmpPwd(String empPwd) {
			this.empPwd = empPwd;
		}

		public char getGender() {
			return gender;
		}

		public void setGender(char gender) {
			this.gender = gender;
		}

		public java.sql.Date getBirthDate() {
			return birthDate;
		}

		public void setBirthDate(java.sql.Date birthDate) {
			this.birthDate = birthDate;
		}

		public java.sql.Date getHirtDate() {
			return hirtDate;
		}

		public void setHirtDate(java.sql.Date hirtDate) {
			this.hirtDate = hirtDate;
		}

		public java.sql.Date getEntDate() {
			return entDate;
		}

		public void setEntDate(java.sql.Date entDate) {
			this.entDate = entDate;
		}

		public int getHomeTel() {
			return homeTel;
		}

		public void setHomeTel(int homeTel) {
			this.homeTel = homeTel;
		}

		public int getPhone() {
			return phone;
		}

		public void setPhone(int phone) {
			this.phone = phone;
		}

		public int getFax() {
			return fax;
		}

		public void setFax(int fax) {
			this.fax = fax;
		}

		public int getEmergencyContact() {
			return emergencyContact;
		}

		public void setEmergencyContact(int emergencyContact) {
			this.emergencyContact = emergencyContact;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getEntYn() {
			return entYn;
		}

		public void setEntYn(String entYn) {
			this.entYn = entYn;
		}

		public char getApprovalAlarm() {
			return approvalAlarm;
		}

		public void setApprovalAlarm(char approvalAlarm) {
			this.approvalAlarm = approvalAlarm;
		}

		public char getProjectAlarm() {
			return projectAlarm;
		}

		public void setProjectAlarm(char projectAlarm) {
			this.projectAlarm = projectAlarm;
		}

		public String getDeptCode() {
			return deptCode;
		}

		public void setDeptCode(String deptCode) {
			this.deptCode = deptCode;
		}

		public String getJobCode() {
			return jobCode;
		}

		public void setJobCode(String jobCode) {
			this.jobCode = jobCode;
		}

		@Override
		public String toString() {
			return "LoginDTO [empNo=" + empNo + ", empName=" + empName + ", empId=" + empId + ", empPwd=" + empPwd
					+ ", gender=" + gender + ", birthDate=" + birthDate + ", hirtDate=" + hirtDate + ", entDate="
					+ entDate + ", homeTel=" + homeTel + ", phone=" + phone + ", fax=" + fax + ", emergencyContact="
					+ emergencyContact + ", address=" + address + ", email=" + email + ", entYn=" + entYn
					+ ", approvalAlarm=" + approvalAlarm + ", projectAlarm=" + projectAlarm + ", deptCode=" + deptCode
					+ ", jobCode=" + jobCode + "]";
		}



}