package com.bearcrew.groubear.mypage.model.dto;

public class PwdUpdateDTO implements java.io.Serializable{

	private String empId;
	private String nowPwd;
	private String newPwd;
	
	public PwdUpdateDTO(){}

	public PwdUpdateDTO(String empId, String nowPwd, String newPwd) {
		super();
		this.empId = empId;
		this.nowPwd = nowPwd;
		this.newPwd = newPwd;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getNowPwd() {
		return nowPwd;
	}

	public void setNowPwd(String nowPwd) {
		this.nowPwd = nowPwd;
	}

	public String getNewPwd() {
		return newPwd;
	}

	public void setNewPwd(String newPwd) {
		this.newPwd = newPwd;
	}

	@Override
	public String toString() {
		return "PwdUpdateDTO [empId=" + empId + ", nowPwd=" + nowPwd + ", newPwd=" + newPwd + "]";
	}

	
	
}