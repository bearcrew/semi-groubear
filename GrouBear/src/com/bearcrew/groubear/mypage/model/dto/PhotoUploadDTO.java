package com.bearcrew.groubear.mypage.model.dto;

public class PhotoUploadDTO implements java.io.Serializable {

	private int fileNoNumber;					//파일 번호 내가 입력하지 않아도 될듯함
	private String savedFileName;					//파일명 V uuid 처리한 파일명
	private String oriFielName;					//실제파일명 V 실제 파일명
	private int fileSize;						//파일크기 V 파일 크기
	private int empNo;							//사원번호  
	private String fileType;					//파일 타입이 이미지 인지 파일인지
	
	public PhotoUploadDTO() {}

	public PhotoUploadDTO(int fileNoNumber, String savedFileName, String oriFielName, int fileSize, int empNo,
			String fileType) {
		super();
		this.fileNoNumber = fileNoNumber;
		this.savedFileName = savedFileName;
		this.oriFielName = oriFielName;
		this.fileSize = fileSize;
		this.empNo = empNo;
		this.fileType = fileType;
	}

	public int getFileNoNumber() {
		return fileNoNumber;
	}

	public void setFileNoNumber(int fileNoNumber) {
		this.fileNoNumber = fileNoNumber;
	}

	public String getSavedFileName() {
		return savedFileName;
	}

	public void setSavedFileName(String savedFileName) {
		this.savedFileName = savedFileName;
	}

	public String getOriFielName() {
		return oriFielName;
	}

	public void setOriFielName(String oriFielName) {
		this.oriFielName = oriFielName;
	}

	public int getFileSize() {
		return fileSize;
	}

	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	@Override
	public String toString() {
		return "PhotoUploadDTO [fileNoNumber=" + fileNoNumber + ", savedFileName=" + savedFileName + ", oriFielName="
				+ oriFielName + ", fileSize=" + fileSize + ", empNo=" + empNo + ", fileType=" + fileType + "]";
	}

}
