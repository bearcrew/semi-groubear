package com.bearcrew.groubear.mypage.model.dto;

public class EmpNumDTO implements java.io.Serializable {

	private int empNum;
	
	public EmpNumDTO() {}

	public EmpNumDTO(int empNum) {
		super();
		this.empNum = empNum;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	@Override
	public String toString() {
		return "EmpNumDTO [empNum=" + empNum + "]";
	}
	
	
}
