package com.bearcrew.groubear.mypage.model.mapper;

import java.util.List;
import java.util.Map;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;

public interface MypageMapper {

	String seletePwd(String id);

	int updatePwd(PwdUpdateDTO pwdDTO);

	List<Integer> selectEmpCheck(int empNo);

	List<Integer> selectSignCheck(int empNo);

	int insertMyPhoto(Map<String, Object> listMap);

	int insertSignPhoto(Map<String, Object> listMap);

	int updateMyPhoto(Map<String, Object> signlistMap);

	int updateSign(Map<String, Object> signlistMap);

	int MyPageInfo(LoginDTO loginDTO);


}
