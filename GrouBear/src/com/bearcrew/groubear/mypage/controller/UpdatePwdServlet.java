package com.bearcrew.groubear.mypage.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;
import com.bearcrew.groubear.mypage.model.service.MypageService;


@WebServlet("/mypage/updatepwd")
public class UpdatePwdServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = "/WEB-INF/views/mypage/passChange.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		
		String userId = loginDTO.getEmpId();																		/*로그인 DTO에 담겨있는 아이디값을 가져온다*/
		String userNowPwd = request.getParameter("userNowPwd");														//현재 비밀번호*/
		String userNewPwd = request.getParameter("pwd");					   										//바꿀 비밀번호*/
		String userNewPwdCheck = request.getParameter("userNewPwdCheck");											//바꿀 비밀번호 확인*/
		
		PwdUpdateDTO pwdDTO = new PwdUpdateDTO();
		
		pwdDTO.setEmpId(userId);																					/*아이디값을 DTO에 담는다*/
		pwdDTO.setNowPwd(userNowPwd);																				/*현재 패스워드 값을 DTO에 담는다*/	
		pwdDTO.setNewPwd(userNewPwd);																				/*바꿀 패스워드 값을 DTO에 담는다*/
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(); 										/*암호화 걸려있는 비밀번호를 매칭해서 비교한다*/	
		String path ="";
		if (passwordEncoder.matches(userNewPwdCheck, userNewPwd)) {													/*순서 바꿔서 적으니깐 제대로 실행 됨*/
		
			LoginDTO requestLogin = new LoginDTO();
			requestLogin.setEmpPwd(userNowPwd);																		/*비밀번호 값을 DTO에 담는다*/
			
			MypageService mypageService = new MypageService();														/*서비스를 호출한다*/
			int MemberPWDUpdate = mypageService.PwdCheck(pwdDTO);													/*서비스를 호출하여 pwdCheck를 한다*/
			
			if(MemberPWDUpdate > 0) {																				/*체크하고 리턴 받을때 값이 있을경우 성공 메세지를 출력한다*/
				
				path = "/WEB-INF/views/common/success.jsp";
				request.setAttribute("successCode", "updateSearchPwd");
				request.getRequestDispatcher(path).forward(request, response);
				
			} else {																								/*값이 없을 경우 에러 메세지를 출력한다*/
				
				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "FailedPassword");
				request.getRequestDispatcher(path).forward(request, response);
			}
			
		} else {																									/*암호화가 틀렸을 경우 메세지를 출력한다*/
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "FailedPassword");
			request.getRequestDispatcher(path).forward(request, response);
		}
	
	}
	
		
}


