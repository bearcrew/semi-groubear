package com.bearcrew.groubear.mypage.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.service.LoginService;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.mypage.model.service.MypageService;


@WebServlet("/mypage/info")
public class UpdateInfoServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String path = "/WEB-INF/views/mypage/infoChange.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		LoginDTO loginDTO = new LoginDTO();
		 
		loginDTO = (LoginDTO) session.getAttribute("loginMember");									/*로그인 할떄 세션에 담겨져있는 유저 정보를 불러온다.*/ 
		
		String userId = loginDTO.getEmpId();														/* 아이디 값을 불러온다.*/
		
		String homeTel = request.getParameter("homeTel");											/*마이페이지에서 수정한 집 전화 번호 값을 가져온다.*/
		String phone = request.getParameter("phone");											    /*마이페이지에서 수정한 폰전화 번호 값을 가져온다.*/
		String fax = request.getParameter("fax");													/*마이페이지에서 수정한 팩스 번호 값을 가져온다.*/
		String emergencyContact = request.getParameter("emergencyContact");							/*마이페이지에서 수정한 긴급 연락처 값을 가져온다.*/
		String address = request.getParameter("address3") + "#" + request.getParameter("address4") + "#" + request.getParameter("address5");		/*마이페이지에서 수정한 우편번호와 집주소 상세주소를 가져온다.*/
		
		String email = request.getParameter("email");												/*마이페이지에서 수정한 이메일 정보를 가져온다.*/
		
		loginDTO.setEmpId(userId);																	/*로그인 DTO에 값을 담아준다*/
		loginDTO.setHomeTel(homeTel);																/*로그인 DTO에 값을 담아준다*/
		loginDTO.setPhone(phone);																	/*로그인 DTO에 값을 담아준다*/
		loginDTO.setFax(fax);																		/*로그인 DTO에 값을 담아준다*/
		loginDTO.setEmergencyContact(emergencyContact);												/*로그인 DTO에 값을 담아준다*/
		loginDTO.setAddress(address);																/*로그인 DTO에 값을 담아준다*/
		loginDTO.setEmail(email);																	/*로그인 DTO에 값을 담아준다*/
		
		
		String path ="";
		
		MypageService mypageService = new MypageService();								/*서비스 생성한다*/
		
		int loginMember = mypageService.MyPageInfo(loginDTO);							/*서비스 메소드 만든후 로그인DTO변수를 넣어준다*/
		
		if (loginMember > 0) {															/*정보 수정에 성공할시 성공 메세지를 출력한다.*/
			
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateInfo");
			request.getRequestDispatcher(path).forward(request, response);
				
		} else {																		/*정보 수정에 실패할시 실패 메세지를 출력한다.*/
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "FailedInfo");
			request.getRequestDispatcher(path).forward(request, response);
			
		}
		
	}

}
