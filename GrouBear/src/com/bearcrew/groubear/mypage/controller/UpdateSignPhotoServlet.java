package com.bearcrew.groubear.mypage.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.mypage.model.service.MypageService;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;
@WebServlet("/mypage/signphotoupdate")
public class UpdateSignPhotoServlet extends HttpServlet {

	int result = 0;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ServletContext context = request.getServletContext(); 																		/* webXml 쪽에 연결*/
		String rootLocation = context.getInitParameter("upload-signPhoto"); 														/* webXml 쪽에 연결(경로)*/
		int maxFileSize = Integer.parseInt(context.getInitParameter("max-file-size")); 												/* webXml 쪽에 연결(파일 용량)*/
		String encodingType = context.getInitParameter("encoding-type"); 															/* webXml 쪽에 연결(인코딩 타입)*/								

		if (ServletFileUpload.isMultipartContent(request)) { 																		/* isMultipartContent : 논리형 메소드 요청한 파일이 있는경우*/

			String fileUploadDirectory = request.getServletContext().getRealPath("/") + rootLocation;								/* 파일 경로 지정된 곳에 추가해서 변수에 담아준다.*/
			
			HttpSession session = request.getSession();																				/*세션 생성*/
			LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");														/*로그인된 loginMember을 가져와서 사원번호를 가져온다*/
			
			int empNo = loginDTO.getEmpNo(); 																						/*사원번호를 가져온다*/
			
			
			File directory = new File(fileUploadDirectory); 																		/* 파일 클래스를 생성한다*/

			if (!directory.exists()) {																								/* 파일이 없을경우 실행된다*/
			}

			Map<String, String> parameter = new HashMap<>();
			List<Map<String, Object>> fileList = new ArrayList<>();

			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();														/*저장소 클래스 생성*/
			fileItemFactory.setRepository(new File(fileUploadDirectory));															/*저장소 경로를 지정해준다*/
			fileItemFactory.setSizeThreshold(maxFileSize);																			/*저장소 사이즈를 지정해준다*/

			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);													/*업로드 클래스 생성*/


			try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);
				for (FileItem item : fileItems) {
				}

				for (int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);

					if (!item.isFormField()) {

						if (item.getSize() > 0) {

							long fileSize = (item.getSize());
							String fileType = item.getFieldName();
							String originFileName = item.getName();
							String sdasd = item.getContentType();

							int dot = originFileName.lastIndexOf("."); 																/* 뒤에서부터 원하는 글자를 찾는다 지금은 (".") 을 찾아 .앞에 있는 글자를 다가져온다 리턴 타입은 int 이다.*/

							String ext = originFileName.substring(dot);

							String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;

							File storeFile = new File(fileUploadDirectory + "/" + randomFileName); 									/* uuid 파일 이름*/

							String savedFileName = (randomFileName);
							
							item.write(storeFile);																					/* 업로드된 파일을 디스크에 저장합니다.*/

							
							Map<String, Object> fileMap = new HashMap<>();															/* 디스크에 저장 유무를 확인하기 위한 map*/

							fileMap.put("savedFileName", savedFileName); 															/* UUID한 세이브 파일 이름*/
							fileMap.put("originFileName", originFileName); 															/* 실제 파일 이름*/
							fileMap.put("fileSize", fileSize); 																		/* 파일 사이즈*/
							fileMap.put("empNo", empNo);																			/* 가져온 사원번호*/
							fileMap.put("fileType", fileType);																		/* 파일 타입*/

							fileList.add(fileMap);

						}

					} else { 																										/* 단순 양식 필드인 경우*/
						parameter.put(item.getFieldName(), item.getString());
						parameter.put(item.getFieldName(),
								new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
					}
				}


				MypageService mypageService = new MypageService();
				
				List<Integer> profileCheck = mypageService.selectSignNo(empNo);														/*사원에 서명 사진이 있는지 먼저 체크한다.*/
				
				if (profileCheck.contains(empNo)) {																					
					int resultUpdate = mypageService.signUpdate(fileList);															/*사원에 서명 사진이 있을 경우 업데이트를 실행한다.*/
					
					
				} else {																											/*사원에 서명 사진이 있을 경우 인서트를 실행한다.*/
					int resultUpload = mypageService.signUpload(fileList);
				}

				response.setCharacterEncoding("UTF-8");																				/*프린트 출력할 문은 UTF-8로 변경한다.*/
				PrintWriter out = response.getWriter();																				/*프린트 출력 메소드*/		

																														
				out.print("사진 업로드 성공");																							/*사진이 완료 되었을 경우 출력되는 메세지 이다.*/
				
				out.flush();
				out.close();
				String path = "/WEB-INF/views/mypage/infoChange.jsp";
				request.getRequestDispatcher(path).forward(request, response);

			} catch (Exception e) {																									/* 어떤 종류의 Exception이 발생 하더라도실패 시 파일을 삭제해야 한다.*/
																																	/* Exception발생하면 임시파일이 생성되서 지워줘야한다 (flush개념이랑 비슷하다)*/
 
				int cnt = 0;

				for (int i = 0; i < result; i++) {


					Map<String, Object> file = fileList.get(i);

					File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
					boolean isDeleted = deleteFile.delete();

					if (isDeleted) {
						cnt++;
					}
				}

				if (cnt == fileList.size()) {
				
					response.setCharacterEncoding("UTF-8");
					PrintWriter out = response.getWriter();
					out.print("사진 업로드 실패");

					out.flush();
					out.close();
					String path = "/WEB-INF/views/mypage/infoChange.jsp";
					request.getRequestDispatcher(path).forward(request, response);
				} else {
					e.printStackTrace();
				}

			}
		}
	}

}
