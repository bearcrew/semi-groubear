package com.bearcrew.groubear.manager.boardmanager.model.dto;

import java.io.Serializable;

public class BoardCodeDTO implements Serializable{

	private String boardCode;
	
	public BoardCodeDTO() {}

	public BoardCodeDTO(String boardCode) {
		super();
		this.boardCode = boardCode;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	@Override
	public String toString() {
		return "BoardCodeDTO [boardCode=" + boardCode + "]";
	}
	
	
}
