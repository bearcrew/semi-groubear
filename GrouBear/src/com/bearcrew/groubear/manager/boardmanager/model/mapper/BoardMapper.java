package com.bearcrew.groubear.manager.boardmanager.model.mapper;

import java.util.List;

import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardCodeDTO;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;

public interface BoardMapper {

	List<BoardDTO> selectAllBoartList();

	List<BoardCodeDTO> selectAllBoardCodeList();

	int insertRefBoard(BoardDTO boardDTO);

	BoardDTO selecOneBoard(BoardDTO oneBoardDTO);

	int updateBoard(BoardDTO boardDTO);

	int deleteBoard(BoardDTO boardDTO);

}
