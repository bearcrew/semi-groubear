package com.bearcrew.groubear.manager.boardmanager.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class BoardDTO implements Serializable{

	private String boardCode;
	private String boardTitle;
	private String category;
	private java.sql.Date createDate;
	
	public BoardDTO() {}

	public BoardDTO(String boardCode, String boardTitle, String category, Date createDate) {
		super();
		this.boardCode = boardCode;
		this.boardTitle = boardTitle;
		this.category = category;
		this.createDate = createDate;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	public String getBoardTitle() {
		return boardTitle;
	}

	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public java.sql.Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(java.sql.Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "BoardDTO [boardCode=" + boardCode + ", boardTitle=" + boardTitle + ", category=" + category
				+ ", createDate=" + createDate + "]";
	}
	
	
	
}
