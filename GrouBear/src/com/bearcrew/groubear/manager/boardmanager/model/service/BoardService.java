package com.bearcrew.groubear.manager.boardmanager.model.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardCodeDTO;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.manager.boardmanager.model.mapper.BoardMapper;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

public class BoardService {

	public List<BoardDTO> selectAllBoardList() {
		//모든 게시판 정보를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		List<BoardDTO> boardList = boardMapper.selectAllBoartList();
		
		sqlSession.close();
		
		return boardList;
	}

	public List<BoardCodeDTO> selecAllBoardCodeList() {
		//보드 코드를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		List<BoardCodeDTO> boardCodeList = boardMapper.selectAllBoardCodeList();
		
		sqlSession.close();
		
		return boardCodeList;
	}

	public int insertRefBoard(BoardDTO boardDTO) {
		//게시판 등록을 한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		int result = boardMapper.insertRefBoard(boardDTO);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public BoardDTO selecOneBoard(BoardDTO oneBoardDTO) {
		//게시판 정보를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		BoardDTO oneBoard = boardMapper.selecOneBoard(oneBoardDTO);
		
		sqlSession.close();
		
		return oneBoard;
	}

	public int updateBoard(BoardDTO boardDTO) {
		//게시판 정보 수정을 한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		int result = boardMapper.updateBoard(boardDTO);
		
		if(result > 0) {
			sqlSession.commit();
		} else{
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	public int deleteBoard(BoardDTO boardDTO) {
		//게시판 정보를 삭제한다
		SqlSession sqlSession = getSqlSession();
		
		BoardMapper boardMapper = sqlSession.getMapper(BoardMapper.class);
		int result = boardMapper.deleteBoard(boardDTO);
		
		if(result > 0) {
			sqlSession.commit();
		} else{
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	


	

}
