package com.bearcrew.groubear.manager.boardmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.manager.boardmanager.model.service.BoardService;

@WebServlet("/manager/board/insert")
public class BoardInsertServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* insertBoard.jsp 페이지로 간다 */
		String path = "/WEB-INF/views/manager/boardmanager/insertBoard.jsp";
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 전달 받은 게시판명을 변수에 담는다 */
		String boardTitle = request.getParameter("boardTitle");
		
		/* 받아온 게시판명을 DTO에 담는다 */
		BoardDTO boardDTO = new BoardDTO();
		boardDTO.setBoardTitle(boardTitle);
		
		/* DTO를 서비스로 전달해서 게시판을 추가한다 */
		BoardService boardService = new BoardService();
		int result = boardService.insertRefBoard(boardDTO);
		
		/* 게시판 추가 성공 여부에 따라 페이지를 연결한다 */
		String path = "";
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "insertRefBoard");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "insertBoard");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}
