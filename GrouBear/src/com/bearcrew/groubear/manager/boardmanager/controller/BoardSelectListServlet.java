package com.bearcrew.groubear.manager.boardmanager.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.manager.boardmanager.model.service.BoardService;

@WebServlet("/manager/board/list")
public class BoardSelectListServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 세션에서 현재 로그인한 계정의 권한을 가져온다 */
		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		String authorityCode = loginDTO.getAuthorityCode();
		
		/* 권한코드를 확인하여 접근 가능 여부를 출력한다 */
		String path = "";
		if (authorityCode.equals("1")) {
			
			/* 서비스를 호출하여 게시판 목록을 조회한 결과를 boardList에 담는다 */
			BoardService boardService = new BoardService();
			List<BoardDTO> boardList = boardService.selectAllBoardList();
			
			/* 조회 결과에 따라 페이지를 연결한다 */
			if(boardList != null) {
				path = "/WEB-INF/views/manager/boardmanager/boardList.jsp";
				session.setAttribute("boardList", boardList);
			} else {
				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "selectboardList");
			}
			
		} else {
			/* 권한코드가 1이 아닐경우 접근 권한이 없다는 메세지를 띄운다 */
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "isNotAdmin");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

}
