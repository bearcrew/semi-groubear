package com.bearcrew.groubear.manager.boardmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.manager.boardmanager.model.service.BoardService;

@WebServlet("/manager/board/delete")
public class BoardDeleteServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 전달 받은 값을 변수에 담는다 */
		String boardTitle = request.getParameter("boardTitle");
		
		/* 변수에 담은 값을 DTO에 담는다 */
		BoardDTO boardDTO = new BoardDTO();
		boardDTO.setBoardTitle(boardTitle);
		
		/* DTO에 담은 값을 서비스로 전달해서 삭제 성공 여부에 따른 result값을 리턴 받는다  */
		BoardService boardService = new BoardService();
		int result = boardService.deleteBoard(boardDTO);
		
		/* 삭제 성공시 success페이지로 실패시 failed페이지로 연결  */
		String path = "";
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteBoard");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "deleteBoard");
		}
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
