package com.bearcrew.groubear.manager.boardmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.manager.boardmanager.model.service.BoardService;

@WebServlet("/manager/board/update")
public class BoardUpdateServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 전달받은  게시판명을 변수에 담는다 */
		String boardTitle = request.getParameter("boardTitle");
		
		/* 변수에 담은 게시판명을 DTO에 담는다 */
		BoardDTO oneBoardDTO = new BoardDTO();
		oneBoardDTO.setBoardTitle(boardTitle);
		
		/* DTO를 매개변수로 서비스로 보내서 해당 게시판 정보를 가져온다 */
		BoardService boardService = new BoardService();
		BoardDTO updateBoard = boardService.selecOneBoard(oneBoardDTO);
		
		/* 조회 성공 여부에 따라 페이지를 연결한다 */
		String path = "";
		if(updateBoard != null) {
			path = "/WEB-INF/views/manager/boardmanager/updateBoard.jsp";
			request.setAttribute("updateBoard", updateBoard);						
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "updateBoard");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 변경할 게시판 명과 코드를 받아온다 */
		String updateBoardTitle = request.getParameter("updateBoardTitle");
		String boardCode = request.getParameter("boardCode");
		
		/* 받아온 정보를 DTO에 담는다 */
		BoardDTO boardDTO = new BoardDTO();
		boardDTO.setBoardTitle(updateBoardTitle);
		boardDTO.setBoardCode(boardCode);
		
		/* DTO에 담은 내용을 서비스에 전달해서 게시판 정보를 변경한다 */
		BoardService boardService = new BoardService();
		int result = boardService.updateBoard(boardDTO);
		
		/* 수정 성공 여부에 따라 페이지를 연결한다. */
		String path = "";
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateBoard");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시판 정보 수정 실패!");
		}
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
