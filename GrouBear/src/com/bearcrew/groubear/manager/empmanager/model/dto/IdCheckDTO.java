package com.bearcrew.groubear.manager.empmanager.model.dto;

import java.io.Serializable;

public class IdCheckDTO implements Serializable{

	private String id;
	
	public IdCheckDTO() {}

	public IdCheckDTO(String id) {
		super();
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return this.id;
	}
	
	
}
