package com.bearcrew.groubear.manager.empmanager.model.dto;

public class AuthorityDTO implements java.io.Serializable {

	private String authorityCode;
	private String authorityName;
	private String authorityYn;
	
	public AuthorityDTO() {}

	public AuthorityDTO(String authorityCode, String authorityName, String authorityYn) {
		super();
		this.authorityCode = authorityCode;
		this.authorityName = authorityName;
		this.authorityYn = authorityYn;
	}

	public String getAuthorityCode() {
		return authorityCode;
	}

	public void setAuthorityCode(String authorityCode) {
		this.authorityCode = authorityCode;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public String getAuthorityYn() {
		return authorityYn;
	}

	public void setAuthorityYn(String authorityYn) {
		this.authorityYn = authorityYn;
	}

	@Override
	public String toString() {
		return "AuthorityDTO [authorityCode=" + authorityCode + ", authorityName=" + authorityName + ", authorityYn="
				+ authorityYn + "]";
	}
	
	
	
	
}
