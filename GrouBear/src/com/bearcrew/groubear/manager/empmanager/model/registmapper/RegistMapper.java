package com.bearcrew.groubear.manager.empmanager.model.registmapper;

import java.util.List;

import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.IdCheckDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;

public interface RegistMapper {

	int insertEmp(EmpDTO registEmp);

	EmpDTO selectDeptCode(EmpDTO registEmp);

	EmpDTO selectJobCode(EmpDTO registEmp);

	List<IdCheckDTO> selectAllId();

	List<JobDTO> selectAllJobGrade();

	List<ChildOrgDTO> selectAllDeptTitle();

}
