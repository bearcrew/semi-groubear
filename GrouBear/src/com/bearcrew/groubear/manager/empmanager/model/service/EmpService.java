package com.bearcrew.groubear.manager.empmanager.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.manager.empmanager.model.dto.AuthorityDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.mapper.EmpMapper;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.mapper.OrgMapper;

public class EmpService {

	/* 사원 상세 정보 조회 메소드 */
	
	public EmpDTO selectEmpDetail(int no) {

		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		EmpDTO empDetail = empMapper.selectEmpDetail(no);
		
		sqlSession.close();

		return empDetail;
	}

	/* 페이징 처리를 위한 전체 사원 수 조회 메소드 */
	
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		int totalCount = empMapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	/* 전체 사원 목록 조회 메소드*/
	
	public List<EmpDTO> selectEmpList(SelectCriteria selectCriteria) {

		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		List<EmpDTO> empList = empMapper.selectEmpList(selectCriteria);
		
		sqlSession.close();
		
		
		return empList;
	}
	
	/* 사원 정보 수정 메소드*/
	
	public int updateEmp(EmpDTO emp, String entYn, Date entDate) {
		
		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		int result = empMapper.updateEmp(emp);
		
		if (entYn.equals("재직") && entDate != null) {		
			result = -1;
		} else if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		} 		
		sqlSession.close();		
		
		return result;
	}

	/* 사원 상세정보 페이지에 권한 selectBox에 담을 권한 목록 조회 메소드*/
	
	public List<AuthorityDTO> selectAuthorityList() {
		
		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		List<AuthorityDTO> autorityList = empMapper.selectAuthorityList();		
		
		sqlSession.close();
		
		return autorityList;
	}

	/* 사원 상세정보 페이지에 부서 selectBox에 담을 부서 목록 조회 메소드*/
	
	public List<OrgDTO> selectDeptList() {

		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		List<OrgDTO> deptList = orgMapper.selectDeptList();
		
		sqlSession.close();
		
		return deptList;
	}

	/* 사원 상세정보 페이지에 직급 selectBox에 담을 직급 목록 조회 메소드*/
	
	public List<JobDTO> selectJobList() {

		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		List<JobDTO> jobList = orgMapper.selectJobList();
		
		sqlSession.close();
		
		return jobList;
	}

	/* 일반사원용 사원목록(퇴사자는 제외) 조회 메소드 */
	
	public List<EmpDTO> nomalSelectEmpList(SelectCriteria selectCriteria) {
		SqlSession sqlSession = getSqlSession();
		
		EmpMapper empMapper = sqlSession.getMapper(EmpMapper.class);
		
		List<EmpDTO> empList = empMapper.nomalSelectEmpList(selectCriteria);
		
		sqlSession.close();
		
		return empList;
	}

}

















