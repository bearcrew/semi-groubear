package com.bearcrew.groubear.manager.empmanager.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.IdCheckDTO;
import com.bearcrew.groubear.manager.empmanager.model.registmapper.RegistMapper;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;

public class InsertEmpService {

	public int insertEmp(EmpDTO registEmp) {
		//사원 등록을 한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		int result = registMapper.insertEmp(registEmp);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public EmpDTO selectDeptCode(EmpDTO registEmp) {
		//부서 코드를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		EmpDTO deptCode = registMapper.selectDeptCode(registEmp);
		
		sqlSession.close();
		
		return deptCode;
	}

	public EmpDTO selectJobCode(EmpDTO registEmp) {
		//직습 코드를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		EmpDTO JobCode = registMapper.selectJobCode(registEmp);
		
		sqlSession.close();
		
		return JobCode;
	}

	public List<IdCheckDTO> selectAllId() {
		//가입된 아이디를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		List<IdCheckDTO> selectAllId = registMapper.selectAllId();
		
		sqlSession.close();
		
		return selectAllId;
	}

	public List<JobDTO> selectAllJobGrade() {
		//모든 직급 목록을 조회한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		
		List<JobDTO> allJobGrade = registMapper.selectAllJobGrade();
		
		sqlSession.close();
		
		return allJobGrade;
	}

	public List<ChildOrgDTO> selectAllDeptTitle() {
		//모든 부서 목록을 조회한다
		SqlSession sqlSession = getSqlSession();
		
		RegistMapper registMapper = sqlSession.getMapper(RegistMapper.class);
		
		List<ChildOrgDTO> allDeptTitle = registMapper.selectAllDeptTitle();
		
		sqlSession.close();
		
		return allDeptTitle;
	}

}
