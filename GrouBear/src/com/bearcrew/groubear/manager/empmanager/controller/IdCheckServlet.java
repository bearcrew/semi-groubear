package com.bearcrew.groubear.manager.empmanager.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.empmanager.model.dto.IdCheckDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.InsertEmpService;

@WebServlet("/manager/idcheck")
public class IdCheckServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* ajax로 회원가입시 입력한 id를 받는다 */
		String registEmpId = request.getParameter("registEmpId");
		
		/* 입력한 id가 없을 경우 동작 */
		if(registEmpId.equals("")) {
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print("아이디를 입력하지 않았습니다.");
			out.flush();
			out.close();
		} else {
			
			/* 입력한 id에 한글이 포함되어 있을 경우 동작 */
			for(int i =0; i < registEmpId.length(); i++) {
				if((registEmpId.charAt(i) < '0' || registEmpId.charAt(i) > '9') && (registEmpId.charAt(i) < 'A' || registEmpId.charAt(i) > 'Z') && (registEmpId.charAt(i) < 'a' || registEmpId.charAt(i) > 'z')) {
					response.setCharacterEncoding("UTF-8");
					PrintWriter out = response.getWriter();
					out.print("영문과 숫자만 입력가능 합니다.");
					out.flush();
					out.close();
				}
			}
			
		}
		
		/* 가입되어 있는 id를 조회해서 List에 담는다 */
		InsertEmpService insertEmpService = new InsertEmpService();
		List<IdCheckDTO> allIdList = insertEmpService.selectAllId();
		
		/* 입력한 id와 조회한 id를 비교하여 중복여부를 판단한다*/
		boolean idCheck = true;
		for(int i =0; i < allIdList.size(); i++) {
			if(registEmpId.equals(allIdList.get(i).toString())) {
				idCheck = false;
			}
		}
		
		/* 판단 결과에 따라 메세지를 전달 한다 */
		String result = "";
		if(idCheck == true) {
			result = "사용가능한 아이디 입니다.";
		} else {
			result = "이미 사용중인 아이디 입니다.";
		}
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(result);
		out.flush();
		out.close();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
