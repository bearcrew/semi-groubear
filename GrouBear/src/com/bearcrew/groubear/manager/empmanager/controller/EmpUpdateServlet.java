package com.bearcrew.groubear.manager.empmanager.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.EmpService;

@WebServlet("/manager/emp/update")
public class EmpUpdateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		int empNo = Integer.parseInt(request.getParameter("empNo"));
		String deptTitle = request.getParameter("deptTitle");
		String entYn = request.getParameter("entYn");
		String empName = request.getParameter("empName");
		java.sql.Date hireDate = java.sql.Date.valueOf(request.getParameter("hireDate"));
		java.sql.Date entDate = null;
		
		/* getParameter로 가져온 값은 STring타입이어서 date타입인 ENT_DATE컬럼을 String값으로 update하지 못한다.
		따라서 사원정보 수정시 입력한 entDate값을 다시 date타입으로 형변환 해준다.(입력한 날짜값이 엾는경우는 " ")  */	
		
		if (request.getParameter("entDate") != null && !"".equals(request.getParameter("entDate"))) {
			entDate = java.sql.Date.valueOf(request.getParameter("entDate"));			
		}
		String empId = request.getParameter("empId");
		String jobGrade = request.getParameter("jobGrade");
		String gender = request.getParameter("gender");
		java.sql.Date birthDate = java.sql.Date.valueOf(request.getParameter("birthDate"));	
		String authorityName = request.getParameter("authority");
		
		EmpDTO emp = new EmpDTO();
		
		emp.setNo(empNo);
		emp.setName(empName);
		emp.setDeptTitle(deptTitle);
		emp.setEntYn(entYn);
		emp.setHireDate(hireDate);
		emp.setEntDate(entDate);
		emp.setId(empId);
		emp.setJobGrade(jobGrade);
		emp.setGender(gender);
		emp.setBirthDate(birthDate);
		emp.setAuthorityName(authorityName);
		
		EmpService empService = new EmpService();
		
		int result = empService.updateEmp(emp, entYn, entDate);
		
		EmpDTO empDetail = empService.selectEmpDetail(empNo);
		
		String path = "";
		
		/* 재직중인 사원의 퇴사일을 입력한 후 정보 수정을 요청한 경우 */
		
		if (result == -1) { 
			path = "/WEB-INF/views/common/failed.jsp"; 
			request.setAttribute("message", "updateEmpInfo1");     /* 먼저 재직상태를 퇴사로 변경후 퇴사일을 입력해야한다는 안내창 출력 */ 
		
		} else if(result > 0 && empDetail != null) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("emp", empDetail);
			request.setAttribute("successCode", "updateEmpInfo");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "updateEmpInfo2");
		}
		request.getRequestDispatcher(path).forward(request, response);		
	
	}

}
