package com.bearcrew.groubear.manager.empmanager.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.empmanager.model.dto.AuthorityDTO;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.EmpService;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;

@WebServlet("/manager/emp/detail")
public class EmpDetailServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int no = Integer.parseInt(request.getParameter("no"));    /* 선택한 사윈의 사원번호를 가져온다. */

		EmpService empService = new EmpService();
		
		EmpDTO empDetail = empService.selectEmpDetail(no);        /* DB에 사원상세정보 조회 요청 */

		String fullAddress = empDetail.getAddress();              /* fullAddress = (우편번호 + 주소 + 상세주소) */

		String addressList[] = fullAddress.split("#");            /* #기준으로 split */

		String addressNo = addressList[0];                        /* 우편번호 */
		String address = addressList[1];                          /* 주소 */
		String detailAddress = addressList[2];                    /* 상세주소 */

		empDetail.setAddressNo(addressNo);
		empDetail.setAddress(address);
		empDetail.setDetailAddress(detailAddress);

		String path = "";
		if (empDetail != null) {
			path = "/WEB-INF/views/manager/empmanager/empDetail.jsp";
			request.setAttribute("emp", empDetail);
			
			List<AuthorityDTO> authorityList = empService.selectAuthorityList();   /*사원상세정보 페이지의 권한selectBox 목록 조회 요청 */			
			if (authorityList != null) {				
				request.setAttribute("authorityList", authorityList);
				
				List<OrgDTO> deptList = empService.selectDeptList();               /* 사원상세정보 페이지의 부서selectBox 목록 조회 요청 */
				if (deptList != null) {
					request.setAttribute("deptList", deptList);
					
					List<JobDTO> jobList = empService.selectJobList();             /* 사원상세정보 페이지의 직위selectBox 목록 조회 요청 */
					if (jobList != null) {
						request.setAttribute("jobList", jobList);
					}
				}

			} else {
				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "상세정보 조회 실패!");
			}	

			request.getRequestDispatcher(path).forward(request, response);

		}

	}

}
