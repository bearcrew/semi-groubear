package com.bearcrew.groubear.manager.empmanager.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.InsertEmpService;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;

@WebServlet("/manager/emp/insert")
public class EmpInsertServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 회원가입시 직위 입력하는 select태그에 넣을 전체 직급명 조회 */
		List<JobDTO> allJobGrade = new InsertEmpService().selectAllJobGrade();
		
		/* 회원가입시 부서 입력하는 select태그에 넣을 전체 부서명 조회 */
		List<ChildOrgDTO> allDeptTitle = new InsertEmpService().selectAllDeptTitle();
		
		/* request영역에 직급명과 부서명 담는다 */
		request.setAttribute("allJobGrade", allJobGrade);
		request.setAttribute("allDeptTitle", allDeptTitle);
		
		/* insertEmp 페이지로 이동 */
		String path = "/WEB-INF/views/manager/empmanager/insertEmp.jsp";
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 회원가입시 입력한  값을 변수에 담는다 */
		String name = request.getParameter("empName");
		String id = request.getParameter("registEmpId");
		String pwd = request.getParameter("pwd");
		String gender = request.getParameter("gender");
		String deptTitle = request.getParameter("deptTitle");
		java.sql.Date hireDate = java.sql.Date.valueOf(request.getParameter("hireDate"));
		String jobGrade = request.getParameter("jobGrade");
		java.sql.Date birthDate = java.sql.Date.valueOf(request.getParameter("birthDate"));
		String homeTel = request.getParameter("homeTel");
		String fax = request.getParameter("fax");
		String address = request.getParameter("zipCode") + "#" + request.getParameter("address1") + "#" + request.getParameter("address2");
		String phone = request.getParameter("phone");
		String emergencyContact = request.getParameter("emergencyContact");
		String email = request.getParameter("email");
		
		/* 변수에 담은 정보를 DTO에 담는다 */
		EmpDTO registEmp = new EmpDTO();
		registEmp.setJobGrade(jobGrade);
		registEmp.setDeptTitle(deptTitle);
		registEmp.setName(name);
		registEmp.setId(id);
		registEmp.setPwd(pwd);
		registEmp.setGender(gender);
		registEmp.setHireDate(hireDate);
		registEmp.setBirthDate(birthDate);
		registEmp.setHomeTel(homeTel);
		registEmp.setFax(fax);
		registEmp.setAddress(address);
		registEmp.setPhone(phone);
		registEmp.setEmergencyContact(emergencyContact);
		registEmp.setEmail(email);

		/* 회원가입시 입력한 부서명으로 부서 코드 가져오기 */
		InsertEmpService insertEmpService = new InsertEmpService();
		EmpDTO deptCode = insertEmpService.selectDeptCode(registEmp);
		String oneDeptCode = deptCode.getDeptCode();
		registEmp.setDeptCode(oneDeptCode);
		
		/* 회원가입시 입력한 직위로 직위코드 가져오기 */
		EmpDTO jobCode = insertEmpService.selectJobCode(registEmp);
		String oneJobCode = jobCode.getJobCode();
		registEmp.setJobCode(oneJobCode);
		
		/* DTO에 있는 정보를 매개변수로 서비스로 보내서 사원 등록하기 */
		int result = insertEmpService.insertEmp(registEmp);
		
		/* 등록 성공 여부에 따라 페이지 이동 */
		String path = "";
		if(result > 0) {
			path ="/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode","insertEmp"); 
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "insertEmp"); 
		}
		request.getRequestDispatcher(path).forward(request, response);

	}

}
