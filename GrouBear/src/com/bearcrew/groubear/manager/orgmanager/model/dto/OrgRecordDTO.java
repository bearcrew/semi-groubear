package com.bearcrew.groubear.manager.orgmanager.model.dto;

public class OrgRecordDTO {

	private int OgnRcdNo;
	private String deptCode;
	private String jobCode;
	private int empNo;
	
	public OrgRecordDTO() {}

	public OrgRecordDTO(int ognRcdNo, String deptCode, String jobCode, int empNo) {
		super();
		OgnRcdNo = ognRcdNo;
		this.deptCode = deptCode;
		this.jobCode = jobCode;
		this.empNo = empNo;
	}

	public int getOgnRcdNo() {
		return OgnRcdNo;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public String getJobCode() {
		return jobCode;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setOgnRcdNo(int ognRcdNo) {
		OgnRcdNo = ognRcdNo;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "OrgRecordDTO [OgnRcdNo=" + OgnRcdNo + ", deptCode=" + deptCode + ", jobCode=" + jobCode + ", empNo="
				+ empNo + "]";
	}
	
	
}
