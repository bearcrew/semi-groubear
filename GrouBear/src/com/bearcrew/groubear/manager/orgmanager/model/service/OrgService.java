package com.bearcrew.groubear.manager.orgmanager.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgEmpDTO;
import com.bearcrew.groubear.manager.orgmanager.model.mapper.OrgMapper;

public class OrgService {

	public List<OrgDTO> selectParentOrg() {
		
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		List<OrgDTO> orgList = orgMapper.selectParentOrg();
		
		sqlSession.close();
		
		return orgList;
	}

	public List<ChildOrgDTO> selectChildOrg(String deptCode) {

		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		List<ChildOrgDTO> childOrgList = orgMapper.selectChildOrg(deptCode);
		
		sqlSession.close();
		
		return childOrgList;
		
		
	}

	public OrgDTO selectOrgInfo(String deptCode) {

		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		OrgDTO orgInfo = orgMapper.selectOrgInfo(deptCode);
		
		sqlSession.close();		
		
		return orgInfo;
	}

	public List<OrgEmpDTO> selectOrgEmpList(String deptCode) {

		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		List<OrgEmpDTO> childOrgList = orgMapper.selectChildEmpOrg(deptCode);
		
		sqlSession.close();
		
		return childOrgList;
	}

	/* 수정하려는 부서의 정보가 담긴 DTO를 orgMapper로 전달 */
	
	public int updateOrgInfo(OrgDTO orgInfo) {
		
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		int result = orgMapper.updateOrgInfo(orgInfo);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();		
			
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteOrg(String orgCode) {
		
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		int result = orgMapper.deleteOrg(orgCode);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	public int insertSubOrg(String refCode) {
		
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		int result = orgMapper.insertSubOrg(refCode);
		
		if (result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();

		return result;
	}

	public OrgDTO selectCompanyInfo() {
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		OrgDTO companyInfo = orgMapper.selectCompany();
		
		sqlSession.close();
		
		return companyInfo;
	}
	
	/* 하위부서를 추가할 부서의코드(상위부서코드)로 부서타입을 조회해온다. */
	public OrgDTO selectDeptType(String refCode) {
		SqlSession sqlSession = getSqlSession();
		
		OrgMapper orgMapper = sqlSession.getMapper(OrgMapper.class);
		
		OrgDTO checkDeptType = orgMapper.selectDeptType(refCode);
		
		sqlSession.close();
		
		return checkDeptType;
	}



	

}
