package com.bearcrew.groubear.manager.orgmanager.model.dto;

public class JobDTO {
	private String jobCode;
	private String jobGrade;
	
	public JobDTO() {}

	public JobDTO(String jobCode, String jobGrade) {
		super();
		this.jobCode = jobCode;
		this.jobGrade = jobGrade;
	}

	public String getJobCode() {
		return jobCode;
	}

	public void setJobCode(String jobCode) {
		this.jobCode = jobCode;
	}

	public String getJobGrade() {
		return jobGrade;
	}

	public void setJobGrade(String jobGrade) {
		this.jobGrade = jobGrade;
	}

	@Override
	public String toString() {
		return "JobDTO [jobCode=" + jobCode + ", jobGrade=" + jobGrade + "]";
	}
	
	
	
}
