package com.bearcrew.groubear.manager.orgmanager.model.dto;

public class OrgEmpDTO {

	private String empNo;
	private String empName;
	private String jobGrade;
	private String deptTitle;
	
	public OrgEmpDTO() {}

	public OrgEmpDTO(String empNo, String empName, String jobGrade, String deptTitle) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.jobGrade = jobGrade;
		this.deptTitle = deptTitle;
	}

	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getJobGrade() {
		return jobGrade;
	}

	public void setJobGrade(String jobGrade) {
		this.jobGrade = jobGrade;
	}

	public String getDeptTitle() {
		return deptTitle;
	}

	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}

	@Override
	public String toString() {
		return "OrgEmpDTO [empNo=" + empNo + ", empName=" + empName + ", jobGrade=" + jobGrade + ", deptTitle="
				+ deptTitle + "]";
	}

	
	
}
