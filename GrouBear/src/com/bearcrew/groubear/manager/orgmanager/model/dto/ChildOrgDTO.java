package com.bearcrew.groubear.manager.orgmanager.model.dto;

import java.util.List;

public class ChildOrgDTO {

	private String deptCode;
	private String deptTitle;
	private String deptContent;
	private String deptType;
	private String refDeptCode;
	private List<OrgEmpDTO> orgEmpList;
	
	public ChildOrgDTO() {}

	public ChildOrgDTO(String deptCode, String deptTitle, String deptContent, String deptType, String refDeptCode,
			List<OrgEmpDTO> orgEmpList) {
		super();
		this.deptCode = deptCode;
		this.deptTitle = deptTitle;
		this.deptContent = deptContent;
		this.deptType = deptType;
		this.refDeptCode = refDeptCode;
		this.orgEmpList = orgEmpList;
	}

	public String getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptTitle() {
		return deptTitle;
	}

	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}

	public String getDeptContent() {
		return deptContent;
	}

	public void setDeptContent(String deptContent) {
		this.deptContent = deptContent;
	}

	public String getDeptType() {
		return deptType;
	}

	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}

	public String getRefDeptCode() {
		return refDeptCode;
	}

	public void setRefDeptCode(String refDeptCode) {
		this.refDeptCode = refDeptCode;
	}

	public List<OrgEmpDTO> getOrgEmpList() {
		return orgEmpList;
	}

	public void setOrgEmpList(List<OrgEmpDTO> orgEmpList) {
		this.orgEmpList = orgEmpList;
	}

	@Override
	public String toString() {
		return "ChildOrgDTO [deptCode=" + deptCode + ", deptTitle=" + deptTitle + ", deptContent=" + deptContent
				+ ", deptType=" + deptType + ", refDeptCode=" + refDeptCode + ", orgEmpList=" + orgEmpList + "]";
	}

	
	
}
