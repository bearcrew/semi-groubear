package com.bearcrew.groubear.manager.orgmanager.model.mapper;

import java.util.List;

import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.JobDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgEmpDTO;

public interface OrgMapper {

	List<OrgDTO> selectParentOrg();

	List<ChildOrgDTO> selectChildOrg(String deptCode);

	OrgDTO selectOrgInfo(String deptCode);

	List<OrgEmpDTO> selectChildEmpOrg(String deptCode);

	List<OrgDTO> selectDeptList();

	int updateOrgInfo(OrgDTO orgInfo);

	int deleteOrg(String orgCode);

	int insertSubOrg(String refCode);

	List<JobDTO> selectJobList();

	OrgDTO selectCompany();

	OrgDTO selectDeptType(String refCode);

}
