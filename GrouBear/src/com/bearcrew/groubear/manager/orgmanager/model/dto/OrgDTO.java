package com.bearcrew.groubear.manager.orgmanager.model.dto;

import java.util.List;

public class OrgDTO {

	private String deptCode;
	private String deptTitle;
	private String deptContent;
	private String deptType;
	private String refDeptCode;
	private List<ChildOrgDTO> childOrgList;
	private List<OrgEmpDTO> orgEmpList;
	public OrgDTO() {}
	
	public OrgDTO(String deptCode, String deptTitle, String deptContent, String deptType, String refDeptCode,
			List<ChildOrgDTO> childOrg) {
		super();
		this.deptCode = deptCode;
		this.deptTitle = deptTitle;
		this.deptContent = deptContent;
		this.deptType = deptType;
		this.refDeptCode = refDeptCode;
		this.childOrgList = childOrg;
	}
	public String getDeptCode() {
		return deptCode;
	}
	public String getDeptTitle() {
		return deptTitle;
	}
	public String getDeptContent() {
		return deptContent;
	}
	public String getDeptType() {
		return deptType;
	}
	public String getRefDeptCode() {
		return refDeptCode;
	}
	public List<ChildOrgDTO> getChildOrgList() {
		return childOrgList;
	}
	public void setDeptCode(String deptCode) {
		this.deptCode = deptCode;
	}
	public void setDeptTitle(String deptTitle) {
		this.deptTitle = deptTitle;
	}
	public void setDeptContent(String deptContent) {
		this.deptContent = deptContent;
	}
	public void setDeptType(String deptType) {
		this.deptType = deptType;
	}
	public void setRefDeptCode(String refDeptCode) {
		this.refDeptCode = refDeptCode;
	}
	public void setChildOrgList(List<ChildOrgDTO> childOrgDTOs) {
		this.childOrgList = childOrgDTOs;
	}
	@Override
	public String toString() {
		return "OrgDTO [deptCode=" + deptCode + ", deptTitle=" + deptTitle + ", deptContent=" + deptContent
				+ ", deptType=" + deptType + ", refDeptCode=" + refDeptCode + ", childOrgDTOs=" + childOrgList + "]";
	}

	
}
