package com.bearcrew.groubear.manager.orgmanager.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/manager/org/update")
public class OrgUpdateServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		/* 부서정보수정 페이지에서 입력한 값들을 가져온다. */
		
		String deptCode = request.getParameter("deptCode");
		String deptTitle = request.getParameter("deptTitle");
		String deptContent = request.getParameter("deptContent");
	
		/* DTO에 넣어줌 */
		
		OrgDTO orgInfo = new OrgDTO();
		orgInfo.setDeptCode(deptCode);
		orgInfo.setDeptTitle(deptTitle);
		orgInfo.setDeptContent(deptContent);		
		
		OrgService orgService = new OrgService();
		int result = orgService.updateOrgInfo(orgInfo);    /* 수정할 부서정보를 넣어둔 DTO를 가지고 service로 이동 */
		
		String path = "";
		if (result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateOrgInfo");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "updateOrgInfo");
		}
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	

}
