package com.bearcrew.groubear.manager.orgmanager.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/manager/org/detail")
public class OrgDetailServlet extends HttpServlet {
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf8");
		String deptCode = request.getParameter("deptCode");	   /* 선택한 부서의 코드를 deptCode변수에 담아준다. */	
		
		OrgService orgService = new OrgService();
		OrgDTO orgInfo = orgService.selectOrgInfo(deptCode);   /* 부서코드를 가지고 부서정보 조회 */
		
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
		
		String jsonString = gson.toJson(orgInfo);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		out.print(jsonString);

		out.close();
	}
		
}
