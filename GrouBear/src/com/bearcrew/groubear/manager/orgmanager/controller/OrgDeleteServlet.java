package com.bearcrew.groubear.manager.orgmanager.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;

@WebServlet("/manager/org/delete")
public class OrgDeleteServlet extends HttpServlet {
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String orgCode = request.getParameter("deptCode");   /* 조직도 관리 페이지에서 선택한 부서코드 */ 
		OrgService orgService = new OrgService();
		int result = orgService.deleteOrg(orgCode);
		
		String path = "";
		if (result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteOrg");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "deleteOrg");
		}
		request.getRequestDispatcher(path).forward(request, response);	}

}
