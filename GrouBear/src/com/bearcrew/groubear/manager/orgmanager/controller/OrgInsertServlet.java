package com.bearcrew.groubear.manager.orgmanager.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;

@WebServlet("/manager/org/insert")
public class OrgInsertServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String path = "";

		String refCode = request.getParameter("deptCode"); 		 /* 하위부서를 추가하려고 선택한 상위부서코드 */
		if (refCode != "") {

			OrgService orgService = new OrgService();
			OrgDTO checkDept = orgService.selectDeptType(refCode);

			String deptType = checkDept.getDeptType(); 			 /* deptType = 부서타입 */ 

			/* 부서타입이 0 or 1 인 부서만 하위부서 추가가능 */

			if (Integer.parseInt(deptType) < 2) { 
				int result = orgService.insertSubOrg(refCode);			

				if (result > 0) {
					path = "/WEB-INF/views/common/success.jsp";
					request.setAttribute("successCode", "insertSubOrg");

					/* DB 요청결과처리 실패시 */

				} else { 
					path = "/WEB-INF/views/common/failed.jsp";
					request.setAttribute("message", "insertSubOrg2");
				}

				/* 최하위부서(하위부서추가불가)를 선택하여 부서추가를 하려는 경우 실패 메세지 */

			} else { 
				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "insertSubOrg3");
			}		

			/* 상위부서를 선택하지 않았을 경우 실패 메세지 */

		} else { 
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "insertSubOrg1");

		}	
		request.getRequestDispatcher(path).forward(request, response);
	} 

}
