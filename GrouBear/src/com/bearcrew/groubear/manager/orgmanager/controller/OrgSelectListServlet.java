package com.bearcrew.groubear.manager.orgmanager.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.ChildOrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgDTO;
import com.bearcrew.groubear.manager.orgmanager.model.dto.OrgEmpDTO;
import com.bearcrew.groubear.manager.orgmanager.model.service.OrgService;

@WebServlet("/manager/org/list")
public class OrgSelectListServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		String authorityCode = loginDTO.getAuthorityCode();

		String path = "";
		if (authorityCode.equals("1")) {

			OrgService orgService = new OrgService();
			
			OrgDTO companyCode = orgService.selectCompanyInfo();  /* 회사코드 조회 */
			
			List<OrgDTO> orgList = orgService.selectParentOrg();  /* 부모부서 리스트(상위부서 목록) 조회*/

			List<OrgEmpDTO> orgEmpList = null;

			/* 부모부서리스트에서 부서들을 하나하나씩 꺼내 꺼낸부서의 부서코드로 자식부서를 조회해서 가져온후 List<자식부서DTO>에 넣어준다. */
			
			for (OrgDTO org : orgList) { 
				List<ChildOrgDTO> childOrgList = orgService.selectChildOrg(org.getDeptCode());
				org.setChildOrgList(childOrgList);
				
				/* 자식부서와 같은 부서이름을 가진 사원 리스트 조회 */
				
				for (ChildOrgDTO chOrg : childOrgList) { 
					orgEmpList = orgService.selectOrgEmpList(chOrg.getDeptCode());
					chOrg.setOrgEmpList(orgEmpList);				
				}
			}
			

			path = "/WEB-INF/views/manager/orgmanager/orgMain.jsp";

			request.setAttribute("company", companyCode);
			request.setAttribute("org", orgList);
			request.setAttribute("orgEmp", orgEmpList);

			request.getRequestDispatcher(path).forward(request, response);

		/* 권한코드가 1(관리자코드)가 아닌경우 */
			
		} else { 
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "isNotAdmin");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}

}
