package com.bearcrew.groubear.board.noticeboard.model.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.common.paging.SelectCriteria;

public interface PostNoticeMapper {

	/* 게시글 목록 전체 조회용 메소드 */
	List<PostNoticeDTO> selectAllNoticeList(SelectCriteria selectCriteria);

	/* 게시글 작성 메소드 */
	int insertNotice(PostNoticeDTO newNotice);

	/* 검색 목록 조회 메소드 */
	int selectTotalCount(Map<String, String> searchMap);

	/* 게시글 상세보기 메소드 */
	PostNoticeDTO selectNoticeDetail(int postNo);

	/* 게시글 조회수 증가 메소드 */
	int incrementNoticeCount(int postNo);

	/* 게시글 수정 메소드 */
	int updateNotice(PostNoticeDTO updateNotice);

	/* 게시글 삭제 메소드 */
	int deleteNotice(PostNoticeDTO deleteNotice);


	
}
