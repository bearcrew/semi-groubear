
package com.bearcrew.groubear.board.noticeboard.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.board.noticeboard.model.mapper.PostNoticeMapper;
import com.bearcrew.groubear.common.paging.SelectCriteria;

public class PostNoticeService {

	/* 전체 조회 메소드 */
	public List<PostNoticeDTO> selectAllNoticeList(SelectCriteria selectCriteria) {

		SqlSession sqlSession = getSqlSession();

		PostNoticeMapper postNoticeMapper = sqlSession.getMapper(PostNoticeMapper.class);
		List<PostNoticeDTO> postNoticeList = postNoticeMapper.selectAllNoticeList(selectCriteria);

		sqlSession.close();

		return postNoticeList;
	}

	/* 게시글 작성 메소드 */
	public int insertNotice(PostNoticeDTO newNotice) {

		SqlSession session = getSqlSession();

		PostNoticeMapper postNoticeMapper = session.getMapper(PostNoticeMapper.class);
		int result = postNoticeMapper.insertNotice(newNotice);

		if (result > 0) {
			session.commit();
		} else {
			session.rollback();
		}

		session.close();

		return result;
	}

	/* 게시글 조회수 메소드 */
	public int selectTotalCount(Map<String, String> searchMap) {

		SqlSession session = getSqlSession();

		PostNoticeMapper postNoticeMapper = session.getMapper(PostNoticeMapper.class);
		int totalCount = postNoticeMapper.selectTotalCount(searchMap);

		session.close();

		return totalCount;
	}

	/* 게시글 상세보기 메소드 */
	public PostNoticeDTO selectNoticeDetail(int postNo) {

		SqlSession session = getSqlSession();

		PostNoticeMapper postNoticeMapper = session.getMapper(PostNoticeMapper.class);
		PostNoticeDTO noticeDetail = null;

		int result = postNoticeMapper.incrementNoticeCount(postNo);

		if (result > 0) {
			noticeDetail = postNoticeMapper.selectNoticeDetail(postNo);

			if (noticeDetail != null) {
				session.commit();
			} else {
				session.rollback();
			}
		} else {
			session.rollback();
		}

		session.close();

		return noticeDetail;
	}

	/* 게시글 수정 메소드 */
	public int updateNotice(PostNoticeDTO updateNotice) {

		SqlSession session = getSqlSession();

		PostNoticeMapper postNoticeMapper = session.getMapper(PostNoticeMapper.class);
		int result = postNoticeMapper.updateNotice(updateNotice);

		if (result > 0) {
			session.commit();
		} else {
			session.rollback();
		}

		session.close();

		return result;

	}

	/* 게시글 삭제 메소드 */
	public int deleteNotice(PostNoticeDTO deleteNotice) {
		SqlSession session = getSqlSession();

		PostNoticeMapper postNoticeMapper = session.getMapper(PostNoticeMapper.class);

		int result = postNoticeMapper.deleteNotice(deleteNotice);

		if (result > 0) {
			session.commit();
		} else {
			session.rollback();
		}

		session.close();

		return result;
	}

}
