package com.bearcrew.groubear.board.noticeboard.model.dto;

import java.sql.Date;

import com.bearcrew.groubear.login.model.dto.LoginDTO;

public class PostNoticeDTO implements java.io.Serializable {
	
	private int postNo;         		 //게시글 번호
	private String boardCode;   		 //게시판 코드
	private String postTitle;   		 //게시글 제목
	private String postContent;  		 //게시글 내용
	private String writer;       		 //작성자명
	private java.sql.Date writeDate;     //작성일자
	private int count;          		 //조회수
	private int empNo;          		 //사원번호
	private String delYn;       		 //삭제여부
	private String boardTitle;   		 //게시판 이름
	
	public PostNoticeDTO() {}

	public PostNoticeDTO(int postNo, String boardCode, String postTitle, String postContent, String writer,
			Date writeDate, int count, int empNo, String delYn, String boardTitle) {
		super();
		this.postNo = postNo;
		this.boardCode = boardCode;
		this.postTitle = postTitle;
		this.postContent = postContent;
		this.writer = writer;
		this.writeDate = writeDate;
		this.count = count;
		this.empNo = empNo;
		this.delYn = delYn;
		this.boardTitle = boardTitle;
	}

	public int getPostNo() {
		return postNo;
	}

	public void setPostNo(int postNo) {
		this.postNo = postNo;
	}

	public String getBoardCode() {
		return boardCode;
	}

	public void setBoardCode(String boardCode) {
		this.boardCode = boardCode;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPostContent() {
		return postContent;
	}

	public void setPostContent(String postContent) {
		this.postContent = postContent;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getDelYn() {
		return delYn;
	}

	public void setDelYn(String delYn) {
		this.delYn = delYn;
	}

	public String getBoardTitle() {
		return boardTitle;
	}

	public void setBoardTitle(String boardTitle) {
		this.boardTitle = boardTitle;
	}

	@Override
	public String toString() {
		return "PostNoticeDTO [postNo=" + postNo + ", boardCode=" + boardCode + ", postTitle=" + postTitle
				+ ", postContent=" + postContent + ", writer=" + writer + ", writeDate=" + writeDate + ", count="
				+ count + ", empNo=" + empNo + ", delYn=" + delYn + ", boardTitle=" + boardTitle + "]";
	}

	

}
