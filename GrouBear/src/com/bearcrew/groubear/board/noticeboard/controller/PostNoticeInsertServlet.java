
package com.bearcrew.groubear.board.noticeboard.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import
javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import
javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import
com.bearcrew.groubear.board.noticeboard.model.service.PostNoticeService;
import com.bearcrew.groubear.login.model.dto.LoginDTO;



@WebServlet("/post/notice/insert") 
public class PostNoticeInsertServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* postNoticeList.jsp에서 쿼리스트링으로 가져온 boardCode를 가져온다 */
		String boardCode = request.getParameter("boardCode");
		
		String path = "/WEB-INF/views/board/noticeboard/postNoticeInsert.jsp";

		request.setAttribute("boardCode", boardCode);
		request.getRequestDispatcher(path).forward(request, response);

	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* session에서 loginMember꺼내서 loginDTO타입으로 담음 */
		HttpSession session = request.getSession(); 
	    LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember"); 
		
	    int empNo = loginDTO.getEmpNo();
		String postTitle = request.getParameter("postTitle"); 
		String writer = request.getParameter("writer"); 
		String postContent = request.getParameter("postContent");
		String boardCode = request.getParameter("boardCode");
		
		/* newNotice에 글작성 시 필요한 값들을 담아준다 */
		PostNoticeDTO newNotice = new PostNoticeDTO();
		newNotice.setPostTitle(postTitle); 
		newNotice.setWriter(writer);
		newNotice.setPostContent(postContent);
		newNotice.setEmpNo(empNo);
		newNotice.setBoardCode(boardCode);
		
		PostNoticeService postNoticeService = new PostNoticeService(); 
		int result = postNoticeService.insertNotice(newNotice);

		String path = ""; 
		if(result > 0) { 
			path = "/WEB-INF/views/common/success.jsp"; 
			request.setAttribute("successCode", "insertNotice"); 
		} else { 
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "insertNotice"); 
		}

		request.getRequestDispatcher(path).forward(request, response);

	}

}
