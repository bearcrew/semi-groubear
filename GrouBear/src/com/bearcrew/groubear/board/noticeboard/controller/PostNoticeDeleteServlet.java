
package com.bearcrew.groubear.board.noticeboard.controller;

import java.io.IOException; import javax.servlet.ServletException; import
javax.servlet.annotation.WebServlet; import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest; import
javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import
com.bearcrew.groubear.board.noticeboard.model.service.PostNoticeService;


@WebServlet("/post/notice/delete") 
public class PostNoticeDeleteServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 삭제기능을 위해 게시글 번호와 게시판 번호를 가져온다 */
		int postNo = Integer.parseInt(request.getParameter("postNo")); 
		String boardCode = request.getParameter("boardCode");

		PostNoticeDTO deleteNotice = new PostNoticeDTO();
		deleteNotice.setPostNo(postNo); 
		deleteNotice.setBoardCode(boardCode);

		PostNoticeService postNoticeService = new PostNoticeService(); 
		int result = postNoticeService.deleteNotice(deleteNotice); 

		String path = ""; 
		if(result > 0) { 
			path = "/WEB-INF/views/common/success.jsp"; 
			request.setAttribute("successCode", "deleteNotice"); 
		} else { 
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "deleteNotice"); }

		request.getRequestDispatcher(path).forward(request, response);

	} 

}
