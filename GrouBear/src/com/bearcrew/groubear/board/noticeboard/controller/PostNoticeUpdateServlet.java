
package com.bearcrew.groubear.board.noticeboard.controller;

import java.io.IOException;

import javax.servlet.ServletException; import
javax.servlet.annotation.WebServlet; import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest; import
javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import
com.bearcrew.groubear.board.noticeboard.model.service.PostNoticeService;


@WebServlet("/post/notice/update") 
public class PostNoticeUpdateServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 게시글 수정을 위해 postNoticeDetail에서 쿼리스트링으로 받은 게시글 번호를 가져온다 */
		int no = Integer.parseInt(request.getParameter("no"));

		PostNoticeService postNoticeService = new PostNoticeService(); 
		PostNoticeDTO postNoticeDetail = postNoticeService.selectNoticeDetail(no);

		String path = ""; 
		if (postNoticeDetail != null) { 
			path = "/WEB-INF/views/board/noticeboard/postNoticeUpdate.jsp";
			request.setAttribute("postNoticeDetail", postNoticeDetail); 
		} else { 
			path = "/WEB-INF/views/common/failed.jsp"; 
			request.setAttribute("message", "공지사항 수정에 실패하셨습니다.."); }

		request.getRequestDispatcher(path).forward(request, response); 
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 게시글 수정을 위해 필요한 값 */
		String postTitle = request.getParameter("postTitle"); 
		String postContent = request.getParameter("postContent"); 
		int postNo = Integer.parseInt(request.getParameter("postNo")); 
		String boardCode = request.getParameter("boardCode");

		/* PostNoticeDTO에 값들을 담아준다 */
		PostNoticeDTO updateNotice = new PostNoticeDTO();
		updateNotice.setPostTitle(postTitle);
		updateNotice.setPostContent(postContent); 
		updateNotice.setPostNo(postNo);
		updateNotice.setBoardCode(boardCode); 

		PostNoticeService postNoticeService = new PostNoticeService(); 
		int result = postNoticeService.updateNotice(updateNotice); 

		String path = ""; 
		if(result > 0) { 
			path = "/WEB-INF/views/common/success.jsp"; 
			request.setAttribute("successCode", "updateNotice"); 
		} else { 
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "updateNotice"); 
		}

		request.getRequestDispatcher(path).forward(request, response);

	}

}


