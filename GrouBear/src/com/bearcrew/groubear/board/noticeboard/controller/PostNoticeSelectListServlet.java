package com.bearcrew.groubear.board.noticeboard.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.board.noticeboard.model.service.PostNoticeService;
import com.bearcrew.groubear.common.paging.Pagenation;
import com.bearcrew.groubear.common.paging.SelectCriteria;


@WebServlet("/post/notice/list")
public class PostNoticeSelectListServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String boardCode = request.getParameter("boardCode");
		String boardTitle = request.getParameter("boardTitle");
		/* 목록보기를 눌렀을 시 가장 처음에 보여지는 페이지는 1페이지이다.
		 * 파라미터로 전달되는 페이지가 있는 경우 currentPage는 파라미터로 전달받은 페이지 수 이다.
		 * */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/* 0보다 작은 숫자값을 입력해도 1페이지를 보여준다 */
		if(pageNo <= 0) {
			pageNo = 1;
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		// 검색 옵션, 키워드 맵에 저장
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		/* 전체 게시물 수가 필요하다.
		 * 데이터베이스에서 먼저 전체 게시물 수를 조회해올 것이다.
		 * 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회한다.
		 * */
		PostNoticeService postNoticeService = new PostNoticeService();
		int totalCount = postNoticeService.selectTotalCount(searchMap);
		
		
		/* 한 페이지에 보여 줄 게시물 수 */
		int limit = 10;		//얘도 파라미터로 전달받아도 된다.
		/* 한 번에 보여질 페이징 버튼의 갯수 */
		int buttonAmount = 10;
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환받는다. */
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue, boardCode);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, boardCode);
		}
		
		/* 여기서 부터 조회 */
		/* 필요한 값들을 List로 담아준다 */
		List<PostNoticeDTO> postNoticeList = postNoticeService.selectAllNoticeList(selectCriteria);

		HttpSession session = request.getSession();	
		
		String path = "";
		if(postNoticeList != null) {
			path = "/WEB-INF/views/board/noticeboard/postNoticeList.jsp";
			session.setAttribute("postNoticeList", postNoticeList);
			request.setAttribute("selectCriteria", selectCriteria);
			session.setAttribute("boardTitle", boardTitle);
			session.setAttribute("boardCode", boardCode);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "게시물 목록 조회 실패!");
		}

		request.getRequestDispatcher(path).forward(request, response);
	}

}
