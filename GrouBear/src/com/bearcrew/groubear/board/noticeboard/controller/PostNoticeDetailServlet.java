
package com.bearcrew.groubear.board.noticeboard.controller;

import java.io.IOException;

import javax.servlet.ServletException; import
javax.servlet.annotation.WebServlet; import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest; import
javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import
com.bearcrew.groubear.board.noticeboard.model.service.PostNoticeService;

@WebServlet("/post/notice/detail") 
public class PostNoticeDetailServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 게시글 상세보기를 위해 게시글 번호를 가져온다 */
		int postNo = Integer.parseInt(request.getParameter("postNo"));

		PostNoticeService postNoticeService = new PostNoticeService(); 
		PostNoticeDTO postNoticeDetail = postNoticeService.selectNoticeDetail(postNo);

		String path = "";
		if (postNoticeDetail != null) { 
			path = "/WEB-INF/views/board/noticeboard/postNoticeDetail.jsp";
			request.setAttribute("postNoticeDetail", postNoticeDetail); 
		} else { 
			path = "/WEB-INF/views/common/failed.jsp"; 
			request.setAttribute("message", "postNoticeDetail"); }

		request.getRequestDispatcher(path).forward(request, response); 

	}

}



