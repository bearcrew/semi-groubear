package com.bearcrew.groubear.login.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.mapper.LoginMapper;
import com.bearcrew.groubear.mypage.model.mapper.MypageMapper;

public class SearchIdService {

	public String emailCheck(LoginDTO loginDTO) {

		SqlSession sqlSession = getSqlSession();										
		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
		
		String resultSearchId = loginMapper.searchIdInfo(loginDTO);
		sqlSession.close();
		
		return resultSearchId;
	}

}
