package com.bearcrew.groubear.login.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.mapper.LoginMapper;

public class SearchPwdService {

	public String emailCheck(LoginDTO loginDTO) {

		SqlSession sqlSession = getSqlSession();
		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
				
		String resultSearchPwd = loginMapper.searchPwdInfo(loginDTO);
		sqlSession.close();
		
		return resultSearchPwd;
		
	}

	public LoginDTO userCheck(LoginDTO loginDTO) {
		
		SqlSession sqlSession = getSqlSession();
		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);
				
		LoginDTO resultSearchinfo = loginMapper.searchUserInfo(loginDTO);
		sqlSession.close();
		
		return resultSearchinfo;
	}

}
