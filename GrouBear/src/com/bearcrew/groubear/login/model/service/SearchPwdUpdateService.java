package com.bearcrew.groubear.login.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.login.model.mapper.LoginMapper;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;

public class SearchPwdUpdateService {

	public int PwdUpdate(PwdUpdateDTO pwdDTO) {

		SqlSession sqlSession = getSqlSession();
		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class);

		int resultPwdUpdate = loginMapper.UpdatePwd(pwdDTO);									

		if (resultPwdUpdate > 0) {

			sqlSession.commit();

		} else {
			sqlSession.rollback();
		}

		sqlSession.close();

		return resultPwdUpdate;
	}

}
