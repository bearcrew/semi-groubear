package com.bearcrew.groubear.login.model.service;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import com.bearcrew.groubear.approval.model.dto.ApprovalLoginDTO;
import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.mapper.LoginMapper;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.project.model.dto.LoginProjectDTO;

public class LoginService {

	
	public LoginDTO loginCheck(LoginDTO requestLogin) {																

		/* 로그인 체크 */
		SqlSession sqlSession = getSqlSession();																	/* requestLogin 입력한 아이디 비밀번호*/
		LoginDTO loginMember = null;
		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 											/* DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/
		String loginMemberPWD = loginMapper.selectIDPWD(requestLogin);

		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();										/* 암호화 해서 비밀번호 비교*/
		if (passwordEncoder.matches(requestLogin.getEmpPwd(), loginMemberPWD)) {					 				/* 비밀번호 입력한값 , DB에 저장되있는 비밀번호 값 비교한다*/

			loginMember = loginMapper.selectUser(requestLogin); 													/* 비밀번호 비교후 맞을 경우 유저 정보를 가져온다*/

		}

		sqlSession.close();

		return loginMember;
	}

	/* 사이드바 목록들 */
	public List<BoardDTO> boardSelect() { 																			/* 사이드 바 게시판 목록들 조회*/

		SqlSession sqlSession = getSqlSession();

		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 											/* DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/
		List<BoardDTO> boardListSelect = loginMapper.selectBoard(); 												/* 로그인후 DB에 저장된 게시판 목록들을 조회한다*/

		sqlSession.close();

		return boardListSelect;
	}

	/* 프로젝트 정보 */
	public List<LoginProjectDTO> loginProjectSelect(LoginProjectDTO loginProjectDTO) {

		SqlSession sqlSession = getSqlSession();

		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 											/* DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/

		List<LoginProjectDTO> resultLoginProject = loginMapper.loginProjectSelect(loginProjectDTO); 				/* 로그인후 프로젝트 정보와 목록을 조회한다*/
																													

		sqlSession.close();

		return resultLoginProject;

	}

	/* 게시판 목록과 안에 정보들 */
	public List<PostNoticeDTO> postSelect(PostNoticeDTO postNoticeDTO) {

		SqlSession sqlSession = getSqlSession();

		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 											/* DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/
		List<PostNoticeDTO> resultPost = loginMapper.postSelect(postNoticeDTO);									    /* 로그인후 게시판 목록과 정보를 조회한다*/

		sqlSession.close();

		return resultPost;

	}

	/* 게시판 목록과 안에 정보들 */
	public List<PostNoticeDTO> postNoticeSelect(PostNoticeDTO postNoticeDTO) {

		SqlSession sqlSession = getSqlSession();

		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 										/* DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/
		List<PostNoticeDTO> resultPostNotice = loginMapper.postNoticeSelect(postNoticeDTO); 					/* 로그인후 게시판 목록과 정보를 조회한다*/

		sqlSession.close();

		return resultPostNotice;

	}

	/* 전자결재 정보 */
	public List<ApprovalLoginDTO> approvalSelect(ApprovalLoginDTO approvalLoginDTO) {

		SqlSession sqlSession = getSqlSession();

		LoginMapper loginMapper = sqlSession.getMapper(LoginMapper.class); 										// DB에서 매퍼로 불러오고 매퍼에 있는걸 불러온다.*/
		List<ApprovalLoginDTO> resultApproval = loginMapper.approvalSelect(approvalLoginDTO); 					// 로그인후 전자결재 정보와 목록을 조회한다*/

		sqlSession.close();

		return resultApproval;

	}

}
