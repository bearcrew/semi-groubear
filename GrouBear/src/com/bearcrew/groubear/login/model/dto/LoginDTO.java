package com.bearcrew.groubear.login.model.dto;

import java.sql.Date;

public class LoginDTO implements java.io.Serializable {
	
			private int empNo;							//사원번호
			private String empName;						//사원명
			private String empId;						//아이디	
			private String empPwd;						//비밀번호
			private char gender;						//성별
			private java.sql.Date birthDate;			//생년월일
			private java.sql.Date hirtDate;				//입사일
			private java.sql.Date entDate;				//퇴사일 
			private String homeTel;						//집전화번호
			private String phone;						//휴대전화번호
			private String fax;							//팩스번호
			private String emergencyContact;			//긴급연락처
			private String address;						//집주소
			private String detailAddress;				//집 상세주소
			private String zipCode;						//집 우편번호
			private String email;						//이메일
			private String entYn;						//퇴사여부
			private char approvalAlarm;					//전자결재 알림
			private char projectAlarm;					//프로젝트 알림
			private String deptCode;					//부서코드
			private String jobCode;						//직급코드
			
			private String jobGrade; 				    //직급명 조인해야함  JOB테이블 JOB_GRADE 우선순위
			private String fileName; 					//파일명 조인해야함 PROFILE테이블  FILE_NAME 우선순위
			private String signFileName; 				//파일명 조인해야함 SIGN_ATTACHMENT테이블  FILE_NAME 우선순위
			private String authorityCode;    
			private String authorityName;
			private String deptTitle;
			
			//EMPLOYEE 랑 JOB은 JOB_CODE 으로조인하면되고
			//EMPLOYEE 랑 PROFILE은 EMP_NO 으로 조인하고
			//EMPLOYEE 랑 
			public LoginDTO () {}

			public LoginDTO(int empNo, String empName, String empId, String empPwd, char gender, Date birthDate,
					Date hirtDate, Date entDate, String homeTel, String phone, String fax, String emergencyContact,
					String address, String detailAddress, String zipCode, String email, String entYn,
					char approvalAlarm, char projectAlarm, String deptCode, String jobCode, String jobGrade,
					String fileName, String signFileName, String authorityCode, String authorityName,
					String deptTitle) {
				super();
				this.empNo = empNo;
				this.empName = empName;
				this.empId = empId;
				this.empPwd = empPwd;
				this.gender = gender;
				this.birthDate = birthDate;
				this.hirtDate = hirtDate;
				this.entDate = entDate;
				this.homeTel = homeTel;
				this.phone = phone;
				this.fax = fax;
				this.emergencyContact = emergencyContact;
				this.address = address;
				this.detailAddress = detailAddress;
				this.zipCode = zipCode;
				this.email = email;
				this.entYn = entYn;
				this.approvalAlarm = approvalAlarm;
				this.projectAlarm = projectAlarm;
				this.deptCode = deptCode;
				this.jobCode = jobCode;
				this.jobGrade = jobGrade;
				this.fileName = fileName;
				this.signFileName = signFileName;
				this.authorityCode = authorityCode;
				this.authorityName = authorityName;
				this.deptTitle = deptTitle;
			}

			public int getEmpNo() {
				return empNo;
			}

			public void setEmpNo(int empNo) {
				this.empNo = empNo;
			}

			public String getEmpName() {
				return empName;
			}

			public void setEmpName(String empName) {
				this.empName = empName;
			}

			public String getEmpId() {
				return empId;
			}

			public void setEmpId(String empId) {
				this.empId = empId;
			}

			public String getEmpPwd() {
				return empPwd;
			}

			public void setEmpPwd(String empPwd) {
				this.empPwd = empPwd;
			}

			public char getGender() {
				return gender;
			}

			public void setGender(char gender) {
				this.gender = gender;
			}

			public java.sql.Date getBirthDate() {
				return birthDate;
			}

			public void setBirthDate(java.sql.Date birthDate) {
				this.birthDate = birthDate;
			}

			public java.sql.Date getHirtDate() {
				return hirtDate;
			}

			public void setHirtDate(java.sql.Date hirtDate) {
				this.hirtDate = hirtDate;
			}

			public java.sql.Date getEntDate() {
				return entDate;
			}

			public void setEntDate(java.sql.Date entDate) {
				this.entDate = entDate;
			}

			public String getHomeTel() {
				return homeTel;
			}

			public void setHomeTel(String homeTel) {
				this.homeTel = homeTel;
			}

			public String getPhone() {
				return phone;
			}

			public void setPhone(String phone) {
				this.phone = phone;
			}

			public String getFax() {
				return fax;
			}

			public void setFax(String fax) {
				this.fax = fax;
			}

			public String getEmergencyContact() {
				return emergencyContact;
			}

			public void setEmergencyContact(String emergencyContact) {
				this.emergencyContact = emergencyContact;
			}

			public String getAddress() {
				return address;
			}

			public void setAddress(String address) {
				this.address = address;
			}

			public String getDetailAddress() {
				return detailAddress;
			}

			public void setDetailAddress(String detailAddress) {
				this.detailAddress = detailAddress;
			}

			public String getZipCode() {
				return zipCode;
			}

			public void setZipCode(String zipCode) {
				this.zipCode = zipCode;
			}

			public String getEmail() {
				return email;
			}

			public void setEmail(String email) {
				this.email = email;
			}

			public String getEntYn() {
				return entYn;
			}

			public void setEntYn(String entYn) {
				this.entYn = entYn;
			}

			public char getApprovalAlarm() {
				return approvalAlarm;
			}

			public void setApprovalAlarm(char approvalAlarm) {
				this.approvalAlarm = approvalAlarm;
			}

			public char getProjectAlarm() {
				return projectAlarm;
			}

			public void setProjectAlarm(char projectAlarm) {
				this.projectAlarm = projectAlarm;
			}

			public String getDeptCode() {
				return deptCode;
			}

			public void setDeptCode(String deptCode) {
				this.deptCode = deptCode;
			}

			public String getJobCode() {
				return jobCode;
			}

			public void setJobCode(String jobCode) {
				this.jobCode = jobCode;
			}

			public String getJobGrade() {
				return jobGrade;
			}

			public void setJobGrade(String jobGrade) {
				this.jobGrade = jobGrade;
			}

			public String getFileName() {
				return fileName;
			}

			public void setFileName(String fileName) {
				this.fileName = fileName;
			}

			public String getSignFileName() {
				return signFileName;
			}

			public void setSignFileName(String signFileName) {
				this.signFileName = signFileName;
			}

			public String getAuthorityCode() {
				return authorityCode;
			}

			public void setAuthorityCode(String authorityCode) {
				this.authorityCode = authorityCode;
			}

			public String getAuthorityName() {
				return authorityName;
			}

			public void setAuthorityName(String authorityName) {
				this.authorityName = authorityName;
			}

			public String getDeptTitle() {
				return deptTitle;
			}

			public void setDeptTitle(String deptTitle) {
				this.deptTitle = deptTitle;
			}

			@Override
			public String toString() {
				return "LoginDTO [empNo=" + empNo + ", empName=" + empName + ", empId=" + empId + ", empPwd=" + empPwd
						+ ", gender=" + gender + ", birthDate=" + birthDate + ", hirtDate=" + hirtDate + ", entDate="
						+ entDate + ", homeTel=" + homeTel + ", phone=" + phone + ", fax=" + fax + ", emergencyContact="
						+ emergencyContact + ", address=" + address + ", detailAddress=" + detailAddress + ", zipCode="
						+ zipCode + ", email=" + email + ", entYn=" + entYn + ", approvalAlarm=" + approvalAlarm
						+ ", projectAlarm=" + projectAlarm + ", deptCode=" + deptCode + ", jobCode=" + jobCode
						+ ", jobGrade=" + jobGrade + ", fileName=" + fileName + ", signFileName=" + signFileName
						+ ", authorityCode=" + authorityCode + ", authorityName=" + authorityName + ", deptTitle="
						+ deptTitle + "]";
			}

		
			
}