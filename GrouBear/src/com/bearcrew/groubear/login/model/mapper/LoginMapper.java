package com.bearcrew.groubear.login.model.mapper;

import java.util.List;

import com.bearcrew.groubear.approval.model.dto.ApprovalLoginDTO;
import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;
import com.bearcrew.groubear.project.model.dto.LoginProjectDTO;

public interface LoginMapper {
	
	LoginDTO selectUser(LoginDTO requestLogin);										/*유저 정보 조회*/

	String searchIdInfo(LoginDTO loginDTO);											/*아이디 찾기 */

	String selectIDPWD(LoginDTO requestLogin);										/*입력한 값을 받아 아이디 비밀번호 값을 가져온다 가져온후 DB에 저장된 값이랑 입력한값이랑 비교한다*/

	String searchPwdInfo(LoginDTO loginDTO);										/*비밀번호를 찾는다*/

	int UpdatePwd(PwdUpdateDTO pwdDTO);												/*비밀번호 찾기후 비밀번호 변경*/

	List<BoardDTO> selectBoard();													/*게시판 목록 조회*/
	
	List<PostNoticeDTO> postSelect(PostNoticeDTO postNoticeDTO);					/*메인페이지에 필요한 최신 게시판 목록 4개만 가져온다*/
	
	List<PostNoticeDTO> postNoticeSelect(PostNoticeDTO postNoticeDTO);				/*메인페이지에 필요한 공지사항 정보 4개만 가져온다*/

	List<ApprovalLoginDTO> approvalSelect(ApprovalLoginDTO approvalLoginDTO);		/*메인페이지에 필요한 결재 정보를 가져온다.*/

	List<LoginProjectDTO> loginProjectSelect(LoginProjectDTO loginProjectDTO);		/*메인페이지에 필요한 프로젝트 정보를 가져온다.*/

	LoginDTO searchUserInfo(LoginDTO loginDTO); 									/*비밀번호 찾기 할때 유저 정보를 가져와서 찾는다*/


}
