package com.bearcrew.groubear.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.service.SearchIdService;

@WebServlet("/login/searchId")
public class SearchIdServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = "/WEB-INF/views/login/searchId.jsp";

		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		String AuthenticationKe = (String) session.getAttribute("AuthenticationKey"); 								/* 보낸 인증메일 번호를 세션에 담은걸 불러온다*/

		String userName = request.getParameter("formUserName"); 													/*입력한 유저 이름*/ 
		String userEmail = request.getParameter("formUserEmail"); 													/*입력한 유저 이메일*/
		String userPhone = request.getParameter("formUserPhone");													/*입력한 유저 휴대폰번호*/
		String emailCheckNumber = request.getParameter("formUserEmailCheck"); 										/*입력한 인증 메일 번호*/

		String userPhoneNum = userPhone.toString().replace("-", ""); 												/*휴대폰 번호를 '-' 를 사용해서 넣을수도있어서 replace를 사용하여 빈 문자열로 바꾸어준다*/ 

		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		String err = "";
		if (userName == null || userName.equals(err)) {																/*유저 네임에 값이 null 이나 빈 문자열이면 에러메세지 출력*/

			out.print("잘못되었습니다 다시 작성해주세요.");

			out.flush();
			out.close();

		} else if (userEmail == null || userEmail.equals(err)) {													/*유저 이메일 값이 null 이나 빈 문자열이면 에러메세지 출력*/

			out.print("잘못되었습니다 다시 작성해주세요.");

			out.flush();
			out.close();

		} else if (userPhoneNum == null || userPhoneNum.equals(err)) {												/*유저 폰번호 값이 null 이나 빈 문자열이면 에러메세지 출력*/

			out.print("잘못되었습니다 다시 작성해주세요.");

			out.flush();
			out.close();

		}

		if (AuthenticationKe.equals(emailCheckNumber)) { 															/* 내가 만든 인증번호와 사용자 이메일로 넘어간 인증번호가 맞는지 비교*/

			LoginDTO loginDTO = new LoginDTO();
			loginDTO.setEmpName(userName);
			loginDTO.setEmail(userEmail);
			loginDTO.setPhone(userPhoneNum);

			SearchIdService searchIdService = new SearchIdService();												/*서비스를 메소드를 호출한다*/
			String resultId = searchIdService.emailCheck(loginDTO);													/*서비스로 넘어가 이메일 체크를 한다*/

			if (resultId.length() > 0) {																			/*리턴 값이 있을경우 찾으시는 아이디를 메세지로 출력해준다*/

				out.print("찾으실 ID는 : " + resultId + " 입니다.");

				out.flush();
				out.close();

			} else {

				out.print("정보가 일치하지 않습니다.");																		/*리턴 값이 없을경우 에러 메세지를 출력해준다*/

				out.flush();
				out.close();

			}

		} else {																									/*이메일 인증에 틀렸을 경우 에러 메세지를 출력해준다*/

			out.print("이메일 인증번호가 틀렸습니다.");

			out.flush();
			out.close();

		}

	}

}
