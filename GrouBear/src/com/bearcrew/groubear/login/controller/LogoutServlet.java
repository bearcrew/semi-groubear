package com.bearcrew.groubear.login.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = request.getContextPath();								
		request.getSession().invalidate();									/*invalidate메소드를 사용하여 세션을 종료한다.*/
		response.sendRedirect(request.getContextPath());					/*로그아웃 후 로그인 화면으로 넘어간다*/
	}
	
}
