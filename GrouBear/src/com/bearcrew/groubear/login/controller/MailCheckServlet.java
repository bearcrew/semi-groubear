package com.bearcrew.groubear.login.controller;

import java.util.Random;

import java.util.Properties;

import java.io.IOException;
import java.io.PrintWriter;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/mail")
public class MailCheckServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession saveKey = request.getSession();
	
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		
		String memberEamil = request.getParameter("formEmail");							/*인증 번호 받을 이메일*/
		
		String host = "smtp.naver.com";   												/* mail server 설정*/
		String user = ""; 																/* 자신의 네이버 계정*/
		String password = "";															/* 자신의 네이버 패스워드*/
		
		String to_email = memberEamil;													/* 메일 받을 주소*/

		Properties props = new Properties();											/* SMTP 서버 정보를 설정한다.*/
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", 465);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.debug", "true");

		StringBuffer temp = new StringBuffer();											/* 인증 번호 생성기 랜덤 메소드*/
		Random rnd = new Random();
		for (int i = 0; i < 10; i++) {
			int rIndex = rnd.nextInt(3);
			switch (rIndex) {
			case 0:
				// a-z
				temp.append((char) ((int) (rnd.nextInt(26)) + 97));
				break;
			case 1:
				// A-Z
				temp.append((char) ((int) (rnd.nextInt(26)) + 65));
				break;
			case 2:
				// 0-9
				temp.append((rnd.nextInt(10)));
				break;
			}
		}
		String AuthenticationKey = temp.toString();   									/*랜덤 메소드 돌려서 나온 인증키를 보낸다*/

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});

		
		try {																			/* email 전송*/
			MimeMessage msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(user, "GrouBear"));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to_email));

			
			msg.setSubject("GrouBear 이메일 인증번호");										/* 메일 제목*/	
			
			msg.setText("===================================================\n"			/* 메일 내용*/
						+ "                  회원님 안녕하세요. \n"
						+ "   GrouBear 회원 이메일 인증번호를 발송해드립니다. \n"
						+ "             [ 인증번호 : " + temp + " ] \n"
						+ "===================================================\n"
						+ "                   - Grou Bear -");

			Transport.send(msg);

		} catch (Exception e) {								
			e.printStackTrace();
		}
		
			saveKey.setAttribute("AuthenticationKey", AuthenticationKey);								/*Session에 이메일 인증 보낸 값을 넣어준다*/
			
			out.print("이메일 인증번호를 발송했습니다.");
			out.flush();
			out.close();

	}

}