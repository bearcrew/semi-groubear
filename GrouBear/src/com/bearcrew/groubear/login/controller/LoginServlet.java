package com.bearcrew.groubear.login.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.approval.model.dto.ApprovalLoginDTO;
import com.bearcrew.groubear.board.noticeboard.model.dto.PostNoticeDTO;
import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.service.LoginService;
import com.bearcrew.groubear.manager.boardmanager.model.dto.BoardDTO;
import com.bearcrew.groubear.project.model.dto.LoginProjectDTO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	List<BoardDTO> boardListSelect = null;


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userId = request.getParameter("userId"); 													/* request 영역에 있는 걸 꺼내온다*/
		String userPwd = request.getParameter("userPwd"); 													/* request 영역에 있는 걸 꺼내온다*/

		LoginDTO requestLogin = new LoginDTO(); 															/* 로그인 DTO를 만든후 값을 넣어준다*/
		requestLogin.setEmpId(userId);																		/* 로그인 할떄 작성한 아이디 값*/
		requestLogin.setEmpPwd(userPwd);																	/* 로그인 할때 작성한 패스워드 값*/

		LoginService loginService = new LoginService(); 													/* 서비스 생성한다*/

		LoginDTO loginMember = loginService.loginCheck(requestLogin); 										/* 서비스 메소드 만든후 로그인DTO변수를 넣어준다*/
		String path ="";
		//
		if (loginMember != null) {																			/*로그인이 될경우 게시판 목록을 가져온다.*/

			boardListSelect = loginService.boardSelect();
			
		} else {                              																/*로그인 정보를 불러오지 못할 경우 로그인 실패를 하게 되어 실패 메세지를 보낸다*/
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "FailedLogin");
			request.getRequestDispatcher(path).forward(request, response);
			
																											
		}

			if (loginMember != null && boardListSelect != null) {

				String empName = loginMember.getEmpName(); 													/* 로그인된 사원에 이름값을 불러온다*/
				int empNo = loginMember.getEmpNo();															 /* 로그인된 사원에 번호를 가져온다*/
				
				/* 프로젝트 번호 */
				LoginProjectDTO loginProjectDTO = new LoginProjectDTO();
				loginProjectDTO.setEmpNo(empNo);
				List<LoginProjectDTO> resultLoginProject = loginService.loginProjectSelect(loginProjectDTO);

				/* 게시판 정보 */
				PostNoticeDTO postNoticeDTO = new PostNoticeDTO(); 
				List<PostNoticeDTO> post = loginService.postSelect(postNoticeDTO); 							/*게시판 최신목록 4개만*/
				List<PostNoticeDTO> Noticepost = loginService.postNoticeSelect(postNoticeDTO); 				/*공지사항 게시판 최신 목록 4개만*/

				/* 결재 정보 */
				ApprovalLoginDTO approvalLoginDTO = new ApprovalLoginDTO();
				approvalLoginDTO.setEmpNo(empNo);
				List<ApprovalLoginDTO> resultApproval = loginService.approvalSelect(approvalLoginDTO);
				
				String fullAddress = loginMember.getAddress(); 												/* 풀 주소*/
				
				String addressList[] = fullAddress.split("#"); 												/* 주소를 짤라서 리스트로 담는다 스필릿으로 짜를시 '#'이 나올때마다 인덱스로 값이 들어간다*/

				/* 주소 짜르기  */
				for (int i = 0; i < addressList.length; i++) {
					System.out.println(addressList[i]);
				
				}
				String zipCode = addressList[0]; 															/* 2번째 인덱스 값 (집 상세주소)*/
				String address = addressList[1]; 															/* 1번째 인덱스 값 (집주소)*/
				String detailAddress = addressList[2]; 														/* 2번째 인덱스 값 (집 상세주소)*/

				loginMember.setZipCode(zipCode);
				loginMember.setAddress(address);
				loginMember.setDetailAddress(detailAddress);
				
				HttpSession session = request.getSession(); 												/* 세션을 생성한다*/
				session.setAttribute("loginMember", loginMember); 											/* 로그인 정보를 세션에 담는다*/
				session.setAttribute("boardListSelect", boardListSelect); 									/* 사이드바 목록을 세션에 담는다*/
				session.setAttribute("resultLoginProject", resultLoginProject); 							/* 목표일자가 다가오는 프로젝트 정보 1개만 담는다*/
				session.setAttribute("post", post); 														/* 게시판 최신목록 4개만 담는다*/
				session.setAttribute("Noticepost", Noticepost); 											/* 공지사항 게시판 최신 목록 4개만 담는다*/
				session.setAttribute("resultApproval", resultApproval); 									/* 결재정보를 4개만 담는다*/
				
			if (loginMember != null) {																		/* 로그인 정보 있을경우 로그인 완료 메세지를 출력한다 */
				
				path = "/WEB-INF/views/common/success.jsp";
				request.setAttribute("successCode", "successLogin");
				request.getRequestDispatcher(path).forward(request, response);
			
			}	
				
			}

	}

}
