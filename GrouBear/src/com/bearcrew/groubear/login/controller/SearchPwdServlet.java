package com.bearcrew.groubear.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.login.model.service.SearchPwdService;

@WebServlet("/login/searchPwd")
public class SearchPwdServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = "/WEB-INF/views/login/searchPwd.jsp";

		request.getRequestDispatcher(path).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String path = "";
		HttpSession session = request.getSession();

		String AuthenticationKe = (String) session.getAttribute("AuthenticationKey");					/*이메일 인증 코드*/

		String userId = request.getParameter("formUserId");												/*비밀번호 찾기 폼에서 입력 받은 아이디 값*/
		String userName = request.getParameter("formUserName");											/*비밀번호 찾기 폼에서 입력 받은 이름 값*/
		String userPhone = request.getParameter("formUserPhone");										/*비밀번호 찾기 폼에서 입력 받은 폰 번호 값*/
		String emailCheckNumber = request.getParameter("formUserEmail");								/*비밀번호 찾기 폼에서 입력 받은 이메일 값*/

		String userPhoneNum = userPhone.toString().replace("-", "");									/*폰 번호를 입력받을시 '-' 문자를 공백으로 바꾸어준다*/

		LoginDTO loginDTO = new LoginDTO();				

		loginDTO.setEmpId(userId);																		/*유저 아이디를 담는다*/
		loginDTO.setEmpName(userName);																	/*유저 이름을 담는다.*/
		loginDTO.setPhone(userPhoneNum);																/*유저 폰번호를 담는다*/

		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();																

		SearchPwdService searchPwdService = new SearchPwdService();
		LoginDTO userCheck = searchPwdService.userCheck(loginDTO);										/*입력 받은 값이 DB에 저장되있는지 먼저 체크를 한다.*/

		if (userCheck.getEmpId() != userId && userCheck.getEmpName() != userName && userCheck.getPhone() != userPhone) {		/*체크후 값이 틀리면 정보를 다시 입력하라는 메세지를 출력한다.*/

			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "EmailAuthentication");
			request.getRequestDispatcher(path).forward(request, response);

		}

		if (AuthenticationKe != null || !AuthenticationKe.equals("")) {							/* 발송한 이메일 인증코드와 비교한다.*/

					if (AuthenticationKe.equals(emailCheckNumber)) {							 /* 인증 요청번호 if 문*/


				String searchId = searchPwdService.emailCheck(loginDTO);						

				if (searchId.length() > 0) {

					response.setCharacterEncoding("UTF-8");

					out.print("확인 되셨습니다 비밀번호를 변경 해주세요");									/*정보 비교후 정보값이 맞을시 확인 되셨습니다 메세지 출력후 비밀번호 변경으로 이동한다.*/

					out.flush();
					out.close();
				}

			}

		} else {																				/* 실패할 경우 에러메세지를 출력한다*/
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "EmailAuthentication");
			request.getRequestDispatcher(path).forward(request, response);
		}
	}

}
