package com.bearcrew.groubear.login.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.bearcrew.groubear.login.model.service.SearchPwdUpdateService;
import com.bearcrew.groubear.mypage.model.dto.PwdUpdateDTO;

@WebServlet("/PwdUpdate")
public class SearchPwdUpdateServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = "/WEB-INF/views/login/pwdUpdate.jsp";

		request.getRequestDispatcher(path).forward(request, response);

	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String path = "";
		try {
			
			HttpSession session = request.getSession();													/* 바로 다시 jsp로 넘어가서 입력 받은 값을 가져 온다.*/

			String userId = request.getParameter("userId");												/*비밀번호 찾기후 비밀번호 변경폼에서 입력받은 유저 ID값*/	
			String userNewPwd = request.getParameter("pwd");											/*비밀번호 찾기후 비밀번호 변경폼에서 입력받은 유저 비밀번호 값*/
			String userNewPwdCheck = request.getParameter("userPwdCheck");								/*비밀번호 찾기후 비밀번호 변경폼에서 입력받은 유저 비밀번호 재확인 값*/	

			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();						
			if (passwordEncoder.matches(userNewPwdCheck, userNewPwd)) {									/*비밀번호 값과 재확인 값을 비교한다.*/

				PwdUpdateDTO pwdDTO = new PwdUpdateDTO();
				pwdDTO.setEmpId(userId);																/*PwdDTO에 값을 담는다*/
				pwdDTO.setNewPwd(userNewPwd);															/*PwdDTO에 값을 담는다*/
				
				SearchPwdUpdateService searchPwdUpdateService = new SearchPwdUpdateService();		
				int result = searchPwdUpdateService.PwdUpdate(pwdDTO);									/*비밀번호를 변경한다.*/

				if (result > 0) {																		/*변경이 문제없이 되었을 경우 성공 메세지를 출력한다.*/

					path = "/WEB-INF/views/common/success.jsp";
					request.setAttribute("successCode", "updateSearchPwd");
					request.getRequestDispatcher(path).forward(request, response);

				} else {																			 	/*변경에 문제가 생길시 에러 메세지를 출력한다*/

					path = "/WEB-INF/views/common/failed.jsp";
					request.setAttribute("message", "FailedPassword");
					request.getRequestDispatcher(path).forward(request, response);
				}

			} else {																				 	/*변경에 문제가 생길시 에러 메세지를 출력한다.*/

				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "FailedPassword");
				request.getRequestDispatcher(path).forward(request, response);

			}

		} catch (Exception e) {																			 /*변경에 문제가 생길시 에러 메세지를 출력한다.*/
			
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "FailedPassword");
			request.getRequestDispatcher(path).forward(request, response);
		
		}
	}

}
