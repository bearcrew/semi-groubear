package com.bearcrew.groubear.project.model.dto;

public class PartWorkMemberDTO implements java.io.Serializable {

	private int partWorkMemberNo;						//단위업무 
	private int partWorkNo;								//단위업무 번호
	private String empName;								//사원 이름
	private int empNo;									//사원 번호
	private int projectMemberNo;						//프로젝트 참여인원 번호
	
	public PartWorkMemberDTO() {}

	public PartWorkMemberDTO(int projectMemberNo, int partWorkMemberNo, int partWorkNo, String empName, int empNo) {
		super();
		this.projectMemberNo = projectMemberNo;
		this.partWorkMemberNo = partWorkMemberNo;
		this.partWorkNo = partWorkNo;
		this.empName = empName;
		this.empNo = empNo;
	}

	public int getProjectMemberNo() {
		return projectMemberNo;
	}

	public void setProjectMemberNo(int projectMemberNo) {
		this.projectMemberNo = projectMemberNo;
	}

	public int getPartWorkMemberNo() {
		return partWorkMemberNo;
	}

	public void setPartWorkMemberNo(int partWorkMemberNo) {
		this.partWorkMemberNo = partWorkMemberNo;
	}

	public int getPartWorkNo() {
		return partWorkNo;
	}

	public void setPartWorkNo(int partWorkNo) {
		this.partWorkNo = partWorkNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "PartWorkMemberDTO [projectMemberNo=" + projectMemberNo + ", partWorkMemberNo=" + partWorkMemberNo
				+ ", partWorkNo=" + partWorkNo + ", empName=" + empName + ", empNo=" + empNo + "]";
	}

}

