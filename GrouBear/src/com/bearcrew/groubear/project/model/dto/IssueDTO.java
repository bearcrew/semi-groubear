package com.bearcrew.groubear.project.model.dto;

import java.sql.Date;

public class IssueDTO implements java.io.Serializable {

	private int issueNo; 							//이슈번호
	private String issueName;						//이슈명
	private String issueContent;					//이슈내용
	private int funcBoardNo;						//기능보드 번호
	private String completeYN;						//완료여부
	private String delYN;							//삭제여부
	private String writer;							//작성자
	private java.sql.Date writeDate;				//작성일자
	private int projectNo;							//프로젝트 번호
	
	public IssueDTO() {}

	public IssueDTO(int issueNo, String issueName, String issueContent, int funcBoardNo, String completeYN,
			String delYN, String writer, Date writeDate, int projectNo) {
		super();
		this.issueNo = issueNo;
		this.issueName = issueName;
		this.issueContent = issueContent;
		this.funcBoardNo = funcBoardNo;
		this.completeYN = completeYN;
		this.delYN = delYN;
		this.writer = writer;
		this.writeDate = writeDate;
		this.projectNo = projectNo;
	}

	public int getIssueNo() {
		return issueNo;
	}

	public String getIssueName() {
		return issueName;
	}

	public String getIssueContent() {
		return issueContent;
	}

	public int getFuncBoardNo() {
		return funcBoardNo;
	}

	public String getCompleteYN() {
		return completeYN;
	}

	public String getDelYN() {
		return delYN;
	}

	public String getWriter() {
		return writer;
	}

	public java.sql.Date getWriteDate() {
		return writeDate;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setIssueNo(int issueNo) {
		this.issueNo = issueNo;
	}

	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}

	public void setIssueContent(String issueContent) {
		this.issueContent = issueContent;
	}

	public void setFuncBoardNo(int funcBoardNo) {
		this.funcBoardNo = funcBoardNo;
	}

	public void setCompleteYN(String completeYN) {
		this.completeYN = completeYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public void setWriteDate(java.sql.Date writeDate) {
		this.writeDate = writeDate;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	@Override
	public String toString() {
		return "IssueDTO [issueNo=" + issueNo + ", issueName=" + issueName + ", issueContent=" + issueContent
				+ ", funcBoardNo=" + funcBoardNo + ", completeYN=" + completeYN + ", delYN=" + delYN + ", writer="
				+ writer + ", writeDate=" + writeDate + ", projectNo=" + projectNo + "]";
	}

	
	
}
