package com.bearcrew.groubear.project.model.dto;

import java.sql.Date;

public class FunctionBoardRecordDTO implements java.io.Serializable {
	
	private int funcBoardRecordNo;     					//기능보드변경 이력 번호
	private int funcBoardNo;  	 	   					//기능보드번호
	private int projectNo;		 	  					//프로젝트번호
	private String funcBoardTitle;	   					//기능보드제목
	private String funcCompleteYN;     					//기능완료 여부
	private String funcBoardContent;   					//기능보드 내용
	private String delYN;			   					//삭제여부
	private java.sql.Date alterDate;  					//변경일자
	
	public FunctionBoardRecordDTO() {}

	public FunctionBoardRecordDTO(int funcBoardRecordNo, int funcBoardNo, int projectNo, String funcBoardTitle,
			String funcCompleteYN, String funcBoardContent, String delYN, Date alterDate) {
		super();
		this.funcBoardRecordNo = funcBoardRecordNo;
		this.funcBoardNo = funcBoardNo;
		this.projectNo = projectNo;
		this.funcBoardTitle = funcBoardTitle;
		this.funcCompleteYN = funcCompleteYN;
		this.funcBoardContent = funcBoardContent;
		this.delYN = delYN;
		this.alterDate = alterDate;
	}

	public int getFuncBoardRecordNo() {
		return funcBoardRecordNo;
	}

	public void setFuncBoardRecordNo(int funcBoardRecordNo) {
		this.funcBoardRecordNo = funcBoardRecordNo;
	}

	public int getFuncBoardNo() {
		return funcBoardNo;
	}

	public void setFuncBoardNo(int funcBoardNo) {
		this.funcBoardNo = funcBoardNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getFuncBoardTitle() {
		return funcBoardTitle;
	}

	public void setFuncBoardTitle(String funcBoardTitle) {
		this.funcBoardTitle = funcBoardTitle;
	}

	public String getFuncCompleteYN() {
		return funcCompleteYN;
	}

	public void setFuncCompleteYN(String funcCompleteYN) {
		this.funcCompleteYN = funcCompleteYN;
	}

	public String getFuncBoardContent() {
		return funcBoardContent;
	}

	public void setFuncBoardContent(String funcBoardContent) {
		this.funcBoardContent = funcBoardContent;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public java.sql.Date getAlterDate() {
		return alterDate;
	}

	public void setAlterDate(java.sql.Date alterDate) {
		this.alterDate = alterDate;
	}

	@Override
	public String toString() {
		return "FunctionBoardRecordDTO [funcBoardRecordNo=" + funcBoardRecordNo + ", funcBoardNo=" + funcBoardNo
				+ ", projectNo=" + projectNo + ", funcBoardTitle=" + funcBoardTitle + ", funcCompleteYN="
				+ funcCompleteYN + ", funcBoardContent=" + funcBoardContent + ", delYN=" + delYN + ", alterDate="
				+ alterDate + "]";
	}
	
	
}
