package com.bearcrew.groubear.project.model.dto;

public class FunctionBoardDTO implements java.io.Serializable {

	
	private int funcBoardNo;  	 	 				//기능보드번호
	private int projectNo;		 					//프로젝트번호
	private String funcBoardTitle;	 				//기능보드제목
	private String funcCompleteYN;   				//기능완료 여부
	private String funcBoardContent; 				//기능보드 내용
	private String delYN;			 				//삭제여부
	
	public FunctionBoardDTO() {}

	public FunctionBoardDTO(int funcBoardNo, int projectNo, String funcBoardTitle, String funcCompleteYN,
			String funcBoardContent, String delYN) {
		super();
		this.funcBoardNo = funcBoardNo;
		this.projectNo = projectNo;
		this.funcBoardTitle = funcBoardTitle;
		this.funcCompleteYN = funcCompleteYN;
		this.funcBoardContent = funcBoardContent;
		this.delYN = delYN;
	}

	public int getFuncBoardNo() {
		return funcBoardNo;
	}

	public void setFuncBoardNo(int funcBoardNo) {
		this.funcBoardNo = funcBoardNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getFuncBoardTitle() {
		return funcBoardTitle;
	}

	public void setFuncBoardTitle(String funcBoardTitle) {
		this.funcBoardTitle = funcBoardTitle;
	}

	public String getFuncCompleteYN() {
		return funcCompleteYN;
	}

	public void setFuncCompleteYN(String funcCompleteYN) {
		this.funcCompleteYN = funcCompleteYN;
	}

	public String getFuncBoardContent() {
		return funcBoardContent;
	}

	public void setFuncBoardContent(String funcBoardContent) {
		this.funcBoardContent = funcBoardContent;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	@Override
	public String toString() {
		return "FunctionBoardDTO [funcBoardNo=" + funcBoardNo + ", projectNo=" + projectNo + ", funcBoardTitle="
				+ funcBoardTitle + ", funcCompleteYN=" + funcCompleteYN + ", funcBoardContent=" + funcBoardContent
				+ ", delYN=" + delYN + "]";
	}
	
	
	
}
