package com.bearcrew.groubear.project.model.dto;

import java.sql.Date;

public class PartWorkRecordDTO implements java.io.Serializable {

	private int partWorkRecordNo;						//단위업무 이력 번호
	private int partWorkNo;								//단위업무 번호
	private int workListNo;								//업무리스트 번호
	private String partWorkName;						//단위업무명
	private java.sql.Date startDate;					//시작일자
	private java.sql.Date targetDate;					//목표일자
	private String partWorkContent;						//단위업무 내용
	private String delYN;								//삭제여부
	private java.sql.Date alterDate;					//변경일자
	
	public PartWorkRecordDTO() {}

	public PartWorkRecordDTO(int partWorkRecordNo, int partWorkNo, int workListNo, String partWorkName, Date startDate,
			Date targetDate, String partWorkContent, String delYN, Date alterDate) {
		super();
		this.partWorkRecordNo = partWorkRecordNo;
		this.partWorkNo = partWorkNo;
		this.workListNo = workListNo;
		this.partWorkName = partWorkName;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.partWorkContent = partWorkContent;
		this.delYN = delYN;
		this.alterDate = alterDate;
	}

	public int getPartWorkRecordNo() {
		return partWorkRecordNo;
	}

	public void setPartWorkRecordNo(int partWorkRecordNo) {
		this.partWorkRecordNo = partWorkRecordNo;
	}

	public int getPartWorkNo() {
		return partWorkNo;
	}

	public void setPartWorkNo(int partWorkNo) {
		this.partWorkNo = partWorkNo;
	}

	public int getWorkListNo() {
		return workListNo;
	}

	public void setWorkListNo(int workListNo) {
		this.workListNo = workListNo;
	}

	public String getPartWorkName() {
		return partWorkName;
	}

	public void setPartWorkName(String partWorkName) {
		this.partWorkName = partWorkName;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(java.sql.Date targetDate) {
		this.targetDate = targetDate;
	}

	public String getPartWorkContent() {
		return partWorkContent;
	}

	public void setPartWorkContent(String partWorkContent) {
		this.partWorkContent = partWorkContent;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	public java.sql.Date getAlterDate() {
		return alterDate;
	}

	public void setAlterDate(java.sql.Date alterDate) {
		this.alterDate = alterDate;
	}

	@Override
	public String toString() {
		return "PartWorkRecordDTO [partWorkRecordNo=" + partWorkRecordNo + ", partWorkNo=" + partWorkNo
				+ ", workListNo=" + workListNo + ", partWorkName=" + partWorkName + ", startDate=" + startDate
				+ ", targetDate=" + targetDate + ", partWorkContent=" + partWorkContent + ", delYN=" + delYN
				+ ", alterDate=" + alterDate + "]";
	}
	
	
	
}
