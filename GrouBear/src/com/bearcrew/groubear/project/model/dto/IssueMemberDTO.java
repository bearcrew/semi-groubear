package com.bearcrew.groubear.project.model.dto;

import java.io.Serializable;

public class IssueMemberDTO implements Serializable {

	private int projectMemberNo;					
	private int issueNo;							
	private String empName;							
	private int empNo;								
	private int issueMemberNo;						
	
	public IssueMemberDTO() {}

	public IssueMemberDTO(int projectMemberNo, int issueNo, String empName, int empNo, int issueMemberNo) {
		super();
		this.projectMemberNo = projectMemberNo;
		this.issueNo = issueNo;
		this.empName = empName;
		this.empNo = empNo;
		this.issueMemberNo = issueMemberNo;
	}

	public int getProjectMemberNo() {
		return projectMemberNo;
	}

	public int getIssueNo() {
		return issueNo;
	}

	public String getEmpName() {
		return empName;
	}

	public int getEmpNo() {
		return empNo;
	}

	public int getIssueMemberNo() {
		return issueMemberNo;
	}

	public void setProjectMemberNo(int projectMemberNo) {
		this.projectMemberNo = projectMemberNo;
	}

	public void setIssueNo(int issueNo) {
		this.issueNo = issueNo;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public void setIssueMemberNo(int issueMemberNo) {
		this.issueMemberNo = issueMemberNo;
	}

	@Override
	public String toString() {
		return this.empName;
	}

	
	
	
	
}
