package com.bearcrew.groubear.project.model.dto;

public class InsertProjectMemberDTO implements java.io.Serializable {

	private int empNo;								//사원번호
	private String empName;							//사원이름
	private String projectTitle;					//프로젝트 명
	
	public InsertProjectMemberDTO() {}

	public InsertProjectMemberDTO(int empNo, String empName, String projectTitle) {
		super();
		this.empNo = empNo;
		this.empName = empName;
		this.projectTitle = projectTitle;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	@Override
	public String toString() {
		return "InsertProjectMemberDTO [empNo=" + empNo + ", empName=" + empName + ", projectTitle=" + projectTitle
				+ "]";
	}



	
	
}
