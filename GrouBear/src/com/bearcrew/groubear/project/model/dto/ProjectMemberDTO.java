package com.bearcrew.groubear.project.model.dto;

public class ProjectMemberDTO implements java.io.Serializable {

	private int projectMemberNo;						//프로젝트 참여자 번호
	private int empNo;									//프로젝트 참여 사원번호
	private int projectNo;								//프로젝트 번호
	private String empName;								//프로젝트 참여 사원이름
	
	public ProjectMemberDTO() {}

	public ProjectMemberDTO(int projectMemberNo, int empNo, int projectNo, String empName) {
		super();
		this.projectMemberNo = projectMemberNo;
		this.empNo = empNo;
		this.projectNo = projectNo;
		this.empName = empName;
	}

	public int getProjectMemberNo() {
		return projectMemberNo;
	}

	public void setProjectMemberNo(int projectMemberNo) {
		this.projectMemberNo = projectMemberNo;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	@Override
	public String toString() {
		return "ProjectMemberDTO [projectMemberNo=" + projectMemberNo + ", empNo=" + empNo + ", projectNo=" + projectNo
				+ ", empName=" + empName + "]";
	}

	
	
}
