package com.bearcrew.groubear.project.model.dto;

public class PartWorkAttachmentDTO implements java.io.Serializable {

	private int partWorkNo;								//단위업무 번호
	private int attachmentNo;							//첨부파일 번호
	private int workListNo;								//업무리스트 번호
	private String attachmentFileName; 					//첨부파일명
	private String oriFileName;							//원본파일명
	private String extension;							//확장자
	private int attachmentSize;							//첨부파일 크기
	private String attachmentPath;						//저장경로
	
	public PartWorkAttachmentDTO() {}

	public PartWorkAttachmentDTO(int partWorkNo, int attachmentNo, int workListNo, String attachmentFileName,
			String oriFileName, String extension, int attachmentSize, String attachmentPath) {
		super();
		this.partWorkNo = partWorkNo;
		this.attachmentNo = attachmentNo;
		this.workListNo = workListNo;
		this.attachmentFileName = attachmentFileName;
		this.oriFileName = oriFileName;
		this.extension = extension;
		this.attachmentSize = attachmentSize;
		this.attachmentPath = attachmentPath;
	}

	public int getPartWorkNo() {
		return partWorkNo;
	}

	public void setPartWorkNo(int partWorkNo) {
		this.partWorkNo = partWorkNo;
	}

	public int getAttachmentNo() {
		return attachmentNo;
	}

	public void setAttachmentNo(int attachmentNo) {
		this.attachmentNo = attachmentNo;
	}

	public int getWorkListNo() {
		return workListNo;
	}

	public void setWorkListNo(int workListNo) {
		this.workListNo = workListNo;
	}

	public String getAttachmentFileName() {
		return attachmentFileName;
	}

	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}

	public String getOriFileName() {
		return oriFileName;
	}

	public void setOriFileName(String oriFileName) {
		this.oriFileName = oriFileName;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public int getAttachmentSize() {
		return attachmentSize;
	}

	public void setAttachmentSize(int attachmentSize) {
		this.attachmentSize = attachmentSize;
	}

	public String getAttachmentPath() {
		return attachmentPath;
	}

	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}

	@Override
	public String toString() {
		return "PartWorkAttachmentDTO [partWorkNo=" + partWorkNo + ", attachmentNo=" + attachmentNo + ", workListNo="
				+ workListNo + ", attachmentFileName=" + attachmentFileName + ", oriFileName=" + oriFileName
				+ ", extension=" + extension + ", attachmentSize=" + attachmentSize + ", attachmentPath="
				+ attachmentPath + "]";
	}	
	
	
	
}
