package com.bearcrew.groubear.project.model.dto;

import java.util.Date;

public class ProjectRecordDTO implements java.io.Serializable {

	private int recordNo;								//프로젝트 이력 번호
	private int projectNo;								//프로젝트 번호
	private String projectTitle;						//프로젝트 명
	private String projectContent;						//프로젝트 내용
	private java.sql.Date startDate;					//시작일자
	private java.sql.Date targetDate;					//목표일자
	private java.sql.Date alterDate;					//변경일자
	private String projectYN;							//프로젝트 완료여부
	private String borderColor;							//테두리 색상
	private String delYN;								//삭제여부
	
	public ProjectRecordDTO() {}

	public ProjectRecordDTO(int recordNo, int projectNo, String projectTitle, String projectContent,
			java.sql.Date startDate, java.sql.Date targetDate, java.sql.Date alterDate, String projectYN,
			String borderColor, String delYN) {
		super();
		this.recordNo = recordNo;
		this.projectNo = projectNo;
		this.projectTitle = projectTitle;
		this.projectContent = projectContent;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.alterDate = alterDate;
		this.projectYN = projectYN;
		this.borderColor = borderColor;
		this.delYN = delYN;
	}

	public int getRecordNo() {
		return recordNo;
	}

	public void setRecordNo(int recordNo) {
		this.recordNo = recordNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public String getProjectContent() {
		return projectContent;
	}

	public void setProjectContent(String projectContent) {
		this.projectContent = projectContent;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(java.sql.Date targetDate) {
		this.targetDate = targetDate;
	}

	public java.sql.Date getAlterDate() {
		return alterDate;
	}

	public void setAlterDate(java.sql.Date alterDate) {
		this.alterDate = alterDate;
	}

	public String getProjectYN() {
		return projectYN;
	}

	public void setProjectYN(String projectYN) {
		this.projectYN = projectYN;
	}

	public String getBorderColor() {
		return borderColor;
	}

	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	@Override
	public String toString() {
		return "ProjectRecordDTO [recordNo=" + recordNo + ", projectNo=" + projectNo + ", projectTitle=" + projectTitle
				+ ", projectContent=" + projectContent + ", startDate=" + startDate + ", targetDate=" + targetDate
				+ ", alterDate=" + alterDate + ", projectYN=" + projectYN + ", borderColor=" + borderColor + ", delYN="
				+ delYN + "]";
	}

 
}
