package com.bearcrew.groubear.project.model.dto;

import java.sql.Date;

public class PartWorkDTO implements java.io.Serializable {

	private int partWorkNo;								//단위업무 번호
	private int projectNo;								//프로젝트 번호
	private int funcBoardNo;							//기능보드 번호
	private int workListNo;								//업무리스트 번호
	private String partWorkName;						//단위업무명
	private java.sql.Date startDate;					//시작일자
	private java.sql.Date targetDate;					//목표일자
	private String partWorkContent;						//단위업무 내용
	private String delYN;								//삭제여부
	
	public PartWorkDTO() {}

	public PartWorkDTO(int partWorkNo, int projectNo, int funcBoardNo, int workListNo, String partWorkName,
			Date startDate, Date targetDate, String partWorkContent, String delYN) {
		super();
		this.partWorkNo = partWorkNo;
		this.projectNo = projectNo;
		this.funcBoardNo = funcBoardNo;
		this.workListNo = workListNo;
		this.partWorkName = partWorkName;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.partWorkContent = partWorkContent;
		this.delYN = delYN;
	}

	public int getPartWorkNo() {
		return partWorkNo;
	}

	public void setPartWorkNo(int partWorkNo) {
		this.partWorkNo = partWorkNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public int getFuncBoardNo() {
		return funcBoardNo;
	}

	public void setFuncBoardNo(int funcBoardNo) {
		this.funcBoardNo = funcBoardNo;
	}

	public int getWorkListNo() {
		return workListNo;
	}

	public void setWorkListNo(int workListNo) {
		this.workListNo = workListNo;
	}

	public String getPartWorkName() {
		return partWorkName;
	}

	public void setPartWorkName(String partWorkName) {
		this.partWorkName = partWorkName;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(java.sql.Date targetDate) {
		this.targetDate = targetDate;
	}

	public String getPartWorkContent() {
		return partWorkContent;
	}

	public void setPartWorkContent(String partWorkContent) {
		this.partWorkContent = partWorkContent;
	}

	public String getDelYN() {
		return delYN;
	}

	public void setDelYN(String delYN) {
		this.delYN = delYN;
	}

	@Override
	public String toString() {
		return "PartWorkDTO [partWorkNo=" + partWorkNo + ", projectNo=" + projectNo + ", funcBoardNo=" + funcBoardNo
				+ ", workListNo=" + workListNo + ", partWorkName=" + partWorkName + ", startDate=" + startDate
				+ ", targetDate=" + targetDate + ", partWorkContent=" + partWorkContent + ", delYN=" + delYN + "]";
	}

	
	
}
