package com.bearcrew.groubear.project.model.dto;

import java.util.Date;

public class LoginProjectDTO implements java.io.Serializable {

	private int projectNo;
	private String projectTitle;
	private java.util.Date startDate;
	private java.util.Date targetDate;
	private char delYn;
	private String empName;
	private int empNo;
	
	public LoginProjectDTO() {}

	public LoginProjectDTO(int projectNo, String projectTitle, Date startDate, Date targetDate, char delYn,
			String empName, int empNo) {
		super();
		this.projectNo = projectNo;
		this.projectTitle = projectTitle;
		this.startDate = startDate;
		this.targetDate = targetDate;
		this.delYn = delYn;
		this.empName = empName;
		this.empNo = empNo;
	}

	public int getProjectNo() {
		return projectNo;
	}

	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}

	public String getProjectTitle() {
		return projectTitle;
	}

	public void setProjectTitle(String projectTitle) {
		this.projectTitle = projectTitle;
	}

	public java.util.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}

	public java.util.Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(java.util.Date targetDate) {
		this.targetDate = targetDate;
	}

	public char getDelYn() {
		return delYn;
	}

	public void setDelYn(char delYn) {
		this.delYn = delYn;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getEmpNo() {
		return empNo;
	}

	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}

	@Override
	public String toString() {
		return "LoginProjectDTO [projectNo=" + projectNo + ", projectTitle=" + projectTitle + ", startDate=" + startDate
				+ ", targetDate=" + targetDate + ", delYn=" + delYn + ", empName=" + empName + ", empNo=" + empNo + "]";
	}

	
}
