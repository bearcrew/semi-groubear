package com.bearcrew.groubear.project.model.dto;

public class IssueRecordDTO {
	
	private int issueRecordNo;			//이슈 이력번호
	private int issueNo; 				//이슈번호
	private String issueName;			//이슈명
	private String issueContent;		//이슈내용
	private int funcBoardNo;			//기능보드 번호
	private String completeYN;			//완료여부
	private String delYN;				//삭제여부
	private java.sql.Date alterDate;	//변경일자

}
