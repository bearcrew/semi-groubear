package com.bearcrew.groubear.project.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.IssueMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.mapper.IssueMapper;

public class IssueService {

	public List<IssueDTO> selectAllIssue(IssueDTO issueDTO) {
		//모든 이슈 목록을 조회한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		List<IssueDTO> issueList = issueMapper.selectAllIssue(issueDTO);
		
		sqlSession.close();
		
		return issueList;
	}

	public List<IssueMemberDTO> selectIssueMember(int issueNo) {
		//이슈 담당자를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		List<IssueMemberDTO> issueMemeber = issueMapper.selectIssueMember(issueNo);
		
		sqlSession.close();
		
		return issueMemeber;
	}

	public int insertIssue(IssueDTO issueDTO) {
		//이슈를 등록한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result = issueMapper.insertIssue(issueDTO);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public List<ProjectMemberDTO> selectAllProjectMember(int projectNo) {
		//프로젝트 담당자를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		List<ProjectMemberDTO> projectMemberList = issueMapper.selectAllProjectMember(projectNo);
		
		sqlSession.close();
		
		return projectMemberList;
	}

	public int insertIssueMember(IssueMemberDTO insertIssueMember) {
		//이슈 담당자를 등록한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result = issueMapper.insertIssueMember(insertIssueMember);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public ProjectMemberDTO selectEmpNo(ProjectMemberDTO selectEmpNoDTO) {
		//사원 번호를 조회한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		ProjectMemberDTO selectEmpNo = issueMapper.selectEmpNo(selectEmpNoDTO);
		
		sqlSession.close();
		
		return selectEmpNo;
	}

	public int deleteIssue(int issueNo, String issueMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result1 = 1;
		if(issueMember.length() > 0) {
			
			//이슈 담당자를 먼저 삭제한다
			result1 = issueMapper.deleteIssueMember(issueNo);
		}
		
		int result2 = 0;
		if(result1 > 0) {
			sqlSession.commit();
			
			//이슈 담당자가 삭제되고 나서 이슈를 삭제 한다
			result2 = issueMapper.deleteIssue(issueNo);
			
			if(result2 > 0) {
				sqlSession.commit();
			} else {
				sqlSession.rollback();
			}
			
		} else {
			sqlSession.rollback();
		}

		sqlSession.close();
		
		return result2;
	}

	public int updateIssue(IssueDTO updateIssueDTO) {
		//이슈 수정을 한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result = issueMapper.updateIssue(updateIssueDTO);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteIssueMember(int issueNo) {
		//이슈 담당자를 삭제한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result = issueMapper.deleteIssueMember(issueNo);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int insertIssueMember2(IssueMemberDTO insertIssueMember) {
		//이슈 담당자를 등록한다
		SqlSession sqlSession = getSqlSession();
		
		IssueMapper issueMapper = sqlSession.getMapper(IssueMapper.class);
		
		int result = issueMapper.insertIssueMember2(insertIssueMember);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	

	
}
