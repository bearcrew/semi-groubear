package com.bearcrew.groubear.project.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.project.model.dto.PartWorkDTO;
import com.bearcrew.groubear.project.model.dto.PartWorkMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.mapper.KanbanMapper;
import com.bearcrew.groubear.project.model.mapper.ProjectLogMapper;

public class KanbanService {

	
	/* 단위업무 전체 목록 조회 */
	public List<PartWorkDTO> searchAllPartWork(PartWorkDTO partWorkDTO) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		List<PartWorkDTO> partWorkList = mapper.searchAllPartWork(partWorkDTO);
		
		session.close();
		
		/* 조회한 단위업무 목록 리턴  */
		return partWorkList;
	}

	/* 단위업무 상세 조회 */
	public PartWorkDTO searchPartWorkDetail(int partWorkNo) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		PartWorkDTO partWorkInfo = mapper.searchPartWorkDetail(partWorkNo);
		
		session.close();
		
		/* 조회한 단위업무 목록 리턴  */
		return partWorkInfo;
	}


	/* 단위업무 조회 시에 담당자 목록 조회하기 */
	public List<PartWorkMemberDTO> searchPartWorkMember(int partWorkNo) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		List<PartWorkMemberDTO> partWorkMember = mapper.searchPartWorkMember(partWorkNo);
		
		session.close();
		
		return partWorkMember;
	}

	/* 단위업무 카드 생성 */
	public int insertPartWork(PartWorkDTO partWorkDTO) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);		
		
		int result = mapper.insertPartWork(partWorkDTO);
		
		if(result > 0) {
			session.commit();
			
			/* 단위업무  insertLog */
			int logUpdate = logMapper.insertPartWork(partWorkDTO);
			
			if(logUpdate > 0) {
				session.commit();
			} else {
				session.rollback();
			}						
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}
	
	/* 단위업무 생성 후 담당자 등록 */
	public int insertPartWorkMember(PartWorkMemberDTO partWorkMemberDTO) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		int result = mapper.insertPartWorkMember(partWorkMemberDTO);
		
		if(result > 0) {
			session.commit();
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}
	
	
	/* 단위업무 생성 시 프로젝트 참여자 전체 조회하기 */
	public List<ProjectMemberDTO> searchAllProjectMember(int projectNo) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		List<ProjectMemberDTO> memberList = mapper.searchAllProjectMember(projectNo);
		
		session.close();
		
		return memberList;
	}
	
	
	/* 단위 업무 수정 */
	public int modifyPartWork(PartWorkDTO partWorkDTO) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);		
		
		int result = mapper.updatePartWork(partWorkDTO);
		
		if(result > 0) {
			
			/* 담당자 목록 초기화 */
			int removeMembers = mapper.removePartWorkMembers(partWorkDTO);
			
			if(removeMembers > 0) {
				session.commit();
			} else {
				session.rollback();
			}
			
			/* 단위업무 updateLog */
			int logUpdate = logMapper.modifyPartWork(partWorkDTO);
			
			if(logUpdate > 0) {
				session.commit();
			} else {
				session.rollback();
			}			
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}
	
	/* 단위업무 담당자 업데이트 */
	public int modifyPartWorkMember(PartWorkMemberDTO partWorkMemberDTO) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		int result = mapper.modifyPartWorkMember(partWorkMemberDTO);
		
		if(result > 0) {
			session.commit();
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}

	
	/* 단위업무 삭제 */
	public int deletePartWorkNo(int partWorkNo) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		int result = mapper.deletePartWork(partWorkNo);
		
		if(result > 0) {
			session.commit();
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}

	/* 단위업무 상태 변경 */
	public int changeWorkListNo(PartWorkDTO partWork) {
		
		SqlSession session = getSqlSession();
		
		KanbanMapper mapper = session.getMapper(KanbanMapper.class);
		
		int result = mapper.changeWorkListNo(partWork);
		
		int checkNo = 0;
		if(result > 0) {
			session.commit();
			
			checkNo = mapper.checkWorkListNo(partWork);
			
		} else {
			session.rollback();
		}
		
		session.close();
		
		return checkNo;
	}


}
