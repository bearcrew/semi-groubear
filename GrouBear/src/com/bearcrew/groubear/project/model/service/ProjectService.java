package com.bearcrew.groubear.project.model.service;

import static com.bearcrew.groubear.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.dto.InsertProjectMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.mapper.ProjectLogMapper;
import com.bearcrew.groubear.project.model.mapper.ProjectMapper;

public class ProjectService {

	/* 프로젝트 조회 */
	public List<ProjectDTO> searchAllProject(SelectCriteria selectCriteria) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		
		List<ProjectDTO> projectList = projectMapper.searchAllProject(selectCriteria);
		
		for (ProjectDTO project : projectList) {
			int num = project.getProjectNo();
			
			int progress = projectMapper.searchProjectProgress(num);
			
			String treePath = "";
			if(progress < 1) {
				treePath = "0";
				project.setProgress(treePath);
			} else if(progress < 11){
				treePath = "10";
				project.setProgress(treePath);
			} else if(progress < 21){
				treePath = "20";
				project.setProgress(treePath);
			} else if(progress < 31){
				treePath = "30";
				project.setProgress(treePath);
			} else if(progress < 41){
				treePath = "40";
				project.setProgress(treePath);
			} else if(progress < 51){
				treePath = "50";
				project.setProgress(treePath);
			} else if(progress < 61){
				treePath = "60";
				project.setProgress(treePath);
			} else if(progress < 71){
				treePath = "70";
				project.setProgress(treePath);
			} else if(progress < 81){
				treePath = "80";
				project.setProgress(treePath);
			} else if(progress < 91){
				treePath = "90";
				project.setProgress(treePath);
			} else {
				treePath = "100";
				project.setProgress(treePath);
			}
			
		}
		
		session.close();
		
		return projectList;
	}
	
	/* 프로젝트 정보 추가 */
	public int insertProjectInfo(ProjectDTO projectDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = projectMapper.insertProjectInfo(projectDTO);
		
		if(result > 0) {
			session.commit();
			
				/* 프로젝트 insertLog */
				int logUpdate = logMapper.insertProjectInfo(projectDTO);
				
				if(logUpdate > 0) {
					session.commit();
				} else {
					session.rollback();
				}
			
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}

	/* 프로젝트 참여 인원 추가 */
	public int insertProjectMember(InsertProjectMemberDTO insertDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		
		int result = projectMapper.insertProjectMember(insertDTO);
		
		if(result > 0) {
			
			int alarmOn = projectMapper.projectAlarmOn(insertDTO);
			
			if(alarmOn > 0) {
				session.commit();
			}
			
		} else {
			session.rollback();
		}
		
		session.close();
		
		return result;
	}

	/*  프로젝트 생성 - 프로젝트 참여 사원 조회(사원번호) */
	public String searchEmpNameForEmpNo(int empNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		
		String empName = projectMapper.searchEmpNameForEmpNo(empNo);
		
		session.close();
		
		return empName;
		
	}

	/*  프로젝트 생성 - 프로젝트 참여 사원 조회(이메일) */
	public String searchEmpNameForEmail(String email) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		
		String empName = projectMapper.searchEmpNameForEmail(email);
		
		session.close();
		
		return empName;
	}

	/* 프로젝트 기능보드 전체조회 */
	public List<FunctionBoardDTO> searchProjectBoradList(int projectNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper projectMapper = session.getMapper(ProjectMapper.class);
		
		List<FunctionBoardDTO> functionBoardList = projectMapper.searchProjectBoradList(projectNo);
		
		session.close();
		
		return functionBoardList;
	}

	/* 프로젝트 번호에 해당하는 프로젝트 정보 조회 */
	public ProjectDTO searchProjectInfo(int projectNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		
		ProjectDTO projectInfo = mapper.searchProjectInfo(projectNo);
		
		session.close();
		
		return projectInfo;
	}

	/* 프로젝트 기능보드 추가 */
	public int insertFuncBoard(FunctionBoardDTO info) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.insertFuncBoard(info);
		
		if(result > 0) { 
			session.commit();
			
			/* 프로젝트 기능보드 insertLog */
			int logUpdate = logMapper.insertFuncBoard(info);
			
			if(logUpdate > 0) {
				session.commit();
			} else {
				session.rollback();
			}
			
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}

	/* 프로젝트 정보 수정 */
	public int modifyProjectInfo(ProjectDTO projectDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.updateProjectInfo(projectDTO);
		
		if(result > 0) { 
			session.commit();
			
			/* 프로젝트 updatelog */
			int logUpdate = logMapper.modifyProjectInfo(projectDTO);
			
			if(logUpdate > 0) {
				session.commit();
			} else {
				session.rollback();
			}
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}

	/* 프로젝트 참여인원 수정 (전체 수정? 부분수정 ?) */
	public int modifyProjectMember(InsertProjectMemberDTO insertDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		
		int result = mapper.updateProjectMember(insertDTO);
		
		if(result > 0) { 
			session.commit();
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}
	
	/* 프로젝트 삭제 */
	public int deleteProject(int projectNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.deleteProject(projectNo);
		
		if(result > 0) { 
			session.commit();
			
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}

	/* 프로젝트 기능보드 정보 수정 */
	public int modifyFuncBoard(FunctionBoardDTO funcBoardDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.updateFuncBoard(funcBoardDTO);
		
		if(result > 0) { 
			session.commit();
			
			/* 프로젝트 기능보드 updateLog */
			int logUpdate = logMapper.updateFuncBoard(funcBoardDTO);
			
			if(logUpdate > 0) {
				session.commit();
			} else {
				session.rollback();
			}			
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}

	/* 프로젝트 기능보드 삭제 */
	public int deleteFuncBoard(int funcBoardNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.deleteFuncBoard(funcBoardNo);
		
		if(result > 0) { 
			session.commit();
			
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
	}

	/* 전체 프로젝트 갯수 구하기 */
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		
		int projectCount = mapper.selectTotalCount(searchMap);
		
		session.close();
		
		return projectCount;
	}

	/* 프로젝트 완료 상태로 변경 */
	public int projectComplete(int projectNo) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		ProjectLogMapper logMapper = session.getMapper(ProjectLogMapper.class);
		
		int result = mapper.projectComplete(projectNo);
		
		if(result > 0) { 
			session.commit();
			
		} else {
			session.rollback();
		}
			
		session.close();
		
		return result;
		
	}

	/* 기능보드 수정 버튼 클릭시 클릭한 기능보드 정보 조회 */
	public FunctionBoardDTO selectedFuncInfo(FunctionBoardDTO funcBoardDTO) {
		
		SqlSession session = getSqlSession();
		
		ProjectMapper mapper = session.getMapper(ProjectMapper.class);
		
		FunctionBoardDTO selectedFuncInfo = mapper.selectedFuncInfo(funcBoardDTO);
		
		session.close();
		
		return selectedFuncInfo;
	}

}
