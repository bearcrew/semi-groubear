package com.bearcrew.groubear.project.model.mapper;

import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.dto.PartWorkDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;

public interface ProjectLogMapper {

	/* 프로젝트 추가 */
	int insertProjectInfo(ProjectDTO projectDTO);

	/* 기능보드 추가 */
	int insertFuncBoard(FunctionBoardDTO info);

	/* 프로젝트 수정 */
	int modifyProjectInfo(ProjectDTO projectDTO);

	/* 기능보드 수정 */
	int updateFuncBoard(FunctionBoardDTO funcBoardDTO);

	/* 단위업무 추가 */
	int insertPartWork(PartWorkDTO partWorkDTO);

	/* 단위업무 수정 */
	int modifyPartWork(PartWorkDTO partWorkDTO);


}
