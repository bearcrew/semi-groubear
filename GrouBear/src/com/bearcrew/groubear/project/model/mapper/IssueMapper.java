package com.bearcrew.groubear.project.model.mapper;

import java.util.List;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.IssueMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;

public interface IssueMapper {

	List<IssueDTO> selectAllIssue(IssueDTO issueDTO);

	List<IssueMemberDTO> selectIssueMember(int issueNo);

	int insertIssue(IssueDTO issueDTO);

	List<ProjectMemberDTO> selectAllProjectMember(int projectNo);

	int insertIssueMember(IssueMemberDTO insertIssueMember);

	ProjectMemberDTO selectEmpNo(ProjectMemberDTO selectEmpNoDTO);

	int deleteIssue(int issueNo);

	int deleteIssueMember(int issueNo);

	int updateIssue(IssueDTO updateIssueDTO);

	int insertIssueMember2(IssueMemberDTO insertIssueMember);


}
