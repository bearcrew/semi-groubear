package com.bearcrew.groubear.project.model.mapper;

import java.util.List;
import java.util.Map;

import com.bearcrew.groubear.project.model.dto.PartWorkDTO;
import com.bearcrew.groubear.project.model.dto.PartWorkMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;

public interface KanbanMapper {
	
	/* 단위업무 전체 조회 (칸반보드 화면) */
	List<PartWorkDTO> searchAllPartWork(PartWorkDTO partWorkDTO);

	/* 단위업무 상세 조회 (카드 modal) */
	PartWorkDTO searchPartWorkDetail(int partWorkNo);

	/* 단위업무 담당자 조회 */
	List<PartWorkMemberDTO> searchPartWorkMember(int partWorkNo);

	/* 프로젝트 전체 참여인원 조회 */
	List<ProjectMemberDTO> searchAllProjectMember(int projectNo);
	
	/* 단위업무 카드 생성 */
	int insertPartWork(PartWorkDTO partWorkDTO);
	
	/* 단위업무 담당자 등록 */
	int insertPartWorkMember(PartWorkMemberDTO partWorkMemberDTO);
	
	/* 단위업무 수정 */
	int updatePartWork(PartWorkDTO partWorkDTO);

	/* 단위업무 수정 전 담당자목록 초기화 */
	int removePartWorkMembers(PartWorkDTO partWorkDTO);
	
	/* 단위업무 담당자 수정 */
	int modifyPartWorkMember(PartWorkMemberDTO partWorkMemberDTO);
	
	/* 단위업무 삭제 */
	int deletePartWork(int partWorkNo);

	/* 단위업무 상태 변경 */
	int changeWorkListNo(PartWorkDTO partWork);

	/* 단위업무 상태 확인 */
	int checkWorkListNo(PartWorkDTO partWork);



	


}
