package com.bearcrew.groubear.project.model.mapper;

import java.util.List;
import java.util.Map;

import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.dto.InsertProjectMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;

public interface ProjectMapper {

	/* 프로젝트 리스트 전체 조회 */
	List<ProjectDTO> searchAllProject(SelectCriteria selectCriteria);
	
	/* 프로젝트 진척률 조회 */
	int searchProjectProgress(int num);

	/* 프로젝트 추가 */
	int insertProjectInfo(ProjectDTO projectDTO);
	
	/* 프로젝트 인원 추가 */
	int insertProjectMember(InsertProjectMemberDTO insertDTO);
	
	/* 프로젝트 참여 인원 알람 ON */
	int projectAlarmOn(InsertProjectMemberDTO insertDTO);

	/* 사원번호로 사원명 조회 */
	String searchEmpNameForEmpNo(int empNo);

	/* 이메일로 사원명 조회 */
	String searchEmpNameForEmail(String email);

	/* 프로젝트 정보 조회 */
	ProjectDTO searchProjectInfo(int projectNo);
	
	/* 프로젝트 정보 수정 */
	int updateProjectInfo(ProjectDTO projectDTO);

	/* 프로젝트 참여 인원 수정 */
	int updateProjectMember(InsertProjectMemberDTO insertDTO);

	/* 프로젝트 삭제 */
	int deleteProject(int projectNo);
	
	

	/* 프로젝트 기능보드 조회 */
	List<FunctionBoardDTO> searchProjectBoradList(int projectNo);
	
	/* 프로젝트 기능보드 추가 */
	int insertFuncBoard(FunctionBoardDTO info);
	
	/* 프로젝트 기능보드 정보 수정 */
	int updateFuncBoard(FunctionBoardDTO funcBoardDTO);

	/* 프로젝트 기능보드 삭제 */
	int deleteFuncBoard(int funcBoardNo);

	/* 페이징에 필요한 전체 프로젝트 갯수 */
	int selectTotalCount(Map<String, String> searchMap);

	/* 프로젝트 완료상태로 변경 */
	int projectComplete(int projectNo);

	/* 선택한 기능보드 정보 조회 */
	FunctionBoardDTO selectedFuncInfo(FunctionBoardDTO funcBoardDTO);

	

	


	

}
