package com.bearcrew.groubear.project.controller.issue;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.login.model.dto.LoginDTO;
import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.IssueMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.service.IssueService;

@WebServlet("/issue/insert")
public class IssueInsertServlet extends HttpServlet {
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 로그인된 사원의 이름을 가져와서 issueDTO에 있는 writer에 담는다 */
		HttpSession session = request.getSession();
		LoginDTO loginDTO = (LoginDTO) session.getAttribute("loginMember");
		String Writer = loginDTO.getEmpName();
		IssueDTO insertIssueDTO = new IssueDTO();
		insertIssueDTO.setWriter(Writer);
		
		/* 전달 받은 값을 issueDTO에 담는다 */
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		String issueName = request.getParameter("issueName");
		String issueContent = request.getParameter("issueContent");
		String issueMember = request.getParameter("issueMember");
		insertIssueDTO.setFuncBoardNo(funcBoardNo);
		insertIssueDTO.setProjectNo(projectNo);
		insertIssueDTO.setIssueName(issueName);
		insertIssueDTO.setIssueContent(issueContent);
		
		/* DTO를 매개변수로 전달하여서 이슈 등록을 한다 */
		int result = new IssueService().insertIssue(insertIssueDTO);
		
		/* 이슈가 등록 되었을 경우 동작 */
		String path = "";
		if(result > 0) {
			
			/* 이슈 담당자가 존재할 경우 */
			if(issueMember.length() > 0) {
				
				/* 이슈 담당자 이름과 프로젝트참여 번호를 분리한다 */
				String issueMember2 = issueMember.replace("#", "");
				String[] members = issueMember2.split(" ");
				for(int i = 0; i < members.length; i++) {
					String empName = members[i].replaceAll("[0-9]", "");
					int projectMemberNo = Integer.parseInt(members[i].replaceAll("[ㄱ-힣]", ""));
					
					/* 분리한 이름과 프로젝트참여 번호로 사원번호를 조회한다 */
					ProjectMemberDTO selectEmpNoDTO = new ProjectMemberDTO();
					selectEmpNoDTO.setEmpName(empName);
					selectEmpNoDTO.setProjectMemberNo(projectMemberNo);
					
					IssueService issueService = new IssueService();
					ProjectMemberDTO selectEmpNo = issueService.selectEmpNo(selectEmpNoDTO);
					int empNo2 = selectEmpNo.getEmpNo();
					
					/* 조회해온 결과를 DTO에 담는다 */
					IssueMemberDTO insertIssueMember = new IssueMemberDTO();
					insertIssueMember.setEmpName(empName);
					insertIssueMember.setEmpNo(empNo2);
					insertIssueMember.setProjectMemberNo(projectMemberNo);
					
					/* DTO를 서비스로 전달해서 이슈 등록을 한다 */
					int result2 = issueService.insertIssueMember(insertIssueMember);
					
					/* 결과에 따라 페이지를 연결한다 */
					if(result2 > 0) {
						request.setAttribute("funcBoardNo", funcBoardNo);
						request.setAttribute("projectNo", projectNo);
						path = "/WEB-INF/views/common/success.jsp";
						request.setAttribute("successCode", "insertIssue");
					} else {
						path = "/WEB-INF/views/common/failed.jsp";
						request.setAttribute("message", "insertIssue");
					}
					
				}
			}
			
			/* 담당자가 없을 경우에는 이슈만 등록 하고  정보를 request영역에 담는다*/
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("successCode", "insertIssue");
			path = "/WEB-INF/views/common/success.jsp";
		}
		
		/* 결과에 따라 페이지 이동*/
		request.getRequestDispatcher(path).forward(request, response);

	}
}
