package com.bearcrew.groubear.project.controller.issue;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.IssueMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.service.IssueService;

@WebServlet("/issue/search")
public class IssueSearchServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 전달 받은 값을 변수에 담는다 */
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		/* 변수에 담은 값을 IssueDTO에 담는다 */
		IssueDTO issueDTO = new IssueDTO();
		issueDTO.setFuncBoardNo(funcBoardNo);
		issueDTO.setProjectNo(projectNo);
		
		/* DTO를 매개변수로 서비스로  넘겨서 이슈목록을 List에 담는다 */
		IssueService issueService = new IssueService();
		List<IssueDTO> issueList = issueService.selectAllIssue(issueDTO);
		
		/* 서비스로 프로젝트 번호를 넘겨서 해당 프로젝트에 참여한 사원을 모두 조회한다 */
		List<ProjectMemberDTO> projectMemberList = issueService.selectAllProjectMember(projectNo);
		
		/* request영역에 이슈목록과 프로젝트 참여자 목록을 저장하고 화면을 전환한다. */
		String path ="/WEB-INF/views/project/searchIssue.jsp";
		request.setAttribute("issueList", issueList);
		request.setAttribute("projectMemberList", projectMemberList);
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*ajax로 값을 보낸것을 변수에 담는다*/
		int issueNo = Integer.parseInt(request.getParameter("issueNo"));
		
		/*서비스로 이슈번호를 보내서 해당 이슈번호에 맞는 담당자를 조회해서 결과를 리스트에 담는다.*/
		List<IssueMemberDTO> issueMemeber = new IssueService().selectIssueMember(issueNo);
		
		/* 인코딩을 하고 request에 조회한 이슈 담당자 리스트를 담고 값을 보낸다 */
		response.setCharacterEncoding("UTF-8");
		request.setAttribute("issueMemeber", issueMemeber);
		PrintWriter out = response.getWriter();
		out.print(issueMemeber.toString());
	}
}
