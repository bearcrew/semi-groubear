package com.bearcrew.groubear.project.controller.issue;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.IssueMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.service.IssueService;

@WebServlet("/issue/update")
public class IssueUpdateServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 수정할 내용을 전달 받는다 */
		String issueName = request.getParameter("issueName");
		String issueContent = request.getParameter("issueContent");
		String issueMember = request.getParameter("issueMember");
		int issueNo = Integer.parseInt(request.getParameter("issueNo"));
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		
		/* 전달 받은 값을 DTO에 담는다 */
		IssueDTO updateIssueDTO = new IssueDTO();
		updateIssueDTO.setIssueName(issueName);
		updateIssueDTO.setIssueContent(issueContent);
		updateIssueDTO.setIssueNo(issueNo);
		
		/* 서비스로 수정할 내용을 전달한다 */
		IssueService issueService = new IssueService();
		int result = issueService.updateIssue(updateIssueDTO);
		
		String path = "";
		if(result > 0) {
			
			/* 이슈 수정 성공시 기존 담당자를 삭제한다 */
			int result2 = issueService.deleteIssueMember(issueNo);
			result2 = 1;
			
			if(result2 > 0) {
				if(issueMember.length() > 0) {
					
					/* 이슈 담당자 이름과 프로젝트참여 번호를 분리한다 */
					String issueMember2 = issueMember.replace("#", "");
					String[] members = issueMember2.split(" ");
					for(int i = 0; i < members.length; i++) {
						String empName = members[i].replaceAll("[0-9]", "");
						int projectMemberNo = Integer.parseInt(members[i].replaceAll("[ㄱ-힣]", ""));
						
						/* 분리한 이름과 프로젝트참여 번호를 DTO에 담는다 */
						ProjectMemberDTO selectEmpNoDTO = new ProjectMemberDTO();
						selectEmpNoDTO.setEmpName(empName);
						selectEmpNoDTO.setProjectMemberNo(projectMemberNo);
						
						/* 사원 번호를 조회한다 */
						ProjectMemberDTO selectEmpNo = issueService.selectEmpNo(selectEmpNoDTO);
						int empNo2 = selectEmpNo.getEmpNo();
						
						/* 조회해온 사원정보를 DTO에 담는다 */
						IssueMemberDTO insertIssueMember = new IssueMemberDTO();
						insertIssueMember.setEmpName(empName);
						insertIssueMember.setEmpNo(empNo2);
						insertIssueMember.setProjectMemberNo(projectMemberNo);
						insertIssueMember.setIssueNo(issueNo);
						
						/* DTO를 서비스로 보내서 담당자를 추가한다 */
						int result3 = issueService.insertIssueMember2(insertIssueMember);
						
						/* 성공 여부에 따라 페이지를 연결한다 */
						if(result3 > 0) {
							path = "/WEB-INF/views/common/success.jsp";
							request.setAttribute("successCode", "updateIssue");
							request.setAttribute("funcBoardNo", funcBoardNo);
							request.setAttribute("projectNo", projectNo);
						} else {
							path = "/WEB-INF/views/common/failed.jsp";
							request.setAttribute("message", "updateIssue");
						}
						
					}
				}
				
				/* 이슈 담당자가 없을시 request영역에 필요한 값을 담고 성공 페이지로 연결한다*/
				path = "/WEB-INF/views/common/success.jsp";
				request.setAttribute("successCode", "updateIssue");
				request.setAttribute("funcBoardNo", funcBoardNo);
				request.setAttribute("projectNo", projectNo);
			} else {
				
				/* 실패시 실패 메세지를 띄운다*/
				path = "/WEB-INF/views/common/failed.jsp";
				request.setAttribute("message", "updateIssue");
			}
		} else {
			
			/* 실패시 실패 메세지를 띄운다*/
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("message", "updateIssue");
		}
		
		/* 결과에 따라 페이지 이동*/
		request.getRequestDispatcher(path).forward(request, response);

			
	}
}
