package com.bearcrew.groubear.project.controller.issue;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.service.IssueService;

@WebServlet("/issue/delete")
public class IssueDeleteServlet extends HttpServlet {
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* ajax로 넘겨받은 값을 변수에 담는다 */
		int issueNo = Integer.parseInt(request.getParameter("issueNo"));
		String issueMember = request.getParameter("issueMember");
		
		/* 전달받은 값을 매개변수로 서비스에 전달한다 */
		int result = new IssueService().deleteIssue(issueNo, issueMember);
		
		/* 인코딩을 하고 결과를 리턴한다. */
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		if(result > 0) {
			out.print("삭제 되었습니다.");
		} else {
			out.print("삭제 실패 하였습니다.");
		}
		
	}
}
