package com.bearcrew.groubear.project.controller.board;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/board/insert")
public class ProjectBoardInsertServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		String path = "";
		if(projectNo > 0 ) {
			path = "/WEB-INF/views/project/insertProjectBoard.jsp";
			request.setAttribute("projectNo", projectNo);
		} else {
			path = "/WEB-INF/views/project/failed.jsp";
			request.setAttribute("message", "기능보드 등록 실패");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FunctionBoardDTO funcBoardDTO = new FunctionBoardDTO();
		ProjectService projectService = new ProjectService();
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		String funcBoardTitle = request.getParameter("funcBoardTitle");
		String funcBoardContent = request.getParameter("funcBoardContent");
		
		funcBoardDTO.setProjectNo(projectNo);
		funcBoardDTO.setFuncBoardTitle(funcBoardTitle);
		funcBoardDTO.setFuncBoardContent(funcBoardContent);
		
		int result = projectService.insertFuncBoard(funcBoardDTO);
		
		String path = "";
		
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "insertFuncBoard");
			request.setAttribute("projectNo", projectNo);
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "insertFuncBoard");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
