package com.bearcrew.groubear.project.controller.board;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/project/board/modify")
public class ProjectBoardModifyServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProjectService projectService = new ProjectService();
		FunctionBoardDTO funcBoardDTO = new FunctionBoardDTO();
		
		int projectNo = Integer.parseInt(request.getParameter("$projectNo"));
		int funcBoardNo = Integer.parseInt(request.getParameter("$funcBoardNo"));
		
		funcBoardDTO.setFuncBoardNo(funcBoardNo);
		funcBoardDTO.setProjectNo(projectNo);
		
		FunctionBoardDTO result = projectService.selectedFuncInfo(funcBoardDTO);
		
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.create();
		
		String jsonString = gson.toJson(result);
		
		response.setContentType("application/json; charset=UTF-8");

		PrintWriter out = response.getWriter();
		
		String path = "";
		
		if(result != null) {
			out.print(jsonString);
		}
			
		out.close();
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FunctionBoardDTO funcBoardDTO = new FunctionBoardDTO();
		ProjectService projectService = new ProjectService();
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		String funcBoardTitle = request.getParameter("funcBoardTitle");
		String funcBoardContent = request.getParameter("funcBoardContent");
		
		funcBoardDTO.setProjectNo(projectNo);
		funcBoardDTO.setFuncBoardNo(funcBoardNo);
		funcBoardDTO.setFuncBoardTitle(funcBoardTitle);
		funcBoardDTO.setFuncBoardContent(funcBoardContent);
		
		int result = projectService.modifyFuncBoard(funcBoardDTO);
		
		String path = "";
		if(result > 0 ) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("successCode", "modifyFuncBoard");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "modifyFuncBoard");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
