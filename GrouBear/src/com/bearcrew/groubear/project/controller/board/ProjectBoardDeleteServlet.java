package com.bearcrew.groubear.project.controller.board;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/board/delete")
public class ProjectBoardDeleteServlet extends HttpServlet {
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProjectService projectService = new ProjectService();
		ProjectDTO projectDTO = new ProjectDTO();
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		
		int result = projectService.deleteFuncBoard(funcBoardNo);
		
		String path = "";
		
		if(result > 0) { 
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("successCode", "deleteFuncBoard");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "deleteFuncBoard");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
}
