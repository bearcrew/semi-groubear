package com.bearcrew.groubear.project.controller.board;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.FunctionBoardDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/board/search")
public class ProjectBoardSearchServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<FunctionBoardDTO> fbList = new ArrayList<> ();
		ProjectDTO projectInfo;
		ProjectService projectService = new ProjectService();
		
		String path = "/WEB-INF/views/project/searchProjectBoard.jsp";
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		projectInfo = projectService.searchProjectInfo(projectNo);
		
		fbList = projectService.searchProjectBoradList(projectNo);
		
		if(fbList != null && projectInfo != null) {
			
			request.setAttribute("projectInfo", projectInfo);
			request.setAttribute("functionBoardList", fbList);
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}


}
