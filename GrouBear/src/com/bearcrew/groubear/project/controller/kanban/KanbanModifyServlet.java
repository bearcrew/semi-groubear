package com.bearcrew.groubear.project.controller.kanban;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.PartWorkDTO;
import com.bearcrew.groubear.project.model.dto.PartWorkMemberDTO;
import com.bearcrew.groubear.project.model.service.KanbanService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/kanban/modify")
public class KanbanModifyServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PartWorkDTO partWorkDTO = new PartWorkDTO();
		PartWorkMemberDTO partWorkMemberDTO = new PartWorkMemberDTO();
		KanbanService kanbanService = new KanbanService();

		int workListNo = Integer.parseInt(request.getParameter("workListNo")); // 업무리스트 번호
		int projectNo = Integer.parseInt(request.getParameter("projectNo")); // 프로젝트 번호
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo")); // 기능보드 번호
		int partWorkNo = Integer.parseInt(request.getParameter("partWorkNo")); // 단위업무 번호

		String partWorkName = request.getParameter("partWorkName"); // 단위업무 이름
		String partWorkContent = request.getParameter("partWorkContent"); // 단위업무 내용
		java.sql.Date startDate = java.sql.Date.valueOf(request.getParameter("startDate")); // 시작일
		java.sql.Date targetDate = java.sql.Date.valueOf(request.getParameter("targetDate")); // 목표일

		partWorkDTO.setWorkListNo(workListNo);
		partWorkDTO.setProjectNo(projectNo);
		partWorkDTO.setFuncBoardNo(funcBoardNo);
		partWorkDTO.setPartWorkName(partWorkName);
		partWorkDTO.setPartWorkNo(partWorkNo);
		partWorkDTO.setPartWorkContent(partWorkContent);
		partWorkDTO.setStartDate(startDate);
		partWorkDTO.setTargetDate(targetDate);

		int result1 = 0;
		int result2 = 0;

		result1 = kanbanService.modifyPartWork(partWorkDTO);

		  if(result1 > 0) {
			  
			  /* 단위업무 담당자 추가에 필요한 요소들 */
				String partWorkMember = request.getParameter("partWorkMember"); // 단위업무 담당자

				/* 문자열 분리 후 사원명과 프로젝트참여 인원번호로 분리 */
				String[] empList = partWorkMember.split(",");
				
				String[] empNList = new String[empList.length];
				String[] empName = new String[empList.length];
				int[] empNo = new int[empList.length];
				 
				for (int i = 0; i < empList.length; i++) {

					empNList[i] = empList[i].replaceAll("#", "");

					empName[i] = empNList[i].replaceAll("[0-9]", "");
					
					empNo[i] = Integer.parseInt(empNList[i].replaceAll("[ㄱ-힣]", ""));
					
					partWorkMemberDTO.setPartWorkNo(partWorkNo);
					partWorkMemberDTO.setEmpName(empName[i]);
					partWorkMemberDTO.setProjectMemberNo(empNo[i]);
					
					result2 = kanbanService.modifyPartWorkMember(partWorkMemberDTO); 
				} 
			}

		String path = "";
		if (result1 > 0 && result2 > 0) {
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("successCode", "partWorkModify");
			path = "/WEB-INF/views/common/success.jsp";
		} else {
			request.setAttribute("failedCode", "partWorkModify");
			path = "/WEB-INF/views/common/failed.jsp";
		}

		request.getRequestDispatcher(path).forward(request, response);
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		KanbanService service = new KanbanService();
		PartWorkDTO partWork = new PartWorkDTO();

		int workListNo = Integer.parseInt(request.getParameter("workListNo"));
		int partWorkNo = Integer.parseInt(request.getParameter("partWorkNo"));
		
		System.out.println("No : " + partWorkNo + " / "  + workListNo);
		
		partWork.setWorkListNo(workListNo);
		partWork.setPartWorkNo(partWorkNo);
		
		int result = service.changeWorkListNo(partWork);
		
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create(); 		
		
		String jsonString = gson.toJson(result);
		
		System.out.println(jsonString);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		if(jsonString != null) {
			out.print(jsonString);
		}
		
		out.close();
		
	}

}
