package com.bearcrew.groubear.project.controller.kanban;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.PartWorkMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.service.KanbanService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/projectMember/search")
public class PartWorkMemberServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		KanbanService kanbanService = new KanbanService();
		List<PartWorkMemberDTO> partWorkMembers = new ArrayList<>();
		
		/* 단위업무 번호 */
		int partWorkNo = Integer.parseInt(request.getParameter("$partWorkNo"));
		
		partWorkMembers = kanbanService.searchPartWorkMember(partWorkNo);
		
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.create();
		
		/* json 형식으로 변환 */
		String jsonString = gson.toJson(partWorkMembers);

		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();

		if (partWorkMembers != null) {
			out.print(jsonString);
		}

		out.close();
		
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		KanbanService kanbanService = new KanbanService();
		ProjectMemberDTO projectMemberDTO = new ProjectMemberDTO();
		List<ProjectMemberDTO> projectMembers = new ArrayList<>();
		
		/* 프로젝트 번호 */
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		projectMemberDTO.setProjectNo(projectNo);
		
		projectMembers = kanbanService.searchAllProjectMember(projectNo);
		
		Gson gson = new GsonBuilder()
				.setPrettyPrinting()
				.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.create();
		
		/* json 형식으로 변환 */
		String jsonString = gson.toJson(projectMembers);

		response.setContentType("application/json; charset=UTF-8");

		PrintWriter out = response.getWriter();

		if (projectMembers != null) {
			out.print(jsonString);
		}

		out.close();
		
	}

}
