package com.bearcrew.groubear.project.controller.kanban;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.service.KanbanService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/kanban/delete")
public class KanbanDeleteServlet extends HttpServlet {
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		KanbanService kanbanService = new KanbanService();
		
		int partWorkNo = Integer.parseInt(request.getParameter("partWorkNo"));
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		
		int result = kanbanService.deletePartWorkNo(partWorkNo);
		
		String path = "";
		
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("successCode", "partWorkDelete");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("failedCode", "partWorkDelete");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
