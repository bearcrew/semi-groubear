package com.bearcrew.groubear.project.controller.kanban;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.IssueDTO;
import com.bearcrew.groubear.project.model.dto.PartWorkDTO;
import com.bearcrew.groubear.project.model.dto.ProjectMemberDTO;
import com.bearcrew.groubear.project.model.service.IssueService;
import com.bearcrew.groubear.project.model.service.KanbanService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/kanban/search")
public class KanbanSearchServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		KanbanService kanbanService = new KanbanService();
		PartWorkDTO partWorkDTO = new PartWorkDTO();
		List<PartWorkDTO> partWorkList = new ArrayList<>();

		/* 기능보드 번호 */
		int funcBoardNo = Integer.parseInt(request.getParameter("funcBoardNo"));
		/* 프로젝트 번호 */
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));

		partWorkDTO.setFuncBoardNo(funcBoardNo);
		partWorkDTO.setProjectNo(projectNo);
		
		/* 기능보드 번호, 프로젝트번호로 단위업무 전체 조회 */
		partWorkList = kanbanService.searchAllPartWork(partWorkDTO);
		
		
		/*------------------------------------------------------------------*/
		//전달받은 값을 IssueDTO에 담는다
		IssueDTO issueDTO = new IssueDTO();
		issueDTO.setFuncBoardNo(funcBoardNo);
		issueDTO.setProjectNo(projectNo);
		
		//서비스로 값을 넘겨서 이슈목록을 List에 담는다
		IssueService issueService = new IssueService();
		List<IssueDTO> issueList = issueService.selectAllIssue(issueDTO);
		
		//서비스로 프로젝트 번호를 넘겨서 해당 프로젝트에 참여한 사원을 모두 조회한다
		List<ProjectMemberDTO> projectMemberList = issueService.selectAllProjectMember(projectNo);
		
		/*------------------------------------------------------------------*/

		/* 칸반보드 조회 페이지로 forward */
		String path = "";

		if (partWorkList != null) {
			path = "/WEB-INF/views/project/kanban.jsp";
			
			request.setAttribute("issueList", issueList);
			request.setAttribute("projectMemberList", projectMemberList);
			
			request.setAttribute("projectNo", projectNo);
			request.setAttribute("funcBoardNo", funcBoardNo);
			request.setAttribute("partWorkList", partWorkList);
			
		} else {
			/* 에러페이지 이동 */
		}

		request.getRequestDispatcher(path).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		KanbanService kanbanService = new KanbanService();
		PartWorkDTO partWorkInfo = new PartWorkDTO();
		
		/* 단위업무 번호 */
		int partWorkNo = Integer.parseInt(request.getParameter("$partWorkNo"));

		/* 단위업무 번호로 해당 단위업무 정보 조회 */
		partWorkInfo = kanbanService.searchPartWorkDetail(partWorkNo);

		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd")	//날짜포맷 변경
				.setPrettyPrinting()
				.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.create();
		
		/* json 형식으로 변환 */
		String jsonString = gson.toJson(partWorkInfo);

		response.setContentType("application/json; charset=UTF-8");

		PrintWriter out = response.getWriter();

		if (partWorkInfo != null) {
			out.print(jsonString);
		}

		out.close();
	}

}
