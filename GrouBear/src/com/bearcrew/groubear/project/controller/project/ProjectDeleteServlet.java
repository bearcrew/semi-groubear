package com.bearcrew.groubear.project.controller.project;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.service.KanbanService;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/delete")
public class ProjectDeleteServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProjectService projectService = new ProjectService();
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		int result = projectService.deleteProject(projectNo);
		
		String path = "";
		
		if(result > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "projectDelete");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "projectDelete");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
