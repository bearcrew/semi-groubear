package com.bearcrew.groubear.project.controller.project;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.InsertProjectMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/modify")
public class ProjectModifyServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ProjectService projectService = new ProjectService();
		
		int projectNo = Integer.parseInt(request.getParameter("projectNo"));
		
		int result = projectService.projectComplete(projectNo);
		
		String path = "";
		if(result > 0 ) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "modifyProject");
		} else {
			path = "";
			request.setAttribute("failedCode", "modifyProject");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProjectService projectService = new ProjectService();
		ProjectDTO projectDTO = new ProjectDTO();
		InsertProjectMemberDTO insertDTO = new InsertProjectMemberDTO();
		
		String projectTitle = request.getParameter("projectTitle");
		String borderColor = request.getParameter("borderColor");
		String projectContent = request.getParameter("projectContent");
		java.sql.Date startDate = java.sql.Date.valueOf(request.getParameter("startDate"));
		java.sql.Date targetDate = java.sql.Date.valueOf(request.getParameter("targetDate"));
		
		projectDTO.setProjectTitle(projectTitle);
		projectDTO.setBorderColor(borderColor);
		projectDTO.setProjectContent(projectContent);
		projectDTO.setStartDate(startDate);
		projectDTO.setTargetDate(targetDate);
		
		String memberNameList = request.getParameter("memberNameList");
		String memberNoList = request.getParameter("memberNoList");
		
		String[] empName = memberNoList.split(",");
		String[] memberNo = memberNoList.split(",");
		
		int[] empNo = new int[memberNo.length];
		
		for (int i = 0; i < memberNo.length; i++) {
			empNo[i] += Integer.parseInt(memberNo[i]);
		}
		
		int result1 = projectService.modifyProjectInfo(projectDTO);
		
		int result2 = 0;
		for (int i = 0; i < empNo.length; i++) {
			
			insertDTO.setEmpNo(empNo[i]);
			insertDTO.setEmpName(empName[i]);
			insertDTO.setProjectTitle(projectTitle);
			
			result2 = projectService.modifyProjectMember(insertDTO);
		}
		
		String path = "";
		if(result1 > 0 && result2 > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "modifyProject");
		} else {
			path = "";
			request.setAttribute("failedCode", "modifyProject");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
