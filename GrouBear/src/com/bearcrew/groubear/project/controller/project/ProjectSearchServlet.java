package com.bearcrew.groubear.project.controller.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.common.paging.Pagenation;
import com.bearcrew.groubear.common.paging.SelectCriteria;
import com.bearcrew.groubear.manager.empmanager.model.dto.EmpDTO;
import com.bearcrew.groubear.manager.empmanager.model.service.EmpService;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@WebServlet("/project/search")
public class ProjectSearchServlet extends HttpServlet {
	
	ProjectService projectService = new ProjectService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/*
		 * 목록보기를 눌렀을 시 가장 처음에 보여지는 페이지는 1페이지이다. 파라미터로 전달되는 페이지가 있는 경우 currentPage는 파라미터로
		 * 전달받은 페이지 수 이다.
		 */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;

		if (currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}

		/* 0보다 작은 숫자값을 입력해도 1페이지를 보여준다 */
		if (pageNo <= 0) {
			pageNo = 1;
		}

		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		if (searchValue == null) {
			searchValue = "";
		}

		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);

		/* 토탈카운트 = 프로젝트 전체 갯수 */
		int totalCount = projectService.selectTotalCount(searchMap);

		/* 한 페이지에 보여 줄 게시물 수 */
		int limit = 10; // 얘도 파라미터로 전달받아도 된다.
		/* 한 번에 보여질 페이징 버튼의 갯수 */
		int buttonAmount = 3;

		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환받는다. */
		SelectCriteria selectCriteria = null;

		if (searchCondition != null && !"".equals(searchCondition)) {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount, searchCondition,
					searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(pageNo, totalCount, limit, buttonAmount);
		}

	//------------------------------------------------------------------------------------	
		List<ProjectDTO> projectList = new ArrayList<>();
		
		projectList = projectService.searchAllProject(selectCriteria);
		
		String path = "";
		
		if(projectList != null) {
			path = "/WEB-INF/views/project/searchProject.jsp";
			request.setAttribute("selectCriteria" , selectCriteria);
			request.setAttribute("projectList", projectList);
		} else {
			/* failed page 이동 추가 */
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int empNo = 0;
		String email = "";
		
		if(request.getParameter("empNo") != null) {
			empNo = Integer.parseInt(request.getParameter("empNo"));
		} else {
			email = request.getParameter("email");
		}
		
		String empName = "";
		if(empNo > 0 ) {
			empName = projectService.searchEmpNameForEmpNo(empNo);
		} else {
			empName = projectService.searchEmpNameForEmail(email);
		}
		
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create(); 		
		
		String jsonString = gson.toJson(empName);
		
		response.setContentType("application/json; charset=UTF-8");
		
		PrintWriter out = response.getWriter();
		
		if(empName != null) {
			out.print(jsonString);
		}
		
		out.close();
		
	      }
		
	}

