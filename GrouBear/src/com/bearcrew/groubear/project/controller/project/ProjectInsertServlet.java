package com.bearcrew.groubear.project.controller.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bearcrew.groubear.project.model.dto.InsertProjectMemberDTO;
import com.bearcrew.groubear.project.model.dto.ProjectDTO;
import com.bearcrew.groubear.project.model.service.ProjectService;

@WebServlet("/project/insert")
public class ProjectInsertServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = "/WEB-INF/views/project/insertProject.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ProjectService projectService = new ProjectService();
		ProjectDTO projectDTO = new ProjectDTO();
		InsertProjectMemberDTO insertDTO = new InsertProjectMemberDTO();
		
		String projectTitle = request.getParameter("projectTitle");
		String borderColor = request.getParameter("borderColor");
		String projectContent = request.getParameter("projectContent");
		java.sql.Date startDate = java.sql.Date.valueOf(request.getParameter("startDate"));
		java.sql.Date targetDate = java.sql.Date.valueOf(request.getParameter("targetDate"));
		
		
		projectDTO.setProjectTitle(projectTitle);
		projectDTO.setBorderColor(borderColor);
		projectDTO.setProjectContent(projectContent);
		projectDTO.setStartDate(startDate);
		projectDTO.setTargetDate(targetDate);
		
		String memberList = request.getParameter("memberList");			// 김병준 [#12], 정우영 [#9], 윤현기 [#21]
		
		System.out.println("memberList : " + memberList);
		
		String[] splitMemberList = memberList.split(", ");	// [김병준 [#12], 정우영 [#9], 윤현기 [#21]]
		
		String[] empNameArray = new String[splitMemberList.length];
		int[] empNoArray = new int[splitMemberList.length];
		
		for(int i = 0; i < splitMemberList.length; i++) {
			String empNoStr = splitMemberList[i].replaceAll("[^0-9]", "");				// 수신 참조자 사번 숫자만 추출할 정규 표현식
			int empNo = Integer.parseInt(empNoStr);
			
			empNoArray[i] = empNo;
		
			
		}
		
		for(int i = 0; i < splitMemberList.length; i++) {
			String empName = splitMemberList[i].substring(0, 3);					// 수신 참조자 이름만 추출할 반복문
			empNameArray[i] = empName;
			
		}
		
		int result1 = projectService.insertProjectInfo(projectDTO);
		
		int result2 = 0;
		for (int i = 0; i < empNoArray.length; i++) {
			
			System.out.println("result2 init : " +  empNoArray[i] + " / " + empNameArray[i]);
			
			insertDTO.setEmpNo(empNoArray[i]);
			insertDTO.setEmpName(empNameArray[i]);
			insertDTO.setProjectTitle(projectTitle);
			
			result2 = projectService.insertProjectMember(insertDTO);
		}
		
		String path = "";
		if(result1 > 0 && result2 > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "insertProject");
		} else {
			path = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "insertProject");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	
	
}
