<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>
<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

<div class="content">
	<!-- 타이틀 -->
	<p class="menuName">
		<c:out value="${ sessionScope.boardTitle } 작성"></c:out>
	</p>

	<!-- 구분선 -->
	<hr id="p-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: 30px;">

	<!-- 게시글 작성 영역 -->
	<div class="table-area" align="center">
		<form
			action="${ pageContext.servletContext.contextPath }/post/notice/insert"
			method="post">
			<table class="b-table">
				<tr>
					<td class="b-th1"><font size="4">제목</font></td>
					<td class="b-td1">
						<font size="4">
							<input id="b-insert-title" placeholder="제목을 입력하세요" autofocus type="text" size="50" name="postTitle">
						</font></td>
				</tr>
				<tr>
					<td class="b-th1"><font size="4">작성자</font></td>
					<td class="b-td1"><font size="4">
						<input id="b-insert-writer" type="text" value="${ sessionScope.loginMember.empName }" name="writer" readonly>
					</font></td>
				</tr>
				<tr>
					<td class="b-th1"><font size="4">내용</font></td>
				</tr>
				<tr>
					<td class="b-td1" colspan="2">
					<textarea placeholder="내용을 입력하세요" id="b-insert-content" rows="7" cols="40" name="postContent" autofocus></textarea></td>
				</tr>
			</table>
			<br>
			
			<!-- 버튼영역 -->
			<div class="b-updatebtn">
				<button class="b-btn" type="reset" id="cancleNotice"
					onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/list?postNo=${ sessionScope.postNo }&boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }'">취소하기</button>
				<input type="hidden" value="${ sessionScope.boardCode }" name="boardCode">
				<button class="b-btn" type="submit">등록하기</button>
			</div>
		</form>
	</div>
</div>
</body>
</html>


