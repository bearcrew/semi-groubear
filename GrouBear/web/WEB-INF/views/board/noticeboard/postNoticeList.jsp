<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>

<div class="b-content">
 	<!-- sessionScope로 게시판 이름 출력 -->
	<p class="menuName"><c:out value="${ sessionScope.boardTitle }"></c:out></p>

	<hr id="p-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">

	<!-- 검색 폼 -->
	<div class="b-search-area2" align="center">
		<form id="loginForm"
			action="${ pageContext.servletContext.contextPath }/post/notice/list"
			method="get" style="display: inline-block">
			<input type="hidden" name="currentPage" value="1"> <select
				class="b-select2" id="searchCondition" name="searchCondition">
				<option value="writer"
					${ requestScope.selectCriteria.searchCondition eq "writer"? "selected": "" }>작성자</option>
				<option value="postTitle"
					${ requestScope.selectCriteria.searchCondition eq "postTitle"? "selected": "" }>제목</option>
				<option value="postContent"
					${ requestScope.selectCriteria.searchCondition eq "postContent"? "selected": "" }>내용</option>
			</select> <input class="b-input2" type="search" id="searchValue"
				name="searchValue"
				value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
			<input type="hidden" value="${ sessionScope.boardCode }"
				name="boardCode"> <input type="hidden"
				value="${ sessionScope.boardTitle }" name="boardTitle">
			<button class="b-btn b-search-btn2" type="submit">
				<font size="4">검색</font>
			</button>
		</form>
		
		<!-- 게시글 작성 버튼 -->
		<button class="b-btn b-write2" id="writeBtn" type="button"
			onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/insert?boardCode=${ sessionScope.boardCode }'">
			<font size="4">작성하기</font>
		</button>
	</div>
	
	<div>
		<!-- 글 목록 테이블 -->
		<table class="b-table2">
			<tr>
				<th class="b-th1"><font size="4">글번호</font></th>
				<th class="b-th2"><font size="4">글제목</font></th>
				<th class="b-th3"><font size="4">작성자</font></th>
				<th class="b-th4"><font size="4">조회수</font></th>
				<th class="b-th5"><font size="4">작성일</font></th>
			</tr>
			
			<!-- 게시글이 없을 경우 출력되는 -->
			<c:if test="${sessionScope.postNoticeList.size() == 0 }">
				<tfoot>
					<tr>
						<td align="center"><font size="3">게시글이 없습니다.</font></td>
					</tr>
				</tfoot>
			</c:if>
			<tbody>
				<!-- forEach문으로 게시글이 생성될 때 마다 게시글을 정보를 출력 -->
				<c:forEach var="notice" items="${ sessionScope.postNoticeList }">
					<tr>
						<td class="b-td1"><font size="4"><c:out value="${ notice.postNo }" /></font></td>
						<td class="b-td2"><font size="4"><c:out value="${ notice.postTitle }" /></font></td>
						<td class="b-td3"><font size="4"><c:out value="${ notice.writer }" /></font></td>
						<td class="b-td4"><font size="4"><c:out value="${ notice.count }" /></font></td>
						<td class="b-td5"><font size="4"><c:out value="${ notice.writeDate}" /></font></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

	<%-- 페이지 처리 --%>
	<jsp:include page="/WEB-INF/views/board/noticeboard/paging.jsp" />

</div>

<!-- 상세보기 스크립트 -->
<script>
	if (document.getElementsByTagName("td")) {

		const $tds = document.getElementsByTagName("td");
		for (let i = 0; i < $tds.length; i++) {

			$tds[i].onmouseenter = function() {
				this.parentNode.style.backgroundColor = "#D9D9D9";
				this.parentNode.style.cursor = "pointer";
			}

			$tds[i].onmouseout = function() {
				this.parentNode.style.backgroundColor = "white";
			}

			$tds[i].onclick = function() {
				const no = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/post/notice/detail?postNo="
						+ no;
			}

		}

	}
</script>
</body>
</html>

