<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>
<%pageContext.setAttribute("replaceChar", "\n");%>

<div class="p-content">
	<p class="menuName">게시글 상세보기</p>

	<hr id="p-underline"
		style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">

	<div>
		<!-- 게시글 상세보기 게시글 정보 출력 영역 -->
		<table class="b-table">
			<tr>
				<td class="b-th2"><font size="4">글번호</font></td>
				<td class="b-td2"><p><font size="5"> <c:out value="${ requestScope.postNoticeDetail.postNo }" /></font></p></td>
			</tr>
			<tr>
				<td class="b-th2"><font size="4">제목</font></td>
				<td class="b-td2"><p><font size="5"><c:out value="${ requestScope.postNoticeDetail.postTitle }" /></font></p></td>
			</tr>
			<tr>
				<td class="b-th2"><font size="4">작성자</font></td>
				<td class="b-td2"><p><font size="5"><c:out value="${ requestScope.postNoticeDetail.writer }" /></font></p>
				</td>
			</tr>
			<tr>
				<td class="b-th2"><font size="4">작성일</font></td>
				<td class="b-td2"><p><font size="5"><c:out value="${ requestScope.postNoticeDetail.writeDate }" /></font></p></td>
			</tr>
			<tr>
				<td class="b-th1"><font size="5">내용</font></td>
				<td class="b-td2" colspan="2"><font size="5">
				<textarea id="b-detail-content" readonly><c:out value="${ requestScope.postNoticeDetail.postContent }" /></textarea></font></td>
			</tr>
		</table>
		
		<!-- get방식으로 boradCode를 보내기 위해 hidden으로 생성 -->
		<input type="hidden" name="boardCode" value="${ requestScope.postNoticeDetail.boardCode }">
		
		<!-- 버튼 영역 -->
		<div class="b-detailbtn">
			<button class="b-btn" type="reset" id="backPostNoticeList"
				onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }'">돌아가기</button>
			<button class="b-btn" type="submit"
				onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/update?no=${ requestScope.postNoticeDetail.postNo }&boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }'">수정하기
			</button>
			<button id="delbtn" class="b-btn">삭제하기</button>
		</div>
	</div>
</div>

<!-- 삭제 메세지 창 -->
<script>
	document.getElementById("delbtn").onclick = function() {
		var result = confirm("정말 삭제하시겠습니까?");
		if (result) {
			location.href = '${ pageContext.servletContext.contextPath }/post/notice/delete?postNo=${ requestScope.postNoticeDetail.postNo }&boardCode=${ requestScope.postNoticeDetail.boardCode }'
		} else {

		}
	}
</script>
</body>
</html>