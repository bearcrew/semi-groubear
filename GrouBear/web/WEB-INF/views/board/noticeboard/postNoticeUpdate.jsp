<%@ page language="java" contentType="text/html; charset=UTF-8"pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>
<div class="content">
	<p class="menuName">게시글 수정</p>

	<hr id="p-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: 30px;">

	<!-- 게시글 수정 영역 -->
	<div align="center" class="table-area">
		<form action="${ pageContext.servletContext.contextPath }/post/notice/update" method="post">
			<table class="b-table">
				<tr>
				 	<!-- input태그를 이용해 제목을 입력할 수 있다 -->
					<td class="b-th1"><font size="4">제목</font></td>
					<td class="b-td1"><font size="4">
						<input id="b-update-title" type="text" size="50" name="postTitle" value="${ requestScope.postNoticeDetail.postTitle }">
						</font>
					</td>
				</tr>
				<tr>
					<!-- 작성자는 readOnly로 수정할 수 없음 -->
					<td class="b-th1"><font size="4">작성자</font></td>
					<td class="b-td1"><font size="4">
						<input id="b-update-writer" type="text" value="${ requestScope.postNoticeDetail.writer }" name="writer" readonly>
						</font>
					</td>
				</tr>
				<tr>
					<!-- 작성일은 readOnly로 수정할 수 없음 -->
					<td class="b-th1"><font size="4">작성일</font></td>
					<td class="b-td1"><font size="4">
						<input id="b-update-date" type="text" value="${ requestScope.postNoticeDetail.writeDate }" name="writeDate" readonly>
						</font>
					</td>
				</tr>
				<tr>
					<td class="b-th1"><font size="4">내용</font></td>
				</tr>
				<tr>
				<!-- textarea에 기존 내용과 수정할 텍스트를 입력할 수 있다. -->
					<td class="b-td1" colspan="2"><font size="4">
						<textarea id="b-update-content" rows="7" cols="40" name="postContent" autofocus>${ requestScope.postNoticeDetail.postContent }
						</textarea></font>
					</td>
				</tr>
			</table>
			<br> 
			
			<!-- 버튼에 postNo와 boardCode를 담아서 넘겨준다 -->
			<input type="hidden" name="postNo" value="${ requestScope.postNoticeDetail.postNo }"> 
			<input type="hidden" name="boardCode" value="${ requestScope.postNoticeDetail.boardCode }">
			
			<!-- 버튼 영역 -->
			<div class="b-updatebtn">
				<button class="b-btn" type="reset" id="cancleBoard"
					onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/detail?postNo=${ requestScope.postNoticeDetail.postNo }'">취소하기</button>
				<button class="b-btn" type="submit">수정하기</button>
			</div>
		</form>
	</div>
</div>
</body>
</html>