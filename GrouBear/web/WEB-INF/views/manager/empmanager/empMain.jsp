<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>

<div class="m-emp-content">

	<p class="menuName">사원 관리</p>
	<hr id="m-underline"  style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
	
	<div class="projectPage" >
			<a style="color: #BBBBBB;">사원 상세정보 (추가/삭제/변경) </a>			
		</div>
	
	<jsp:include page="empList.jsp" />
	
	<jsp:include page="/WEB-INF/views/manager/orgmanager/treeView.jsp" />

</div>

</body>
</html>