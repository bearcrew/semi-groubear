<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/emp.css">
	
	<div class="m-empArea">
	<div class="m-empAreaRight">
	<div class="m-search-area">
			 <form id="m-empSearchForm" action="${ pageContext.servletContext.contextPath }/manager/emp/list" method="GET">		
			 
			 <!-- 검색 카테고리 목록 -->
			 <select id="m-searchCondition" name="searchCondition"> 
					<option value="all" ${ requestScope.selectCriteria.searchCondition eq "all"? "selected": "" }>전체</option>
					<option value="empNo" ${ requestScope.selectCriteria.searchCondition eq "empNo"? "selected": "" }>사원번호</option>
					<option value="empName" ${ requestScope.selectCriteria.searchCondition eq "empName"? "selected": "" }>사원명</option>
					<option value="deptTitle" ${ requestScope.selectCriteria.searchCondition eq "deptTitle"? "selected": "" }>부서명</option>
					<option value="gender" ${ requestScope.selectCriteria.searchCondition eq "gender"? "selected": "" }>성별(남/여)</option>
					<option value="entN" ${ requestScope.selectCriteria.searchCondition eq "entN"? "selected": "" }>재직</option>
					<option value="entY" ${ requestScope.selectCriteria.searchCondition eq "entY"? "selected": "" }>퇴사</option>
			</select>
			
			<input type="search" id="m-searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>" placeholder="검색어를 입력해주세요">
			<button id="m-searchBtn" type="submit">검색</button>
			<input type="button" value="사원 ID생성" id="m-createIdBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/emp/insert'">
			
			</form>
			
		</div>
		
		<br>
		
		<hr id="m-hr" >
		
		<div class="orderByHireDate" >
		
		<!-- 입사일 (오름/내림차순) 정렬 버튼 -->
		<select id="m-orderByHireDate" name="orderCondition" style="">
			<option id="orderUp" value="up">입사일▲</option> 
	   		<option id="orderDown" value="down">입사일▼</option> 		
		</select>
		</div>
		
		<div class="orderByJobGrade" >	
			
		<!-- 직급 (오름/내림차순) selectBox -->
		<select id="m-orderByJobGrade" name="orderCondition" style="">
			<option id="jobGradeUp" value="up">직급▲</option> 
	   		<option id="jobGradeDown" value="down">직급▼</option> 		
		</select>
		</div>
				
		<div id="me-Table-area"> 
		<!-- 조회해온 사원목록 출력하는 테이블 -->
			<table class="me-Table-area" id="listArea">
				<tr class="m-empListHead" >
					<th class="m-th1" style="font-weight: bold; font-size: 20px;">사원번호</th>
					<th class="m-th2" style="font-weight: bold; font-size: 20px;">사원명</th>
					<th class="m-th3" style="font-weight: bold; font-size: 20px;">소속부서</th>
					<th class="m-th4" style="font-weight: bold; font-size: 20px;">&nbsp&nbsp&nbsp&nbsp&nbsp직급</th>
					<th class="m-th5" style="font-weight: bold; font-size: 20px;">성별</th>
					<th class="m-th6" style="font-weight: bold; font-size: 20px;">&nbsp&nbsp&nbsp&nbsp&nbsp입사일</th>				
					<th class="m-th7" style="font-weight: bold; font-size: 20px;"></th>
					
				</tr>
				
				<c:forEach items="${ requestScope.empList }" var="emp">
					
				<!-- 퇴사자는 배경색 다르게 설정 -->
					<c:if test="${fn:contains(emp.entYn, '퇴사')}">			
					<tr id="m-empList" align="center">	
						<td class="m-entN1" id="no" style="width:110px; height:40px; border:1px solid gray; font-size:18px; text-align: center; text-decoration:line-through; background-color: lightGray;"><c:out value="${ emp.no }"/></td>
						<td class="m-entN2"  id="name" style="width:120px; height:40px; border:1px solid gray; font-size:18px; text-align: center; background-color: lightGray;"><c:out value="${ emp.name }"/></td>
						<td class="m-entN3" id="deptTitle" style="width:160px; height:40px; border:1px solid gray; font-size:18px; text-align: center; text-decoration:line-through; background-color: lightGray;"><c:out value="${ emp.deptTitle }"/></td>
						<td class="m-entN4" id="jobGrade" style="width:140px; height:40px; border:1px solid gray; font-size:18px; text-align: center; text-decoration:line-through; background-color: lightGray;"><c:out value="${ emp.jobGrade }"/></td>
						<td class="m-entN5" id="geender" style="width:120px; height:40px; border:1px solid gray; font-size:18px; text-align: center; text-decoration:line-through; background-color: lightGray;"><c:out value="${ emp.gender }"/></td>
						<td class="m-entN6" id="phone" style="width:170px; height:40px; border:1px solid gray; font-size:18px; text-align: center; text-decoration:line-through; background-color: lightGray;"><c:out value="${ emp.hireDate }"/></td>
						<td class="m-entN7" id="authorityName" style="width:180px; height:40px; border:none; font-size:18px; text-align: center; text-decoration:line-through;">
						<input id="m-infoBtn" type="button" value="상세정보" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/emp/detail?no=${ emp.no }'"/>
						</td>
					</tr>	
				</c:if>	
				
					<c:if test="${fn:contains(emp.entYn, '재직')}">
					<tr id="m-empList" align="center">				
						<td class="m-td1 m-td" id="no"><c:out value="${ emp.no }"/></td>
						<td class="m-td2 m-td" id="name"><c:out value="${ emp.name }"/></td>
						<td class="m-td3 m-td" id="deptTitle"><c:out value="${ emp.deptTitle }"/></td>
						<td class="m-td4 m-td" id="jobGrade"><c:out value="${ emp.jobGrade }"/></td>
						<td class="m-td5 m-td" id="entYn"><c:out value="${ emp.gender }"/></td>
						<td class="m-td6 m-td" id="hireDate"><c:out value="${ emp.hireDate }"/></td>
						<td class="m-td7 m-td" >						
					<!-- 선택한 사원의 상세정보 페이지로 이동하는 버튼  -->
						<input id="m-infoBtn" type="button" value="상세정보" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/emp/detail?no=${ emp.no }'"/>
						</td>
					</tr>	 
				 </c:if>
					
				</c:forEach>								
							
			</table>
			</div>
			
			<div id="me-paging">		
			<!-- 사원정보 테이블아래쪽 페이징 버튼 -->	
				<jsp:include page="empPaging.jsp"/> 
			</div> 
		</div>
	</div>
	
<script>

/* selectBox의 옵션(사원명▲,사원명▼) 선택해 변경할 경우 동작하는 함수 */
 
$('#m-orderByJobGrade').change(function() {
	var state = $('#m-orderByJobGrade option:selected').val();		
	
	$.ajax({
		url : "${ pageContext.servletContext.contextPath }/manager/emp/list",
		type : "POST",
		dataType: "json",
		data : {"searchCondition" : (state == "up"? "jobGradeU": "jobGradeD"), "searchValue" : "" },			
		success: function (data) {
			
				/* #listArea > #m-empList의 전체 내용 비워 주기 */
			
			 	$("#listArea >tbody").children("#m-empList").each(function() {	
 					$(this).remove()
 				})
			 
 				/* 반복문 사용해 검색된 사원마다 사원정보를 td에 넣어 row를 추가 */
 				
				for(var i in data){				
					const no = data[i].no;
					
					var row = "<tr id='m-empList' align='center'>"
					row += "<td class='m-td1 m-td' id='no'>"+ no +"</td>"
					row += "<td class='m-td2 m-td' id='name'>"+ data[i].name +"</td>"
					row += "<td class='m-td3 m-td' id='deptTitle'>"+ data[i].deptTitle +"</td>"
					row += "<td class='m-td4 m-td' id='jobGrade'>"+ data[i].jobGrade +"</td>"
					row += "<td class='m-td5 m-td' id='entYn'>"+ data[i].entYn +"</td>"
					row += "<td class='m-td6 m-td' id='entYn'>"+ data[i].hireDate +"</td>"
					row += '<td class="m-td7 m-td"><input id="m-infoBtn" type="button" value="상세정보" onclick="location.href='+ "'" + "${ pageContext.servletContext.contextPath }/manager/emp/detail?no="+ no + "'" + '"/></td>'
					row += "</tr>"
					$("#listArea").append(row)
			} 
		}
	})
});


/* selectBox의 옵션(입사일▲,입사일▼) 선택해 변경할 경우 동작하는 함수 */

$('#m-orderByHireDate').change(function() {
	var state = $('#m-orderByHireDate option:selected').val();		
	
	$.ajax({
		url : "${ pageContext.servletContext.contextPath }/manager/emp/list",
		type : "POST",
		dataType: "json",
		data : {"searchCondition" : (state == "up"? "hireDateU": "hireDateD"), "searchValue" : "" },			
		success: function (data) {
			
				/* #listArea > #m-empList의 전체 내용 비워 주기 */
			
			 	$("#listArea >tbody").children("#m-empList").each(function() {	
 					$(this).remove()
 				})
			 
 				/* 반복문 사용해 검색된 사원마다 사원정보를 td에 넣어 row를 추가 */	
 				
				for(var i in data){				
					const no = data[i].no;
					
					var row = "<tr id='m-empList' align='center'>"
					row += "<td class='m-td1 m-td' id='no'>"+ no +"</td>"
					row += "<td class='m-td2 m-td' id='name'>"+ data[i].name +"</td>"
					row += "<td class='m-td3 m-td' id='deptTitle'>"+ data[i].deptTitle +"</td>"
					row += "<td class='m-td4 m-td' id='jobGrade'>"+ data[i].jobGrade +"</td>"
					row += "<td class='m-td5 m-td' id='entYn'>"+ data[i].entYn +"</td>"
					row += "<td class='m-td6 m-td' id='entYn'>"+ data[i].hireDate +"</td>"
					row += '<td class="m-td7 m-td"><input id="m-infoBtn" type="button" value="상세정보" onclick="location.href='+ "'" + "${ pageContext.servletContext.contextPath }/manager/emp/detail?no="+ no + "'" + '"/></td>'
					row += "</tr>"
					$("#listArea").append(row)
			} 
		}
	})
});

</script>



	
