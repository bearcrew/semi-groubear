<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 

<style>
.e-hcontent{
	position: absolute;
	left: 241px;
	top: 81px;
	height: 150px;
	width: 1662px;
}
.e-menuName {
	position: absolute;
	width: 412px;
	height: 51px;
	left: 50px;
	top: 38px;
   
	font-family: 'Noto Sans KR', sans-serif;
	font-style: normal;
	font-weight: bold;
	font-size: 37.33px;
	line-height: 51px;
   
	display: flex;
	align-items: center;
	letter-spacing: -0.04em;
}
.e-registContent {
	position: absolute;
	left: 300px;
	top: 260px;
	height: 680px;
	width: 1340px;
	border: 1px solid black;
}
.e-from1{
	position: absolute;
	left: 100px;
	top: 10px;
}
.e-form2{
	position: absolute;
	left: 100px;
	top: 320px;
}
.e-basicTable{
	position: absolute;
	width: 1140px;
	height: 250px;
	border: 3px solid #75CB5E;
	border-radius: 16px;
	/* box-shadow: inset 0 0 3px #75CB5E; */
}
.e-phoneTable{
	position: absolute;
	width: 1140px;
	height: 250px;
	border: 3px solid #75CB5E;
	border-radius: 16px;
}
.e-basicTable > tbody > tr > td {
	font-size: 12pt;
	border: 1px solid #75CB5E;
	padding: 10px;
}
.e-phoneTable > tbody > tr > td {
	font-size: 12pt;
	border: 1px solid #75CB5E;
	padding: 10px;
}
.e-basicTable > tbody > tr > th {
	font-size: 15pt;
	border: 2px solid white;
	background: #75CB5E;
	color: white;
	text-align: center;
	border-radius: 5%;
	width: 130px;
}
.e-phoneTable > tbody > tr > th {
	font-size: 15pt;
	border: 2px solid white;
	background: #75CB5E;
	color: white;
	text-align: center;
	border-radius: 5%;
	width: 130px;
}

.e-input{
	border: 2px solid #75CB5E;
	width: 300px;
}
.e-detailAddress{
	border: 2px solid #75CB5E;
	width: 420px;
}
.e-idCheck{
	background: #75CB5E;
	color: white;
	width: 80px;
	border-radius: 5px;
	font-weight: bold;
	border-color: #75CB5E;
}
.e-pwdCheck{
	background: #75CB5E;
	color: white;
	width: 110px;
	border-radius: 5px;
	font-weight: bold;
	border-color: #75CB5E;
}
.e-searchZipCode{
	background: #75CB5E;
	color: white;
	width: 80px;
	border-radius: 5px;
	font-weight: bold;
	border-color: #75CB5E;
}
.e-gender{
	width: 50px;
}
.e-btns{
	position: absolute;
	left: 600px;
	top: 640px;
}
.e-btns2{
	font-size: 10pt;
	font-weight: bold;
	border-color: #75CB5E;
	border-radius: 5px;
	background: #75CB5E;
	color: white;
	width: 70px;
}
</style>

<!-- 제목 start -->
<div class="e-hcontent">
	<p class="e-menuName">사원 ID 생성</p>
	<hr id="e-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
</div>
<!-- 제목 end -->

<!-- 사원 기본 정보 입력 start -->
<div class="e-registContent">
	<form action="${ pageContext.servletContext.contextPath }/manager/emp/insert" method="post">
		<div class="e-from1">
			<h3>사원 기본 정보</h3>
			<table class="e-basicTable" style="border-radius: 10px;">
				<tr>
					<th>사원번호</th>
					<td><input class="e-input" type="text" name="empNo" readonly="readonly"></td>
					<th>부서</th>
					<td><select name="deptTitle" style="border: 2px solid #75CB5E;">
							<c:forEach items="${ requestScope.allDeptTitle }" var="allDeptTitle">
								<option><c:out value="${ allDeptTitle.deptTitle }"/></option>
							</c:forEach>
						</select></td>
				</tr>
				<tr>
					<th>사원명</th>
					<td><input class="e-input" type="text" name="empName" required></td>
					<th>입사일</th>
					<td><input class="e-input" type="date" name="hireDate" required></td>
				</tr>
				<tr>
					<th>아이디</th>
					<td><input class="e-input" type="text" name="registEmpId" id="registEmpId" required> <input class="e-idCheck" type="button" value="중복확인" id="idCheck"></td>
					<th>직위</th>
					<td><select name="jobGrade" style="border: 2px solid #75CB5E;">
							<c:forEach items="${ requestScope.allJobGrade }" var="allJobGrade">
								<option><c:out value="${ allJobGrade.jobGrade }"/></option>
							</c:forEach>
						</select></td>
				</tr>
				<tr>
					<th>비밀번호</th>
					<td><input class="e-input" type="password" name="pwd" id="pwd" required></td>
					<th>비밀번호 확인</th>
					<td><input class="e-input" type="password" name="pwd2" id="pwd2" required> <input class="e-pwdCheck" type="button" value="비밀번호확인" id="pwdCheck"></td>
				</tr>
				<tr>
					<th>성별</th>
					<td><input class="e-gender" type="radio" name="gender" value="남" checked="checked">남<input class="e-gender" type="radio" name="gender" value="여">여</td>
					<th>생년월일</th>
					<td><input class="e-input" type="date" name="birthDate" required></td>
				</tr>
			</table>
		</div>
	
		<div class="e-form2">
			<h3>사원 연락처 정보</h3>
			<table class="e-phoneTable">
				<tr>
					<th>전화번호</th>
					<td width="422px"><input class="e-input" type="text" name="homeTel"></td>
					<th>휴대전화</th>
					<td><input class="e-input" type="text" name="phone" required></td>
				</tr>
				<tr>
					<th>팩스번호</th>
					<td width="422px"><input class="e-input" type="text" name="fax"></td>
					<th>긴급연락처</th>
					<td><input class="e-input" type="text" name="emergencyContact"></td>
				</tr>
				<tr>
					<th>우편번호</th>
					<td width="422px"><input class="e-input" type="text" name="zipCode" id="zipCode" readonly required> <input class="e-searchZipCode" type="button" value="주소검색" id="searchZipCode"></td>
					<th>이메일주소</th>
					<td><input class="e-input" type="email" name="email" required></td>
				</tr>
				<tr>
					<th>주소</th>
					<td width="422px"><input class="e-input" type="text" name="address1" id="address1" readonly required></td>
					<th>상세주소</th>
					<td><input class="e-detailAddress" type="text" name="address2" id="address2" required></td>
				</tr>
			</table>
			<br>
		</div>
		
		<div class="e-btns">
			<input class="e-btns2" type="reset" value="생성취소" id="cancle"> <input class="e-btns2" type="submit" value="  ID생성  "> 
		</div>
	</form>
</div>
<!-- 사원 기본 정보 입력 end -->

<!-- 우편번호 검색  -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script>

	/* searchZipCode라는 id를 가진 요소를 $searchZipCode에 담는다 */
	const $searchZipCode = document.getElementById("searchZipCode");
	
	/* $searchZipCode클릭시 동작할 함수 */
	$searchZipCode.onclick = function() {
	
		/* 다음 우편번호 검색 창을 오픈하면서 동작할 콜백 메소드를 포함한 객체를 매개변수로 전달한다. */
		new daum.Postcode({
			oncomplete: function(data){
				
				/* 팝업에서 검색결과 항목을 클릭했을 시 실행할 코드를 작성하는 부분 */
				document.getElementById("zipCode").value = data.zonecode;
				document.getElementById("address1").value = data.address;
				document.getElementById("address2").focus();
			}
		}).open();
	}
	
	/* 취소 버튼 동작 */
	const $cancle = document.getElementById("cancle");
	$cancle.onclick = function() {
		location.href = "${ pageContext.servletContext.contextPath }";
	}
	
	/* 비밀번호 확인 버튼 동작 */
	const $pwdCheck = document.getElementById("pwdCheck");
	$pwdCheck.onclick = function() {
	var pwd = document.getElementById("pwd").value;
	var pwd2 = document.getElementById("pwd2").value;
		if(pwd == pwd2) {
			alert("사용 가능한 비밀번호 입니다.");
		} else {
			alert("비밀번호가 일치하지 않습니다.");
		}
	}
	
	/* 아이디 중복확인 버튼 동작 */
	$("#idCheck").click(function() {
		
		const registEmpId = $("#registEmpId").val();
		
		/* ajax로 값을 전달한 후 데이터를 리턴 받는다 */
		$.ajax({
			url: "/groubear/manager/idcheck",
			type: "get",
			data: { registEmpId: registEmpId },
			success: function(data, textStatus, xhr) {
				alert(data);
			},
			error: function(xhr, status, error) {
				consloe.log(xhr);
			}
		});
	});
	
	
</script>

</body>
</html>