<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
   
<div class="emp-content">  <!-- content -->
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="${ pageContext.servletContext.contextPath }/resources/js/emp.js"></script>
   
         <p class="menuName">사원정보 관리</p>         
         <hr id="p-underline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
        
        
        <form id="updateForm"action="${ pageContext.servletContext.contextPath }/manager/emp/update" method="post"><!-- form태그 start -->
         
         <div class="m-detail">
            
            <div class="m-detailLeftFrame">
            
               <div class="m-profileHeader">
               		<h4>◈프로필 사진</h4>
               </div>
               
			<!-- [사진] start -->         
               <div class="m-profileSpan">
               		<div class="m-profileLeft" align="left">
               			<a>사진</a>
               		</div>
               		<div class="m-profileRight">
               			<img src="/groubear/resources/profileImages/${ emp.profileImageName }"
               			 style="width:210px; height:250px; background-color:none; margin:13px auto;display: block; object-fit: cover;">
               		</div>
               </div>
               
               <div class="m-profileAdd">
               <a><input type="file"></a>
               </div>
			<!-- [사진] end -->               
               
			<!-- [서명] start -->               
               <div class="m-signSapn">
               		<div class="m-signLeft">
               			<a>서명</a>
               		</div>
               		<div class="m-signRight" style="text-align:center">
               			<img src="/groubear/resources/signImages/${ emp.signImageName }" 
               			style="width:150px; height:150px; margin:25px auto; display: block; object-fit: cover;">
               		</div>
               </div>
               
               <div class="m-signAdd">
               		<a><input type="file"></a>
               </div>
            </div>
			<!-- [서명] end -->               
            
            <div class="m-detailRightFrame">
               <div class="m-rightHeader">
                  <h4>◈사원 기본 정보</h4>
               </div>
               
			<!-- [사원기본정보 table] start -->               
               <div class="m-empInfo">
               		<table id="m-empInfoTable">
               			<tbody>
							<tr>
								<td id="m-empinfo1" class="m-empinfo">사원번호</td>
								<td id="m-empinfo2" class="m-empinfo">								
								<input style="width: 205px; height: 40px; background: none; border: none; ;"  type="text" id="empNo" name="empNo" value="${ emp.no }" readonly/>
								</td>
								
								<td id="m-empinfo3">부서</td>
								
								<!-- 부서목록 셀렉트박스 -->
								<td id="m-empinfo4">
									<select style="width:165px; height:40px; border: none; background: none;" name="deptTitle">
										<c:forEach var="dept" items="${ requestScope.deptList }">
											<option value="<c:out value="${ dept.deptTitle }" />"
												<c:if test="${dept.deptTitle == emp.deptTitle }">selected="selected"</c:if>
												<c:if test="${dept.deptTitle == 'GrouBear'}">hidden="hidden"</c:if>>
												<c:out value="${dept.deptTitle }" />
											</option>
										</c:forEach>
									</select>
								</td>

								<td id="m-empinfo5" class="m-empinfo">재직상태</td>	
															
								<!-- 퇴사여부 select박스 -->
								<td id="m-empinfo6" class="m-empinfo">
									<select id="entYnStatus" style="width:75px; height:40px; border: none; background: none;" id="entYn" name="entYn">
										<c:if test="${ emp.entYn eq '재직' }">
											<option selected="selected">재직</option>
											<option>퇴사</option>
										</c:if>
										<c:if test="${ emp.entYn eq '퇴사' }">
											<option selected="selected">퇴사</option>
											<option>재직</option>
										</c:if>
									</select>
								</td>
							</tr>
							 
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>
							              			
							<tr>							
								<td id="m-empinfo1" class="m-empinfo">사원명</td>
								<td id="m-empinfo2" class="m-empinfo">
								<input type="text"  class="m-empinfo" name="empName" value="${ emp.name }"  style="width: 205px; height: 40px; border: none; background-color: none;">
								</td>
								
								<td id="m-empinfo3">입사일</td>
								<td id="m-empinfo4"  class="m-empinfo">
								<input type="date" name="hireDate" class="m-empinfo" value="${ emp.hireDate }" style="width: 145px; height: 40px; border: none; background-color: none;">
								</td>
								
								<td id="m-empinfo5">퇴사일</td>
								<td id="m-empinfo6" class="m-empinfo">
								
								<!-- 퇴사자가 재입사할 경우 퇴사일 초기화 -->
								<input id="entDate"  class="m-empinfo" type="date" name="entDate" value="${ emp.entDate }" style="width: 135px; height: 40px; border: none; background-color: none;">
               					<input type="button"  class="m-empinfo"id="entBtn" value="퇴사일초기화" onclick="entDateReset()">
               					</td>								
							</tr>    
							           			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>
							              			
							<tr>
								<td id="m-empinfo1">아이디</td>
								<td id="m-empinfo2">
								<input type="text"  class="m-empinfo" name="empId" value="${ emp.id }" readonly="readonly"  style="width: 205px; height: 40px; background: none; border: none;">
								</td>
								
								<td id="m-empinfo3">직위</td>
								<td id="m-empinfo4">
								
									<!-- 직위목록 셀렉트박스 --> 
									<select name="jobGrade" style="width:95px; height:40px; border: none; background: none;">
										<c:forEach var="job" items="${ requestScope.jobList }">
											<option value="<c:out value="${ job.jobGrade }" />"
												<c:if test="${job.jobGrade == emp.jobGrade }">selected="selected"</c:if>>
												<c:out value="${job.jobGrade }" />
											</option>
										</c:forEach>
									</select>
								</td>
								
								<td id="m-empinfo5">권한</td>
								<td id="m-empinfo6">
								
									<!-- 권한목록 셀렉트박스 -->
									 <select name="authority" style="width:245px; height:40px; border: none; background: none;">
										<c:forEach var="authority"
											items="${ requestScope.authorityList }">
											<option
												value="<c:out value="${ authority.authorityName }" />"
												<c:if test="${emp.authorityName == null }">selected="selected"</c:if>
												<c:if test="${emp.authorityName == authority.authorityName }">selected="selected"</c:if>>
												<c:out value="${authority.authorityName }" />
											</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr> 
							             			
							<tr>
								<td id="m-empinfo1">성별</td>
								<td id="m-empinfo8" >
								
									<!-- 성별 select 박스 --> 
									<c:if test="${fn:contains(emp.gender, '남')}">
                        				&nbsp&nbsp&nbsp남<input type="radio" id="gender" name="gender" value="남" checked="checked"  style="width: 35px; height: 20px; text-align: center">
                        				여<input type="radio" id="gender" name="gender" value="여"  style="width: 35px; height: 20px; text-align: center">
                        				</c:if>
                        		    <c:if test="${fn:contains(emp.gender, '여')}">
                        				&nbsp&nbsp&nbsp여<input type="radio" id="gender" name="gender"	value="여" checked="checked"  style="width: 35px; height: 20px; text-align: center">
                        				남<input type="radio" id="gender" name="gender" value="남"  style="width: 35px; height: 20px; text-align: center">
									</c:if>
									
								</td>
								
								<td id="m-empinfo3">생년월일</td>
								<td id="m-empinfo7" >
									<input type="date" name="birthDate"  class="m-empinfo" value="${ emp.birthDate }" style="width: 145px; height: 40px; border: none;"/>
								</td>
							</tr> 
							              			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
               			</tbody>
               		</table>
				<!-- [사원기본정보 table] end -->     
				          
               			<br>
               			<hr>
               			
               			<div style="width: 100%; height: 35px; margin-top: 20px;">
	               			<h4>◈사원 연락처 정보</h4>
               			</div>
               			
				<!-- [사원연락처정보 table] start -->               
               		<table id="m-empCallTable" >
               			<tbody>
							<tr>
								<td id="m-empinfo1">전화번호</td>
								<td id="m-empinfo2"><c:out value="${ emp.homeTel }" /></td>
								<td id="m-empinfo1">휴대전화</td>
								<td id="m-empinfo2"><c:out value="${ emp.phone }" /></td>
							</tr>  
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="m-empinfo1">팩스번호</td>
								<td id="m-empinfo2"><c:out value="${ emp.fax }" /></td>
								<td id="m-empinfo1">긴급연락처</td>
								<td id="m-empinfo2"><c:out value="${ emp.emergencyContact }" /></td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="m-empinfo1">우편번호</td>
								<td id="m-empinfo2"><c:out value="${ emp.addressNo }" /></td>
								<td id="m-empinfo1">이메일주소</td>
								<td id="m-empinfo2"><c:out value="${ emp.email }" /></td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              		
							<tr>
								<td id="m-empinfo1">주소</td>
								<td id="m-empinfo2"><c:out value="${ emp.address }" /></td>
								<td id="m-empinfo1">상세주소</td>
								<td id="m-empinfo2"><c:out value="${ emp.detailAddress }" /></td>
							</tr>               			
               			</tbody>
               		</table>
				<!-- [사원연락처정보 table] end -->               
               
               </div>
            </div>
         </div>
         
		<!-- [버튼 area] start -->               
         <div class="m-empDetailBtnArea">
          <button class="eback-btn" type="button" onclick="history.back()">뒤로가기</button>
          <button class="em-btn" id="changeEmpInfo" type="submit">정보수정</button>
         </div>
	<!-- [버튼 area] end -->               
         
       </form> <!-- form태그 end -->  
      
   </div> <!-- content end -->
   
</body>
</html>
