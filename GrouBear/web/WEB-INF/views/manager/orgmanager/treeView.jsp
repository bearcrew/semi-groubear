<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script
	src="${pageContext.servletContext.contextPath}/resources/js/jquery.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/jquery.treeview.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/organization.js"
	type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		$("#tree").treeview({
			collapsed : true,
			animated : "medium",
			control : "#sidetreecontrol",
			persist : "location"
		});
	})

	/* 선택한 사원 번호를 가지고 해당사원의 상세정보 페이지로 이동할지 묻는alert 출력 */ 

	function selectEmp(value) {  
		if (this.host !== window.location.host) {
			if (window.confirm('선택한 사원의 상세정보 페이지로 이동하시겠습니까?')) {
				
				/* 확인 선택 시 이동 */
				
				location.href = "${ pageContext.servletContext.contextPath }/manager/emp/detail?no="+ value;
			} else {
				
				/* 취소 선택시 이동 */
				
				console.log('이전 페이지로 복귀');
				return false;
			}
		}
	}

	/* 선택한 부서번호로 ajax를 사용하여 비동기 방식으로 부서 정보 조회-1 */
	
	function selectDept(value) {  
			
			$.ajax({
					url : "${pageContext.servletContext.contextPath }/manager/org/detail",
					type : "POST",
					dataType : "json",
					data : {deptCode : value},
					success : function(data) {
						
						$("#deptCode").val(data.deptCode);
						$("#deptTitle").val(data.deptTitle);
						$("#deptContent").val(data.deptContent);
						var refCode = data.refDeptCode;
						$("#listArea").append($("<input>").attr("type", "hidden").attr("name", "refCode").val(refCode).attr("id", "refCode"));
					},
					error : function(request, status, error) {
						window.alert("정보조회실패");
					}
				});

	}

	/* 선택한 부서번호로 ajax를 사용하여 비동기 방식으로 부서 정보 조회-2 */
	
	function selectParentDept(value) {
			
			$.ajax({
					url : "${ pageContext.servletContext.contextPath }/manager/org/detail",
					type : "POST",
					dataType : "json",
					data : {deptCode : value},
					success : function(data) {
						var deptCode = data.deptCode;
						var deptTitle = data.deptTitle;
						var deptContent = data.deptContent;
						$("#deptCode").val(deptCode), $("#deptTitle").val(deptTitle), $("#deptContent").val(deptContent)
					},
					error : function(request, status, error) {
						window.alert("정보조회실패");
					}
				});
	}

	/* 최상위 부서(회사코드번호) 콘솔 출력 */
	
	function selectCompany(value) {
		
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/manager/org/detail",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					$("#deptCode").val(data.deptCode), $("#deptTitle").val(data.deptTitle), $("#deptContent").val(data.deptContent)
				},
				error : function(request, status, error) {
					window.alert("정보조회실패")
				}
		});
	}
	
</script>


<div class="m-orgTreeArea"
	style="overflow: auto; width: 320px; height: 610px;">

	<div class="sidetree" id="sidetree">
		<h3 align="center">조직도</h3>

		<div class="treeheader">&nbsp;</div>

		<div id="sidetreecontrol">
			<a href="?#"><font size="3px" color="gray" face="">전체 닫기</a> |
			<a href="?#">전체 열기</a>
		</div>
		<ul id="tree">
			<li><a onclick="selectCompany('${ company.deptCode }')">
			<!-- 회사이름 출력 -->
			 <strong><c:out	value="${ company.deptTitle }" /> </strong></a> 
			 	<!-- 조직도 전체정보 = orglist -->
				<c:forEach items="${ requestScope.org }" var="orglist">
					<ul>
						<li><a onclick="selectParentDept('${ orglist.deptCode }')">
						<!-- 부모(최상위)부서이름 출력 --> 
							<strong><c:out value="${ orglist.deptTitle } [#${ orglist.deptCode }]" /> </strong>	</a> 
								<c:forEach items="${ orglist.childOrgList }" var="childList">
								<ul>
									<li><a onclick="selectDept('${ childList.deptCode }')">
									<!-- 자식(하위)부서이름 출력 -->
										<strong> <c:out	value="${ childList.deptTitle } [#${ childList.deptCode }]" /></strong>	</a> 
									 	<c:forEach	items="${ childList.orgEmpList }" var="empList">
											<ul>
												<li><a onclick="selectEmp('${ empList.empNo }')">
												 <em><c:out value="[#${ empList.empNo }]  ${ empList.jobGrade } / ${ empList.empName }" />	</em></a></li>
											</ul>
										</c:forEach></li>
								</ul>
							</c:forEach></li>
					</ul>
				</c:forEach></li>
		</ul>
	</div>
</div>


