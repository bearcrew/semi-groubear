<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>

<div class="m-org-content">

	<p class="menuName">조직도 관리</p>
	<hr id="m-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">

	<div class="projectPage" >
			<a style="color: #BBBBBB;"> 부서관리(추가/수정/삭제)  </a>	
		</div>

	<jsp:include page="treeView.jsp" />

	<jsp:include page="orgDetail.jsp" />

</div>

</body>
</html>