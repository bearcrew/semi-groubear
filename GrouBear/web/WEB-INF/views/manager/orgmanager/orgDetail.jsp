<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<div class="m-orgDetailArea">
	<div class="m-orgDetailArea2">

		<form>
			<div id="m-detailHeader">
				<div id="m-detailleft1"
					style="display: inline-block; border: 1px solid gray;">부서코드</div>
				<div id="m-detailRight1"
					style="display: inline-block; border: 1px solid gray;">
					<input class="m-orgInfoInput" type="text" name="deptCode"
						id="deptCode" readonly placeholder="부서를 선택해주세요">
				</div>

				<br>

				<div id="m-detailleft2"
					style="display: inline-block; border: 1px solid gray;">부서이름</div>
				<div id="m-detailRight2"
					style="display: inline-block; border: 1px solid gray;">
					<input class="m-orgInfoInput" type="text" name="deptTitle"
						id="deptTitle">
				</div>

				<br>
				<div id="m-detailHeader3">
					<div id="m-detailleft3"
						style="display: inline-block; border: 1px solid gray;">부서설명</div>
					<div id="m-detailRight3"
						style="display: inline-block; border: 1px solid gray;">
						<input class="m-orgInfoInput" type="text" name="deptContent"
							id="deptContent">
					</div>

				</div>
			</div>

		<!-- form태그 안에 각기 다른기능의 3개 버튼의 각기 다른 경로 지정  -->
			<div class="orgDetailBtnArea"> 
				<hr>
				<button id="orgDetailBtn1" type="submit"
					formaction="${ pageContext.servletContext.contextPath }/manager/org/delete"
					formmethod="get" formtarget="_self">삭제</button>

				<button id="orgDetailBtn2" type="submit"
					formaction="${ pageContext.servletContext.contextPath }/manager/org/update"
					formmethod="post" formtarget="_self">수정</button>

				<button id="orgDetailBtn3" type="submit"
					formaction="${ pageContext.servletContext.contextPath }/manager/org/insert"
					formmethod="get" formtarget="_self">하위부서추가</button>
			</div>
		</form>
	</div>
</div>
