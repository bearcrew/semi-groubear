<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %>
<style>
.e-hcontent{
	position: absolute;
	left: 241px;
	top: 81px;
	height: 150px;
	width: 1662px;
}
.e-menuName {
	position: absolute;
	width: 412px;
	height: 51px;
	left: 50px;
	top: 38px;
   
	font-family: 'Noto Sans KR', sans-serif;
	font-style: normal;
	font-weight: bold;
	font-size: 37.33px;
	line-height: 51px;
   
	display: flex;
	align-items: center;
	letter-spacing: -0.04em;
}
.e-ListContent {
	position: absolute;
	left: 300px;
	top: 260px;
	height: 750px;
	width: 1340px;
	border: 1px solid black;
}
.e-subTitle{
	position: absolute;
	left: 0px;
	top: 15px;
	height: 70px;
	width: 1600px;
}
.e-subName{
	position: absolute;
	width: 412px;
	height: 50px;
	left: 50px;
	top: 0px;
   
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 30px;
	line-height: 51px;
   
	display: flex;
	align-items: center;
	letter-spacing: -0.04em;
}
.e-subNameUnder{
	position: absolute;
	width: 1300px;
	left: 20px;
	top: 40px;
}
.e-tHeader {
	position: absolute;
	left: 20px;
	top: 70px;
	width: 1300px;
	height: 90px;
	font-size: 17pt;
	text-align: center;
	/* background: lightgreen; */
	border-radius: 10px;
}

.e-tHeadUnder{
	position: absolute;
	width: 1300px;
	left: 20px;
	top: 140px;
}
.e-listArea {
	position: absolute;
	left: 20px;
	top: 160px;
	width: 1300px;
	height: 485px;
	overflow-y: scroll;
}
.e-listArea > tbody > tr > td{
	font-size: 13pt;
	font-weight: bold;
	text-align: center;
	width: 25%;
}
.e-bordListbtn {
	position: absolute;
	right: 370px;
	top: 10px;
	width: 100px;
	height: 30px;
	font-size: 10pt;
	font-weight: bold;
	border-color: #75CB5E;
	border-radius: 5px;
	background: #75CB5E;
	color: white;
}
.e-updateBtn{
	border-color: #75CB5E;
	border-radius: 5px;
	background: #75CB5E;
	color: white;
}
.e-delbtn{
	border-color: #FF7171;
	border-radius: 5px;
	background: #FF7171;
	color: white;
}
.e-listArea > tbody >  tr:hover{
	background-color: lightgreen;
}
/* .e-listArea > tbody >  tr{
	border-bottom: 1px solid black;
} */
/* .e-listArea > tbody > tr:nth-child(odd){
	background-color: lightgreen;
}
.e-listArea > tbody > tr:nth-child(even){
	background-color: white;
} */
 
</style>

<!-- 제목 start -->
<div class="e-hcontent">
	<p class="e-menuName">게시판 관리</p>
	<hr id="e-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
</div>
<!-- 제목 end -->

<div class="e-ListContent">
	
	<!-- 부제목 start -->
	<div class="e-subTitle">
	<p class="e-subName">게시판 목록</p>
	<button class="e-bordListbtn" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/board/insert'">게시판 추가</button>
	<hr class="e-subNameUnder" style="height: 2px;">
	</div>
	<!-- 부제목 end -->
	
	<!-- 테이블 헤더 start -->
	<table class="e-tHeader">
		<tr>
			<th style="text-align: center;" width="25%">게시판명</th>
			<th style="text-align: center;" width="25%">만든 날짜</th>
			<th style="text-align: center;" width="25%">수정</th>
			<th style="text-align: center;" width="25%">삭제</th>
		</tr>
	</table>
	<!-- 테이블 헤더 end -->
	
	<hr class="e-tHeadUnder" style="height: 2px;">
	
	<!-- 게시판 목록 start -->
	<table align="center" class="e-listArea" id="e-listArea" >
		<tbody style="display: block; height: 485px; width:1300px; overflow-y: auto">
			<c:forEach items="${ sessionScope.boardList }" var="board">
				<tr align="center">
					<td style="width: 325px; height: 80px; text-align: center;"><c:out value="${ board.boardTitle }"/></td>
					<td style="width: 325px; height: 80px; text-align: center;"><c:out value="${ board.createDate }"/></td>
					<td style="width: 325px; height: 80px; text-align: center;"><input class="e-updateBtn" type="button" value="수정" onclick="location.href ='${ pageContext.servletContext.contextPath }/manager/board/update?boardTitle=${ board.boardTitle }'"></td>
					<td style="width: 325px; height: 80px; text-align: center;"><input class="e-delbtn" type="button" value="삭제" name="deleteBoard" id="deleteBoard"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<!-- 게시판 목록 end -->
</div>

<script>
	/* listArea 테이블에 있는 td 태그를 rows에 담는다 */
	var rows = document.getElementById("e-listArea").getElementsByTagName("td");
	
	/* 삭제 버튼 클릭시 해당 행의 게시판명을 가져가 삭제를 한다 */
	for(let i = 0; i < rows.length; i++) {
		rows[i].parentNode.children[3].onclick = function() {
			var title = this.parentNode.children[0].innerText;
			
			var result = confirm("정말 삭제 하시겠습니까?");
			
			if(result) {
				location.href ='${ pageContext.servletContext.contextPath }/manager/board/delete?boardTitle=' + title;
			} else {
				
			}
		}
	}
</script>

</body>
</html>