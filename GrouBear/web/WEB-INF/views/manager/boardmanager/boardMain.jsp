<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
	
<div class="content">

	<div align="left">
		<button onclick="location.href='${ pageContext.servletContext.contextPath }/manager/board/insert'">추가</button>
		<button name="management" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/board/list'">관리</button>
	</div>
	
	<div align="left">
		<table align="left" id="listArea">
			<tr>
				<th width="140px">게시판 목록</th>
			</tr>
			<c:forEach items="${ sessionScope.boardList }" var="board">
				<tr align="center">
					<td align="left"><c:out value="${ board.boardTitle }"/></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</div>

</body>
</html>
