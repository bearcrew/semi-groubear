<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %>

<style>
.e-hcontent{
	position: absolute;
	left: 241px;
	top: 81px;
	height: 150px;
	width: 1662px;
}
.e-menuName {
	position: absolute;
	width: 412px;
	height: 51px;
	left: 50px;
	top: 38px;
   
	font-family: 'Noto Sans KR', sans-serif;
	font-style: normal;
	font-weight: bold;
	font-size: 37.33px;
	line-height: 51px;
   
	display: flex;
	align-items: center;
	letter-spacing: -0.04em;
}
.e-updateContent {
	position: absolute;
	left: 300px;
	top: 260px;
	height: 550px;
	width: 1340px;
	border: 1px solid black;
}
.e-subTitle{
	position: absolute;
	left: 0px;
	top: 15px;
	height: 70px;
	width: 1600px;
}
.e-subName{
	position: absolute;
	width: 412px;
	height: 50px;
	left: 50px;
	top: 0px;
   
	font-family: 'Noto Sans KR', sans-serif;
	font-size: 30px;
	line-height: 51px;
   
	display: flex;
	align-items: center;
	letter-spacing: -0.04em;
}
.e-subNameUnder{
	position: absolute;
	width: 1300px;
	left: 20px;
	top: 40px;
}
.e-bordListbtn {
	position: absolute;
	right: 370px;
	top: 10px;
	width: 100px;
	height: 30px;
	font-size: 10pt;
	font-weight: bold;
	border-color: #75CB5E;
	border-radius: 5%;
	background: #75CB5E;
	color: white;
}
.e-updateForm{
	position: absolute;
	top: 200px;
	left: 300px;
}
.e-boardTitle{
	font-size: 30px;
}
.e-inputBoardTitle{
	position: absolute;
	left: 200px;
	width: 500px;
	height: 40px;
	font-size: 12pt;
	font-weight: bold;
	border: 3px solid #75CB5E;
}
.e-updatetbtn{
	position: absolute;
	left: 370px;
	top: 150px;
	width: 50px;
	height: 30px;
	font-size: 10pt;
	font-weight: bold;
	border-color: #75CB5E;
	border-radius: 5%;
	background: #75CB5E;
	color: white;
}
.e-updateCanclebtn{
	position: absolute;
	left: 600px;
	top: 350px;
	width: 50px;
	height: 30px;
	font-size: 10pt;
	font-weight: bold;
	border-color: #75CB5E;
	border-radius: 5%;
	background: #75CB5E;
	color: white;
}
</style>

<!-- 제목 start -->
<div class="e-hcontent">
	<p class="e-menuName">게시판 관리</p>
	<hr id="e-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
</div>
<!-- 제목 end -->

<div class="e-updateContent">
	
	<!-- 부제목 start -->
	<div class="e-subTitle">
	<p class="e-subName">게시판 수정</p>
	<hr class="e-subNameUnder" style="height: 2px;">
	</div>
	<!-- 부제목 end -->
	
	<!-- 변경할 게시판명을 입력하여서 post방식으로 전달한다 -->
	<form method="post" action="${ pageContext.servletContext.contextPath }/manager/board/update">
		<div class="e-updateForm">
			<label class="e-boardTitle">게시판 이름 </label><input class="e-inputBoardTitle" type="text" value="${ requestScope.updateBoard.boardTitle }" name="updateBoardTitle">
			<input type="hidden" value="${ requestScope.updateBoard.boardCode }" name="boardCode">
			<button class="e-updatetbtn" type="submit">저장</button>
		</div>
	</form>
	
	<!-- 취소 버튼 클릭시 화면 전환 -->
	<button class="e-updateCanclebtn" onclick="location.href='${ pageContext.servletContext.contextPath }/manager/board/list'">취소</button>
	
</div>
</body>
</html>