<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %>
	
<!-- summernote 제어 스트립트 이용 -->
<script>
	$(document).ready(function() {
		$('#summernote').summernote({
			placeholder : 'content',
			minHeight : 370,
			maxHeight : null,
			focus : true,
			lang : 'ko-KR'
		});
	});
</script>


<script>

/* -------------- select 박스 이용해서 수신 참조자 지정하기 시작 -------------- */


	/* 상위 부서 (회사코드번호) 출력 함수 */
	function selectParentDept(value) {
		
	    /* 처음에 미리 초기화 해준다. */
	    $("#ParentDept1").empty();
	 
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					if($("#company1").val() == -1) {
						
						$("#parentDept1 option").remove();
						$("#parentDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#childDept1 option").remove();
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						for(i in data) {
							$("#parentDept1").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>")
						}
						
					}
					
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				}
		});
	}
	
 
	// 하위 부서 (회사코드번호) 출력 함수
	function selectchildDept(value) {
		
		/* 처음에 미리 초기화 해준다. */
		$("#childDept1").empty();

		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					if($("#parentDept1").val() == -1) {
						
						$("#childDept1 option").remove();
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#childDept1").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>");
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		});
	}
	
	// 하위 부서에 속하는 사원(회사코드번호) 출력 함수
	function selectEmp(value) {
		
		/* 처음에 미리 초기화 해준다. */
		$("#reference").empty();
		
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/reference/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* '선택하세요'를 선택하였을 경우 */
					if($("#childDept1").val() == -1) {
						
						/* $("#reference option").remove(); */
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					/* 사원이 없는 부서를 선택하였을 경우 */
					} else if (data == "") {
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +" disabled>사원이 없습니다.</option>");
					
					/* 정상적으로 사원을 선택하였을 경우 */
					} else {
						
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#reference").append("<option value='" + data[i].empNo + "'>" + data[i].empName + " " + data[i].jobGrade + " [#" + data[i].empNo + "]" + "</option>");
					 		
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		  });
		
	}
		
	
	$(function() {
		$("#insertReference").click(function() {
			
			var name = $("#reference option:selected").text();
			
			
			if(name === "선택하세요" || name === "") {
				alert("사원을 선택해주세요.");
				
				
			} else {
				console.log(name);
				
				/* 참조자 삽입 시, 비어 있을 경우 */
				if($("#refReceiverName").val() == ""){
					
					$("#refReceiverName").append(name);
					
				} else {
					
					$("#refReceiverName").append(", ");
					$("#refReceiverName").append(name);
					
				}
				
			}
			
		});
	});
	
	
	$(function() {
		$("#deleteReference").click(function() {
			
			if($("#refReceiverName").val() != ""){
				
				if(confirm("수신 참조자 목록을 전체 삭제하시겠습니까?") == true){
					$("#refReceiverName").empty();
			    	
			    } else {
			        return ;
			        
			    }
				
			} else {
				
				alert("삭제할 수신 참조자 목록이 없습니다.");
				
			}
			
		});
	});


/* -------------- select 박스 이용해서 수신 참조자 지정하기 끝 -------------------*/



/* -------------- select 박스 이용해서 결재자 지정하기 시작 -------------- */

	// 상위 부서 (회사코드번호) 출력 함수
	function selectParentDept2(value) {
		
	    /* 처음에 미리 초기화 해준다. */
	    $("#ParentDept2").empty();
	 
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					if($("#company2").val() == -1) {
						
						$("#parentDept2 option").remove();
						$("#parentDept2").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#childDept2 option").remove();
						$("#childDept2").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#approver option").remove();
						$("#approver").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						for(i in data) {
							$("#parentDept2").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>")
						}
						
					}
					
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				}
		});
	}
	
 
	// 하위 부서 (회사코드번호) 출력 함수
	function selectchildDept2(value) {
		
		/* 처음에 미리 초기화 해준다. */
		$("#childDept2").empty();

		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					if($("#parentDept2").val() == -1) {
						
						$("#childDept2 option").remove();
						$("#childDept2").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#approver option").remove();
						$("#approver").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						$("#childDept2").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#childDept2").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>");
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		});
	}
	
	// 하위 부서에 속하는 사원(회사코드번호) 출력 함수
	function selectEmp2(value) {
		
		/* 처음에 미리 초기화 해준다. */
		$("#approver").empty();
		
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/reference/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* '선택하세요'를 선택하였을 경우 */
					if($("#childDept2").val() == -1) {
						
						/* $("#reference option").remove(); */
						$("#approver").append("<option value="+ -1 +">선택하세요</option>");
						
					/* 사원이 없는 부서를 선택하였을 경우 */
					} else if (data == "") {
						
						$("#approver option").remove();
						$("#approver").append("<option value="+ -1 +" disabled>사원이 없습니다.</option>");
						
					/* 정상적으로 사원을 선택하였을 경우 */
					} else {
						
						$("#approver").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#approver").append("<option value='" + data[i].empNo + "'>" + data[i].empName + " " + data[i].jobGrade + " [#" + data[i].empNo + "]" + "</option>");
					 		
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		});
		
	}
	

	$(function() {
		$("#insertApprover").click(function() {
			
			var name = $("#approver option:selected").text();
			
			
			if(name === "선택하세요" || name === "") {
				alert("사원을 선택해주세요");
				
				
			} else {
				console.log(name);
				
				/* 참조자 삽입 시, 비어 있을 경우 */
				if($("#approverName").val() == ""){
					
					$("#approverName").append(name);
					
				} else {
					
					$("#approverName").append(", ");
					$("#approverName").append(name);
					
				}
				
			}
			
		});
	});
	
	
	$(function() {
		$("#deleteApprover").click(function() {
			
			if($("#approverName").val() != "") {
				
				if(confirm("결재자 목록을 전체 삭제하시겠습니까?") == true){
					$("#approverName").empty();
			        
			    } else {
			        return ;
			    }
				
			} else {
				
				alert("삭제할 결재자 목록이 없습니다.");
				
			}
		

		});
	});
	
	
/* --------------------- select 박스 이용해서 결재자 지정하기 끝 ------------------------ */
</script>

<!-- content 영역 시작-->
<!-- 제목 start -->

<div class="a-content"> 
	
	<p class="menuName">전자결재 작성</p> 
			
		<div id="menuNameUnderline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;"></div> 

<!-- 제목 end -->
		
		<!-- 전자결재 작성 start -->
		
		<div class="a-funcFrame">
		
			<div class="a-funcCenterFrame">
				<form id="a-approvalform" method="post" action="${ pageContext.servletContext.contextPath }/approval/insert" style="width: 860px;">
					
					<div>
						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">문서 제목</span>
							<input type="text" name="DraftLetterTitle" class="a-inputInfo" style="width: 770px;"required="required"/>
						</span>
					</div>
					
					<div>
						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">수신 참조자 지정</span>
							<select id="company1" class="a-selectBox" onchange="selectParentDept(this.value)">
								<option value="-1">선택하세요</option>
								<option value="0">GrouBear</option>
							</select>
							<select id="parentDept1" class="a-selectBox" onchange="selectchildDept(this.value)">
								<option value="-1">선택하세요</option>
							</select>
							<select id="childDept1" class="a-selectBox" onchange="selectEmp(this.value)">
								<option value="-1">선택하세요</option>
							</select>
							<select id="reference" class="a-selectBox">
								<option value="-1">선택하세요</option>
							</select>
							<textarea id="refReceiverName" name="refReceiverName" class="a-approvalTextArea" readonly></textarea>
							<button type="button" id="insertReference" class="a-inputInfoBtn a-addEmp">사원 추가</button>
							<button type="button" id="deleteReference" class="a-inputInfoBtn a-deleteEmp">사원 삭제</button>
						</span>
					</div>
					
					<div>
						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">결재자 지정</span>
							<select id="company2" class="a-selectBox" onchange="selectParentDept2(this.value)">
								<option value="-1">선택하세요</option>
								<option value="0">GrouBear</option>
							</select>
							<select id="parentDept2" class="a-selectBox" onchange="selectchildDept2(this.value)">
								<option value="-1">선택하세요</option>
							</select>
							<select id="childDept2" class="a-selectBox" onchange="selectEmp2(this.value)">
								<option value="-1">선택하세요</option>
							</select>
							<select id="approver" class="a-selectBox">
								<option value="-1">선택하세요</option>
							</select>
							<textarea id="approverName" name="approverName" class="a-approvalTextArea" readonly></textarea>
							<button type="button" id="insertApprover" class="a-inputInfoBtn a-addEmp">사원 추가</button>
							<button type="button" id="deleteApprover" class="a-inputInfoBtn a-deleteEmp">사원 삭제</button>
						</span>
					</div>
				
					<div>
						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">참조 문서</span>
							<input type="file" name="refDocumentFile" id="refDocumentFile" class="a-inputRefDocumentBtn" onChange="uploadFile();" accept="text/html, image/*, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" style=" display: inline-block;">
						</span>
					</div>
					
					<div>
						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">기안 제목</span>
							<input type="text" name="draftDocTitle" class="a-inputInfo" required="required"/>
							<span class="a-approvalInsertHeader">기안 항목</span>
							<select name="draftType" class="a-selectBox">
							    <option>선택</option>
							    <option value="기안서">기안서</option>
							    <option value="보고서">보고서</option>
							</select>
						</span>

						<span class="a-approvalRow">
							<span class="a-approvalInsertHeader">기안 내용</span>
							<br><br>
							<textarea id="summernote" name="docContent" style="width: 100%; height: 300px; resize: none; margin-top: 10px;" required="required"></textarea>
						</span>
					</div>
					
					<button id="submitBtn" type="submit" value="글 작성" class="a-submitBtn">글 작성</button>
					
					<!-- 첨부파일 업로드 JS start-->
					
					<script> 

						$("#submitBtn").click(function() {
							
							let fileInfo = document.getElementById("refDocumentFile").files[0];				/* id가 refDocumentFile 인 파일내용을 담아 변수에 저장한다. */
							
							let reader = new FileReader();													/* 파일을 보낼 수 있는 파일 리더를 생성한다. */
							
							if (fileInfo) {
																	
								reader.readAsDataURL(fileInfo);												/* @details readAsDataURL( )을 통해 파일의 URL을 읽어온다. */
							}	
							
						console.log($("#refDocumentFile")[0].files[0]);
						
						const formData = new FormData();           											/* formData 생성 */
						
						formData.append("refDocumentFile", $("#refDocumentFile")[0].files[0]);
						
							$.ajax({
								url: "${ pageContext.servletContext.contextPath }/approval/attachment",		/* 넘겨줄 url주소 (맵핑주소로 넘어간다) */
								type: "post",																/* 타입은 post 타입이다. */
								data: formData,																/* 넘겨줄 데이터는 formData이다 */
								contentType: false,															/* 기본값 : charset = UTF-8 */
								processData: false,															/* 기본값 : true */
								success: function(data, textStatus, xhr) {
									
								},
								error: function(xhr, status, error) {
									console.log(xhr);
								} 
							});
						});
							
						function resetFormElement($obj) {
							$obj.val("");
						}
					
					</script>
					
					<!-- 첨부파일 업로드 JS end -->
					
				</form>
				
				<!-- 전자결재 작성 end -->
				
			</div>
		
		</div>
		
	</div>
</body>
</html>