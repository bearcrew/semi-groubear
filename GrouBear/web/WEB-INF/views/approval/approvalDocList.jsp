<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %>
	
<div class="a-content">

<p class="menuName">전자결재 문서 보관함</p>

<hr style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">

<!-- 검색영역 -->
<div class="b-search-area" align="left">
			<form id="loginForm" action="${ pageContext.servletContext.contextPath }/approval/document/list" method="get" style="display: inline-block">
				<input type="hidden" name="currentPage" value="1"> 
				<select class="b-select" id="searchCondition" name="searchCondition">
					<option value="writer" ${ requestScope.selectCriteria.searchCondition eq "writer"? "selected": "" }>작성자</option>
					<option value="postTitle" ${ requestScope.selectCriteria.searchCondition eq "docTitle"? "selected": "" }>제목</option>
					<option value="postContent" ${ requestScope.selectCriteria.searchCondition eq "docContent"? "selected": "" }>내용</option>
				</select> 
				<input class="b-input" type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">

				<button class="b-btn b-search-btn" type="submit"><font size="4">검색</font></button>
			</form>
		</div>
		<br><br><br><br><br><br><br>
		
		<!-- 전체 승인 반려 버튼 영역 -->
		<div class="b-select-area">
		<form id="allFormBtn" action="${ pageContext.servletContext.contextPath }/approval/document/list" method="post">
						<button id="actbtn" class="b-condition " type="submit"><font size="4">승인</font></button>
		</form>
						<button id="allbtn" class="b-btn3" onclick="location.href='${ pageContext.servletContext.contextPath }/approval/document/list'"><font size="4">전체</font></button>
						<button id="petbtn" class="b-condition" onclick="location.href='${ pageContext.servletContext.contextPath }/approval/document/list2'"><font size="4">반려</font></button>
		</div>
		<br>
		<div>
			<!-- 목록 조회 영역 -->
			<table class="b-table">
				<tr>
					<th class="b-th1" width="50px"><font size="4">문서번호</font></th>
					<th class="b-th2" width="300px"><font size="4">문서제목</font></th>
					<th class="b-th5" width="100px"><font size="4">작성일자</font></th>
					<th class="b-th1" width="100px"><font size="4">작성자</font></th>
					<th class="b-th1" width="100px"><font size="4">결재상태</font></th>
				</tr>
				<c:forEach var="docList" items="${ requestScope.approvalDocList }">
					<tr>
						<td id ="docNo" class="b-td1"><font size="4"><c:out value="${ docList.no }" /></font></td>
						<td id="docTitle" class="b-td1"><font size="4"><c:out value="${ docList.title }" /></font></td>
						<td id="docDate" class="b-td1"><font size="4"><c:out value="${ docList.writeDate }" /></font></td>
						<td id="docWriter" class="b-td1"><font size="4"><c:out value="${ sessionScope.loginMember.empName }" /></font></td>
						
					<!-- 결재 상태 정보를 테이블에서 가져오기 위한 c:set -->
					<c:set var="condition" value="${ docList.approvalCondition }" />
	
					<!-- 테이블의 결재 상태가 승인일 경우 "승인"표시, 반려일 경우 "반려"표시, 디폴트는 "진행중" -->
					<c:choose>
	   					 <c:when test="${condition eq '승인'}">
							<td id="docCondition" class="b-td1"><c:out value="승인" /></td>
	   					 </c:when>
	   					 
	    				 <c:when test="${condition eq '반려'}">
	        				<td id="docCondition" class="b-td1"><c:out value="반려" /></td>
	    				 </c:when>
	
	    				<c:otherwise>
	        			    <td id="docCondition" class="b-td1"><c:out value="진행중" /></td>
	    				</c:otherwise>
	
					</c:choose>
				
					</tr>
				</c:forEach>
			</table>
				
		</div>
		
</div>

</body>
</html>