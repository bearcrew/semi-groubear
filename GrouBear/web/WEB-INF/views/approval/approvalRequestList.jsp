<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 

	<!-- 전자결재 승인 요청 목록 시작 -->
	<div class="a-content" align="center">
	
	<p class="menuName">전자결재 승인 요청 목록</p> 
			
		<div id="menuNameUnderline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;"></div>
		
		<br><br>
		
		<div>
			<table class="a-table" >
				<tr>
					<th width="100px"><font size="4">문서번호</font></th>
					<th width="100px"><font size="4">결재항목</font></th>
					<th width="300px"><font size="4">제목</font></th>
					<th width="300px"><font size="4">요청자</font></th>
					<th width="100px"><font size="4">작성일자</font></th>
				</tr>
				
				<c:forEach items="${ approvalRequestList }" var="request">
				<tr>
					<td><font size="4"><c:out value="${ request.docNo }"/></font></td>
					<td><font size="4"><c:out value="${ request.type }"/></font></td>
					<td><font size="4"><c:out value="${ request.title }"/></font></td>
					<td><font size="4"><c:out value="${ request.requesterName }"/></font></td>
					<td><font size="4"><c:out value="${ request.date }"/></font></td>
				</tr>
				</c:forEach>
			</table>
		</div>
							
		<!-- 전자결재 승인 요청 목록 끝 -->
	</div>
		
	<!-- 목록 커서 동작 스크립트 -->
	
	<script>
		if (document.getElementsByTagName("td")) {

			const $tds = document.getElementsByTagName("td");
			
			for (let i = 0; i < $tds.length; i++) {

				$tds[i].onmouseenter = function() {
					this.parentNode.style.backgroundColor = "yellowgreen";
					this.parentNode.style.cursor = "pointer";
				}

				$tds[i].onmouseout = function() {
					this.parentNode.style.backgroundColor = "white";
				} 

				$tds[i].onclick = function() {
					const no = this.parentNode.children[0].innerText;
					location.href = "${ pageContext.servletContext.contextPath }/approval/request/detail?requestNo=" + no;
				}

			}

		}
	</script>
	
	<!-- 목록 커서 동작 스크립트 -->
	
</body>
</html>