<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
	<div class="content">
	
	<p class="menuName">전자결재 승인 요청 상세 페이지</p> 
			
		<div id="menuNameUnderline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;"></div>
	

		<div>
			<table class="a-table" >
				<tr>
					<th width="100px"><font size="4">결재번호</font></th>
					<th width="100px"><font size="4">기안제목</font></th>
					<th width="300px"><font size="4">기안내용</font></th>
					<th width="300px"><font size="4">결재자번호</font></th>
					<th width="100px"><font size="4">참조자번호</font></th>
					<th width="100px"><font size="4">작성자</font></th>
					<th width="100px"><font size="4">소속</font></th>
					<th width="100px"><font size="4">첨부파일명</font></th>
				</tr>
				
				<c:forEach items="${ approvalRequestDetail }" var="request">
				<tr>
					<td><font size="4"><c:out value="${ request.no }"/></font></td>
					<td><font size="4"><c:out value="${ request.title }"/></font></td>
					<td><font size="4"><c:out value="${ request.content }"/></font></td>
					<td><font size="4"><c:out value="${ request.approver }"/></font></td>
					<td><font size="4"><c:out value="${ request.attachmentNo }"/></font></td>
					<td><font size="4"><c:out value="${ request.empNo }"/></font></td>
					<td><font size="4"><c:out value=""/></font></td>
					<td><font size="4"><c:out value="${ request.savedFileName }"/></font></td>
				</tr>
				</c:forEach>
			</table>
		</div>
		
		<input type="hidden" name="boardCode" value="${ requestScope.postNoticeDetail.boardCode }">
		<br> <input type="hidden" name="boardCode"
			value="${ requestScope.postNoticeDetail.boardCode }">
		<div class="b-detailbtn">
			<button class="b-btn" type="reset" id="backPostNoticeList"
				onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }'">돌아가기</button>
			<button class="b-btn" type="submit"
				onclick="location.href='${ pageContext.servletContext.contextPath }/post/notice/update?no=${ requestScope.postNoticeDetail.postNo }&boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }'">수정하기
			</button>
			<button id="delbtn" class="b-btn">삭제하기</button>
		</div>
	
	</div>

</body>
</html>