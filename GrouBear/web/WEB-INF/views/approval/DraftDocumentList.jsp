<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	
	<!-- 합쳐지고 최소화된 최신 CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<!-- 부가적인 테마 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="../common/headerAndSidebar.jsp"></jsp:include>
	
	<br><br><br><br><br><br>
	
	<div class="container">
		<div class="form-group row pull-right">
			<div class="col-xs-8">
				<input class="form-control" type="text" size="20">
			</div>
			<div>
				<button class="btn btn-success" type="button">검색</button>
			</div>
		</div>
		<table class="table" style="text-align: center; border: 1px soild #dddddd">
			<thead>
				<tr>
					<th style="background-color: #fafafa; text-align: center;">유형</th>
					<th style="background-color: #fafafa; text-align: center;">제목</th>
					<th style="background-color: #fafafa; text-align: center;">작성자</th>
					<th style="background-color: #fafafa; text-align: center;">진행상황</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>지출결의서</td>
					<td>개발 1팀 법인카드 지출 결의서</td>
					<td>정 우 영 사원</td>
					<td>결재 요청</td>
				</tr>
				<tr>
					<td>경조비신청서</td>
					<td>김병준 팀장 결혼으로 인한 축하금 지급에 대한 건</td>
					<td>정 우 영 사원</td>
					<td>결재 완료</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>
