<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/vendor/fonts/circular-std/style.css" >
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/libs/css/style.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <%-- <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/vendor/charts/chartist-bundle/chartist.css"> --%>
    
    <!-- 트리 뷰 CSS -->
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/organization.css"/>
    
    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    
    <!-- bootstap bundle js -->
    <script src="${ pageContext.servletContext.contextPath }/resources/css/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    
	<!-- 트리 뷰 자바스크립트  -->
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.cookie.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.treeview.js" type="text/javascript"></script>


<script type="text/javascript">
		$(function() {
			$("#tree").treeview({
				collapsed: true,
				animated: "medium",
				control:"#sidetreecontrol",
				persist: "location"
			});
		})
		
</script>

<title>Insert title here</title>
</head>
<body>

    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    
    <div class="dashboard-main-wrapper">
    
        <!-- ============================================================== -->
        <!-- 메뉴바 -->
        <!-- ============================================================== -->
        
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top" style="height: 70px">
            
                <a class="navbar-brand" href="index.jsp"><img alt="Brand" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png"></a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" 
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <!-- 상단 메뉴바에 있는 메뉴들을 나타낸다. -->
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                    
                     <li class="nav-item dropdown notification">
                            <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-envelope"></i> 
                            <span class="indicator"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                                <li>
                                    <div class="notification-title"> Notification</div>
                                    <div class="notification-list">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item list-group-item-action active">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-2.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jeremy Rakestraw</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-3.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">John Abraham </span>is now following you
                                                        <div class="notification-date">2 days ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-4.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Monaan Pechi</span> is watching your main repository
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-5.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jessica Caruso</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="list-footer"> <a href="#">View all notifications</a></div>
                                </li>
                            </ul>
                        </li>
                    
                    
                        <li class="nav-item dropdown notification">
                            <a class="nav-link nav-icons" href="#" id="navbarDropdownMenuLink1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-fw fa-bell"></i> 
                            <span class="indicator"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right notification-dropdown">
                                <li>
                                    <div class="notification-title"> Notification</div>
                                    <div class="notification-list">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item list-group-item-action active">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-2.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jeremy Rakestraw</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-3.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">John Abraham </span>is now following you
                                                        <div class="notification-date">2 days ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-4.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Monaan Pechi</span> is watching your main repository
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="notification-info">
                                                    <div class="notification-list-user-img"><img src="assets/images/avatar-5.jpg" alt="" class="user-avatar-md rounded-circle"></div>
                                                    <div class="notification-list-user-block"><span class="notification-list-user-name">Jessica Caruso</span>accepted your invitation to join the team.
                                                        <div class="notification-date">2 min ago</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="list-footer"> <a href="#">View all notifications</a></div>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img src="${ pageContext.servletContext.contextPath }/resources/images/3X4.jpg" alt="" class="user-avatar-md rounded-circle"> 
                            	<font size="3", face="Noto Sans">정 우 영 사원</p> </a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                                <a class="dropdown-item" href="#"><i class="fas fa-user mr-2"></i>마이페이지</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-cog mr-2"></i>비밀번호 변경</a>
                                <a class="dropdown-item" href="#"><i class="fas fa-power-off mr-2"></i>로그아웃</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        
        <!-- ============================================================== -->
        <!-- 메뉴바 끝 -->
        <!-- ============================================================== -->
        
        
        <!-- ============================================================== -->
        <!-- 사이드바 -->
        <!-- ============================================================== -->
        
        <div class="nav-left-sidebar sidebar-white">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Project
                            </li>
	                            <li class="nav-item">
	                           		<a class="nav-link" href="dashboard-finance.html">그루보드(Grou Board)</a>
	                            </li>
                            <li class="nav-divider">
                                Board
                            </li>
                             	<li class="nav-item">
	                           		<a class="nav-link" href="dashboard-finance.html">공지사항</a>
	                            </li>
	                             <li class="nav-item">
	                           		<a class="nav-link" href="dashboard-finance.html">부서별 게시판</a>
	                            </li>
	                             <li class="nav-item">
	                           		<a class="nav-link" href="dashboard-finance.html">건의함</a>
	                            </li>
	                             <li class="nav-item">
	                           		<a class="nav-link" href="dashboard-finance.html">문서 자료실</a>
	                            </li>
                            	
	                        <li class="nav-divider">
                                E-Sign
                            </li>
                            	<li class="nav-item">
	                           		<a class="nav-link" href="">전자결재 작성</a>
	                            </li>
	                            <li class="nav-item">
	                           		<a class="nav-link" href="">전자결재 문서 보관함</a>
	                            </li>
	                            <li class="nav-item">
	                           		<a class="nav-link" href="">전자결재 승인 요청 목록</a>
	                            </li>
	                        <li class="nav-divider">
                                Manager Menu
                            </li>
                             	<li class="nav-item">
	                           		<a class="nav-link" href="">게시판 관리</a>
	                            </li>
	                            <li class="nav-item">
	                           		<a class="nav-link" href="">사원 관리</a>
	                            </li>
	                            <li class="nav-item">
	                           		<a class="nav-link" href="">조직도 관리</a>
	                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        
        <!-- ============================================================== -->
        <!-- 사이드바 끝 -->
        <!-- ============================================================== -->
        
        
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                
                    <!-- ============================================================== -->
                    <!-- 페이지 헤더  -->
                    <!-- ============================================================== -->
                    
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">전자결재 작성 </h2>
                                <p class="pageheader-text"></p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">E-Sign</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">전자결재 작성</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- ============================================================== -->
                    <!-- 페이지 헤더 끝  -->
                    <!-- ============================================================== -->
                    
                    <div class="ecommerce-widget">
                    	<div class="row">
                      
                            <!-- ============================================================== -->
                                          <!-- 양식함, 양식 미리보기  -->
                            <!-- ============================================================== -->
                            
                            
							<!-- ============================================================== -->
                            <!-- 양식함  -->
                            <!-- ============================================================== -->
                            
                            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">양식함</h5>
                                    <div class="card-body">
                                        <div class="ct-chart ct-golden-section" style="height: 660px;">
                                            
                                            <div id="sidetree">
												<div class="treeheader"></div>
												<div id="sidetreecontrol">
													<a href="?#">전체 닫기</a> | <a href="?#">전체 열기</a>
												</div>
												
												<br>
												<ul id="tree">
													<c:forEach items="${ requestScope.approvalFormList }" var="approvalForm" >
													
													<li>
														<strong><c:out value="${ approvalForm.title }"></c:out></strong>
														
														<c:forEach var="childApprovalForm" items="${ approvalForm.childFormList }">
														<ul>
															<li>
																<a href="${ pageContext.servletContext.contextPath }/approval/insert?formNo=${ childApprovalForm.formNo }">
																<c:out value="${ childApprovalForm.name }"/>
																</a>
															</li>
														</ul>
														</c:forEach>
													</li>
													
													</c:forEach>
												</ul>
											</div>
                                        
                                        </div>
                                        <!-- <div class="text-center"></div> -->
                                    </div>
                                </div>
                            </div>
                            
                            <!-- ============================================================== -->
                            <!-- 양식함 끝  -->
                            <!-- ============================================================== -->
                            
                            <!-- ============================================================== -->
                            <!-- 양식 미리보기  -->
                            <!-- ============================================================== -->
                            
                            <div class="col-xl-9 col-lg-12 col-md-6 col-sm-12 col-12">
                                <div class="card">
                                    <h5 class="card-header">양식 미리보기 </h5>
                                    <div class="card-body">
                                        <div class="ct-chart ct-golden-section" style="height: 660px;">
	                                        <ul>
												<li><a href="${ pageContext.servletContext.contextPath }/approval/select">양식함 보기</a></li>
											</ul>
                                        </div>
                                        
                                        
                                        
                                        <!-- <div class="text-center"></div> -->
                                    </div>		
                                </div>
                            </div>
                            
                            <!-- ============================================================== -->
                            <!-- 양식 미리보기 끝  -->
 							<!-- ============================================================== -->
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
            <!-- <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                             Copyright © 2021 Concept. All rights reserved. GroupWare by <a href="">Grou Bear</a>.
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="text-md-right footer-links d-none d-sm-block">
                                <a href="javascript: void(0);">About</a>
                                <a href="javascript: void(0);">Support</a>
                                <a href="javascript: void(0);">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
            
        </div>
        
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
        
    </div>
    
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->

</body>
</html>