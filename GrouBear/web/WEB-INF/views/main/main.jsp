<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<jsp:include page="../common/headerAndSidebar.jsp" />
	<div class="k-content">
	
		<div class="k-mainFrame">
		
			<div class="k-leftFrame">
				<div class="k-profileArea">
				
					<!-- 미니 프로필 메인 화면 -->
					<div class="k-profileTop">
						<!-- 프로필 -->
						<img alt="x" src="${ pageContext.servletContext.contextPath }/resources/profileImages/${ sessionScope.loginMember.fileName }" class="k-mainimage">
						<!-- 사원명 -->
						<a id="k-empName"><c:out value="${ sessionScope.loginMember.empName }"/></a>	<!-- empName -->
						<!-- 직급 -->
						<a id="k-job"> <c:out value="${ sessionScope.loginMember.jobGrade }"/> </a>  <!-- JobCode -->
						<!-- 님 환영합니다. -->
						<a id="k-hi">님 환영합니다.</a>
					</div>
					
					<!-- 미니 프로필 버튼 -->
					<div class="k-profileDown" align="center">
						<!-- 마이페이지 -->
						<a class="k-111" href="${ pageContext.servletContext.contextPath }/mypage/info">마이페이지</a>
						<!-- 비번 변경 -->
						<a class="k-222" href="${ pageContext.servletContext.contextPath }/mypage/updatepwd">비밀번호 변경</a>
						<!-- 로그아웃  -->
						<a class="k-333" href="${ pageContext.servletContext.contextPath }/logout">로그아웃</a>
					</div>
				</div>
				
				<!-- 전자 결재 메인 -->
				<div class="k-approvalArea">
					<div class="k-mainHeader k-left" id="k-approvalHeader">
						<a class="k-mainHeaderText">APPROVAL</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr1">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultApproval }" var="approval" begin="0" end="0">
								<p class="k-writer"><c:out value="${ approval.draftTitle }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr2">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultApproval }" var="approval" begin="1" end="1">
								<p class="k-writer"><c:out value="${ approval.draftTitle }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr3">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultApproval }" var="approval" begin="2" end="2">
								<p class="k-writer"><c:out value="${ approval.draftTitle }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr4">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultApproval }" var="approval" begin="3" end="3">
								<p class="k-writer"><c:out value="${ approval.draftTitle }"/></p>
							</c:forEach>
						</a>
					</div>
				</div>
				
				<!-- 프로젝트 메인 -->
				<div class="k-projectArea">
					<div class="k-mainHeader k-left">
						<a class="k-mainHeaderText">PROJECT</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr1">
						<a class="k-mainText">			
							<c:forEach items="${ sessionScope.resultLoginProject }" var="project" begin="0" end="0">
								<p class="k-writer"><c:out value="${ project.projectTitle }"/></p>
								<p class="k-writeDate"><c:out value="${ project.startDate }"/> 
								- <c:out value="${ project.targetDate }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr2">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultLoginProject }" var="project" begin="1" end="1">
								<p class="k-writer"><c:out value="${ project.projectTitle }"/></p>
								<p class="k-writeDate"><c:out value="${ project.startDate }"/> 
								- <c:out value="${ project.targetDate }"/></p>
							</c:forEach>

						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr3">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultLoginProject }" var="project" begin="2" end="2">
								<p class="k-writer"><c:out value="${ project.projectTitle }"/></p>
								<p class="k-writeDate"><c:out value="${ project.startDate }"/> 
								- <c:out value="${ project.targetDate }"/></p>
							</c:forEach>
						
						</a>
					</div>
					<div class="k-mainRow k-left" id="k-mlr4">
						<a class="k-mainText">
							<c:forEach items="${ sessionScope.resultLoginProject }" var="project" begin="3" end="3">
								<p class="k-writer"><c:out value="${ project.projectTitle }"/></p>
								<p class="k-writeDate"><c:out value="${ project.startDate }"/> 
								- <c:out value="${ project.targetDate }"/></p>
							</c:forEach>
						</a>
					</div>
				</div>
			</div>
			
			<div class="k-centerFrame">
				<div class="k-calendarArea">
					<div class="k-mainHeader k-center">
						<a class="k-mainHeaderText">CALENDAR</a>
						<jsp:include page="../common/calender.jsp" />
					</div>
				</div>
				
				<!-- 게시판 메인 -->
				<div class="k-postListArea">
					<div class="k-mainHeader k-center">
						<a class="k-mainHeaderText">POST</a>
					</div>
					<div class="k-mainRow k-center" id="k-mcr1">
						<a class="k-mainText" style="font-size: 14px;">
							<c:forEach items="${ sessionScope.post }" var="post" begin="0" end="0">
								<p class="k-postTitle"><c:out value="${ post.boardTitle }"/></p>
								<p><c:out value="${ post.postTitle }"/></p>
								<p class="k-writer"><c:out value="${ post.writer }"/></p>
								<p class="k-writeDate"><c:out value="${ post.writeDate }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-center" id="k-mcr2">
						<a class="k-mainText" style="font-size: 14px;">
							<c:forEach items="${ sessionScope.post }" var="post" begin="1" end="1">
								<p class="k-postTitle"><c:out value="${ post.boardTitle }"/></p>
								<p><c:out value="${ post.postTitle }"/></p>
								<p class="k-writer"><c:out value="${ post.writer }"/></p>
								<p class="k-writeDate"><c:out value="${ post.writeDate }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-center" id="k-mcr3" align="right">
						<a class="k-mainText" style="font-size: 14px;">
							<c:forEach items="${ sessionScope.post }" var="post" begin="2" end="2">
								<p class="k-postTitle"><c:out value="${ post.boardTitle }"/></p>
								<p><c:out value="${ post.postTitle }"/></p>
								<p class="k-writer"><c:out value="${ post.writer }"/></p>
								<p class="k-writeDate"><c:out value="${ post.writeDate }"/></p>
							</c:forEach>
						</a>
					</div>
					<div class="k-mainRow k-center" id="k-mcr4">
						<a class="k-mainText" style="font-size: 14px;">
							<c:forEach items="${ sessionScope.post }" var="post" begin="3" end="3">
								<p class="k-postTitle"><c:out value="${ post.boardTitle }"/></p>
								<p><c:out value="${ post.postTitle }"/></p>
								<p class="k-writer"><c:out value="${ post.writer }"/></p>
								<p class="k-writeDate"><c:out value="${ post.writeDate }"/></p>
							</c:forEach>
						</a>
					</div>
				</div>
			
			</div>
			
			
			<div class="k-rightFrame">
			
				<!-- 투두 리스트 메인 -->
				<div class="k-todoListArea">
					<div class="k-mainHeader k-right">
						<a class="k-mainHeaderText">TO-DO</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr1">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.01" value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr2">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.02"  value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr3">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.03"  value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr4">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.04"  value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr5">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.05"  value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
					<div class="k-mainRow k-right" id="k-mrr6">
						<a class="k-mainText">
							<input type="checkbox">
							<input type="text" id="k-todoText" placeholder="No.06"  value="" style="border: none; margin-left: 5px;">
						</a>
					</div>
				</div>
				
				<!-- 공지사항 메인 -->
				<div class="k-noticeArea">
					<div class="k-mainHeader k-right">
						<a class="k-mainHeaderText">NOTICE</a>
					</div>
						<div class="k-mainRow k-right" id="k-mrn1">
							<a class="k-mainText">
								<c:forEach items="${ sessionScope.Noticepost }" var="Noticepost" begin="0" end="0">
									<p class="k-noticeTitle"><c:out value="${ Noticepost.postTitle }"/></p>
									<p class="k-writeDate2"><c:out value="${ Noticepost.writeDate }"/></p>
								</c:forEach>
							</a>
						</div>
						<div class="k-mainRow k-right" id="k-mrn2">
							<a class="k-mainText">
								<c:forEach items="${ sessionScope.Noticepost }" var="Noticepost" begin="1" end="1">
									<p class="k-noticeTitle"><c:out value="${ Noticepost.postTitle }"/></p>
									<p class="k-writeDate2"><c:out value="${ Noticepost.writeDate }"/></p>
								</c:forEach>
							</a>
						</div>
						<div class="k-mainRow k-right" id="k-mrn3">
							<a class="k-mainText">
								<c:forEach items="${ sessionScope.Noticepost }" var="Noticepost" begin="2" end="2">
									<p class="k-noticeTitle"><c:out value="${ Noticepost.postTitle }"/></p>
									<p class="k-writeDate2"><c:out value="${ Noticepost.writeDate }"/></p>
								</c:forEach>
							</a>
						</div>
						<div class="k-mainRow k-right" id="k-mrn4">
							<a class="k-mainText">
								<c:forEach items="${ sessionScope.Noticepost }" var="Noticepost" begin="3" end="3">
									<p class="k-noticeTitle"><c:out value="${ Noticepost.postTitle }"/></p>
									<p class="k-writeDate2"><c:out value="${ Noticepost.writeDate }"/></p>
								</c:forEach>
							</a>
						</div>
				</div>
			</div>
		</div>
	</div> <!-- content -->
</body>
</html>