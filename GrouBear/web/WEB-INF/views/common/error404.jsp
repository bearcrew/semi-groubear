<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

<!--  파비콘 변경 링크 -->
<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">

<!-- css 링크 -->
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/login.css">

</head>
<body>
	<div class="error">
		<header> <!-- header -->
			<a href="javascript:history.back()" class="headerBtn" onclick="" >	
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="headerLogo">
			</a>	
		</header> 
			<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/404error.png" class="404error" style="position: absolute; left: 335px; top:107px;">
	</div>
</body>
</html>