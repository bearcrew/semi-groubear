<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		(function(){
			const alarmCode = "${ sessionScope.alarmCode }";
			
			let movePath = "";
			
			switch(alarmCode){
				case "projectAlarmOff" : 
					movePath = "${ pageContext.servletContext.contextPath }/project/search";
					break;

				case "approvalAlarmOff" :
					movePath = "${ pageContext.servletContext.contextPath }";
					break;	
			}
			location.replace(movePath);
		})();
	
	</script>
</body>
</html>