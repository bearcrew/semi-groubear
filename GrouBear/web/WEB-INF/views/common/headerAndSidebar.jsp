<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<!--  파비콘 변경 링크 -->
	<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">
	
	<!-- css 링크 -->
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/summernote/summernote-lite.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/headerAndSidebar.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/project.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/organization.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/approval.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/boardManager.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/emp.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/kanban.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/issue.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/myPage.css">
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/post.css">
    <link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/kanban.css">
    <link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/main.css">
    <link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/passChange.css">
    <link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/paging.css">

	
	<!-- js 링크 -->
	<script src="${ pageContext.servletContext.contextPath }/resources/js/headerAndSidebar.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/emp.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/event.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/organization.js" type="text/javascript"></script>
	
	<!-- treeview -->
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.cookie.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.js" type="text/javascript"></script>
	<script src="${ pageContext.servletContext.contextPath }/resources/js/jquery.treeview.js" type="text/javascript"></script>
	
	<!-- 부트스트랩 링크 -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>

	<!-- 폰트 관련 링크 -->
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@700;900&display=swap" rel="stylesheet">
	
	<!-- Google JQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	
	<!-- include libraries(jQuery, bootstrap) -->
	<link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">
	<script   src="http://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
	<script   src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
	
	<!-- include summernote css/js-->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
	<script   src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
	
	<!-- include summernote-ko-KR -->
	<script src="/resources/js/summernote-ko-KR.js"></script>
	
</head>
<body>
	
	<div class="wrap">		
	
		<div class="header"> <!-- header -->
		
			<!-- Logo -->
			<a href="${ pageContext.servletContext.contextPath }" class="headerBtn" onclick="" >
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="logo" >
			</a>
			
			<!-- header alram btn -->
			<a class="headerBtn">		<!-- 전자결재 승인 요청목록으로 링크 추가 -->
				<c:if test="${fn:contains(sessionScope.loginMember.approvalAlarm, 'Y')}">
					<a href="#" class="approvalAlarm" onclick="approvalAlarm()">
						<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/approvalAlarmOn.png" class="post">
					</a>
				</c:if>
				<c:if test="${fn:contains(sessionScope.loginMember.approvalAlarm, 'N')}">
					<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/approvalAlarmOff.png" class="post">
				</c:if>
			</a>
			
			<a class="headerBtn" id="projectAlarmArea">
				<!-- project Alarm 버튼 영역  -->
			</a>
			

			<!-- emp Info -->
			<img alt="x" src="${ pageContext.servletContext.contextPath }/resources/profileImages/${ sessionScope.loginMember.fileName }" class="image">
			<a id="empName"><c:out value="${ sessionScope.loginMember.empName }"/></a>	<!-- empName -->
			<a id="job"> <c:out value="${ sessionScope.loginMember.jobGrade }"/> </a>  <!-- JobCode -->
			
			<!-- Drop down Button -->
			<div class="dropdown" style="float:right;">
		  		<button class="dropbtn"><img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/dropdown.png"></button>
		  		
			<!-- Drop down Menu -->
		  	<div class="dropdown-content">
		    	<a class="dropdown-item" href="${ pageContext.servletContext.contextPath }/mypage/info">
		    	<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/myPage.png">마이페이지</a>
			    <a class="dropdown-item" href="${ pageContext.servletContext.contextPath }/mypage/updatepwd">
			    <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/pw.png">비밀번호 변경</a>
			    <a class="dropdown-item" href="${ pageContext.servletContext.contextPath }/logout">
			    <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logout.png">로그아웃</a>
		  	</div>
		  
		</div>	<!-- header end -->
		
		<div class="sidebar" >	<!-- sidebar -->
		
			<% String sp = "&nbsp;&nbsp;&nbsp;"; %>
		
			<dl class="menuList" align="left" style="color: #636363">	<!-- Menu -->
					<dd class="category">Project</dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/project/search"><%=sp%>그루보드</a></dd>
				<br>
					<dd class="category">Board</dd>
					<c:forEach items="${ sessionScope.boardListSelect }" var="board">
						<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ board.boardCode }&boardTitle=${ board.boardTitle }" ><%=sp%><c:out value="${ board.boardTitle }"/></a></dd>
					</c:forEach>
				<br>
					<dd class="category">E-Sign</dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/approval/insert" ><%=sp%>전자결재 작성</a></dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/approval/document/list" ><%=sp%>전자결재 문서 보관함</a></dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/approval/request/list" ><%=sp%>전자결재 승인 요청 목록</a></dd>
				<br>
					<dd class="category">Organization</dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/emp/organization/list" ><%=sp%>전체 사원 조회</a></dd>
				<br>
					<c:if test="${ sessionScope.loginMember.authorityCode eq '1' || sessionScope.loginMember.authorityCode eq '2' || sessionScope.loginMember.authorityCode eq '3' || sessionScope.loginMember.authorityCode eq '4' }">
					<dd class="category">Manager Menu</dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/manager/board/list" ><%=sp%>게시판 관리</a></dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/manager/emp/list"><%=sp%>사원 관리</a></dd>
					<dd><a id="sideMenu" href="${ pageContext.servletContext.contextPath }/manager/org/list" ><%=sp%>조직도 관리</a></dd>
					</c:if>
			</dl>	<!-- Menu end -->
			
		</div>	<!-- sidebar end -->
	</div>
	
	<script>
	
	/* 페이지 로드시 마다 프로젝트 알람 온오프 여부를 확인 해옴 */
	$(document).ready(function(){
		const empNo = ${ sessionScope.loginMember.empNo };
		
		$.ajax({
			url : "${ pageContext.servletContext.contextPath }/alarm/project",
			data : { empNo : empNo },
			method : "POST",
			dataType : "json",
			success : function(data) {
				
				/* 상태에 따라 이미지를 달리 삽입해줌 */
				const on = "<a href='#' class='projectAlarm' onclick='projectAlarm()'> <img alt='' src='${ pageContext.servletContext.contextPath }/resources/images/projectAlarmOn.png' class='bell'> </a>";
				const off = "<img alt='' src='${ pageContext.servletContext.contextPath }/resources/images/projectAlarmOff.png' class='bell'>";
				
				if(data == 'Y'){
					$("#projectAlarmArea").append(on);
				} else {
					$("#projectAlarmArea").append(off);
				}
				
			}, 
			error : function(xhr) {
				console.table(xhr);
			}
		});
		
	});

	
	/* 프로젝트 알람 on 상태에서 클릭 시 프로젝트 알람 off */
	function projectAlarm() {
		
		const empNo = ${ sessionScope.loginMember.empNo };
		
		location.href ="${ pageContext.servletContext.contextPath }/alarm/project?empNo="+empNo+"";
		
	}
	</script>
