<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		(function(){
			const successCode = "${ requestScope.successCode }";
			
			let successMessage = "";
			let movePath = "";
			
			switch(successCode){
				case "insertRefBoard" : 
					successMessage = "게시판 추가에 성공하셨습니다!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					break;

				case "updateEmpInfo" :
					successMessage = "사원정보 수정 성공!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/list";
					break;	

				case "updateOrgInfo" :
					successMessage = "조직도정보 수정 성공!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					break;	
				
				case "deleteOrg" :
					successMessage = "조직도 부서 삭제 성공!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					break;	

				case "insertSubOrg" :
					successMessage = "하위 부서 추가 성공! \n 추가하신 하위부서의 부서명과 부서설명을 수정해주세요";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					break;	

				case "insertEmp" : 
					successMessage = "사원 등록에 성공하셨습니다!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/insert";
					break;
				case "updateBoard" : 
					successMessage = "게시판 정보 수정에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					break;
				case "deleteBoard" : 
					successMessage = "게시판 정보 삭제에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					break;
				
				case "insertNotice" : 
					successMessage = "게시글 등록에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					break;

				case "updateNotice" : 
					successMessage = "게시글 수정에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					break;
					
				case "insertAppDoc" : 
					successMessage = "전자결재 요청에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/approval/insert";
					break;
				
				case "deleteNotice" : 
					successMessage = "게시글 삭제에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					break;
					
				case "insertProject" :
					successMessage = "프로젝트가 생성되었습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/project/search?projectNo=${ requestScope.projectNo }";
					break;
					
				case "modifyProject" :
					successMessage = "프로젝트가 완료되었습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/project/search";
					break;
					
				case "projectDelete" :
					successMessage = "프로젝트 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/search";
					break;
					
				case "insertFuncBoard" :
					successMessage = "기능 보드 추가 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ requestScope.projectNo }";
					break;
					
				case "modifyFuncBoard" :
					successMessage = "기능 보드 수정 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ requestScope.projectNo }";
					break;
					
				case "deleteFuncBoard" :
					successMessage = "기능 보드 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ requestScope.projectNo }";
					break;
					
				case "partWorkInsert" :
					successMessage = "단위업무 추가 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
					break;
					
				case "partWorkModify" :
					successMessage = "단위업무 수정 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
					break;
					
				case "partWorkDelete" :
					successMessage = "단위업무 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
					break;
					
				case "insertIssue" : 
	               successMessage = "이슈 등록에 성공하였습니다.";
	               movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
	               break;
	               
				case "updateIssue" : 
		               successMessage = "이슈 수정에 성공하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
		               break;
				
				case "updateInfo" : 
		               successMessage = "정보 수정에 성공하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               break;

				case "updateSearchPwd" : 
		               successMessage = "비밀번호 변경에 성공하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               break;
		               
				case "successLogin" : 
		               successMessage = "로그인에 성공하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               break;       
			}

			alert(successMessage);
			
			location.replace(movePath);
		})();
	
	</script>
</body>
</html>