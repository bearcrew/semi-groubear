<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		(function(){
			const failedCode = "${ requestScope.message  }";
			
			let failedMessage = "";
			let movePath = "";
			
			switch(failedCode){
				case "isNotAdmin" : 
					failedMessage = "해당 기능을 사용하실 수 없습니다. \n [관리자 권한 필요!]";
					alert(failedMessage);
					javascript:history.back();
					break;	
					
				case "insertRefBoard" : 
					failedMessage = "게시판 추가에 성공하셨습니다!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					alert(failedMessage);
					break;

				case "searchEmpList" :
					failedMessage = "사원목록 조회실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/list";
					alert(failedMessage);
					break;			
					
				case "updateEmpInfo1" :
					failedMessage = "[재직]상태로 변경하려면 퇴사일을 초기화해주세요";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/list";
					alert(failedMessage);
					break;		
					
				case "updateEmpInfo2" :
					failedMessage = "사원정보 수정실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/list";
					alert(failedMessage);
					break;	

				case "updateOrgInfo" :
					failedMessage = "조직도정보 수정 실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					alert(failedMessage);
					break;	
				
				case "deleteOrg" :
					failedMessage = "부서 삭제 실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					alert(failedMessage);
					break;	

				case "insertSubOrg1" :
					failedMessage = "부서를 먼저 선택해 주세요";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					break;	
				
				case "insertSubOrg2" :
					failedMessage = "하위부서 추가 실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					alert(failedMessage);
					break;
					
				case "insertSubOrg3" :
					failedMessage = "선택하신 부서는 하위부서를 추가할 수 없습니다";
					movePath = "${ pageContext.servletContext.contextPath }/manager/org/list";
					alert(failedMessage);
					break;	
					
				case "insertEmp" : 
					failedMessage = "사원등록 실패!";
					movePath = "${ pageContext.servletContext.contextPath }/manager/emp/insert";
					alert(failedMessage);
					break;
				case "updateBoard" : 
					failedMessage = "게시판 정보 수정에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					alert(failedMessage);
					break;
				case "deleteBoard" : 
					failedMessage = "게시판 정보 삭제에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/manager/board/list";
					alert(failedMessage);
					break;
				
				case "insertNotice" : 
					failedMessage = "게시글 등록에 실패하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					alert(failedMessage);
					break;

				case "updateNotice" : 
					failedMessage = "게시글 수정에 실패하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					alert(failedMessage);
					break;
					
				case "insertAppDoc" : 
					failedMessage = "전자결재 요청에 성공하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/approval/insert";
					alert(failedMessage);
					break;
				
				case "deleteNotice" : 
					failedMessage = "게시글 삭제에 실패하셨습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/post/notice/list?boardCode=${ sessionScope.boardCode }&boardTitle=${ sessionScope.boardTitle }";
					alert(failedMessage);
					break;
					
				case "insertProject" :
					failedMessage = "프로젝트가 생성되었습니다.";
					movePath = "${ pageContext.servletContext.contextPath }/project/search?projectNo=${ requestScope.projectNo }";
					alert(failedMessage);
					break;
					
				case "updateProject" :
					failedMessage = "프로젝트 정보 수정 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/modify";
					alert(failedMessage);
					break;
					
				case "projectDelete" :
					failedMessage = "프로젝트 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/search";
					alert(failedMessage);
					break;
					
				case "insertFuncBoard" :
					failedMessage = "기능 보드 추가 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ requestScope.projectNo }";
					alert(failedMessage);
					break;
					
				case "updateFuncBoard" :
					failedMessage = "기능 보드 수정 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/modify";
					alert(failedMessage);
					break;
					
				case "deleteFuncBoard" :
					failedMessage = "기능 보드 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ requestScope.projectNo }";
					alert(failedMessage);
					break;
					
				case "partWorkInsert" :
					failedMessage = "단위업무 추가 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ requestScope.funcBoardNo }&projectNo=${ requestScope.projectNo }";
					alert(failedMessage);
					break;
					
				case "partWorkModify" :
					failedMessage = "단위업무 수정 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/modify";
					alert(failedMessage);
					break;
					
				case "deleteFuncBoard" :
					failedMessage = "단위업무 삭제 성공";
					movePath = "${ pageContext.servletContext.contextPath }/kanban/delete";
					alert(failedMessage);
					break;
					
				case "insertIssue" : 
					failedMessage = "이슈 등록에 성공하였습니다.";
	                movePath = "${ pageContext.servletContext.contextPath }/issue/search";
	                alert(failedMessage);
	               break;
	               
				case "updateIssue" : 
					failedMessage = "이슈 수정에 성공하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }/issue/search";
		               alert(failedMessage);
		               break;
				
				case "FailedPassword" : 
					failedMessage = "비밀번호를 변경하지 못하였습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               alert(failedMessage);
		               break;
				
				case "FailedLogin" : 
					failedMessage = "잘못입력하셨습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               alert(failedMessage);
		               break;
				
				case "FailedInfo" : 
					failedMessage = "변경에 실패 하셨습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               alert(failedMessage);
		               break;
		               
				case "EmailAuthentication" : 
					failedMessage = "이메일 인증에 실패하셨습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }";
		               alert(failedMessage);
		               break;
		        
				case "FailedRequestDetail" : 
					failedMessage = "결재 승인요청 조회에 실패하셨습니다.";
		               movePath = "${ pageContext.servletContext.contextPath }/approval/request/list";
		               alert(failedMessage);
		               break;
		        
			}

			
			
			location.replace(movePath);
		})();
	
	</script>
</body>
</html>