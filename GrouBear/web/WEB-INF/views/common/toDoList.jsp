
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


	<!-- css 링크 -->
	<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/toDoList.css">
	<div class="list-box">
		 <div style=" font-size: 40px; background: #75CB5E; color: #FFFFFF;">
			TO-DO
		</div>
		<div class="write-box">
			<input style="border: 1px solid #eee; margin-top: 7px;" type="text" class="text-basic">
			<button type="button" id="btnAdd">추가</button>
		</div>
		<table class="list-table">
			<colgroup>
				<col width="10%">
				<col width="90%">
			</colgroup>
			<thead>
				<tr>
					<th>check</th>
					<th>To do List</th>
				</tr>
			</thead>
			<tbody id="listBody">
			
			</tbody>
		</table>
		<div class="btn-area">
			<button type="button" id="DeleteSel">선택 삭제</button>
			<button type="button" id="btnDelLast">마지막 항목 삭제</button>
			<button type="button" id="btnDelAll">전체 삭제</button>
		</div>
	</div>
	<script>
	
		document.getElementById('btnAdd').addEventListener('click', addList); // 추가
		document.getElementById('btnDelAll').addEventListener('click', delAllEle); // 전체삭제
		document.getElementById('btnDelLast').addEventListener('click', delLastEle); // 마지막 요소 삭제
		document.getElementById('DeleteSel').addEventListener('click', delSelected); // 선택 삭제
		// 추가
		function addList() {
		}
		// 전체삭제
		function delAllEle() {
		}
		// 마지막 항목 삭제
		function delLastEle() {
		}
		// 선택 삭제
		function delSelected() {
		}
	
		function addList() {
			var contents = document.querySelector('.text-basic');
			if (!contents.value) {
				alert('내용을 입력해주세요.');
				contents.focus();
				return false;
			}
			var tr = document.createElement('tr');
			var input = document.createElement('input');
			input.setAttribute('type', 'checkbox');
			input.setAttribute('class', 'btn-chk');
			var td01 = document.createElement('td');
			td01.appendChild(input);
			tr.appendChild(td01);
			var td02 = document.createElement('td');
			td02.innerHTML = contents.value;
			tr.appendChild(td02);
			document.getElementById('listBody').appendChild(tr);
			contents.value = '';
			contents.focus();
		}
		function delAllEle() {
			var list = document.getElementById('listBody');
			var listChild = list.children;
			for (var i = 0; i < listChild.length; i++) {
				list.removeChild(listChild[i])
				i--;
			}
		}
	
		function delLastEle() {
			var body = document.getElementById('listBody');
			var list = document.querySelectorAll('#listBody > tr');
			if (list.length > 0) {
				var liLen = list.length - 1;
				body.removeChild(list[liLen]);
			} else {
				alert('삭제할 항목이 없습니다.')
				return false;
			}
		}
	
		function delSelected() {
			var body = document.getElementById('listBody');
			var chkbox = document.querySelectorAll('#listBody .btn-chk');
			for ( var i in chkbox) {
				if (chkbox[i].nodeType == 1 && chkbox[i].checked == true) {
					body.removeChild(chkbox[i].parentNode.parentNode);
				}
			}
		}
	</script>
	
