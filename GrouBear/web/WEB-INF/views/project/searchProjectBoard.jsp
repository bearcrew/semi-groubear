<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
	
	<div class="p-content">  <!-- content -->
	
			<p class="menuName">프로젝트</p>
			
			<hr id="p-underline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
			
			<div class="p-funcFrame">
				
				<div class="p-funcLeftFrame">
					
					<!-- 프로젝트 정보 -->
					
					<div class="p-projectNameArea">
						<input type="text" value="<c:out |value="${ requestScope.projectInfo.projectNo }"/>" style="display: none;"> 
						
						<!-- 프로젝트명 -->
						
						<a id="p-projectName"><c:out value="${ requestScope.projectInfo.projectTitle }"/></a> 
					</div>
					
					<!-- 프로젝트 설명  -->
					
					<div class="p-projectContent">
						<c:out value="${ requestScope.projectInfo.projectContent }"/> <!-- 프로젝트 설명 -->
						
						<!-- 프로젝트 삭제 버튼 -->
						
						<button id="p-deleteProject" onclick="deleteProject()">
							<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/remove.png" style="margin-bottom: 5px;">
							Remove Project
						</button> 	
					</div>
					
					<!-- 기능보드 추가, 삭제버튼 -->
					
					<div class="p-funcAddAndRemove">
						<button class="p-funcAdd" id="p-funcModalOpen" onclick="addFuncBoard()">+ Board</button>   
						<button class="p-funcDelete" onclick="deleteFuncBoard()">- Board</button> 
					</div>
					
					<!-- 기능보드 내용  -->
					
					<div class="p-funcContent">
						<textarea class="p-funcTextarea" rows="9" cols="48" readonly></textarea>
						<textarea class="p-funcBoardNo" rows="1" cols="10" style="display: none;"></textarea>
						<button class="p-funcModify" onclick="editBoard()">Edit Board</button>
					</div>
					
				</div> <!-- p-funcLeftFrame end -->
				
				<div class="p-funcRigthFrame">
					<div class="p-funcBoardHeader">
						<a style="color: #BBBBBB;">GROU BOARD - BOARD PAGE</a>         
						<button id="p-projectComplete" onclick="projectComplete()">Project Complete</button>
					</div>
					<div class="p-funcBoardList">
						<table class="p-funcTable" style="table-layout: fixed; margin-left: auto; margin-right: auto; overflow: auto;">
							<c:forEach items="${ requestScope.functionBoardList }" var="funcBoard" varStatus="i">
							
									<!-- 5로나눠서 나머지가 1인경우 tr을 열어줌 -->
									
									<c:if test="${ i.count % 5 eq 1 }">
										<tr>
											<td class="p-funcTd">
												<div></div>
												<a><c:out value="${ funcBoard.funcBoardNo }"/></a> 
												<strong><c:out value="${ funcBoard.funcBoardTitle }"/> </strong> 
												<p><c:out value="${ funcBoard.funcBoardContent }"/></p><br> 
												<input type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ funcBoard.funcBoardNo }&projectNo=${ requestScope.projectInfo.projectNo }'" value=""> 
											</td>  
									</c:if>
									
									<!-- 5개가 쌓이면 tr을 닫아줌  -->
									
									<c:if test="${ i.count % 5 eq 0 }">
											<td class="p-funcTd">
												<div></div>
												<a><c:out value="${ funcBoard.funcBoardNo }"/></a> 
												<strong><c:out value="${ funcBoard.funcBoardTitle }"/> </strong> 
												<p><c:out value="${ funcBoard.funcBoardContent }"/></p><br>  
												<input type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ funcBoard.funcBoardNo }&projectNo=${ requestScope.projectInfo.projectNo }'" value=""> 
											</td>
										</tr>
									</c:if>
									
									<!-- 나머지 경우에는 td만 추가 -->
									
									<c:if test="${ i.count % 5 ne 1 &&  i.count % 5 ne 0 }">
										<td class="p-funcTd">
											<div></div>
											<a><c:out value="${ funcBoard.funcBoardNo }"/></a> 
											<strong><c:out value="${ funcBoard.funcBoardTitle }"/> </strong> 
											<p><c:out value="${ funcBoard.funcBoardContent }"/></p><br> 
											<input type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/kanban/search?funcBoardNo=${ funcBoard.funcBoardNo }&projectNo=${ requestScope.projectInfo.projectNo }'" value=""> 
										</td>  
									</c:if>
									
							</c:forEach>
						</table>
					</div> <!-- p-funcBoardList end -->
				</div> <!-- p-funcRigthFrame end -->
			
			</div>
			
	
		<!-- 기능보드 추가 모달창 -->
		
        <a data-toggle="modal" data-target="#intro" id="p-funcModalOpen" style="display: none;"></a>
         <div class="modal modal2 fade bd-example-modal-xl" id="intro" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-centered" id="p-modalArea" role="document">
             <div class="modal-content">
                 <div class="modal-header" style="background: #75CB5E;">
                    <h4 class="modal-title" id="dlogLabel" >
                    <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
                   </h4>
               	 </div>
	               	<form id="p-funcForm" method="post">
	                 <div class="modal-body">
			   				<input id="p-projectNoModal" name="ProjectNo" style="display: none;" value=""> 
			   				<input id="p-funcBoardNoModal" name="funcBoardNo" style="display: none;" value=""> 
		                 <span class="p-modalRow">
		                 <input type="text" name="projectNo" value="${ requestScope.projectInfo.projectNo }" style="display: none;"> 
			   				<input id="p-funcBoardName2" name="funcBoardTitle" size="30" value="" placeholder="보드 이름을 입력해주세요" required> 
		                 </span>
		                 <span class="p-modalRow">
			   				<input id="p-funcBoardContent2" name="funcBoardContent" size="60" value="" placeholder="보드의 간략한 설명을 입력해주세요 (최대 30자)" required> 
		                 </span>
	                 </div>
	                	<div class="modal-footer">
	                     <a href="#" role="button" class="btn btn-default" id="p-insertBtn" data-dismiss="modal">취소</a> 
	                       <input type="submit" class="btn btn-default" id="p-submitBtn" 
	                       	formaction="${ pageContext.servletContext.contextPath }/project/board/insert" value="완료 ">
	                       <input type="submit" class="btn btn-default" id="p-updateBtn" 
	                       formaction="${ pageContext.servletContext.contextPath }/project/board/modify" value="수정 ">
		                </div>
		            </form>
	             </div>
            </div>
        </div>


</div> <!-- content end -->

           
	<script>
	
	if($("td")) {
		
		const $tds = $("td");
		
		for(let i = 0; i < $tds.length; i++) {
			
			$tds[i].onmouseover = function() {
				$tds[i].style.cursor = "pointer";
			}
			
			$tds[i].onmouseout = function() {
				$tds[i].style.cursor = "default";
			}
			
		    /* td 클릭시 이벤트 발생 */	
		    
			$tds[i].onclick = function() {
				
				/* 클릭한 객체의 테두리 변경 */
				
				$("td").css("border","7px solid #E3E3E3");
				this.style.border = "7px solid #2BAF66";
				
				const $funcBoardNo = this.children[1].innerText;
				const $funcBoardContent = this.children[3].innerText;
				
				$(".p-funcTextarea").empty();
				$(".p-funcTextarea").append($funcBoardContent);
				
				$(".p-funcBoardNo").empty();
				$(".p-funcBoardNo").append($funcBoardNo);
				
			} <!-- onclick event end -->
			
		} <!-- for end -->
		
	}; <!-- if end --> 
	
	/* 기능보드 추가 버튼 클릭 시  */
	
	function addFuncBoard() {
		
		$("#p-submitBtn").css("display","inline");
		$("#p-updateBtn").css("display","none");
		$("#p-funcBoardName2").val("");
		$("#p-funcBoardContent2").val("");
		
		$("a[id=p-funcModalOpen]").click();
		
	}
	
	/* 기능보드 삭제 버튼 클릭시  */
	
	function deleteFuncBoard() {
		
		const $projectNo = ${ requestScope.projectInfo.projectNo };
		const $funcBoardNo = $(".p-funcBoardNo").text();
		
		if (confirm("선택한 기능보드를 삭제하시겠습니까?\n 작업중이던 내용이 전부 삭제됩니다.")) {
        	location.href='${ pageContext.servletContext.contextPath }/project/board/delete?funcBoardNo=' + $funcBoardNo +'&projectNo=' + $projectNo;
        } else {
        	return;
        }
		
	}
	
	/* 기능보드 수정 버튼 클릭 시 */
	
	function editBoard() {
		
		const $projectNo = ${ requestScope.projectInfo.projectNo };
		const $funcBoardNo = $(".p-funcBoardNo").text();
		
		
		$.ajax({
			url : "${ pageContext.servletContext.contextPath }/project/board/modify",	
			data : { $funcBoardNo : $funcBoardNo , $projectNo : $projectNo },
			dataType : "json",
			method : "GET",	
			success : function(data) {
				
				/* update 버튼 on */
				
				$("#p-submitBtn").css("display","none");
				$("#p-updateBtn").css("display","inline");
				console.table(data);
				
				const funcTitle = data.funcBoardTitle;
				const funcContent = data.funcBoardContent;
				
				/* 조회한 정보 입력 */
				
				$("#p-projectNoModal").val($projectNo);
				$("#p-funcBoardNoModal").val($funcBoardNo);
				$("#p-funcBoardName2").val(funcTitle);
				$("#p-funcBoardContent2").val(funcContent);
				
				$("a[id=p-funcModalOpen]").click();
				
			},
			error : function(xhr) {
				console.table(xhr);				
			}
			
		});
		
	}
	
	
	/* 프로젝트 삭제버튼 클릭 시  */
	
	function deleteProject() {
		
        if (confirm("정말 프로젝트를 삭제하시겠습니까?\n 작업중이던 프로젝트 내용이 전부 삭제됩니다.")) {
        	location.href='${ pageContext.servletContext.contextPath }/project/delete?projectNo=${ requestScope.projectInfo.projectNo }';
        } else {
        	return;
        }
	}
	
	/* 프로젝트 완료 버튼 클릭 시 */
	
	function projectComplete() {
		
		if (confirm("프로젝트를 완료상태로 변경 하시겠습니까?\n 완료상태가 된 프로젝트는 다시 진행할 수 없습니다.")) {
        	location.href='${ pageContext.servletContext.contextPath }/project/modify?projectNo=${ requestScope.projectInfo.projectNo }';
        } else {
        	return;
        }
	}
	
	</script>
	
</body>
</html>