<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
	
	
	<div class="content">
	
	<p class="menuName">단위업무 & 이슈</p> 
			
		<div id="menuNameUnderline"></div> 
		
		<!-- 프로젝트 참여 인원 목록 -->	
		<div id="p-projectMemberList">
			<p>Project Member</p>
			<c:forEach items="${ requestScope.projectMemberList }" var="member">
				<c:out value="${ member.empName }"/>(#<c:out value="${ member.empNo }"/>)
			</c:forEach>
		</div>
		<div class="p-kanbanBoard" id="p-kanbanBoard">
		
			<div class="p-kanban p-kbNo1" id="p-kanban1">
				<ol>
					<p>해야할 일</p>
					
					<!-- 전체 단위업무를 조회하여 업무리스트 번호별로 구분  -->
					<c:forEach var="list" items="${ requestScope.partWorkList }">
						<c:if test="${ list.workListNo + 0 eq 1}">
							<dd>
								<!-- 단위업무 이름  -->
								<a id="p-partWorkName"><c:out value="${ list.partWorkName }"/></a> <br>
								<!-- 단위업무 기간 -->
								<a id="p-partWorkDate"><c:out value="${ list.startDate }"/> ~ <c:out value="${ list.targetDate }"/></a>
								<a id="p-partWorkNo"><c:out value="${ list.partWorkNo }"/></a> <!-- 단위업무 번호 -->
								<input type="hidden" id="projectNo" value="${ list.projectNo }"> 	<!-- projectNo -->
								<input type="hidden" id="funcBoardNo" value="${ list.funcBoardNo }">  <!-- funcBoardNo -->
								<img alt="" class="p-clock" src="${ pageContext.servletContext.contextPath }/resources/images/clock.png" >
							</dd>
						</c:if>
					</c:forEach>
				</ol>
				<span id="p-kanbanFooter">
					<!-- 단위업무 추가 버튼 -->
					<a id="p-partWorkAdd1" class="p-partWorkAdd" href="#">+ADD NEW TASK</a>
				</span>	
			</div>
			
			<div class="p-kanban p-kbNo2" id="p-kanban2">
				<ol>
					<p>개발중</p>
					
					<c:forEach var="list" items="${ requestScope.partWorkList }">
						<c:if test="${ list.workListNo + 0 eq 2}">
							<dd>
								<a id="p-partWorkName"><c:out value="${ list.partWorkName }"/></a> <br>
								<a id="p-partWorkDate"><c:out value="${ list.startDate }"/> ~ <c:out value="${ list.targetDate }"/></a>
								<a id="p-partWorkNo"><c:out value="${ list.partWorkNo }"/></a>
								<input type="hidden" id="projectNo" value="${ list.projectNo }"> 	<!-- projectNo -->
								<input type="hidden" id="funcBoardNo" value="${ list.funcBoardNo }">  <!-- funcBoardNo -->
								<img class="p-clock" alt="" src="${ pageContext.servletContext.contextPath }/resources/images/clock.png"  >
							</dd>
						</c:if>
					</c:forEach>
				</ol>
				<span id="p-kanbanFooter">
					<a id="p-partWorkAdd2" class="p-partWorkAdd" href="#">+ADD NEW TASK</a>
				</span>
			</div>
			
			<div class="p-kanban p-kbNo3" id="p-kanban3">
				<ol>
					<p>테스트 진행중</p>
					<c:forEach var="list" items="${ requestScope.partWorkList }">
						<c:if test="${ list.workListNo + 0 eq 3}">
							<dd>
								<a id="p-partWorkName"><c:out value="${ list.partWorkName }"/></a> <br>
								<a id="p-partWorkDate"><c:out value="${ list.startDate }"/> ~ <c:out value="${ list.targetDate }"/></a>
								<a id="p-partWorkNo"><c:out value="${ list.partWorkNo }"/></a>
								<input type="hidden" id="projectNo" value="${ list.projectNo }"> 	<!-- projectNo -->
								<input type="hidden" id="funcBoardNo" value="${ list.funcBoardNo }">  <!-- funcBoardNo -->
								<img alt="" class="p-clock" src="${ pageContext.servletContext.contextPath }/resources/images/clock.png">
							</dd>
						</c:if>
					</c:forEach>
				</ol>
				<span id="p-kanbanFooter">
					<a id="p-partWorkAdd3" class="p-partWorkAdd" href="#">+ADD NEW TASK</a>
				</span>
			</div>
			
			<div class="p-kanban p-kbNo4" id="p-kanban4">
				<ol>
						<p>완료</p>
					<c:forEach var="list" items="${ requestScope.partWorkList }">
						<c:if test="${ list.workListNo + 0 eq 4}">
							<dd>
								<a id="p-partWorkName"><c:out value="${ list.partWorkName }"/></a> <br>
								<a id="p-partWorkDate"><c:out value="${ list.startDate }"/> ~ <c:out value="${ list.targetDate }"/></a>
								<a id="p-partWorkNo"><c:out value="${ list.partWorkNo }"/></a>
								<input type="hidden" id="projectNo" value="${ list.projectNo }"> 	<!-- projectNo -->
								<input type="hidden" id="funcBoardNo" value="${ list.funcBoardNo }">  <!-- funcBoardNo -->
								<img alt="" class="p-clock" src="${ pageContext.servletContext.contextPath }/resources/images/clock.png" >
							</dd>
						</c:if>
					</c:forEach>
				</ol>
				<span id="p-kanbanFooter">
					<a id="p-partWorkAdd4" class="p-partWorkAdd" href="#">+ADD NEW TASK</a>
				</span>
			</div>
			
			<div class="p-kanban p-kbNo5">
				<ol>
					<p>이슈</p>
					<c:forEach var="list" items="${ requestScope.issueList }">
						<dt>
							<a id="p-partWorkName"><c:out value="${ list.issueName }"/></a> <br>
							<a id="p-issueWriter"><c:out value="${ list.writer }"/> - </a>   
							<a id="p-issueDate"><c:out value="${ list.writeDate }"/></a>
							<a hidden><c:out value="${ list.issueNo }"/></a>
							<br>
							<a id="p-issueContent"><c:out value="${ list.issueContent }"/></a>
							<a hidden id="projectNo"><c:out value="${ list.projectNo }"/></a>
							<a hidden id="funcBoardNo"><c:out value="${ list.funcBoardNo }"/></a>
						</dt>
					</c:forEach>
				</ol>
				<span id="p-kanbanFooter">
					<a id="e-issueAdd" class="e-issueAdd" href="#">+ADD NEW ISSUE</a>
				</span>
			</div>
			
		</div>	<!-- 칸반 끝 -->
		

		
	<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->
			
			<!-- 단위업무 모달 -->
			<a href="#dialog" id="p-modalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: none;"></a>
			             
	        <div class="modal fade bd-example-modal-xl" id="dialog" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">
	 
 	             <div class="modal-dialog modal-xl" id="p-modalArea" role="document">
	             
	             
	                <div class="modal-content">
	                    <div class="modal-header" style="background: #75CB5E;">
	                         <h4 class="modal-title" id="dlogLabel" >
	                         <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
	                        </h4>
	                    </div>
						<form method="get" onsubmit="readyTransfer()">
							<input type="hidden" id="p-projectNo" name="projectNo" value="${ requestScope.projectNo }">
							<input type="hidden" id="p-funcBoardNo" name="funcBoardNo" value="${ requestScope.funcBoardNo }">
							<input type="hidden" id="p-workListNo" name="workListNo" value="">
		                    <div class="modal-body">
		                    	<span class="p-modalRow">
		   							<span class="p-modalTextHeader">제목</span> 
		   							<input id="p-partWorkName2" name="partWorkName" size="30" value="" placeholder="제목을 입력하세요." required> 
		                    	</span>
		                    	<br> 
		   						<table id="p-workListTable">
		   							<tr id="p-workList">
		   								<input type="hidden" name="workListNo" id="p-workListNo" value="">
		   								<td id="p-workList1" class="p-workList">
		   									<a id="p-workListBtn1" class="p-workListBtn">해야할 일</a>
		   								</td>
		   								<td id="p-workList2" class="p-workList">
		   									<a id="p-workListBtn2" class="p-workListBtn">개발중</a>
		   								</td>
		   								<td id="p-workList3" class="p-workList">
		   									<a id="p-workListBtn3" class="p-workListBtn" >테스트 진행중</a>
		   								</td>
		   								<td id="p-workList4" class="p-workList">
		   									<a id="p-workListBtn4" class="p-workListBtn" >완료</a>
		   								</td>
		   							</tr>
		   						</table>
		   						<span class="p-modalRow">
			   						<span class="p-modalTextHeader">담당자</span> 
			   						<select id="p-projectMember" > <!-- 프로젝트 참여인원 불러오는 곳  --> </select>
			   						<div>
				   						<p id="p-partWorkMember"></p> 
				   						<!-- 단위업무 참여인원 목록 담아서 controller로 전송하는 곳 -->
			   							<input id="p-partWorkMember" name="partWorkMember" style="display: none;"></input> 
			   						</div>
		   						</span>
		   						<span class="p-modalRow">
		                        	<span class="p-modalTextHeader">기간</span>
			                        	<input type="date" id="p-startDate" name="startDate" required> <!-- 시작일 -->
			                        	<a id="p-dateline">-</a> 
			                        	<input type="date" id="p-targetDate" name="targetDate" required> <!-- 목표일 -->
		   						</span>
		   						<br>
		   						<span class="p-modalRow">
			   						<span class="p-modalTextHeader">내용</span> 
		                        	<textarea cols="120" rows="15" id="p-partWorkContent" name="partWorkContent" placeholder="내용을 입력하세요." maxlength="650" required></textarea>
	                        	</span>
	                        	<br>
		                    </div>
		                    <div class="modal-footer">
		                    
		                    	<!-- 단위업무 번호 저장하는 위치  -->
		                    	<input type="text" id="partWorkNo" name="partWorkNo" value="" style="display: none;">
		                    	
		                    	<!-- 취소 버튼 -->
		                        <a href="#" role="button" class="btn btn-default" id="p-return" data-dismiss="modal">취소</a> 
		                        
		                        <!-- 작성 완료 버튼  -->
		                        <input type="submit" class="btn btn-default" id="p-insertBtn" formaction="${ pageContext.servletContext.contextPath }/kanban/insert" value="완료 ">
		                        
		                        <!-- 수정 완료 버튼 -->
		                        <input type="submit" class="btn btn-default" id="p-updateBtn" formaction="${ pageContext.servletContext.contextPath }/kanban/modify" value="완료 " >
		                        
		                        <!-- 수정페이지 이동 버튼 -->
		                        <p href="#" id="p-enterModify" onclick="enterModify()">수정</p>
		                        
		                        <!-- 삭제 버튼 -->
		                        <p href="#" id="p-deleteBtn">삭제</p>
		                    </div>
						</form>	                    
	                </div>
	                 
	            </div> 
	             
	        </div>
	        
	        
	        
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->

		<script>
		
		/* 업무리스트 버튼 클릭시 업무리스트 위치 이동 */
		$(".p-workListBtn").click(function() {
			
			/* 업무리스트 속성 전체 초기화 */
			$("#p-workList").children().css({"background" : "white" , "color" : "#333333"});
			$(".p-workListBtn").css({"background" : "white" , "color" : "#333333"});
			$("#p-workListNo").val("");
			
			const id = $(this).attr("id");
			
			switch(id) {
				case "p-workListBtn1" : $("#p-workListNo").val(1);
										$("#p-workList1").css({"background" : "#C6D82E" , "color" : "white"});
										$("#p-workListBtn1").css({"background" : "#C6D82E" , "color" : "white"});
										break;
				case "p-workListBtn2" : $("#p-workListNo").val(2);
										$("#p-workList2").css({"background" : "#9FC266" , "color" : "white"});
										$("#p-workListBtn2").css({"background" : "#9FC266" , "color" : "white"});
										break;
				case "p-workListBtn3" : $("#p-workListNo").val(3);
										$("#p-workList3").css({"background" : "#75CB5E" , "color" : "white"});
										$("#p-workListBtn3").css({"background" : "#75CB5E" , "color" : "white"});
										break;
				case "p-workListBtn4" : $("#p-workListNo").val(4);
										$("#p-workList4").css({"background" : "#2BAF66" , "color" : "white"});
										$("#p-workListBtn4").css({"background" : "#2BAF66" , "color" : "white"});
										break;
			}
			
			const workListNo = $("#p-workListNo").val();
			const partWorkNo = $("#partWorkNo").val();
			
			/* 선택한 업무리스트 번호로  update */
			$.ajax({
				url : "${ pageContext.servletContext.contextPath }/kanban/modify",
				data : { partWorkNo : partWorkNo , workListNo : workListNo },
				method : "POST",
				dataType : "json",
				success : function(data) {
					
					/* 수정후에 페이지를 새로고침 */
					  location.reload(); 
				},
				error : function(xhr) {
					
				}
			});
			
		});
		
		
		
		
		/* 각 단위업무 상세조회 모달창 띄우기 */
		if(document.getElementsByTagName("dd")) {
			
			const $root = "${ pageContext.servletContext.contextPath }";
			
			const $dds = document.getElementsByTagName("dd");
			
			for(let i = 0; i < $dds.length; i++) {
				
				$dds[i].onmouseover = function() {
					$dds[i].style.backgroundColor = "#F1F1F1";
					$dds[i].style.cursor = "pointer";
				}
				
				$dds[i].onmouseout = function() {
					$dds[i].style.backgroundColor = "white";
					$dds[i].style.cursor = "default";
				}
				
				$dds[i].onclick = function() {
					const $partWorkNo = $dds[i].children[3].innerText;
					
					/* jquery promise */
					partWorkInfoAjax($partWorkNo)
					.then(partWorkMemberAjax)
					.then(successFunction)
					.catch(errorFunction);	
					
				} <!-- onclick event end -->
				
			} <!-- for end -->
			
		}; <!-- if end -->
		
				/* 단위업무 번호로 해당 단위업무의 정보를 조회해옴 */
				function partWorkInfoAjax($partWorkNo) {
					return new Promise(function(resolve, reject){
						
						$.ajax({
							url : "${ pageContext.servletContext.contextPath }/kanban/search",	
							data : { $partWorkNo : $partWorkNo },
							dataType : "json",
							method : "POST",	
							success : function(data) {
								
								/* 현재 모달창에 들어가있는 정보들 초기화 */
								$("#p-partWorkContent").empty();
								$("#p-partWorkName2").empty();
								$("#p-projectMember").empty();
								$("#p-startDate").empty();
								$("#p-targetDate").empty();
								$("#p-partWorkMember").empty();
								$("#p-workListNo").empty();
								$("#p-workListNo").val("");
								$("#p-workList").children().css({"background" : "white" , "color" : "black"});
								$(".p-workListBtn").css({"background" : "white" , "color" : "black"});
								$("#partWorkNo").val("");
								
								/* 조회기능이므로 정보들 수정 불가로 변경 */
								$("#p-projectMember").css("display","none");
								$("#p-insertBtn").css("display","none");
								$("#p-updateBtn").css("display","none");
								$("#p-deleteBtn").css("display","inline");
								$("#p-enterModify").css("display","inline");
								$("#p-partWorkContent").attr("readonly", true);
								$("#p-partWorkName2").attr("readonly", true);
								$("#p-startDate").attr("readonly", true);
								$("#p-targetDate").attr("readonly", true);
								$("#p-partWorkMember").attr("readonly", true);
								$("#p-workListNo").attr("readonly", true); 
		
								/* 업무리스트 번호에 따라 해당 칸 색 변경 */
								  switch(data.workListNo) {
									case 1 : $("#p-workList1").css({"background" : "#C6D82E" , "color" : "white"});
											$("#p-workListBtn1").css({"background" : "#C6D82E" , "color" : "white"});
											$("#p-workListNo").val(1);	
											break; 
									case 2 : $("#p-workList2").css({"background" : "#9FC266" , "color" : "white"}); 
											$("#p-workListBtn2").css({"background" : "#9FC266" , "color" : "white"});	
											$("#p-workListNo").val(2);	
											break; 
									case 3 : $("#p-workList3").css({"background" : "#75CB5E" , "color" : "white"}); 
											$("#p-workListBtn3").css({"background" : "#75CB5E" , "color" : "white"});
											$("#p-workListNo").val(3);
											break; 
									case 4 : $("#p-workList4").css({"background" : "#2BAF66" , "color" : "white"}); 
											$("#p-workListBtn4").css({"background" : "#2BAF66" , "color" : "white"});
											$("#p-workListNo").val(4);
											break; 
									default : console.log(data.workListNo); 
											  console.table(data);
											  break;
								}  
								
								/* 조회해온 정보들 모달창에 입력 */
								$("#partWorkNo").val(data.partWorkNo);
								$("#p-partWorkName2").val(data.partWorkName);
								$("#p-partWorkContent").append(data.partWorkContent); 
								$("#p-startDate").val(data.startDate);
								$("#p-targetDate").val(data.targetDate);
								
								/* 성공시 다음 호출되는 function에 단위업무번호 전달 */
								let $partWorkNo = data.partWorkNo;
								resolve($partWorkNo);
								
							},
							error : function(xhr) {
								console.table(xhr);
							}
						}); <!-- ajax end -->
						
					}); <!-- return end -->
				
				} <!-- first promise end -->
					
				/* 단위업무 정보 조회에 성공하였을 경우 단위업무 번호로 해당 단위업무의 담당자를 조회해옴 */
				function partWorkMemberAjax($partWorkNo){
					return new Promise(function(resolve, reject){
						
						$.ajax({
							url : "${ pageContext.servletContext.contextPath }/projectMember/search",
							data : { $partWorkNo : $partWorkNo },
							dataType : 'json',
							method : 'GET',
							success : function(data){
								
								$("#p-projectMember").empty();
									
								/* 값이 JSON Array 형태로 넘어오기 때문에 반복문으로 값을 출력 */
								for (var i in data) {
									let empNo = data[i].empNo;
									let empName = data[i].empName;
									let empInfo = empName+"#"+empNo;
									
									/* 출력한 값을 변수에 담아 담당자칸에 삽입 */
									if($("#p-partWorkMember").is(':empty')) {
										$("#p-partWorkMember").append(empInfo);
									} else {
										$("#p-partWorkMember").append("," + empInfo);
									}
								 	
								}
									/* 다음 함수 실행 */
									resolve();
								
							},
							error : function(xhr) {
							}
							
						}); <!-- ajax end -->
						
					}); <!-- partWorkMemberAjax return end -->
					
				} <!-- partWorkMemberAjax end -->
				
				
				/* 단위업무정보와 담당자 모두 조회 성공시 모달창 출력*/
				function successFunction(){
					$("a[id=p-modalOpen]").click();
					return false;
				}
				
				/* 둘 중 하나라도 에러날 경우 error */
				function errorFunction(){
					alert("error");
					/* 중간에 에러 발생 시 모달 창에 입력된 칸들 비워 줌 */
					$("#p-partWorkName2").val("");
					$("#p-partWorkContent").val("");
					$("#p-projectMember").val("");
					$("#p-startDate").val("");
					$("#p-targetDate").val("");
					$("#p-partWorkMember").val("");
					$("#p-partWorkNo").val("");
					$("#p-workList").children().css({"background" : "white" , "color" : "black"});
					return false;
				}

		
		/* 단위업무 수정하기 */
		function enterModify() {
			$("#p-enterModify").css("display","none");
			$("#p-deleteBtn").css("display","none");
			$("#p-insertBtn").css("display","none");
			$("#p-updateBtn").css("display","inline");
			$("#p-projectMember").css("display","inline");
			$("#p-partWorkContent").attr("readonly", false);
			$("#p-partWorkName2").attr("readonly", false);
			$("#p-startDate").attr("readonly", false);
			$("#p-targetDate").attr("readonly", false);
			$("#p-partWorkMember").attr("readonly", false);
			$("#p-workListNo").attr("readonly", false); 
			
			const projectNo = $("#p-projectNo").val();
			
			$.ajax({
				url : "${ pageContext.servletContext.contextPath }/projectMember/search",
				data : { projectNo : projectNo },
				dataType : 'json',
				method : 'POST',
				success : function(data){
					
					let empName ="";
					
					for (var i in data) {
						empName += "<option value1='" + data[i].projectMemberNo 
									   + "' value2='" + data[i].empName 
									   + "'>" + data[i].empName + "</option>";
						
					}
						
						$("#p-projectMember").empty();
						$("#p-projectMember").append(empName);
				},
				error : function(xhr) {
					
				}
				
			});
		}
		
				
				
		/* 새 단위업무 추가하기 */
		$(".p-partWorkAdd").click(function() {
			
			const $addId = $(this).attr("id");
			
			const projectNo = $("#p-projectNo").val();
			const funcBoardNo = $("#p-funcBoardNo").val();
			
			$("#p-workList").children().css({"background" : "white" , "color" : "black"});
			$(".p-workListBtn").css({"background" : "white" , "color" : "black"});
			
			/* 선택한 업무리스트 번호에 따라서 모달창에서 업무리스트 영역 배경 지정 */
			switch($addId) {
				case "p-partWorkAdd1" : 
										$("#p-workListNo").val(""); 
										$("#p-workListNo").val(1); 
										$("#p-workList1").css({"background" : "#C6D82E" , "color" : "white"});
										$("#p-workListBtn1").css({"background" : "#C6D82E" , "color" : "white"});
										break;
				case "p-partWorkAdd2" : 
										$("#p-workListNo").val(""); 
										$("#p-workListNo").val(2); 
										$("#p-workList2").css({"background" : "#9FC266" , "color" : "white"});
										$("#p-workListBtn2").css({"background" : "#9FC266" , "color" : "white"});
										break;
				case "p-partWorkAdd3" : 
										$("#p-workListNo").val(""); 
										$("#p-workListNo").val(3); 
										$("#p-workList3").css({"background" : "#75CB5E" , "color" : "white"});
										$("#p-workListBtn3").css({"background" : "#75CB5E" , "color" : "white"});
										break;
				case "p-partWorkAdd4" : 
										$("#p-workListNo").val(""); 
										$("#p-workListNo").val(4); 
										$("#p-workList4").css({"background" : "#2BAF66" , "color" : "white"});
										$("#p-workListBtn4").css({"background" : "#2BAF66" , "color" : "white"});
										break;
				default : 
										alert("$addId" + $addId);
										break;
			}
			
			$.ajax({
				url : "${ pageContext.servletContext.contextPath }/projectMember/search",
				data : { projectNo : projectNo },
				dataType : 'json',
				method : 'POST',
				success : function(data){
					console.table(data);
					
					/* 현재 모달창에 들어가있는 정보들 초기화 */
					$("#p-partWorkNo").val("");
					$("#p-partWorkName2").val("");
					$("#p-partWorkContent").empty();
					$("#p-startDate").val("");
					$("#p-targetDate").val("");
					$("p[id=p-partWorkMember]").empty();
					$("#p-projectMember").removeAttr('style');
					$("#p-projectMember").children().remove();
					
					/* 작성 기능이므로 정보들 수정 가능으로 변경 */ 
					$("#p-enterModify").css("display","none");
					$("#p-deleteBtn").css("display","none");
					$("#p-insertBtn").css("display","inline");
					$("#p-updateBtn").css("display","none");
					$("#p-partWorkContent").attr("readonly", false);
					$("#p-partWorkName2").attr("readonly", false);
					$("#p-startDate").attr("readonly", false);
					$("#p-targetDate").attr("readonly", false);
					$("#p-partWorkMember").attr("readonly", false);	//담당자는 직접 입력으로 수정불가
					
					let empName ="";
					
					for (var i in data) {
						empName += "<option value1='" + data[i].projectMemberNo 
									   + "' value2='" + data[i].empName 
									   + "'>" + data[i].empName + "</option>";
						
					}
						$("#p-projectMember").append(empName);
					
						$("a[id=p-modalOpen]").click();
				},
				error : function(xhr) {
					console.table(xhr);		
				}
				
			});
			
				
			
		});
		
		
		
		/* modal창 select 선택 시 발생 함수 */
		$("#p-projectMember").on('change', function() {
			
	          const $memberNo = $(this).children("option:selected").attr("value1");
	          const $empName = $(this).children("option:selected").attr("value2");
	          
	          const removeName = "<a href='#'onclick='removeText()' id='p-members'>"+$empName+"#"+$memberNo+"</a>"
	          const removeName2 = "<a href='#'onclick='removeText()'id='p-members'>,"+$empName+"#"+$memberNo+"</a>"
	          
	          const innerText = $("p[id=p-partWorkMember]").text()
	          
	          if(!innerText.includes($empName)){
	        	  if(innerText.length < 1) {
			          $("p[id=p-partWorkMember]").append(removeName);
	        	  } else {
			          $("p[id=p-partWorkMember]").append(removeName2);
	        	  }
	          } else {
	        	  alert("이미 추가된 사원입니다.");
	          }

		});
		
		/* 단위업무 삭제 버튼 클릭 */
		$("#p-deleteBtn").click(function() {
			
			if(confirm("선택한 단위업무를 삭제하시겠습니까?\n 작업중이던 내용이 전부 삭제됩니다.")){
				
				const partWorkNo = $("#partWorkNo").val();
				const projectNo = $("#p-projectNo").val();
				const funcBoardNo = $("#p-funcBoardNo").val();
				
				location.href="/groubear/kanban/delete?funcBoardNo="+funcBoardNo+"&projectNo="+projectNo+"&partWorkNo="+partWorkNo+"";
				
			} else {
				return;
			}
			
			
		});
		
		
		/* 프로젝트 담당자 지정 목록에서 클릭시에 실행 */
		function removeText() {
			
			/* 전체 a태그를 지정 */
			const $a = document.getElementsByTagName("a");
			
			for (var i = 0; i < $a.length; i++) {
				
				/* i번째 a태그를 누를시에 i번째 a태그를 삭제 */
				$a[i].onclick = function() {
					
					$(this).remove();
				}
				
			}
			
		}
		
		/* form data 전송 submit 전에 실행 */
		function readyTransfer() {
			
			const innerText = $("p[id=p-partWorkMember]").text()
			
			/* p태그 사이에 들어간 "사원명#프로젝트참여번호"로 이루어진 문자열을 input태그의 value값으로 전달 */
			const partWorkList = $("input[id=p-partWorkMember]").val(innerText);
			
			if(partWorkList != null) {
				return true;
			} else {
				return false;
			}
			
		}
		
		
		
		
		</script>
		
<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->				
		
		<!-- 이슈 상세 보기 모달 -->
	<a href="#dialog2" id="e-modalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: none;"></a>

		<div class="modal fade bd-example-modal-xl" id="dialog2" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">

			<div class="modal-dialog modal-xl" id="p-modalArea" role="document">
			<form id="formId" action="${ pageContext.servletContext.contextPath }/issue/test" method="post">
				<div class="modal-content">

					<!-- 모달 타이틀 -->
					<div class="modal-header" style="background: #75CB5E;">
	                	<h4 class="modal-title" id="dlogLabel" >
	                         <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
	                    </h4>
					</div>
					<!-- 모달 타이틀 end -->
					
					<input type="hidden" id="e-projectNo" name="projectNo" value="${ requestScope.projectNo }">
					<input type="hidden" id="e-funcBoardNo" name="funcBoardNo" value="${ requestScope.funcBoardNo }">
					<input type="hidden" id="e-issueNo" name="issueNo" value="">
					<input type="hidden" id="e-writer" name="writer" value="">
                    <div class="modal-body">
            		    <span class="p-modalRow">
							<span class="p-modalTextHeader">제목</span> 
  							<input id="e-issueName" name="issueName" value="" readonly>
                   		</span>
   						 <br>
   						 <span class="p-modalRow">
	   						<span class="p-modalTextHeader">담당자</span> 
	   						<select id="e-selMember">
	   						<c:forEach items="${ requestScope.projectMemberList }" var="proMember">
	   								<option><c:out value="${ proMember.empName }#${ proMember.projectMemberNo }"/></option>
	   						</c:forEach>
	   							   </select><input type="text" id="e-add2" value="추가" readonly>
   							<p id="e-issueMember"></p>
		   				</span>
   						 <span class="p-modalRow" id="e-dateModalRow">
	   						<input id="e-issueMember" name="issueMember" style="display: none;" readonly>
	                       	<span class="p-modalTextHeader" id="writeDate" value="작성일">작성일</span><input type="date" id="e-writeDate" name="writeDate" value="" readonly><br>
                       	</span>
   						 <span class="p-modalRow" >
                       		<textarea cols="115" rows="15" id="e-issueContent" name="issueContent" readonly></textarea>
                       	</span>
                       	<br>
                    </div>
                    <div class="modal-footer">
                    	<input type="submit" class="btn btn-default" id="btn-insert" value="등록">
                    	<input type="submit" class="btn btn-default" id="btn-updateComplete" value="수정완료">
                        <text id="btn-update" class="btn btn-update">수정</text> 
                        <text id="btn-delete" class="btn btn-delete">삭제</text> 
                        <text id="btn-close" class="btn btn-closed" data-dismiss="modal">닫기</text> 
                    </div>
				</div>
			</form>
			</div>

		</div>
		<!-- 이슈 상세 보기 모달 end -->
<!-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------ -->		
	<script>
		/* 이슈 목록 클릭시 동작할 내용 */
		if(document.getElementsByTagName("dt")) {
			
			const $dts = document.getElementsByTagName("dt");
			
			for(let i = 0; i < $dts.length; i++) {
				
				$dts[i].onmouseover = function() {
					$dts[i].style.backgroundColor = "#F1F1F1";
					$dts[i].style.cursor = "pointer";
				}
				
				$dts[i].onmouseout = function() {
					$dts[i].style.backgroundColor = "white";
					$dts[i].style.cursor = "default";
				}
				
				$dts[i].onclick = function() {
					var login = "${ sessionScope.loginMember.empName } - "; 
					var writer2 = this.children[2].innerText;
					if(login==writer2) {
						var btnUpdate = document.getElementById("btn-update");
						btnUpdate.style.display = 'inline';
						var btnDelete = document.getElementById("btn-delete");
						btnDelete.style.display = 'inline';
					} else {
						var btnUpdate = document.getElementById("btn-update");
						btnUpdate.style.display = 'none';
						var btnDelete = document.getElementById("btn-delete");
						btnDelete.style.display = 'none';
					}
					/* 조회 기능에 맞게 디스플레이 */
					var btnInsert = document.getElementById("btn-insert");
					btnInsert.style.display = 'none';
					var selMember = document.getElementById("e-selMember");
					selMember.style.display = 'none';
					var add2 = document.getElementById("e-add2");
					add2.style.display = 'none';
					var dateModalRow = document.getElementById("e-dateModalRow");
					dateModalRow.style.display = 'block';
					var writeDate = document.getElementById("e-writeDate");
					writeDate.style.display = 'inline';
					var writeDate2 = document.getElementById("writeDate");
					writeDate2.style.display = 'inline';
					var updateComplete = document.getElementById("btn-updateComplete");
					updateComplete.style.display = 'none';
					
					/* 조회 기능이므로 정보들 수정 불가능으로 변경 */
					$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/insert");
					$("#e-issueName").attr("readonly", true);
					$("#e-writeDate").attr("readonly", true);
					$("#e-issueContent").attr("readonly", true);
					$("#e-issueMember").attr("readonly", true);
					
					//이슈 번호를 ajax로 전송해서 이슈 담당자를 리턴 받음
					const issueNo = this.children[4].innerText;
					
					$.ajax({
						url: "/groubear/issue/search",
						type: "post",
						data: { issueNo: issueNo },
						success: function(data) {
							var issueMember = data;
							var issueMembers = issueMember.replace("]"," ").replace("["," ");
							$("p[id=e-issueMember]").text(issueMembers);
						},
						error: function(xhr) {
							console.log(xhr);
						}
					});
					document.getElementById("e-issueName").value = this.children[0].innerText;
					document.getElementById("e-writer").value = this.children[2].innerText;
					document.getElementById("e-writeDate").value = this.children[3].innerText;
					document.getElementById("e-issueNo").value = this.children[4].innerText;
					document.getElementById("e-issueContent").value = this.children[6].innerText;
					document.getElementById("e-projectNo").value = this.children[7].innerText;
					document.getElementById("e-funcBoardNo").value = this.children[8].innerText;
					
					$("a[id=e-modalOpen]").click();
				}
			}
		}
		
		/* 이슈 추가하기 버튼 클릭시 동작할 내용 */
		$(".e-issueAdd").click(function() {
			
			/* 현재 모달창에 들어가있는 정보들 초기화 */
			$("#e-issueName").val("");
			$("p[id=e-issueMember]").text("");
			$("#e-writeDate").val("");
			$("#e-issueContent").val("");
			
			/* 작성 기능에 맞게 디스플레이 */
			var btnUpdate = document.getElementById("btn-update");
			btnUpdate.style.display = 'none';
			var btnDelete = document.getElementById("btn-delete");
			btnDelete.style.display = 'none';
			var btnInsert = document.getElementById("btn-insert");
			btnInsert.style.display = 'inline';
			var selMember = document.getElementById("e-selMember");
			selMember.style.display = 'inline';
			var add2 = document.getElementById("e-add2");
			add2.style.display = 'inline';
			var dateModalRow = document.getElementById("e-dateModalRow");
			dateModalRow.style.display = 'none';
			var writeDate = document.getElementById("e-writeDate");
			writeDate.style.display = 'none';
			var writeDate2 = document.getElementById("writeDate");
			writeDate2.style.display = 'none';
			var updateComplete = document.getElementById("btn-updateComplete");
			updateComplete.style.display = 'none';
			
			/* 작성 기능이므로 정보들 수정 가능으로 변경 */
			$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/insert");
			$("#e-issueName").attr("readonly", false);
			$("#e-writeDate").attr("readonly", true);
			$("#e-issueContent").attr("readonly", false);
			$("#e-issueMember").attr("readonly", false);
			
			$("a[id=e-modalOpen]").click();
		});
		
		/* 담당자 추가 버튼 클릭시 동작할 내용 */
		$("#e-add2").click(function() {
			var sel = document.getElementById('e-selMember').value;
			var appendContent = "<a href='#'onclick='removeText()' id='e-members'>" + sel + "</a>" + " ";
			$("p[id=e-issueMember]").append(appendContent);
			var issueMembers2 = $("p[id=e-issueMember]").text();
			var aa = $("input[id=e-issueMember]").val(issueMembers2);
		});
		function removeText() {
			
			/* 전체 a태그를 지정 */
			const $a = document.getElementsByTagName("a");
			
			for (var i = 0; i < $a.length; i++) {
				
				/* i번째 a태그를 누를시에 i번째 a태그를 삭제 */
				$a[i].onclick = function() {
					
					$(this).remove();
				}
			}
		}
		
		//삭제 버튼 클릭시 동작할 내용
		$("#btn-delete").click(function() {
			var result = confirm("정말 삭제 하시겠습니까?");
			
			if(result) {
				const issueNo = document.getElementById("e-issueNo").value;
				const projectNo = document.getElementById("e-projectNo").value;
				const funcBoardNo = document.getElementById("e-funcBoardNo").value;
				const issueMember = document.getElementById("e-issueMember").innerText;
				
				$.ajax({
					url: "/groubear/issue/delete",
					type: "post",
					data: { issueNo: issueNo ,issueMember : issueMember },
					success: function(data) {
						alert(data);
						location.href="/groubear/kanban/search?funcBoardNo="+funcBoardNo+"&projectNo="+projectNo+"";
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				
			}
		});
		
		//수정 버튼 클릭시 동작할 내용
		$("#btn-update").click(function() {
			
			/* 수정 기능에 맞게 디스플레이 */
			var btnUpdate = document.getElementById("btn-update");
			btnUpdate.style.display = 'none';
			var btnDelete = document.getElementById("btn-delete");
			btnDelete.style.display = 'none';
			var btnInsert = document.getElementById("btn-insert");
			btnInsert.style.display = 'none';
			var selMember = document.getElementById("e-selMember");
			selMember.style.display = 'inline';
			var add2 = document.getElementById("e-add2");
			add2.style.display = 'inline';
			var dateModalRow = document.getElementById("e-dateModalRow");
			dateModalRow.style.display = 'none';
			var writeDate = document.getElementById("e-writeDate");
			writeDate.style.display = 'none';
			var writeDate2 = document.getElementById("writeDate");
			writeDate2.style.display = 'none';
			var updateComplete = document.getElementById("btn-updateComplete");
			updateComplete.style.display = 'inline';
			
			/* 수정 기능이므로 정보들 수정 가능으로 변경 */
			$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/update");
			$("#e-issueName").attr("readonly", false);
			$("#e-writeDate").attr("readonly", true);
			$("#e-issueContent").attr("readonly", false);
			$("#e-issueMember").attr("readonly", false);
			$("p[id=e-issueMember]").text("");
		});
		</script>
	</div>
	
</body>
</html>