<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
	
	<div class="p-content">  <!-- content -->
	
			<p class="menuName">프로젝트</p>
			
		<hr id="p-underline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
		
		<button id="p-searchAllProject" class="p-projectSearchBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/project/search'">전체</button>
		<button id="p-searchProjectInProgress" class="p-projectSearchBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/project/search?searchCondition=projectY&searchValue=N         '">완료</button>
		<button id="p-searchCompletedProject" class="p-projectSearchBtn" onclick="location.href='${ pageContext.servletContext.contextPath }/project/search?searchCondition=projectN&searchValue=Y         '">진행중</button>
		
		<div class="projectPage" >
			<a style="color: #BBBBBB;">GROU BOARD - PROJECT PAGE </a>	
			<button id="p-addProject" onclick="addProject()">+ Project</button>
		</div>
		
		<div class="pjt">
			<table class="pj" align="center">
				<c:forEach items="${ requestScope.projectList }" var="project" varStatus="i">
				
					<!-- 나머지 경우에는 td만 추가 -->
					<c:if test="${ i.count % 5 ne 1 &&  i.count % 5 ne 0 }">
							<td style="border-color: ${ project.borderColor };" align="center">
								<a id="p-projectPeriod"><c:out value="${ project.startDate }"/> - <c:out value="${ project.targetDate }"/></a> 
								<a id="p-projectSuccess"><c:out value="${ project.progress }"/>%</a>
								<br><br>
								<a href="${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ project.projectNo }">
									<img id="p-projectTree"src="${ pageContext.servletContext.contextPath }/resources/images/tree${ project.progress }.png">
								</a>
								<br>
								<a id="p-projectTitle" ><c:out value="${ project.projectTitle }"/></a>
							</td>
					</c:if>
					
					<!-- 5로나눠서 나머지가 1인경우 tr을 열어줌 -->
					<c:if test="${ i.count % 5 eq 1 }">
						
					  	<tr class="pj1 tree" align="center">	<!-- tr start -->
					  	
							<td style="border-color: ${ project.borderColor };" align="center">
								<a id="p-projectPeriod"><c:out value="${ project.startDate }"/> - <c:out value="${ project.targetDate }"/></a> 
								<a id="p-projectSuccess"><c:out value="${ project.progress }"/>%</a>
								<br><br>
								<a href="${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ project.projectNo }">
									<img id="p-projectTree"src="${ pageContext.servletContext.contextPath }/resources/images/tree${ project.progress }.png">
								</a>
								<br>
								<a id="p-projectTitle" ><c:out value="${ project.projectTitle }"/></a>
							</td>
					</c:if>
					
					<!-- 5개가 쌓이면 tr을 닫아줌  -->
					<c:if test="${ i.count % 5 eq 0 }">
							<td style="border-color: ${ project.borderColor };" align="center">
								<a id="p-projectPeriod"><c:out value="${ project.startDate }"/> - <c:out value="${ project.targetDate }"/></a> 
								<a id="p-projectSuccess"><c:out value="${ project.progress }"/>%</a>
								<br><br>
								<a href="${ pageContext.servletContext.contextPath }/project/board/search?projectNo=${ project.projectNo }">
									<img id="p-projectTree"src="${ pageContext.servletContext.contextPath }/resources/images/tree${ project.progress }.png">
								</a>
								<br>
								<a id="p-projectTitle" ><c:out value="${ project.projectTitle }"/></a>
							</td>
							
						</tr> 	<!-- tr end -->
						
					</c:if>
				</c:forEach> 
			</table>
		</div>
		<hr id="tableUnderLine" width="1606px" color="1px solid #D9D9D9">
		<div id="tableUnderline"></div>
		<br>
		<jsp:include page="/WEB-INF/views/project/paging.jsp"/>
	
	
	<!--  프로젝트 추가 모달   -->
	
			<a href="#dialog3" id="p-modalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: none;"></a>
			             
	        <div class="modal fade bd-example-modal-xl" id="dialog3" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">
	 
 	             <div class="modal-dialog modal-xl" id="p-modalArea" role="document">
	             
	                <div class="modal-content">
	                
	                    <div class="modal-header" style="background: #75CB5E;">
	                    
	                         <h4 class="modal-title" id="dlogLabel" >
	                         <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
	                        </h4>
	                    </div>
						<form action="${ pageContext.servletContext.contextPath }/project/insert" method="post" >
		                    <div class="modal-body">
		                    
		                    	<span class="p-projectModalRow">
		                    	
		   							<span class="p-projectInseartHeader">프로젝트명</span> 
		   							<input id="p-projectTitle2" name="projectTitle" size="30" value="" placeholder="프로젝트명을 입력하세요." required> 
		   							
   									<span class="p-projectInseartHeader">프로젝트 색상</span> 
		   							<input type="color" name="borderColor" style="width: 280px; margin-left: 15px; height: 20px;">
		   							
		                    	</span>
		                    	
		                    	<br> 
		                    	
		   						<span class="p-projectModalRow">
		   							<span class="p-projectInseartHeader">프로젝트 설명</span> 
		   							<input type="text" id="p-projectContent" name="projectContent" value="" placeholder="프로젝트 설명을 입력하세요." required>
		   						</span>
		   						
		   						<span class="p-projectModalRow">
		                        	<span class="p-projectInseartHeader">프로젝트 기간</span>
			                        	<input type="date" id="p-startDate2" name="startDate" required> 
			                        	<a id="p-dateline">-</a> 
			                        	<input type="date" id="p-targetDate2" name="targetDate" required>
		   						</span>
		   						
		   						<br>
		   						
		   						<span class="p-projectModalRow">'
		   						
			   						<span class="p-projectInseartHeader" style="">프로젝트 참여인원</span> 
			   						
										<select id="company1" onchange="selectParentDept(this.value)">
											<option value="-1">선택하세요</option>
											<option value="0">GrouBear</option>
										</select>
										<select id="parentDept1" onchange="selectchildDept(this.value)">
											<option value="-1">선택하세요</option>
										</select>
										<select id="childDept1" onchange="selectEmp(this.value)">
											<option value="-1">선택하세요</option>
										</select>
										<select id="reference">
											<option value="-1">선택하세요</option>
										</select>
										<button type="button" id="empAdd" class="p-inputInfoBtn addEmp">  ADD  </button>
										<button type="button" id="deleteEmp" class="p-inputInfoBtn deleteEmp">  REMOVE  </button><br>
		   						</span>
		   						<span class="p-projectModalRow">
		   							<textarea id="memberList" name="memberList" class="p-projectTextArea" rows="10" cols="40" readonly></textarea>
		   						</span>
	                        	<br>
		                    </div>
		                    <div class="modal-footer">
		                        <a href="#" role="button" class="btn btn-default" id="p-return" data-dismiss="modal">취소</a> 
		                        <input type="submit" class="btn btn-default" id="p-insertBtn" value="추가" align="center">
		                    </div>
						</form>	                    
	                </div>
	                 
	            </div> 
	             
	        </div>
		
		</div>
		
		<script>
		
		function addProject() {
			
			$("#p-modalOpen").click();
		}
		
			
		//==================================================================================================
		
	/* 상위 부서 (회사코드번호) 출력 함수 */
	
	function selectParentDept(value) {
		
	    /* 처음에 미리 초기화 해준다. */
	    
	    $("#ParentDept1").empty();
	 
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					
					if($("#company1").val() == -1) {
						
						$("#parentDept1 option").remove();
						$("#parentDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#childDept1 option").remove();
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						for(i in data) {
							$("#parentDept1").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>")
						}
						
					}
					
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				}
		});
	}
	
 
	/* 하위 부서 (회사코드번호) 출력 함수 */
	
	function selectchildDept(value) {
		
		/* 처음에 미리 초기화 해준다. */
		
		$("#childDept1").empty();

		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/approver/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* selectbox 에서 '선택하시요'를 선택할 경우 하위 selectbox에서 선택한 목록들을 초기화 해준다. */
					
					if($("#parentDept1").val() == -1) {
						
						$("#childDept1 option").remove();
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					} else {
						
						$("#childDept1").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#childDept1").append("<option value='" + data[i].deptCode + "'>" + data[i].deptTitle + "</option>");
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		});
	}
	
	/* 하위 부서에 속하는 사원(회사코드번호) 출력 함수 */
	
	function selectEmp(value) {
		
		/* 처음에 미리 초기화 해준다. */
		
		$("#reference").empty();
		
		$.ajax({
				url : "${ pageContext.servletContext.contextPath }/reference/select",
				type : "POST",
				dataType : "json",
				data : {deptCode : value},
				success : function(data) {
					
					/* '선택하세요'를 선택하였을 경우 */
					
					if($("#childDept1").val() == -1) {
						
						/* $("#reference option").remove(); */
						
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
					/* 사원이 없는 부서를 선택하였을 경우 */
					
					} else if (data == "") {
						
						$("#reference option").remove();
						$("#reference").append("<option value="+ -1 +" disabled>사원이 없습니다.</option>");
					
					/* 정상적으로 사원을 선택하였을 경우 */
					
					} else {
						
						$("#reference").append("<option value="+ -1 +">선택하세요</option>");
						
						for(i in data) {
					 		$("#reference").append("<option>" + data[i].empName + " " + data[i].jobGrade + "[#"+data[i].empNo+"]"+"</option>");
						}
						
					}
					
				},
				error : function(request, status, error) {
					window.alert("정보 조회 실패")
				} 
		  });
		
	}
		
	/* 프로젝트 참여자 추가 버튼 눌렀을 시 */
	
	$(function() {
		$("#empAdd").click(function() {
			
			const name = $("#reference option:selected").text();
			
			console.log(name);
			
			if(name === "선택하세요" || name === "") {
				alert("사원을 선택해주세요.");
				
				
			} else {
				console.log(name);
				
				/* 프로젝트 참여인원 삽입 시, 비어 있을 경우 */
				
				if($("#memberList").val() == ""){
					
					$("#memberList").append(name);
					
				} else {
					$("#memberList").append(", " + name);
					
				}
				
			}
			
		});
	});

	/* 프로젝트 참여 인원 목록 전체 삭제 */
	
	$(function() {
		$("#deleteEmp").click(function() {
			
			if($("#memberList").val() != ""){
				
				if(confirm("프로젝트 참여인원 목록을 전체 삭제하시겠습니까?") == true){
					$("#memberList").empty();
			    	
			    } else {
			        return ;
			        
			    }
				
			} else {
				
				alert("삭제할 프로젝트 참여인원 목록이 없습니다.");
				
			}
			
		});
	});
		
	</script>
</body>
</html>