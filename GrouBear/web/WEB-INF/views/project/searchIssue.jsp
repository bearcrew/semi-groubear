<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %>
<style>
#e-add2 {
	width: 30px;
}
#writeDate {
	width: 40px;
	border: none;
}
</style>
<div class="content">

	<!-- 이슈 목록 -->
	<div class="p-kanban p-kbNo1">
		<ol>
			<p>이슈</p>
			<c:forEach var="list" items="${ sessionScope.issueList }">
				<dd>
					<a><c:out value="${ list.issueName }"/></a> <br>
					<a><c:out value="${ list.writer }"/></a>
					<a><c:out value="${ list.writeDate }"/></a>
					<a hidden><c:out value="${ list.issueNo }"/></a>
					<a hidden><c:out value="${ list.issueContent }"/></a>
					<a hidden><c:out value="${ list.projectNo }"/></a>
					<a hidden><c:out value="${ list.funcBoardNo }"/></a>
				</dd>
			</c:forEach>
		</ol>
		<text id="e-issueAdd" class="e-issueAdd">+ADD NEW TASK</text>
	</div>
	<!-- 이슈 목록 end -->
	
	<!-- 이슈 상세 보기 모달 -->
	<a href="#dialog" id="e-modalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: none;"></a>

		<div class="modal fade bd-example-modal-xl" id="dialog" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">

			<div class="modal-dialog modal-xl" id="p-modalArea" role="document">
			<form id="formId" action="${ pageContext.servletContext.contextPath }/issue/test" method="post">
				<div class="modal-content">

					<!-- 모달 타이틀 -->
					<div class="modal-header" style="background: #75CB5E;">
	                	<h4 class="modal-title" id="dlogLabel" >
	                         	이슈		
	                	</h4>
					</div>
					<!-- 모달 타이틀 end -->
					
					<input type="hidden" id="e-projectNo" name="projectNo" value="${ sessionScope.issueList[1].projectNo }">
					<input type="hidden" id="e-funcBoardNo" name="funcBoardNo" value="${ sessionScope.issueList[1].funcBoardNo }">
					<input type="hidden" id="e-issueNo" name="issueNo" value="">
					<input type="hidden" id="e-writer" name="writer" value="">
                    <div class="modal-body">
   						제목 : <input id="e-issueName" name="issueName" value="" readonly> <br> 
   						담당자 : <select id="e-selMember">
   						<c:forEach items="${ sessionScope.projectMemberList }" var="proMember">
   								<option><c:out value="${ proMember.empName }#${ proMember.projectMemberNo }"/></option>
   						</c:forEach>
   							   </select><input type="text" id="e-add2" value="추가">
   						<p id="e-issueMember"></p>
   						<input id="e-issueMember" name="issueMember" style="display: none;" readonly>
                       	<input type="text" id="writeDate" value="작성일"><input type="date" id="e-writeDate" name="writeDate" value="" readonly><br>
                       	<textarea cols="170" rows="15" id="e-issueContent" name="issueContent" readonly></textarea>
                       	<br>
                    </div>
                    <div class="modal-footer">
                    	<input type="submit" id="btn-insert" value="등록">
                    	<input type="submit" id="btn-updateComplete" value="수정완료">
                        <text id="btn-update" class="btn btn-update">수정</text> 
                        <text id="btn-delete" class="btn btn-delete">삭제</text> 
                        <text id="btn-close" class="btn btn-close" data-dismiss="modal">닫기</text> 
                    </div>
				</div>
			</form>
			</div>

		</div>
		<!-- 이슈 상세 보기 모달 end -->
		
		
		<script>
		/* 이슈 목록 클릭시 동작할 내용 */
		if(document.getElementsByTagName("dd")) {
			
			const $dds = document.getElementsByTagName("dd");
			
			for(let i = 0; i < $dds.length; i++) {
				
				$dds[i].onmouseover = function() {
					$dds[i].style.backgroundColor = "#c8c8c8";
					$dds[i].style.cursor = "pointer";
				}
				
				$dds[i].onmouseout = function() {
					$dds[i].style.backgroundColor = "white";
					$dds[i].style.cursor = "default";
				}
				
				$dds[i].onclick = function() {
					var login = "${ sessionScope.loginMember.empName }";
					var writer2 = this.children[2].innerText;
					if(login==writer2) {
						var btnUpdate = document.getElementById("btn-update");
						btnUpdate.style.display = 'inline';
						var btnDelete = document.getElementById("btn-delete");
						btnDelete.style.display = 'inline';
					} else {
						var btnUpdate = document.getElementById("btn-update");
						btnUpdate.style.display = 'none';
						var btnDelete = document.getElementById("btn-delete");
						btnDelete.style.display = 'none';
					}
					/* 조회 기능에 맞게 디스플레이 */
					var btnInsert = document.getElementById("btn-insert");
					btnInsert.style.display = 'none';
					var selMember = document.getElementById("e-selMember");
					selMember.style.display = 'none';
					var add2 = document.getElementById("e-add2");
					add2.style.display = 'none';
					var writeDate = document.getElementById("e-writeDate");
					writeDate.style.display = 'inline';
					var writeDate2 = document.getElementById("writeDate");
					writeDate2.style.display = 'inline';
					var updateComplete = document.getElementById("btn-updateComplete");
					updateComplete.style.display = 'none';
					
					/* 조회 기능이므로 정보들 수정 불가능으로 변경 */
					$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/insert");
					$("#e-issueName").attr("readonly", true);
					$("#e-writeDate").attr("readonly", true);
					$("#e-issueContent").attr("readonly", true);
					$("#e-issueMember").attr("readonly", true);
					
					//이슈 번호를 ajax로 전송해서 이슈 담당자를 리턴 받음
					const issueNo = this.children[4].innerText;
					
					$.ajax({
						url: "/groubear/issue/search",
						type: "post",
						data: { issueNo: issueNo },
						success: function(data) {
							var issueMember = data;
							var issueMembers = issueMember.replace("]"," ").replace("["," ");
							$("p[id=e-issueMember]").text(issueMembers);
						},
						error: function(xhr) {
							console.log(xhr);
						}
					});
					document.getElementById("e-issueName").value = this.children[0].innerText;
					document.getElementById("e-writer").value = this.children[2].innerText;
					document.getElementById("e-writeDate").value = this.children[3].innerText;
					document.getElementById("e-issueNo").value = this.children[4].innerText;
					document.getElementById("e-issueContent").value = this.children[5].innerText;
					document.getElementById("e-projectNo").value = this.children[6].innerText;
					document.getElementById("e-funcBoardNo").value = this.children[7].innerText;
					
					$("a[id=e-modalOpen]").click();
				}
			}
		}
		
		/* 이슈 추가하기 버튼 클릭시 동작할 내용 */
		$(".e-issueAdd").click(function() {
			
			/* 현재 모달창에 들어가있는 정보들 초기화 */
			$("#e-issueName").val("");
			$("p[id=e-issueMember]").text("");
			$("#e-writeDate").val("");
			$("#e-issueContent").val("");
			
			/* 작성 기능에 맞게 디스플레이 */
			var btnUpdate = document.getElementById("btn-update");
			btnUpdate.style.display = 'none';
			var btnDelete = document.getElementById("btn-delete");
			btnDelete.style.display = 'none';
			var btnInsert = document.getElementById("btn-insert");
			btnInsert.style.display = 'inline';
			var selMember = document.getElementById("e-selMember");
			selMember.style.display = 'inline';
			var add2 = document.getElementById("e-add2");
			add2.style.display = 'inline';
			var writeDate = document.getElementById("e-writeDate");
			writeDate.style.display = 'none';
			var writeDate2 = document.getElementById("writeDate");
			writeDate2.style.display = 'none';
			var updateComplete = document.getElementById("btn-updateComplete");
			updateComplete.style.display = 'none';
			
			/* 작성 기능이므로 정보들 수정 가능으로 변경 */
			$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/insert");
			$("#e-issueName").attr("readonly", false);
			$("#e-writeDate").attr("readonly", true);
			$("#e-issueContent").attr("readonly", false);
			$("#e-issueMember").attr("readonly", false);
			
			$("a[id=e-modalOpen]").click();
		});
		
		/* 담당자 추가 버튼 클릭시 동작할 내용 */
		$("#e-add2").click(function() {
			var sel = document.getElementById('e-selMember').value;
			var appendContent = "<a href='#'onclick='removeText()' id='e-members'>" + sel + "</a>" + " ";
			$("p[id=e-issueMember]").append(appendContent);
			var issueMembers2 = $("p[id=e-issueMember]").text();
			var aa = $("input[id=e-issueMember]").val(issueMembers2);
		});
		function removeText() {
			
			/* 전체 a태그를 지정 */
			const $a = document.getElementsByTagName("a");
			
			for (var i = 0; i < $a.length; i++) {
				
				/* i번째 a태그를 누를시에 i번째 a태그를 삭제 */
				$a[i].onclick = function() {
					
					$(this).remove();
				}
			}
		}
		
		//삭제 버튼 클릭시 동작할 내용
		$("#btn-delete").click(function() {
			var result = confirm("정말 삭제 하시겠습니까?");
			
			if(result) {
				const issueNo = document.getElementById("e-issueNo").value
				
				$.ajax({
					url: "/groubear/issue/delete",
					type: "post",
					data: { issueNo: issueNo },
					success: function(data) {
						alert(data);
						location.href="/groubear/issue/search?funcBoardNo=1&projectNo=1"
					},
					error: function(xhr) {
						console.log(xhr);
					}
				});
			} else {
				
			}
		});
		
		//수정 버튼 클릭시 동작할 내용
		$("#btn-update").click(function() {
			
			/* 수정 기능에 맞게 디스플레이 */
			var btnUpdate = document.getElementById("btn-update");
			btnUpdate.style.display = 'none';
			var btnDelete = document.getElementById("btn-delete");
			btnDelete.style.display = 'none';
			var btnInsert = document.getElementById("btn-insert");
			btnInsert.style.display = 'none';
			var selMember = document.getElementById("e-selMember");
			selMember.style.display = 'inline';
			var add2 = document.getElementById("e-add2");
			add2.style.display = 'inline';
			var writeDate = document.getElementById("e-writeDate");
			writeDate.style.display = 'none';
			var writeDate2 = document.getElementById("writeDate");
			writeDate2.style.display = 'none';
			var updateComplete = document.getElementById("btn-updateComplete");
			updateComplete.style.display = 'inline';
			
			/* 수정 기능이므로 정보들 수정 가능으로 변경 */
			$("#formId").attr("action", "${ pageContext.servletContext.contextPath }/issue/update");
			$("#e-issueName").attr("readonly", false);
			$("#e-writeDate").attr("readonly", true);
			$("#e-issueContent").attr("readonly", false);
			$("#e-issueMember").attr("readonly", false);
			$("p[id=e-issueMember]").text("");
		});
		</script>
		
</div>
</body>
</html>