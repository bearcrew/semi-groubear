<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

		
	<a href="#dialog" id="p-funcModalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: none;"></a>
             
      <div class="modal fade bd-example-modal-xl" id="dialog" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">

           <div class="modal-dialog modal" id="p-funcModalArea" role="document">
           
              <div class="modal-content">
                  <div class="modal-header" style="background: #75CB5E;">
                       <h4 class="modal-title" id="dlogLabel" >
                       <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
                      </h4>
                  </div>
			<form action="${ pageContext.servletContext.contextPath }/project/board/insert" method="post">
                   <div class="modal-body">
                   	<span class="p-funcModalRow">
                   		<input type="text" name="projectNo" value="${ requestScope.projectNo }" style="display: none;"> 
  							<span class="p-funcModalTextHeader">제목</span> 
  							<input type="text" name="funcBoardTitle" placeholder="보드 이름을 입력해주세요" > <br>
                   	</span>
  						<br>
  						<span class="p-funcModalRow">
   						<span class="p-funcModalTextHeader">내용</span> 
                       	<input type="text" name="funcBoardContent" placeholder="보드의 간략한 설명을 입력해주세요 (최대 30자)" maxlength="30"><br>
                       	</span>
                      	<br>
                   </div>
                   <div class="modal-footer">
                       <a href="#" role="button" class="btn btn-default" id="p-insertBtn" data-dismiss="modal">작성 취소</a> 
                       <input type="submit" class="btn btn-default" id="p-insertBtn" value="작성 완료 ">
                   </div>
			</form>	                    
              </div>
               
          </div> 
           
      </div> <!-- modal end -->
