<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


	<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 


	<div class="p-content">  <!-- content -->
	
	<!-- 단위업무 모달 -->
			<a href="#dialog3" id="p-modalOpen" role="button" class="btn btn-default" data-toggle="modal" style="display: ;"></a>
			             
	        <div class="modal fade bd-example-modal-xl" id="dialog3" role="dialog" aria-labelledby="dlogLabel" aria-hidden="true">
	 
 	             <div class="modal-dialog modal-xl" id="p-modalArea" role="document">
	             
	                <div class="modal-content">
	                    <div class="modal-header" style="background: #75CB5E;">
	                         <h4 class="modal-title" id="dlogLabel" >
	                         <img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="p-modallogo" >
	                        </h4>
	                    </div>
						<form action="${ pageContext.servletContext.contextPath }/project/insert" method="post" >
		                    <div class="modal-body">
		                    	<span class="p-projectModalRow">
		   							<span class="p-projectInseartHeader">프로젝트명</span> 
		   							<input id="p-projectTitle" name="projectTitle" size="30" value="" placeholder="프로젝트명을 입력하세요." required> 
		   							<span class="p-projectInseartHeader">프로젝트 색상</span> 
		   							<input type="color" name="borderColor">
		                    	</span>
		                    	<br> 
		   						<span class="p-projectModalRow">
		   							<span class="p-projectInseartHeader">프로젝트 설명</span> 
		   							<input type="text" id="p-projectContent" name="projectContent" value="" placeholder="프로젝트 설명을 입력하세요." required>
		   						</span>
		   						<span class="p-projectModalRow">
		                        	<span class="p-projectInseartHeader">프로젝트 기간</span>
			                        	<input type="date" id="p-startDate2" name="startDate" required> 
			                        	<a id="p-dateline">-</a> 
			                        	<input type="date" id="p-targetDate2" name="targetDate" required>
		   						</span>
		   						<br>
		   						<span class="p-projectModalRow">
			   						<span class="p-projectInseartHeader">프로젝트 참여인원</span> 
			   						<select id="selectBox" class="p-selectBox" >
				   						<option value="empNo" id="empNo">사원번호</option>
										<option value="email" id="email">이메일</option>
			   						</select>
				   						<input type="text" id="inputEmpNo" class="p-inputInfo" placeholder="사원번호">
										<input type="text" id="inputEmail" class="p-inputInfo" placeholder="이메일" style="display: none;">
										<button type="button" id="empAdd" class="p-inputInfoBtn addEmp">  ADD  </button>
										<button type="button" id="deleteEmp" class="p-inputInfoBtn deleteEmp">  REMOVE  </button><br>
		   						</span>
		   						<span class="p-projectModalRow">
		   							<textarea id="memberList" name="memberList" class="p-projectTextArea" rows="10" cols="40" readonly></textarea>
									<div style="display: none;">
									<textarea id="memberNoList" name="memberNoList" rows="10" cols="45"></textarea>
									</div>
		   						</span>
	                        	<br>
		                    </div>
		                    <div class="modal-footer">
		                        <a href="#" role="button" class="btn btn-default" id="p-return" data-dismiss="modal">취소</a> 
		                        <input type="submit" class="btn btn-default" id="p-insertBtn" value="추가" align="center">
		                    </div>
						</form>	                    
	                </div>
	                 
	            </div> 
	             
	        </div>
	
	
	</div>
	
	
	
	
	<script>
			
		$('#selectBox').change(function() {
				 	
			var state = $('#selectBox option:selected').val();
			
			if ( state == 'empNo' ) {
				$('#inputEmpNo').show();
				$('#inputEmail').hide();
			} else {
				$('#inputEmail').show();
				$('#inputEmpNo').hide();
			}
		});	
			
		
		$("#empAdd").click(function() {
			
			const empNo = $("#inputEmpNo").val();
			const email = $("#inputEmail").val();
			
			
			/* 이메일 고려안함 나중에 수정하는 걸로 */
			$.ajax({
				url : "/groubear/project/search",
				data : (empNo + 0 > 0)? { empNo : empNo } : { email : email },
				dataType : "json",
				method : "POST",	
				success : function(data) {
					
					const memberList = $("#memberList").val();
					
					if(memberList.length < 1){
						$("#memberList").append(data); 
						$("#memberNoList").append(empNo);
						$("#inputEmpNo").val("");
						$("#inputEmail").val("");
					} else {
						$("#memberList").append("," + data);
						$("#memberNoList").append("," + empNo);
						$("#inputEmpNo").val("");
						$("#inputEmail").val("");
					}
				},
				error : function(xhr) {
					alert("존재하지 않는 사원입니다.");
					$("#inputEmpNo").val("");
					$("#inputEmail").val("");
				}
			});
			
		});
		
		
		$("#deleteEmp").click(function() {
			
		    if(confirm("목록을 전체 삭제하시겠습니까 ?") == true){
		        alert("목록 삭제 완료");
		        $("#memberList").empty();
		        $("#memberNoList").empty();
		    } else {
		        return ;
		    }

		});
		
	</script>
	

</body>
</html>