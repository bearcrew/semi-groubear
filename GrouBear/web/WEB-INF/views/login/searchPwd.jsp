<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

<!--  파비콘 변경 링크 -->
<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">

<!-- 폰트 관련 링크 -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@700;900&display=swap" rel="stylesheet">

<!-- css 링크 -->
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/searchPwd.css">
</head>

<body>


	<div class="s-wrap">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	
			<!-- header -->
			<header> 
				<a href="${ pageContext.servletContext.contextPath }" class="headerBtn" onclick="" >
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="s-headerLogo">
				</a>
			</header> 
			
			<!-- 아이디 찾기 -->
			<input type="button" id="searchId" class="k-search k-id" value="Search ID"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchId'">
			
			<!-- 비밀번호 찾기 -->
			<input type="button" id="searchPwd" class="k-search k-pwd" value="Search Password"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchPwd'">
					
			<!-- 로그인 form -->
			<content>
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/bigLogo.png" class="s-logo">
				
				<div>
					<input type="text" id="k-userId" class="k-input" name="userId" placeholder=" 아이디">
					<input type="text" id="k-userName" class="k-input" name="userName" placeholder=" 이름">
					<input type="email" id="k-userEmail" class="k-input" name="userEmail" placeholder=" 이메일">
					<input type="button"  id="k-emailcheckbutton"  value="인증 요청" name="k-emailCheckButtons">
					<input type="text" id="k-emailCheckNum" class="k-input" name="emailCheckNum" placeholder=" 이메일 인증번호">
					<input type="tel" id="k-userPhone" class="k-input" name="userPhone" placeholder=" 핸드폰">
					<input type="button" id="k-pwdCheckButton"  value="비밀번호 찾기" name="k-emailCheckButtons">
				</div>
			</content>
		
		<script> 
		/* 이메일 체크 */
		$("#k-emailcheckbutton").click(function() {
		         const formEmail = $("#k-userEmail").val();            													/*formData 생성*/
	      $.ajax({
	         url: "${ pageContext.servletContext.contextPath }/mail",              										/*넘겨줄 url주소 (맵핑주소로 넘어간다)*/
	         method: "post",                                    														/*타입은 post타입이다*/
	         data: { formEmail: formEmail },                                   											/*넘겨줄 데이터는 formData이다*/
	         success: function(data) {
	            alert(data);
	         },
	         error: function(xhr) {
	            console.log(xhr);
	         } 
	      });
	    });
	      function resetFormElement($obj) {
	         $obj.val("");
	      }
 		/*비밀번호 체크*/
	      $("#k-pwdCheckButton").click(function() {
	          
	          var formUserId = $("#k-userId").val();
	          var formUserName = $("#k-userName").val();
	          var formUserPhone = $("#k-userPhone").val();
	          var formUserEmail = $("#k-emailCheckNum").val();
	         if (formUserId == null || formUserId == ""  || formUserId == 'undefined' ) {								/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/
	        	  $("#k-userId").focus();
	        	  return false; 
	         } if (formUserName == null || formUserName == ""  || formUserName == 'undefined') {						/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/
	        	  alert("Name을 입력해주세요");
	        	  $("#k-userName").focus();
	        	  return false; 								
	         } if (formUserPhone == null || formUserPhone == ""  || formUserPhone == 'undefined') {						/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/	
	        	  alert("Phone 입력해주세요");
	        	  $("#k-userPhone").focus();
	        	  return false; 
	         } if (formUserEmail == null || formUserEmail == ""  || formUserEmail == 'undefined') {						/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/
	        	  alert("Email 입력해주세요");
	        	  $("#k-emailCheckNum").focus();
	        	  return false; 
		     } 
		       $.ajax({
			          url: "${ pageContext.servletContext.contextPath }/login/searchPwd",              					 /*넘겨줄 url주소 (맵핑주소로 넘어간다)*/
			          method: "post",                                   												 /*타입은 post타입이다*/
			          data: { formUserId : formUserId , formUserName : formUserName, formUserPhone : formUserPhone, formUserEmail : formUserEmail },               /*넘겨줄 데이터는 formData이다*/
			          success: function(data) {
			             
			             location.href = "${ pageContext.servletContext.contextPath }/PwdUpdate"; 
			        },  
			          error: function(xhr) {
			        	 alert("입력하신 정보가 잘못되었습니다 다시 작성해주세요.");
			        	 return false;
			          } 
		       });
	     });
	      
       function resetFormElement($obj) {
          $obj.val("");
       }
		
		</script> 
		
	</div>
</body>
</html>
