<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

<!--  파비콘 변경 링크 -->
<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">

<!-- 폰트 관련 링크 -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@700;900&display=swap" rel="stylesheet">

<!-- css 링크 -->
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/login.css">

</head>
<body>
	<div class="wrap">
	
		<!-- 로그인 필요  -->
		<c:if test="${ empty sessionScope.loginMember }">
		
			<header> <!-- header -->
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="headerLogo">
			</header> 
			
			<!-- 아이디 찾기 -->
			<input type="button" id="searchId" class="search id" value="Search ID"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchId'">
			
			<!-- 비밀번호 찾기 -->
			<input type="button" id="searchPwd" class="search pwd" value="Search Password"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchPwd'">
					
			<!-- 로그인 form -->
			<content>
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/bigLogo.png" class="logo">
				
				<form id="loginForm" action="${ pageContext.servletContext.contextPath }/login" method="post">
					<input type="text" id="uesrId" name="userId" placeholder=" 계정">
					<input type="password" id="userPwd" name="userPwd" placeholder=" 비밀번호">
					
					<input type="submit" id="loginBtn" value="로그인">
				</form>
			</content>
		</c:if>
		
		<!-- 로그인 불필요 -->
		<c:if test="${ !empty sessionScope.loginMember }">
							<jsp:forward page="../main/main.jsp" />
			
		</c:if> 
	</div>
</body>
</html>