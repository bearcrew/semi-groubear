<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

<!--  파비콘 변경 링크 -->
<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">

<!-- 폰트 관련 링크 -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@700;900&display=swap" rel="stylesheet">

<!-- css 링크 -->
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/searchId.css">

</head>
<body>
	<div class="s-wrap">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
			
			<!-- header -->
			<header> 
				<a href="${ pageContext.servletContext.contextPath }" class="headerBtn" onclick="" >
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="s-headerLogo">
				</a>
			</header> 
			
			<!-- 아이디 찾기 -->
			<input type="button" id="searchId" class="s-search s-id" value="Search ID"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchId'">
			
			<!-- 비밀번호 찾기 -->
			<input type="button" id="searchPwd" class="s-search s-pwd" value="Search Password"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchPwd'">
					
			<!-- 로그인 form -->
			<content>
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/bigLogo.png" class="s-logo">
					<div>
						<input type="text" id="k-userName" class="k-input" name="userName" placeholder=" 이름">
						<input type="email" id="k-userEmail" class="k-input" name="userEmail" placeholder=" 이메일">
						<input type="button"  id="k-emailcheckbutton"  value="인증 요청" name="k-emailCheckButtons">
						<input type="text" id="k-emailCheckNum" class="k-input" name="emailCheckNum" placeholder=" 이메일 인증번호">
						<input type="tel" id="k-userPhone" class="k-input" name="userPhone" placeholder=" 핸드폰">
						<input type="button" id="k-IdCheckButton" class="k-input" value="아이디 찾기" name="k-emailCheckButtons">
					</div>
			</content>
		<script> 
	 
			$("#k-emailcheckbutton").click(function() {
				 
				const formEmail = $("#k-userEmail").val();           							/*formData 생성*/
	
			$.ajax({
				url: "${ pageContext.servletContext.contextPath }/mail",						/*넘겨줄 url주소 (맵핑주소로 넘어간다)*/
				method: "post",																	/*타입은 post타입이다*/
				data: { formEmail: formEmail },													/*넘겨줄 데이터는 formData이다*/
				success: function(data) {
					alert(data);
					
				},
				error: function(xhr) {
					console.log(xhr);
				} 
			});
	 	});
			function resetFormElement($obj) {
				$obj.val("");
			}
		
		/*아이디 찾기*/	
		$("#k-IdCheckButton").click(function() {
			
			const formUserName = $("#k-userName").val();
			const formUserEmail = $("#k-userEmail").val();
			const formUserPhone = $("#k-userPhone").val();
			const formUserEmailCheck = $("#k-emailCheckNum").val();

		$.ajax({
			url: "${ pageContext.servletContext.contextPath }/login/searchId",					/*넘겨줄 url주소 (맵핑주소로 넘어간다)*/
			method: "post",																		/*타입은 post타입이다*/
			data: { formUserName : formUserName , formUserEmail : formUserEmail, formUserPhone : formUserPhone, formUserEmailCheck : formUserEmailCheck },		/*넘겨줄 데이터는 formData이다*/
			
			success: function(data) {
				alert(data);
				location.href ='${ pageContext.servletContext.contextPath }'
			},
			error: function(xhr) {
				alert("입력하신 정보가 잘못되었습니다.");
			} 
		});
 	});
		function resetFormElement($obj) {
			$obj.val("");
		}
		
		</script> 
	</div>
</body>
</html>
