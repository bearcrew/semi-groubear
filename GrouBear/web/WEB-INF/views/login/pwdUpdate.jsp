<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Grou Bear</title>

<!--  파비콘 변경 링크 -->
<link rel="shortcut icon" href="${ pageContext.servletContext.contextPath }/resources/images/groubearLOGO.png">

<!-- 폰트 관련 링크 -->
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@700;900&display=swap" rel="stylesheet">

<!-- css 링크 -->
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/pwdUpdate.css">


</head>
<body>
	<div class="s-wrap">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		
		<!-- header -->
		<header> 
				<a href="${ pageContext.servletContext.contextPath }" class="headerBtn" onclick="" >
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/logo.png" class="s-headerLogo">
				</a>
		</header> 
			<!-- 아이디 찾기 -->
			<input type="button" id="searchId" class="s-search s-id" value="Search ID"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchId'">
			
			<!-- 비밀번호 찾기 -->
			<input type="button" id="searchPwd" class="s-search s-pwd" value="Search Password"
			onclick="location.href='${ pageContext.servletContext.contextPath }/login/searchPwd'">
					
			<!-- 비밀번호 찾기 폼 -->		
			<content>		
				<img alt="" src="${ pageContext.servletContext.contextPath }/resources/images/bigLogo.png" class="s-logo">
					<form action="${ pageContext.servletContext.contextPath }/PwdUpdate" method="post">
							<input type="text" id="k-userId" class="k-input" name="userId" placeholder=" 아이디">
							<input type="password" id="k-userPwd" class="k-input" name="pwd" placeholder=" 새 비밀번호">
							<input type="password" id="k-userPwdCheck" class="k-input "name="userPwdCheck" placeholder=" 새 비밀번호 확인">
							<input id="alert-success"style="display: none;" value="비밀번호가 일치합니다.">
							<input id="alert-danger" style="display: none; color: #d92742; font-weight: bold;" value="비밀번호가 일치하지 않습니다.">
							<input type="submit" id="k-passChange" class="k-input" value="비밀번호 변경" name="PwdCheckButtons"> 
					</form>
		</content>	
		  </div>	
		<!-- 비밀번호 변경 -->
			<script type="text/javascript">
			$("#passChange").click(function () {
				var userId = $("#userId").val();
				var pwd = $("#pwd").val();
				var userNewPwdCheck = $("#userNewPwdCheck").val()
				
				
				if (userId == null || userId == ""  || userId == 'undefined' ) {										/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/
		      	  alert("ID를 입력해주세요");
		      	  $("#userId").focus();
		      	  return false; 
		       	} 
				if (pwd == null || pwd == ""  || pwd == 'undefined') {													/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/
		      	  alert("비밀번호를 입력해주세요");
		      	  $("#pwd").focus();
		      	  return false; 
		        }
				if (userNewPwdCheck == null || userNewPwdCheck == ""  || userNewPwdCheck == 'undefined') {				/*값을 입력하지 않았을 경우 출력 해주는 메세지 이다.*/	
			      	  alert("비밀번호를 입력해주세요");
			      	  $("#userNewPwdCheck").focus();
			      	  return false; 
			    } else {
			    	if(pwd == userNewPwdCheck) {
			    		
			    	} else {
			    		alert("비밀번호를 확인해주세요");
			    		return false;
			    	}
			    }
			})	
			</script>		
					
		
		</body>
		</html>