<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/emp.css">
<script type="text/javascript" src="${ pageContext.servletContext.contextPath }/resources/js/emp.js"></script>
	
	<div class="m-empArea">
	<div class="m-empAreaRight">
	<div class="m-search-area">
	
			 <form id="m-empTable-empSearchForm" action="${ pageContext.servletContext.contextPath }/emp/organization/list" method="GET">
			 
			 <!-- 검색 카테고리 selectBox -->
			 <select id="m-searchCondition" name="searchCondition"> 
					<option value="all" ${ requestScope.selectCriteria.searchCondition eq "all"? "selected": "" }>전체</option>
					<option value="empName" ${ requestScope.selectCriteria.searchCondition eq "empName"? "selected": "" }>사원명</option>
					<option value="deptTitle" ${ requestScope.selectCriteria.searchCondition eq "deptTitle"? "selected": "" }>부서명</option>
					<option value="jobGrade" ${ requestScope.selectCriteria.searchCondition eq "jobGrade"? "selected": "" }>직급명</option>
					<option value="gender" ${ requestScope.selectCriteria.searchCondition eq "gender"? "selected": "" }>성별(남/여)</option>
			</select>
			
			<!-- 검색 키워드 입력 -->
			<input type="search" id="m-searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>" placeholder="검색어를 입력해주세요">
			<button id="m-searchBtn" type="submit">검색</button>
			
			</form>
			
		</div>
		
			<hr id="em-hr" >
			
			<div class="e-orderByJobGrade" >
			
		<!-- 직급 (오름/내림차순) 정렬 버튼 -->
		<select id="e-orderByJobGrade" name="orderCondition" style="">
			<option id="jobGradeUp" value="up">직급▲</option> 
	   		<option id="jobGradeDown" value="down">직급▼</option> 		
		</select>
		</div>
			
		<div id="e-Table-area">		
			<table class="m-Table-area" id="listArea">	
						
				<tr class="m-empListHead" >
					<th class="m-th1 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">사원번호</th>
					<th class="m-th2 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">사원명</th>
					<th class="m-th3 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">소속부서</th>
					<th class="m-th4 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">&nbsp&nbsp&nbsp&nbsp 직급</th>
					<th class="m-th5 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">성별</th>
					<th class="m-th6-1 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">연락처</th>				
					<th class="m-th7-1 th1-heder" style=" color:black; font-weight: bold; font-size: 20px;">보유권한</th>
				</tr>
				
				<c:forEach items="${ requestScope.empList }" var="emp">				
					<tr id="m-empList" align="center">				
						<td class="m-td1 gray" id="no"><c:out value="${ emp.no }"/></td>
						<td class="m-td2" id="name"><c:out value="${ emp.name }"/></td>
						<td class="m-td3 gray" id="deptTitle"><c:out value="${ emp.deptTitle }"/></td>
						<td class="m-td4" id="jobGrade"><c:out value="${ emp.jobGrade }"/></td>
						<td class="m-td5 gray" id="gender"><c:out value="${ emp.gender }"/></td>
						<td class="m-td6-1" id="phone"><c:out value="${ emp.phone }"/></td>
						<td class="m-td7-1 gray" id="authorityName"><c:out value="${ emp.authorityName }"/></td>
					</tr>				 
				</c:forEach>								
							
			</table>
			</div>
			
			<div id="m-paging">			
				<jsp:include page="empOrgPaging.jsp"/>
			</div> 
		</div>
	</div>

<script>

/* selectBox의 옵션(직급▲,직급▼) 선택해 변경할 경우 동작하는 함수 */
 
$('#e-orderByJobGrade').change(function() {
	var state = $('#e-orderByJobGrade option:selected').val();		
	
	$.ajax({
		url : "${ pageContext.servletContext.contextPath }/emp/organization/list",
		type : "POST",
		dataType: "json",
		data : {"searchCondition" : (state == "up"? "jobGradeU": "jobGradeD"), "searchValue" : "" },
		success: function (data) {
			 	
			/* #listArea > #m-empList의 전체 내용 비워 주기 */ 
			
			 	$("#listArea >tbody").children("#m-empList").each(function() {			
 					$(this).remove()
 				})
 				
			 /* 반복문 사용해 검색된 사원마다 사원정보를 td에 넣어 row를 추가 */	
			 
				for(var i in data){															
					const no = data[i].no;
					
					var row = "<tr id='m-empList' align='center'>"
					row += "<td class='m-td1 m-td' id='no'>"+ no +"</td>"
					row += "<td class='m-td2 m-td' id='name'>"+ data[i].name +"</td>"
					row += "<td class='m-td3 m-td' id='deptTitle'>"+ data[i].deptTitle +"</td>"
					row += "<td class='m-td4 m-td' id='jobGrade'>"+ data[i].jobGrade +"</td>"
					row += "<td class='m-td5 m-td' id='gender'>"+ data[i].gender +"</td>"
					row += "<td class='m-td6-1 m-td' id='phone'>"+ data[i].phone +"</td>"
					row += "<td class='m-td7-1 m-td' id='authorityName'>"+ data[i].authorityName +"</td>"
					row += "</tr>"
					$("#listArea").append(row)
			} 
		}
	})
});

</script>



	
