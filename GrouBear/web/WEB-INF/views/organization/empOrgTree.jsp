<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script
	src="${pageContext.servletContext.contextPath}/resources/js/jquery.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/jquery.treeview.js"
	type="text/javascript"></script>
<script
	src="${pageContext.servletContext.contextPath}/resources/js/organization.js"
	type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		$("#tree").treeview({
			collapsed : true,
			animated : "medium",
			control : "#sidetreecontrol",
			persist : "location"
		});
	})
</script>

<div class="m-orgTreeArea"
	style="overflow: auto; width: 320px; height: 610px;">

	<div class="sidetree" id="sidetree">
		<h3 align="center">조직도</h3>

		<div class="treeheader">&nbsp;</div>

		<div id="sidetreecontrol">
			<a href="?#"><font size="3px" color="gray" face="">전체 닫기</a> |
			<a href="?#">전체 열기</a>
		</div>
		<ul id="tree">
			<li>
				<!-- 회사이름 출력 -->
			 <a><strong><c:out value="${ company.deptTitle }" /> </strong></a>
				 <!-- 조직도 전체정보 = ${ requestScope.org } -->
				<c:forEach items="${ requestScope.org }" var="orglist">
					<ul>
						<li>
							<!-- 부모(최상위)부서이름과 부서코드 출력 --> 
							<a><strong><c:out value="${ orglist.deptTitle } [#${ orglist.deptCode }]" /> </strong></a>
							 <c:forEach	items="${ orglist.childOrgList }" var="childList">
								<ul>
									<li>
										<!-- 자식(하위)부서이름과 부서코드 출력 -->
										 <a><strong><c:out value="${ childList.deptTitle } [#${ childList.deptCode }]" /></strong></a>
											<c:forEach items="${ childList.orgEmpList }" var="empList">
											<ul>
												<li>
													<a><em><c:out value="[#${ empList.empNo }]  ${ empList.jobGrade } / ${ empList.empName }" /></em></a>
												</li>
											</ul>
										</c:forEach>
									</li>
								</ul>
							</c:forEach>
						</li>
					</ul>
				</c:forEach>
			</li>
		</ul>
	</div>
</div>


