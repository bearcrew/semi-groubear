<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>

<div class="m-emp-content">

	<p class="menuName">사원 조회</p>
	<hr id="p-underline" style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
	
	<div class="projectPage" >
			<a style="color: #BBBBBB;">전체 사원 조회</a>
		</div>

	<jsp:include page="empOrgTree.jsp" />
	
	<jsp:include page="empTable.jsp" />
	
	</div>

</body>
</html>	