
 	<script> 
		$("#send-file-1").click(function() {
			
			let fileInfo = document.getElementById("profileimage").files[0];							/*id=image 파일내용을 담아 변수에 저장한다*/
			let reader = new FileReader();																/*파일 을 보낼수 있는 파일 리더를 생성한다*/
																									
			reader.onload = function() {																/* @details readAsDataURL( )을 통해 파일을 읽어 들일때 onload가 실행*/
																										/* @details 파일의 URL을 Base64 형태로 가져온다.*/
				document.getElementById("thumbnailprofileimage").src = reader.result;
			};
			if (fileInfo) {
																										/* @details readAsDataURL( )을 통해 파일의 URL을 읽어온다.*/
				reader.readAsDataURL(fileInfo);
			}	
			
		console.log($("#profileimage")[0].files[0]);
		
		const formData = new FormData();            													/*formData 생성*/
		
		formData.append("profileimage", $("#profileimage")[0].files[0]);
	
		
		$.ajax({
			url: "/groubear/mypage/profilephotoupdate",													/*넘겨줄 url주소 (맵핑주소로 넘어간다)*/
			type: "post",																				/*타입은 post타입이다*/
			data: formData,																				/*넘겨줄 데이터는 formData이다*/
			contentType: false,																			/* 기본값 : charset = UTF-8*/
			processData: false,																			/* 기본값 : true*/
			success: function(data, textStatus, xhr) {
				alert(data);
			},
			error: function(xhr, status, error) {
				console.log(xhr);
			} 
		});
 	});
		function resetFormElement($obj) {
			$obj.val("");
		}
		</script>
