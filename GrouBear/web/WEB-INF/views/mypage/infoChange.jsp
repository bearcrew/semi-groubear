<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

   <%@include file="/WEB-INF/views/common/headerAndSidebar.jsp" %> 
   
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>     <!--  //ajax 스크립트 -->
   
<link rel="stylesheet" type="text/css" href="${ pageContext.servletContext.contextPath }/resources/css/myPage.css">
   
<div class="k-info-content">  <!-- content -->
   
         <p class="menuName">마이 페이지</p>
         
         <hr id="k-underline" style="width:1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">
        
        <!-- 마이페이지 정보수정 폼 --> -->
        <form id="updateForm"action="${ pageContext.servletContext.contextPath }/mypage/info" method="post">
         
         <div class="k-detail">
            
            <div class="k-detailLeftFrame">
            
            	<!-- 프로필 사진 수정 -->
               <div class="k-profileHeader">

                     <h4>◈프로필 사진</h4>
               
               </div>
               <div class="k-profileSpan">
                     <div class="k-profileLeft" align="left">
                        <a>사진</a>
                     </div>
                     <div class="k-profileRight">
                        <img id="thumbnailprofileimage" src="${ pageContext.servletContext.contextPath }/resources/profileImages/${ sessionScope.loginMember.fileName }">
                  
                     </div>
               
               </div>
               
               <div class="k-profileAdd">
                     <a>
                        <input type="file" id="profileimage" onChange="uploadImgPreview();" accept="image/*" >
                       <input type="button" value="전송" id="send-file-1" name="photoChange" >
                       <%@include file="/WEB-INF/views/mypage/profilePhotoChange.jsp" %>
                     </a>
               
               
               
               </div>
               
               <!-- 서명 사진 수정 -->
               <div class="k-signSapn">
               		<div class="k-signLeft">
               			<a>서명</a>
               		</div>
               		<div class="k-signRight">
               			<img id="thumbnailSignImage" src="${ pageContext.servletContext.contextPath }/resources/signImages/${ sessionScope.loginMember.signFileName }"  style=" margin-top: 50px; margin-left: 100px; width: 100px; height: 100px; display: block;">
               		</div>

               </div>
               
               <div class="k-signAdd">
                     <a>
                        <input type="file" id="signImage" onChange="uploadImgPreview();"  accept="image/*" >
                  <input type="button" value="전송" id="click-file-2" name="photoChange" > 
                  <%@include file="/WEB-INF/views/mypage/signPhotoChange.jsp" %> 
               </a>
               </div>
            </div>
            
            
            <div class="k-detailRightFrame">
            
            <!-- 사원 기본 정보 조회 -->
               <div class="k-rightHeader">
                  <h4>◈사원 기본 정보</h4>
               </div>
               <div class="k-empInfo">

               		<table id="k-empInfoTable">
               			<tbody>
							<tr>
								<td id="k-empinfo1">사원번호</td>								
								<td id="k-empinfo2">								
								<input  id="k-info-input" type="text" name="empNo" value="${ sessionScope.loginMember.empNo }" readonly="readonly">
								</td>
								
								<td id="k-empinfo3">부서</td>
								<td id="k-empinfo7" colspan="3">
								<input id="k-info-input" type="tel" name="deptTitle" value="${  sessionScope.loginMember.deptTitle }" readonly="readonly">
								</td>
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1">사원명</td>
								<td id="k-empinfo2">
								<input id="k-info-input" type="text" name="empName" value="${  sessionScope.loginMember.empName }" readonly="readonly">
								</td>
								<td id="k-empinfo3">입사일</td>
								<td id="k-empinfo7" colspan="3">
								<input  id="k-info-input" type="tel" name="hirtDate" value="${  sessionScope.loginMember.hirtDate }" readonly="readonly">
								</td>
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1">아이디</td>
								<td id="k-empinfo2">
								<input id="k-info-input" type="text" name="empId" value="${  sessionScope.loginMember.empId }" readonly="readonly">
								</td>
								<td id="k-empinfo3">직위</td>
								<td id="k-empinfo4">
									<input id="k-info-input1" type="text" name="jobGrade" value="${  sessionScope.loginMember.jobGrade }" readonly="readonly">
								</td>
								<td id="k-empinfo5">권한</td>
								<td id="k-empinfo6">
										<input id="k-info-input3" type="text" name="jobGrade" value="${  sessionScope.loginMember.authorityName }" readonly="readonly">
								</td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1">성별</td>
								<td id="k-empinfo2">
								<input id="k-info-input" type="text" name="gender" value="${  sessionScope.loginMember.gender }" readonly="readonly">
								<td id="k-empinfo3">생년월일</td>
								<td id="k-empinfo7" colspan="3">
									<input id="k-info-input2" type="email" name="birthDate" value="${  sessionScope.loginMember.birthDate }" readonly="readonly">
								</td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
               			</tbody>
               		</table>
               			<br>
               			<hr>
               			<div style="width: 100%; height: 35px; margin-top: 43px;">
	               			
	               			<!-- 사원 연락처 정보 수정 -->
	               			<h4>◈사원 연락처 정보</h4>
               			</div>
               		<table id=k-empCallTable >
               			<tbody>
							<tr>
								<td id="k-empinfo1">전화번호</td>
								<td id="k-empinfo2"><input id="k-info-input" type="tel" name="homeTel" value="${  sessionScope.loginMember.homeTel }" /></td>
								<td id="k-empinfo1">휴대전화</td>
								<td id="k-empinfo2"><input id="k-info-input" type="tel" name="phone" value="${  sessionScope.loginMember.phone }" /></td>
							</tr>  
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1">팩스번호</td>
								<td id="k-empinfo2"><input id="k-info-input" type="tel" name="fax" value="${  sessionScope.loginMember.fax }" /></td>
								<td id="k-empinfo1">긴급연락처</td>
								<td id="k-empinfo2"><input id="k-info-input" type="tel" name="emergencyContact" value="${  sessionScope.loginMember.emergencyContact }" /></td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1">우편주소</td>
								<td id="k-empinfo2"><input id ="k-address1" type="text" name="address3" value="${  sessionScope.loginMember.zipCode }" readonly="readonly"/><input type="button" value="주소검색" id="k-searchZipCode"></td>
								<td id="k-empinfo1">이메일주소</td>
								<td id="k-empinfo2"><input id="k-info-input" type="email" name="email" value="${  sessionScope.loginMember.email }" /></td>
							</tr>               			
							<tr>
								<td style="height: 10px; border:none;"></td>
							</tr>              			
							<tr>
								<td id="k-empinfo1" >주소</td>
								<td id="k-empinfo2" colspan="1"><input id ="k-address2" type="text" name="address4" value="${  sessionScope.loginMember.address }" readonly="readonly"/></td>
								<td id="k-empinfo1" >상세주소</td>
								<td id="k-empinfo2" colspan="1"><input id ="k-address3" type="text" name="address5" value="${  sessionScope.loginMember.detailAddress }" readonly="readonly"/></td>
							</tr>               			
               			</tbody>
               		</table>

                     <table id="k-empInfoTable">
                        <tbody>
                     <tr>
                        <td id="k-empinfo1">사원번호</td>                        
                        <td id="k-empinfo2">                        
                        <input  id="k-info-input" type="text" name="empNo" value="${ sessionScope.loginMember.empNo }" readonly="readonly">
                        </td>
                        
                        <td id="k-empinfo3">부서</td>
                        <td id="k-empinfo7" colspan="3">
                        <input id="k-info-input" type="tel" name="deptTitle" value="${  sessionScope.loginMember.deptTitle }" readonly="readonly">
                        </td>

                     
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1">사원명</td>
                        <td id="k-empinfo2">
                        <input id="k-info-input" type="text" name="empName" value="${  sessionScope.loginMember.empName }" readonly="readonly">
                        </td>
                        <td id="k-empinfo3">입사일</td>
                        <td id="k-empinfo7" colspan="3">
                        <input  id="k-info-input" type="tel" name="hirtDate" value="${  sessionScope.loginMember.hirtDate }" readonly="readonly">
                        </td>
                     
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1">아이디</td>
                        <td id="k-empinfo2">
                        <input id="k-info-input" type="text" name="empId" value="${  sessionScope.loginMember.empId }" readonly="readonly">
                        </td>
                        <td id="k-empinfo3">직위</td>
                        <td id="k-empinfo4">
                           <input id="k-info-input1" type="text" name="jobGrade" value="${  sessionScope.loginMember.jobGrade }" readonly="readonly">
                              
                           
                        </td>
                        <td id="k-empinfo5">권한</td>
                        <td id="k-empinfo6">
                              <input id="k-info-input3" type="text" name="jobGrade" value="${  sessionScope.loginMember.authorityName }" readonly="readonly">
                        </td>

                     </tr>                        
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1">성별</td>
                        <td id="k-empinfo2">
                        <input id="k-info-input" type="text" name="gender" value="${  sessionScope.loginMember.gender }" readonly="readonly">
                        <td id="k-empinfo3">생년월일</td>
                        <td id="k-empinfo7" colspan="3">
                           <input id="k-info-input2" type="email" name="birthDate" value="${  sessionScope.loginMember.birthDate }" readonly="readonly">
                        </td>
                     </tr>                        
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                        </tbody>
                     </table>
                        <br>
                        <hr>
                        <div style="width: 100%; height: 35px; margin-top: 43px;">
                           <h4>◈사원 연락처 정보</h4>
                        </div>
                     <table id=k-empCallTable >
                        <tbody>
                     <tr>
                        <td id="k-empinfo1">전화번호</td>
                        <td id="k-empinfo2"><input id="k-info-input" type="tel" name="homeTel" value="${  sessionScope.loginMember.homeTel }" /></td>
                        <td id="k-empinfo1">휴대전화</td>
                        <td id="k-empinfo2"><input id="k-info-input" type="tel" name="phone" value="${  sessionScope.loginMember.phone }" /></td>
                     </tr>  
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1">팩스번호</td>
                        <td id="k-empinfo2"><input id="k-info-input" type="tel" name="fax" value="${  sessionScope.loginMember.fax }" /></td>
                        <td id="k-empinfo1">긴급연락처</td>
                        <td id="k-empinfo2"><input id="k-info-input" type="tel" name="emergencyContact" value="${  sessionScope.loginMember.emergencyContact }" /></td>
                     </tr>                        
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1">우편주소</td>
                        <td id="k-empinfo2"><input id ="k-address1" type="text" name="address3" value="${  sessionScope.loginMember.zipCode }" readonly="readonly"/><input type="button" value="주소검색" id="k-searchZipCode"></td>
                        <td id="k-empinfo1">이메일주소</td>
                        <td id="k-empinfo2"><input id="k-info-input" type="email" name="email" value="${  sessionScope.loginMember.email }" /></td>
                     </tr>                        
                     <tr>
                        <td style="height: 10px; border:none;"></td>
                     </tr>                       
                     <tr>
                        <td id="k-empinfo1" >주소</td>
                        <td id="k-empinfo2" colspan="1"><input id ="k-address2" type="text" name="address4" value="${  sessionScope.loginMember.address }" readonly="readonly"/></td>
                        <td id="k-empinfo1" >상세주소</td>
                        <td id="k-empinfo2" colspan="1"><input id ="k-address3" type="text" name="address5" value="${  sessionScope.loginMember.detailAddress }" readonly="readonly"/></td>
                     </tr>                        
                        </tbody>
                     </table>
               

               </div>
            </div>
         </div>
         <div class="empDetailBtnArea">
          <button class="eback-btn" type="button" onclick="history.back()">뒤로가기</button>
          <button class="em-btn" id="changeEmpInfo" type="submit">정보수정</button>
         </div>
       </form>  
       
       <!-- 우편번호 검색  -->
	<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
		<script>
				const $searchZipCode = document.getElementById("k-searchZipCode");
				
				$searchZipCode.onclick = function() {
				
																									/*다음 우편번호 검색 창을 오픈하면서 동작할 콜백 메소드를 포함한 객체를 매개변수로 전달한다.*/
					new daum.Postcode({
						oncomplete: function(data){
																									/*팝업에서 검색결과 항목을 클릭했을 시 실행할 코드를 작성하는 부분*/
							document.getElementById("k-address1").value = data.zonecode;
							document.getElementById("k-address2").value = data.address;
							document.getElementById("k-address3").focus();
						}
					}).open();
				}

      </script>
   </div> <!-- content end -->
   
</body>
</html>