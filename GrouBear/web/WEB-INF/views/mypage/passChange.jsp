<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@include file="/WEB-INF/views/common/headerAndSidebar.jsp"%>

<div class="k-pass-content">

	<p class="menuName">비밀번호 변경</p>

	<!-- 비밀번호 변경 -->
	<hr id="k-underline"
		style="width: 1601px; margin-top: 120px; margin-right: auto; margin-left: auto;">

	<div class="k-orgDetailArea">
		<br>
		<br>
		<form>
			<div id="k-detailHeader1">
				<div id="k-detailleft1">현재 비밀번호</div>
				<div id="k-detailRight1" style="border: 1px solid gray;">
					<input class="k-orgInfoInput" type="password" name="userNowPwd"
						id="deptCode" style="width: 400px; height: 25px;">
				</div>

				<br>

				<div id="k-detailHeader2">
					<div id="k-detailleft2">새 비밀번호</div>
					<div id="k-detailRight2" style="border: 1px solid gray;">
						<input class="k-orgInfoInput" type="password" name="pwd"
							id="deptTitle" style="width: 400px; height: 25px;">
					</div>
					<br>
					<div id="k-detailHeader3">
						<div id="k-detailleft3">새 비밀번호 재확인</div>
						<div id="k-detailRight3" style="border: 1px solid gray;">
							<input class="k-orgInfoInput" type="password"
								name="userNewPwdCheck" id="deptContent"
								style="width: 400px; height: 25px;">
						</div>
					</div>
				</div>
			</div>

			<div class="k-orgDetailBtnArea">
				<button id="k-orgDetailBtn1" type="reset">취소</button>
				<button id="k-orgDetailBtn2" type="submit"
					formaction="${ pageContext.servletContext.contextPath }/mypage/updatepwd"
					formmethod="post" formtarget="_self">변경</button>
			</div>
		</form>
	</div>
</div>
</body>
</html>