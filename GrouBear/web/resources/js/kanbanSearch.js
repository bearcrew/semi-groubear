	/* 각 단위업무 상세조회 모달창 띄우기 */
	if(document.getElementsByTagName("dd")) {
		
		const $root = "${ pageContext.servletContext.contextPath }";
		
		const $dds = document.getElementsByTagName("dd");
		
		for(let i = 0; i < $dds.length; i++) {
			
			$dds[i].onmouseover = function() {
				$dds[i].style.backgroundColor = "#c8c8c8";
				$dds[i].style.cursor = "pointer";
			}
			
			$dds[i].onmouseout = function() {
				$dds[i].style.backgroundColor = "white";
				$dds[i].style.cursor = "default";
			}
			
			$dds[i].onclick = function() {
				const $partWorkNo = $dds[i].children[3].innerText;
				
				/* jquery promise */
				partWorkInfoAjax($partWorkNo)
				.then(partWorkMemberAjax)
				.then(successFunction)
				.catch(errorFunction);	
				
			} <!-- onclick event end -->
			
		} <!-- for end -->
		
	}; <!-- if end -->
	
			/* 단위업무 번호로 해당 단위업무의 정보를 조회해옴 */
			function partWorkInfoAjax($partWorkNo) {
				return new Promise(function(resolve, reject){
					
					$.ajax({
						url : "${ pageContext.servletContext.contextPath }/kanban/search",	
						data : { $partWorkNo : $partWorkNo },
						dataType : "json",
						method : "POST",	
						success : function(data) {
							
							/* 현재 모달창에 들어가있는 정보들 초기화 */
							$("#p-partWorkContent").empty();
							$("#p-partWorkTitle").empty();
							$("#p-startDate").empty();
							$("#p-targetDate").empty();
							$("#p-partWorkMember").empty();
							$("#p-workList").children().css({"background" : "white" , "color" : "black"});
	
							/* 업무리스트 번호에 따라 해당 칸 색 변경 */
							  switch(data.workListNo) {
								case 1 : $("#p-workList1").css({"background" : "#C6D82E" , "color" : "white"}); break; 
								case 2 : $("#p-workList2").css({"background" : "#9FC266" , "color" : "white"}); break; 
								case 3 : $("#p-workList3").css({"background" : "#75CB5E" , "color" : "white"}); break; 
								case 4 : $("#p-workList4").css({"background" : "#2BAF66" , "color" : "white"}); break; 
								default : console.log(data.workListNo); 
										  console.table(data);
										  break;
							}  
							
							/* 조회해온 정보들 모달창에 입력 */
							$("#p-partWorkContent").append(data.partWorkContent); 
							$("#p-partWorkTitle").val(data.partWorkName);
							$("#p-startDate").val(data.startDate);
							$("#p-targetDate").val(data.targetDate);
							
							/* 성공시 다음 호출되는 function에 단위업무번호 전달 */
							let $partWorkNo = data.partWorkNo;
							resolve($partWorkNo);
							
						},
						error : function(xhr) {
							alert("1번 실패");
							console.table(xhr)
						}
					}); <!-- ajax end -->
					
				}); <!-- return end -->
			
			} <!-- first promise end -->
				
			/* 단위업무 정보 조회에 성공하였을 경우 단위업무 번호로 해당 단위업무의 담당자를 조회해옴 */
			function partWorkMemberAjax($partWorkNo){
				return new Promise(function(resolve, reject){
					
					$.ajax({
						url : "${ pageContext.servletContext.contextPath }/projectMember/search",
						data : { $partWorkNo : $partWorkNo },
						dataType : 'json',
						method : 'GET',
						success : function(data){
								
							/* 값이 JSON Array 형태로 넘어오기 때문에 반복문으로 값을 출력 */
							for (var i in data) {
								let empNo = data[i].empNo;
								let empName = data[i].empName;
								
								/* 출력한 값을 변수에 담아 담당자칸에 삽입 */
								$("#p-partWorkMember").append(empName);
								$("#p-partWorkMember").append("추가됐누?");
							 	
							}
								/* 다음 함수 실행 */
								resolve();
							
						},
						error : function(xhr) {
							alert("2번 에러");
						}
						
					}); <!-- ajax end -->
					
				}); <!-- partWorkMemberAjax return end -->
				
			} <!-- partWorkMemberAjax end -->
			
			
			/* 단위업무정보와 담당자 모두 조회 성공시 모달창 출력*/
			function successFunction(){
				alert("success");
				$("a[id=p-modalOpen]").click();
				return false;
			}
			
			/* 둘 중 하나라도 에러날 경우 error */
			function errorFunction(){
				alert("error");
				/* 중간에 에러 발생 시 모달 창에 입력된 칸들 비워 줌 */
				$("#p-partWorkContent").empty();
				$("#p-partWorkTitle").empty();
				$("#p-startDate").empty();
				$("#p-targetDate").empty();
				$("#p-partWorkMember").empty();
				$("#p-workList").children().css({"background" : "white" , "color" : "black"});
				return false;
			}
			
			
			
			
	/* 새 단위업무 추가하기 */
	$(".p-partWorkAdd").click(function() {
		
		const $addId = $(this).attr("id");
		
		let workListNo = "";
		
		switch($addId) {
			case "p-partWorkAdd1" : workListNo = 1; break;
			case "p-partWorkAdd2" : workListNo = 2; break;
			case "p-partWorkAdd3" : workListNo = 3; break;
			case "p-partWorkAdd4" : workListNo = 4; break;
			default : alert("$addId" + $addId);
		}
		
		$("a[id=p-modalOpen]").click();
		
		
			
		
	});
	